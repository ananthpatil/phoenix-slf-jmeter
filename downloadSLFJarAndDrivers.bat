@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
set slf-framework=%~1
set jarPath=%slf-framework%\..\Jars
set versionFile=%slf-framework%\version.properties
call javac -d %slf-framework%\bin %slf-framework%\src\com\utilities\ParsePOM.java
call java -cp ".;%slf-framework%\bin" com.utilities.ParsePOM
if not exist %versionFile% @echo on && echo "version.properties is not generated, so unable to download jar and drivers" && exit /b
for /f "delims=" %%x in (%versionFile%) do (
for /f %%i in ("%%x") do (
if not exist %jarPath%\%%~nxi (
echo "%%~nxi does not exist.Started downloading %%~nxi" && call powershell -Command "Invoke-WebRequest %%x -OutFile %jarPath%\%%~nxi" && echo "%%~nxi is downloaded"
) else (
echo "%%~nxi already exists"
)
)
)
del %versionFile%
endlocal
goto :eof