@echo off
setlocal enableDelayedExpansion
goto :main
:main
setlocal
set slf-framework=F:\sonata\phoenix-slf-framework
set jars-dir=%slf-framework%\..\Jars
if not exist %jars-dir% (mkdir %jars-dir%)
call downloadSLFJarAndDrivers.bat "%slf-framework%"
javac -d %slf-framework%\bin -cp "%jars-dir%\*;%jars-dir%\AppliTool_Libraries\*;%jars-dir%\*" %slf-framework%\src\com\driver\DriverExcuteThrough_Threads.java %slf-framework%\src\com\driver\Driver.java src\com\utilities\DriverReusable.java %slf-framework%\src\com\commonfunctions\CommonFunctions.java src\com\commonfunctions\Action.java %slf-framework%\src\com\utilities\Constants.java src\com\utilities\ReadExcel.java
::javac -d bin -cp "libraries\*;libraries\AppliTool_Libraries\*;drivers\*" src\com\commonfunctions\CommonFunctions.java
start java -cp ".;%slf-framework%\bin;%slf-framework%\..\Jars\*" com.driver.DriverExcuteThrough_Threads TH-Desktop
endlocal
goto :eof