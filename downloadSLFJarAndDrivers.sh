#!/bin/sh
JAVA_HOME=$1
PATH=$JAVA_HOME:$PATH:$HOME/bin
export PATH
slfFramework=$2
jarPath=$3
if [[ -z "$JAVA_HOME" || -z "$slfFramework" || -z "$jarPath" ]]; then
        echo "usage: $0 slfJavaPath slf_frameworkPath jarPath" >&2
        exit 1
fi
versionFile=$slfFramework/version.properties
`javac -d $slfFramework/bin $slfFramework/src/com/utilities/ParsePOM.java`
`java -cp .:$slfFramework/bin com.utilities.ParsePOM`
if [ ! -f "$versionFile" ]
then
echo "version.properties is not generated, so unable to download jar and drivers"
exit 1
fi
for line in $(cat $versionFile)
do
jarFile=`echo $line|awk -F"/" '{print $NF}'`
if [ ! -f "$jarPath/$jarFile" ]
then
echo "$jarFile does not exist.Started downloading $jarFile"
`wget $line -P $jarPath`
else
echo "$jarFile already exists"
fi
done
`rm -f $versionFile`
