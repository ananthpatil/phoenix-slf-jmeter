@ECHO OFF
cd c:
c:
cd selGrid
set HUB_NAME=10.174.1.51
set HUB_PORT=4444
set NODE_PORT=5566
set SELENIUM_SERVER=selenium-server-standalone-3.4.0.jar
set CHROME_DRIVER_PATH=chromedriver.exe
set IE_DRIVER_PATH=IEDriverServer-32 Bit.exe
set GECKO_DRIVER_PATH=geckodriver.exe

::java -Dwebdriver.gecko.driver=%GECKO_DRIVER_PATH% -jar %SELENIUM_SERVER% -role webdriver -hub http://%HUB_NAME%:%HUB_PORT%/grid/register -port %NODE_PORT%
java -Dwebdriver.chrome.driver=%CHROME_DRIVER_PATH% -jar %SELENIUM_SERVER% -role webdriver -hub http://%HUB_NAME%:%HUB_PORT%/grid/register -port %NODE_PORT%
::java -Dwebdriver.ie.driver=%IE_DRIVER_PATH% -jar %SELENIUM_SERVER% -role webdriver -hub http://%HUB_NAME%:%HUB_PORT%/grid/register -port %NODE_PORT%
pause