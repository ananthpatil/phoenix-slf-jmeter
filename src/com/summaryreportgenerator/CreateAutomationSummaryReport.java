package com.summaryreportgenerator;

import java.io.IOException;
import java.text.ParseException;

import com.utilities.ExportSummeryReport;
import com.utilities.JUnitReport;
import com.utilities.ReportGenerationNew;
import com.utilities.SummaryReportWithDefects;

public class CreateAutomationSummaryReport {

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws ParseException, IOException {
		SummaryReportWithDefects summaryReport = new SummaryReportWithDefects();
	try{
		summaryReport.reportForamtion();
		ExportSummeryReport.readXML();
		ExportSummeryReport.readXMlForFailure();
		JUnitReport.generateJunitXml();
	}catch(Exception e){}
  }
}
