package com.commonfunctions;

import io.appium.java_client.remote.MobileCapabilityType;
import js.DynamicElementFinder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.Color;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import au.com.bytecode.opencsv.CSVReader;

import com.applitools.eyes.Eyes;
import com.driver.Driver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import com.utilities.Constants;
import com.utilities.CreateXMLSummery;

public class Action extends Constants {

	public static Hashtable<String, String> objectDetails = new Hashtable<String, String>();
	//to store different flags to handle object identification
	public static Hashtable<String, String> objectExtraDetails = new Hashtable<String, String>();
	public static Hashtable<String, String> objectExtraRegexDetails = new Hashtable<String, String>();
	public static boolean isDynamic=true;
	public static boolean useCaptureUrl=false;
	public static boolean isSLFException1=false;
	public static boolean isSLFException2=false;
	
	
	public static Hashtable<String, String> objectDetails2 = new Hashtable<String, String>();
	public static List<Integer> successCount = new ArrayList<Integer>();

	public static Set<Object[]> testData = new HashSet<Object[]>();
	public static Map<Integer, List<String[]>> testDataAtcore = new HashMap<Integer, List<String[]>>();
	public static Map<Integer, List<String>> testData1 = new HashMap<Integer, List<String>>();

	public static String[][] searchTestData = null;
	public static ExtentReports report = ExtentReports.get(Action.class);

	public static Map<String, List<List<String>>> testDataAtcre = new HashMap<String, List<List<String>>>();
	public static WebDriver driver;
	public static Eyes eyes;
	public static WebDriverWait wait;
	public static DesiredCapabilities cap1;
	public Properties prop = null;

	public static boolean result = true;
	public static String browser = null;

	private static String child1, fldPath;
	private static String[] child;
	protected static String browserName, browserVer, osOrPlatform, osVersion,
			device;
	public static String onMode, reportPath;
	List<String> AllWindowHandles;
	public static Map<Integer, List<String>> browserProp = new ConcurrentHashMap<Integer, List<String>>();
	public static Map<String, String> mp = new HashMap<String, String>();
	Logger logger = Logger.getLogger(this.getClass().toString());
	public static String browserStack_UserName, browserStack_Password;
	public static String pound = "\u00a3";
	public static String euro = "\u20ac";
	public static String plusPound = "\u002B\u00a3";
	public static String plusEuro = "\u002B\u20ac";
	public static String space = "\u0020";
	public String finalReportName = null;
	
	public static String jmeterHome,jmeterConfFldpath,jmeterTstFldPath,jmeterResltFldPath,jmxFldPath;
	public static String JMX_FILE_NAME;
	DocumentBuilderFactory factory;
	DocumentBuilder builder;

	// public String URL =
	// "https://sureshjashti1:zeJiyAqMr2KzsWYpD7Gz@hub-cloud.browserstack.com/wd/hub";
	/**
	 * Method to return reportLocationPath
	 * 
	 * @date Oct 2016
	 * @author Anant
	 * @return reportpath
	 */
	public String getReportLocation() {
		// return getProjectPath() + CONSTANTS_EXTENTREPORT_LOCATION;

		return reportPath + File.separator;
		/*
		 * String curDat, fldPath, curHour, amPmPath, folderdPath = null;
		 * boolean rptFlag, imgFlag, amPmFlag, execute = false; File rptFolder,
		 * imgFolder, amPmFolder = null, readFolder;
		 * 
		 * Date d = new Date(); SimpleDateFormat fmt = new
		 * SimpleDateFormat("dd-MM-yyyy");
		 * 
		 * curDat = fmt.format(d); fmt.applyPattern("hh a"); curHour =
		 * fmt.format(d);
		 * 
		 * // fldPath = getProjectPath()+"\\reports\\"+curDat; fldPath =
		 * reportPath + File.separator + curDat; rptFolder = new File(fldPath);
		 * if (!rptFolder.exists()) { rptFlag = rptFolder.mkdir(); if (rptFlag)
		 * { logger.info("The folder created in the mention dirctory");
		 * amPmFolder = new File(fldPath + File.separator + curHour); amPmFlag =
		 * amPmFolder.mkdir(); if (amPmFlag) { amPmPath =
		 * amPmFolder.getAbsolutePath(); folderdPath = amPmFolder.toString();
		 * imgFolder = new File(amPmPath + File.separator + "images"); imgFlag =
		 * imgFolder.mkdir(); if (imgFlag)
		 * logger.info("The image folder created"); else
		 * logger.info("The image folder not created"); } else {
		 * logger.info("Folder is not created based on timezone"); } } else
		 * logger.info("The folder is not created based on system date"); } else
		 * { readFolder = new File(fldPath); File[] allFolder =
		 * readFolder.listFiles(); for (File fil : allFolder) { execute = false;
		 * if (fil.getName().trim().equals(curHour)) { folderdPath =
		 * fil.getAbsolutePath(); execute = true; break; } } if (execute ==
		 * false) { amPmFolder = new File(fldPath + File.separator + curHour);
		 * amPmFlag = amPmFolder.mkdir(); if (amPmFlag) { amPmPath =
		 * amPmFolder.getAbsolutePath(); folderdPath = amPmFolder.toString();
		 * imgFolder = new File(amPmPath + File.separator + "images"); imgFlag =
		 * imgFolder.mkdir(); if (imgFlag)
		 * logger.info("The image folder created"); else
		 * logger.info("The image folder not created"); } else { // folderdPath
		 * = fil.getAbsolutePath();
		 * logger.info("Folder is not created based on timezone"); } }
		 *//*
			 * else{ // folderdPath = fil.getAbsolutePath(); System.out.
			 * println(
			 * "Already folder exist by current hour in the current date folder"
			 * ); }
			 */
		/*
		 * logger.info("The given directory already exist in the system"); }
		 * return folderdPath.toString() + File.separator;
		 */
	}

	/**
	 * Method to return reportImagePath
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return imagePath
	 */
	public String getReportImageLocation() {
		return CONSTANT_EXTENTREPORT_IMAGES;
	}

	/**
	 * Method to return logger instance
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return logger
	 */
	public Logger getLoggerInstance() {
		return logger;
	}

	/**
	 * This method returns the method name based on parameter
	 * 
	 * @param onMode
	 * @return method name
	 */
	public String getMethod(String onMode) {
		String method;
		if (onMode.contains("browserstack")) {
			method = "BrowserSetupBrowserStack";
		} else if (onMode.contains("grid")) {
			method = "BrowserSetupgrid";
		} else if (onMode.contains("appium")) {
			method = "MobileBrowserSetupChrome";
		} else {
			method = "BrowserSetup";
		}
		return method;
	}

	/**
	 * Method to return project path
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return logger
	 */
	public String getProjectPath() {
		File currentDirFile = new File("");
		String path = currentDirFile.getAbsolutePath();
		return path;
	}

	/**
	 * Method to set up Test
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return void
	 */
	/*
	 * public void setUpTest() {
	 * 
	 * Properties prop = new Properties(); result = true; try { InputStream
	 * input = new FileInputStream(getProjectPath() + CONSTANTS_CONFIG_PATH);
	 * prop.load(input); PropertyConfigurator.configure(getProjectPath() +
	 * CONSTANTS_LOG_PROPERTIES_PATH); browser = (String)
	 * prop.get(CONSTANTS_CONFIG_BUT); if
	 * (browser.equals(CONSTANTS_CONFIG_FF_BROWSER)) { driver = new
	 * FirefoxDriver(); wait = new WebDriverWait(driver, 40);
	 * report.log(LogStatus.INFO, "Firefox driver has started");
	 * logger.info("Firefox driver has started");
	 * 
	 * } else if (browser.equals(CONSTANTS_CONFIG_CHROME_BROWSER)) {
	 * System.setProperty(CONSTANTS_CHROME_PROPERTY, getProjectPath() +
	 * CONSTANTS_CHROME_DRIVER_PATH); driver = new ChromeDriver(); wait = new
	 * WebDriverWait(driver, 40); report.log(LogStatus.INFO,
	 * "Chrome driver has started"); logger.info("Chrome driver has started"); }
	 * else if (browser.equals(CONSTANTS_CONFIG_IE_BROWSER)) {
	 * System.setProperty(CONSTANTS_IE_PROPERTY, getProjectPath() +
	 * CONSTANTS_IE_DRIVER_PATH); driver = new InternetExplorerDriver(); wait =
	 * new WebDriverWait(driver, 40); report.log(LogStatus.INFO,
	 * "Internet Explorer driver has started");
	 * logger.info("Internet Explorer driver has started"); }
	 * 
	 * } catch (FileNotFoundException e) {
	 * logger.info("Driver has not started **********");
	 * report.log(LogStatus.ERROR, "Driver has not started"); logger.error(e);
	 * result = false;
	 * 
	 * } catch (IOException e) { report.log(LogStatus.ERROR,
	 * "Driver has not started"); logger.error(e); result = false;
	 * 
	 * } finally { try { Assert.assertTrue(result, "Set up Test"); } catch
	 * (Exception e) { logger.error("Try and catch block while assert " + e); }
	 * }
	 * 
	 * }
	 */

	/**
	 * Method to set up Test on IE Browser only
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return void
	 */
	/*
	 * public void setUpTestOnIE() {
	 * 
	 * Properties prop = new Properties(); result = true; try { InputStream
	 * input = new FileInputStream(getProjectPath() + CONSTANTS_CONFIG_PATH);
	 * prop.load(input); PropertyConfigurator.configure(getProjectPath() +
	 * CONSTANTS_LOG_PROPERTIES_PATH); browser = (String)
	 * prop.get(CONSTANTS_CONFIG_BUT);
	 * 
	 * System.setProperty(CONSTANTS_IE_PROPERTY, getProjectPath() +
	 * CONSTANTS_IE_DRIVER_PATH); driver = new InternetExplorerDriver(); wait =
	 * new WebDriverWait(driver, 40); report.log(LogStatus.INFO,
	 * "Internet Explorer driver has started");
	 * logger.info("Internet Explorer driver has started");
	 * 
	 * } catch (FileNotFoundException e) {
	 * logger.info("Driver has not started **********");
	 * report.log(LogStatus.ERROR, "Driver has not started"); logger.error(e);
	 * result = false;
	 * 
	 * } catch (IOException e) { report.log(LogStatus.ERROR,
	 * "Driver has not started"); logger.error(e); result = false;
	 * 
	 * } finally { try { Assert.assertTrue(result, "Set up Test"); } catch
	 * (Exception e) { logger.error("Try and catch block while assert " + e); }
	 * }
	 * 
	 * }
	 */

	/**
	 * The method used to call wherever the test data required for particular
	 * test case It returns void
	 */
	public void testDataNA() {
		Driver.totTestData = 1;
		Driver.isTestDatNa = true;
		logger.info("Test data Not required is set");
	}

	/**
	 * The method used to capture the text and it will concatenate all the text
	 * based on comma operator
	 * 
	 * @param objectName
	 * @author Anant
	 * @param keyVal
	 */
	public void captureTextValuesandHold(String objectName, String keyVal) {
		String finconCatVal = "";
		try {
			List<String> values = performActionMultipleGetText(objectName);
			for (int j = 0; j < values.size(); j++) {
				if (!values.get(j).equals("")) {
					if (values.size() == 1 || j == values.size() - 1) {
						finconCatVal = finconCatVal + values.get(j);
					} else {
						finconCatVal = finconCatVal + values.get(j) + ",";
					}
				}
			}
			finconCatVal = objectName + "^^" + finconCatVal;

			report.log(LogStatus.INFO, "The values got concatenated "
					+ finconCatVal);
			logger.info("The values got concatenated " + finconCatVal);
		} catch (Exception e) {
			report.log(LogStatus.INFO,
					"The values are not fetched so unable to concatenate "
							+ finconCatVal);
			logger.info("The values are not fetched so unable to concatenate "
					+ finconCatVal);
		}
		mp.put(keyVal, finconCatVal);
	}

	/**
	 * Method to launch application
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param String
	 *            url
	 * @return void
	 */
public void launchApplication() {
		result = true;
		String url = CONSTANTS_AAPLNURL;
		try {
			driver.manage().window().maximize();
			driver.get(url);
			waitForPageLoad(driver);
			/*try{
			Cookie ck = new Cookie("notified-Compliance-page", "1");
			driver.manage().addCookie(ck);
			driver.get(url);
			waitForPageLoad(driver);
			}catch(Exception e){
				logger.info("Exception while setting cookie: "+e);
			}*/
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
			report.log(LogStatus.INFO, "Launching URL:" + url);
			logger.info("Application under test is launching");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "URL has not launched");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info("URL has not launched **********");
			logger.error(e);
			result = false;
		} finally {
			try {
				Assert.assertTrue(result, "Launch URL");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}
	


	/**
	 * Method to launch application
	 * 
	 * @date April 2015
	 * @param String
	 *            url
	 * @return void
	 */
	public void launchApplicationUsingDeeeplink(String label) {
		result = true;
		String url = CONSTANTS_AAPLNURL;
		try {
			label = fetchTestDataFromMap(label);
			driver.manage().window().maximize();
			driver.get(url + label.trim());
			waitForPageLoad(driver);
			try{
				Cookie ck = new Cookie("notified-Compliance-page", "1");
				driver.manage().addCookie(ck);
				driver.get(url+label.trim());
				waitForPageLoad(driver);
				}catch(Exception e){
					logger.info("Exception while setting cookie: "+e);
				}
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			report.log(LogStatus.INFO, "Launching URL:" + url+label.trim());
			logger.info("Application under test is launching");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "URL has not launched");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info("URL has not launched **********");
			logger.error(e);
			result = false;
		} finally {
			try {
				Assert.assertTrue(result, "Launch URL");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}
	/**
	 * Method to launch Pega application
	 * 
	 * @date 25th June 2015
	 * @author Hima
	 * @param String
	 *            url
	 * @return void
	 */
	public void launchPegaApplication(String url) {
		result = true;
		try {
			driver.get(url);
			try {
				driver.navigate()
						.to("javascript:document.getElementById('overridelink').click()");
			} catch (Exception e) {
			}
			waitForPageLoad(driver);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			logger.info("Application under test is launching");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "URL has not launched");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info("URL has not launched **********");
			logger.error(e);
			result = false;
		} finally {
			report.log(LogStatus.INFO, url + "URL has launched");
			try {
				Assert.assertTrue(result, "Launch URL");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	/**
	 * Method to quit the driver
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return void
	 */
	public void tearDownTest() throws Exception {
		try {
			createXMLSummery();
			report.log(LogStatus.INFO, "Browser has closed successfully");
			createXmlSummaryForFail();
			driver.quit();
		} finally {
			// eyes.abortIfNotClosed();
		}
	}

	public  void createXMLSummery() throws ParseException {

		String line, scenarioName = null, line1 = "", testCaseName = null, flagStatus = null, iteratorVal = null,
				envName = null, driverName = null, brand = null;

		int lineCt = 0, failCt = 0, failCt1 = 0;
		boolean failFound = true, repeatSteps = false, testCaseId, testCaseIdFound;
		float passed = 0, failed = 0, perPass, perFail;
		List<String[]> finalList = new ArrayList<String[]>();

		List<String[]> scenarioList = null;

		float passedTests = 0, failedTests = 0;
		testCaseIdFound = false;
		String reportStartTime = null, reportEndTime = null;
		long diffMinutes = 0;
		File htmlFileName = new File(Constants.REPORT_NAME);
		BufferedReader reader;
		try {
			scenarioList = new ArrayList<String[]>();
			reader = new BufferedReader(new FileReader(htmlFileName));
			scenarioName = htmlFileName.getName().replace(".html", "");
			if (htmlFileName.length() != 0) {
				String[] arr1 = new String[5];
				String failString1 = "", urlToLog = "";
				arr1[2] = htmlFileName.getName();
				failFound = true;
				while ((line = reader.readLine()) != null) {
					testCaseId = false;
					line = line.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
					line1 = line1.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();

					if (line.contains("driver has started")) {
						driverName = line.trim();
					}
					if (line.contains("Launching URL")) {
						envName = line.trim();

						if (envName.contains("holiday")) {
							brand = "FC";
						} else if (envName.contains("cruise")) {
							brand = "Cruise";
						} else if (envName.contains("flight")) {
							brand = "FO";
						} else if (envName.contains("falcon")) {
							brand = "Falcon";
						} else if (envName.contains("Retail")) {
							brand = "Hybris Retail";
						} else {
							brand = "TH";
						}

					}

					// if(line.contains("http")||line.contains("https")){
					if (line.contains("Launching URL:")) {
						envName = line.replace("http:", "").replace("https:", "").replace("//", "")
								.replace("Launching URL:", "").replace("Page Url:", "").trim();

						try {
							envName = envName.substring(0, envName.indexOf(".")).trim();
							if (envName.contains("/")) {
								envName = envName.substring(0, envName.indexOf("/"));
							}
						} catch (StringIndexOutOfBoundsException s) {
						}
					}
					if (line.contains("Test started time")) {

						reportStartTime = line.substring(line.indexOf("</i>") + 10, line.length() - 7);
						if (reportStartTime != null)
							reportStartTime = reportStartTime.trim();

					}
					if (line.contains("Test ended time")) {

						reportEndTime = line.substring(line.indexOf("%%-->") + 11, line.length() - 29);

						if (reportEndTime != null)
							reportEndTime = reportEndTime.trim();

					}

					if (reportStartTime != null && reportEndTime != null) {
						SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
						Date date1 = format.parse(reportStartTime);
						Date date2 = format.parse(reportEndTime);
						long difference = date2.getTime() - date1.getTime();
						diffMinutes = difference / (60 * 1000) % 60;
					}

					if (line.contains("Launching URL") || line.contains("Page Url")) {
						// urlToLog =
						// line.replace("Launching
						// URL:", "").replace("Page
						// Url:", "");
						urlToLog = urlToLog + line.replace("Launching URL:", "").replace("Page Url:", "") + "\n";
					}
					if (line.contains("ScenarioName:")) {
						scenarioName = line.replace("ScenarioName", "").replace(":", "").trim();
					}
					arr1[0] = scenarioName;
					if (repeatSteps && lineCt == 1) {
						testCaseName = line1;
						if (line1.contains(">>")
								|| line1.contains("Test case desription not specifed in the description column")) {
							testCaseId = true;
							testCaseIdFound = true;
						}
					} else if (line.contains(">>")
							|| line.contains("Test case desription not specifed in the description column")) {
						testCaseName = line;
						testCaseId = true;
						testCaseIdFound = true;
					}

					lineCt++;
					if (testCaseId) {
						failFound = true;
						failCt = 0;
						String failString = "";
						String[] arr = new String[5];
						if (scenarioList.size() != 0) {
							scenarioList.clear();
						}
						while ((line = reader.readLine()) != null) {
							lineCt++;
							repeatSteps = false;
							if (line.contains("title='FAIL") || line.contains("title='ERROR")) {
								flagStatus = "FAIL";
								failFound = false;
								line = reader.readLine().replace("<td class='step-details' colspan='2'>", "")
										.replace("</td>", "").trim();
								failString = failString + line + "\n";
								arr[4] = urlToLog;
								if (failCt == 0) {
									//failed++;
									failedTests++;
								}
								failCt++;
							}
							if (line.contains(">>")
									|| line.contains("Test case desription not specifed in the description column")) {
								repeatSteps = true;
								lineCt = 1;
								line1 = line;
								break;
							}
						}
						if (failFound == true) {
							flagStatus = "PASS";
							arr[4] = "";
							//passed++;
							passedTests++;
						}
						arr[0] = testCaseName;
						arr[1] = flagStatus;
						arr[2] = htmlFileName.getName();
						arr[3] = failString;
						finalList.add(arr);
					} else if (testCaseIdFound == false) {
						if (line.contains("title='FAIL'") || line.contains("title='ERROR")) {
							flagStatus = "FAIL";
							failFound = false;
							arr1[1] = flagStatus;
							failString1 = failString1 + reader.readLine()
									.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim()
									+ "\n";
							arr1[3] = failString1;
							arr1[4] = urlToLog;
							scenarioList.add(arr1);
							if (failCt1 == 0) {
								//failed++;
								failedTests++;
							}
							failCt1++;
						}
					}
				}
				if (failFound == true && testCaseIdFound == false) {
					flagStatus = "PASS";
					arr1[1] = flagStatus;
					arr1[3] = failString1;
					arr1[4] = "";
					finalList.add(arr1);
					//passed++;
					passedTests++;
				}
				if (scenarioList.size() != 0) {
					finalList.add(scenarioList.get(scenarioList.size() - 1));
				}
			} else {
				System.out.println("The html file doesn't have the content:");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// variable to hold the environment
		String evironment = envName;

		// variable to hold the brand
		// if (htFlName.split("_").length > 1) {
		// brand = htFlName.split("_")[1];
		// System.out.println(brand);
		// }
		// variable to hold the driver name
		String browserName = null;

		if (driverName.toLowerCase().contains("Internet Explorer".toLowerCase())) {
			browserName = "IE";
		} else if (driverName.toLowerCase().contains("Firefox".toLowerCase())) {
			browserName = "Firefox";
		} else if (driverName.toLowerCase().contains("Chrome".toLowerCase())) {
			browserName = "Chrome";

		} else if (driverName.toLowerCase().contains("Mobile-iOS".toLowerCase())) {
			browserName = "Mobile-iOS";

		} else if (driverName.toLowerCase().contains("Mobile-Andriod".toLowerCase())) {
			browserName = "Mobile-Andriod";

		} else if (driverName.toLowerCase().contains("Tab-iOS".toLowerCase())) {
			browserName = "Tab-iOS";

		} else if (driverName.toLowerCase().contains("Tab-Andriod".toLowerCase())) {
			browserName = "Tab-Andriod";

		}else if (driverName.toLowerCase().contains("Android".toLowerCase())) {
			browserName = "Mobile-Andriod";

		}else if (driverName.toLowerCase().contains("Ipad".toLowerCase())) {
			browserName = "Ipad";

		}else if (driverName.toLowerCase().contains("Safari".toLowerCase())) {
			browserName = "Safari";

		}else if (driverName.toLowerCase().contains("Iphone".toLowerCase())) {
            browserName = "Iphone";
        }

		logger.info("*******************************************");
		logger.info("ENVIRONMENT :" + evironment);
		logger.info("BRAND :" + brand);
		logger.info("BROWSER :" + browserName);
		logger.info("PASSED TESTS:" + passedTests);
		logger.info("FAILED TESTS:" + failedTests);
		logger.info("TOTAL TESTS:" + (new Float(passedTests) + new Float(failedTests)));
		logger.info("EXECUTION TIME IN MINS :" + diffMinutes);
		logger.info("*******************************************");

		// populating xml with above data
		CreateXMLSummery.addSummary(evironment.toUpperCase(), browserName, brand, new Float(passedTests) + new Float(failedTests),
				passedTests, failedTests, diffMinutes);

	}
	
	
	/**
	 * The mnethod to get the the list of failures with respect scenarios
	 */
	public void createXmlSummaryForFail(){
			String line, scenarioName = null,envName = null ,brand = null;
			List<List<String>> passFail = null;
			List<String> failList = null,errList =null;
			Map<String,Map<String,List<List<String>>>> passFailMap = new HashMap<String,Map<String,List<List<String>>>>();
			Map<String,List<List<String>>> urlMap = new HashMap<String,List<List<String>>>();
			File htmlFileName = new File(Constants.REPORT_NAME);
			BufferedReader reader;
			
			try {
				reader = new BufferedReader(new FileReader(htmlFileName));
				scenarioName = htmlFileName.getName().replace(".html", "");
				int urlCt=0;
				if (htmlFileName.length() != 0) {
					String failString = "",errString ="", urlToLog = "";
					while ((line = reader.readLine()) != null) {
						line = line.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
						if (line.contains("Launching URL")) {
							envName = line.trim();
							if (envName.toLowerCase().contains("holiday")&&!envName.toLowerCase().contains(".ie")) {
								brand = "FC";
							} else if (envName.toLowerCase().contains("cruise")) {
								brand = "Cruise";
							} else if (envName.toLowerCase().contains("flight")) {
								brand = "FO";
							} else if (envName.toLowerCase().contains("falcon")) {
								brand = "Falcon";
							} else if (envName.toLowerCase().contains("retail")) {
								brand = "Hybris Retail";
							}else if (envName.toLowerCase().contains(".ie")||envName.toLowerCase().contains("/f")) {
								brand = "Falcon";
							} else {
								brand = "TH";
							}
						}
						
						if (line.toLowerCase().contains("driver has started")) {
							passFail = new ArrayList<List<String>>();
							failList = new ArrayList<String>();
							errList = new ArrayList<String>();
							urlCt++;
							urlToLog = "";
						}

						if (line.contains("Launching URL:")) {
							envName = line.replace("http:", "").replace("https:", "").replace("//", "")
									.replace("Launching URL:", "").replace("Page Url:", "").trim();

							try {
								envName = envName.substring(0, envName.indexOf(".")).trim();
								if (envName.contains("/")) {
									envName = envName.substring(0, envName.indexOf("/"));
								}
							} catch (StringIndexOutOfBoundsException s) {}
						}
						
						if (line.contains("Launching URL") || line.contains("Page Url")) {
							urlToLog = urlToLog + line.replace("Launching URL:", "").replace("Page Url:", "") + "\n";
						}
						if (line.contains("ScenarioName:")) {
							scenarioName = line.replace("ScenarioName", "").replace(":", "").trim();
						}
						
						if(!passFailMap.containsKey(scenarioName) && passFailMap.size()!=0){
							urlMap = new HashMap<String,List<List<String>>>();
						}

						if (line.contains("title='FAIL'") ) {
						     failString = reader.readLine()
										.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
						     failList.add(failString.trim());
					     }
						if (line.contains("title='ERROR")) {
							errString = reader.readLine()
									.replace("<td class='step-details' colspan='2'>", "").replace("</td>", "").trim();
							errList.add(errString.trim());	
				        }

					   if(line.toLowerCase().contains("Browser has closed successfully".toLowerCase()) ){
							if (urlMap.size() == 0) {
								  passFail.add(failList);
								  passFail.add(errList);
							      urlMap.put(urlCt+"."+urlToLog, passFail);
							}
							else{
								for(Map.Entry<String, List<List<String>>> urlStoredVal: urlMap.entrySet()){
									List<List<String>> mapErrFail  = urlStoredVal.getValue();
									List<String> failMapLt,errMapLt;
									failMapLt = mapErrFail.get(0);
									errMapLt = mapErrFail.get(1);
									
									failList.removeAll(failMapLt);
									errList.removeAll(errMapLt);
							   }
								if(failList.size()!=0 || errList.size()!=0){
								   passFail.add(failList);
								   passFail.add(errList);
								   urlMap.put(urlCt+"."+urlToLog, passFail);
								}
						    }
							passFailMap.put(scenarioName, urlMap);
						}
					 }
				} else {
					System.out.println("The html file doesn't have the content:");
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			// variable to hold the environment
			String evironment = envName;
			CreateXMLSummery.addXmlSummaryForFail(evironment.toUpperCase(), brand.toUpperCase(), passFailMap);
			logger.info("*******************************************");
			logger.info("ENVIRONMENT In Failure:" + evironment);
			logger.info("BRAND In Failure:" + brand);
			logger.info("*******************************************");
		}

	
	/**
	 * Method to close the eye
	 * 
	 * @return void
	 */
	public void closeEye() {
		try {
			eyes.close();
		} finally {
			eyes.close();
		}
	}

	/**
	 * Method to get the object details from the object repository
	 * 
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @return Hashtable<String,String> Modified by omar on 20-06-107
	 */
	@SuppressWarnings("resource")
	public Hashtable<String, String> getObjectDetails(String objectName) {
		// String csvFile = getProjectPath() +"\\"+ CONSTANTS_OR_PATH;
		String csvFile = CONSTANTS_OR_PATH;
		// BufferedReader br = null;
		try {
			CSVReader reader = new CSVReader(new FileReader(csvFile));
			String[] line;
			boolean objectfound = false;
			logger.info("Object being read: " + objectName);
			DynamicElementFinder DF = new DynamicElementFinder();
			logger.info("Obj resolved" + DF.resolveJQConflict(driver));

			while ((line = reader.readNext()) != null) {
				if (line[0].equalsIgnoreCase(objectName)) {
					String index4;
					if(line.length==4){
						index4 = "";
					}else{
						index4 = line[4];
					}
					objectDetails.put(CONSTANTS_OBJECT_NAME, line[0]);
					objectDetails.put(CONSTANTS_OBJECT_TYPE, line[1]);
					objectDetails.put(CONSTANTS_OBJECT_LOCATOR, line[2]);
					objectDetails.put(CONSTANTS_OBJECT_LOCATOR_VALUE, line[3]);
					// added to include dynamic text in obj repo and to get
					// regex from obj repo
					/*
					 * objectDetails.put(CONSTANTS_OBJECT_LOCATOR_VALUE,
					 * line[4]);
					 * objectDetails.put(CONSTANTS_OBJECT_LOCATOR_VALUE,
					 * line[5]);
					 */
					objectExtraDetails.put(objectName, index4);

					try {
						objectExtraRegexDetails.put("ObjName", objectName);
						objectExtraRegexDetails.put(objectName + "_regex",
								line[5]);
						objectExtraRegexDetails.put(objectName + "_regex2",
								line[6]);
					} catch (Exception e) {
						logger.info("error while reading regex to click");
					}
					objectfound = true; // added for dynm obj locator
					break;
				}

			}

			if (!objectfound) {
				logger.info("Obj resolved" + DF.resolveJQConflict(driver));
				String xpathfound;
				try {
					long lastSec = 0;
					int i = 1;
					while (true) {
						// waiting for 2 sec
						long sec = System.currentTimeMillis() / 1000;
						if (sec != lastSec) {
							DF.resolveJQConflict(driver);
							try {
								xpathfound = getXpathByValue(objectExtraDetails
										.get(objectName));
							} catch (Exception e) {
								xpathfound = getXpathByValue(objectName);
							}

							if (!xpathfound.equals("")) {
								break;
							}
							if (i == 1) {
								break;
							}
							lastSec = sec;
							i++;
							logger.info("Unable to calculat"
									+ "e the xpath so waiting for 5sec");
						}
					}
				} catch (Exception e) {
					logger.error("error while waiting for 5 sec");
					useCaptureUrl = true;
					xpathfound = "";
				}
				objectDetails.put(CONSTANTS_OBJECT_NAME, objectName);
				objectDetails.put(CONSTANTS_OBJECT_TYPE, "TextBox");
				objectDetails.put(CONSTANTS_OBJECT_LOCATOR, "xpath");
				objectDetails.put(CONSTANTS_OBJECT_LOCATOR_VALUE, xpathfound);

			}
		} catch (FileNotFoundException e) {
			logger.info("Could not fetch object details from OR  **********");
			logger.error(e);
		} catch (IOException e) {
			logger.info("Could not fetch object details from OR **********");
			logger.error(e);
		} /*
		 * finally { if (br != null) { try { br.close(); } catch (IOException e)
		 * { logger.info("Could not fetch object details from OR **********");
		 * logger.error(e); } } }
		 */
		return objectDetails;
	}
	
	
	/*
	 * @Author-Omar Method to return xpath for a text specified, it may be
	 * either a tag value or a a attribute value
	 * 
	 * @Params-String return String (xpath in the form of a string
	 */
	public String getXpathByValue(String textvalue) {
		String xpath = "";
		int matchNo = 0;
		if (textvalue.contains(",")) {
			try {
				String[] countNo = textvalue.split(",");
				textvalue = countNo[0];
				matchNo = Integer.parseInt(countNo[1]);
			} catch (ArrayIndexOutOfBoundsException ar) {

			}
		}
		if (matchNo == 0) {
			try {
				DynamicElementFinder def = new DynamicElementFinder();
				if (((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete")) {
					try {
						if ((long) ((JavascriptExecutor) driver)
								.executeScript("return jQuery.active") == 0) {
							if (textvalue.contains("=")) {
								String[] values = textvalue.split("=");

								xpath = def.getXpathforAttributeNameValue(
										driver, values[0], values[1]);
							} else {
								xpath = def.getXpathforTagValue(driver,
										textvalue);
								if (xpath.equals("")) {
									xpath = def.getXpathforAttributeValue(
											driver, textvalue);
								}
								if (xpath.equals("")) {
									xpath = def.getXpathforAttributeName(
											driver, textvalue);
								}
								if (xpath.equals("")) {
									xpath = def.getXpathforTagValueWithMatch(
											driver, textvalue,
											Integer.toString(matchNo));
								}

							}

						}
					} catch (Exception e) {
						// e.printStackTrace();
						if (textvalue.contains("=")) {
							String[] values = textvalue.split("=");

							xpath = def.getXpathforAttributeNameValue(driver,
									values[0], values[1]);
						} else {
							xpath = def.getXpathforTagValue(driver, textvalue);
							if (xpath.equals("")) {
								xpath = def.getXpathforAttributeValue(driver,
										textvalue);
							}
							if (xpath.equals("")) {
								xpath = def.getXpathforAttributeName(driver,
										textvalue);
							}
							if (xpath.equals("")) {
								xpath = def.getXpathforTagValueWithMatch(
										driver, textvalue,
										Integer.toString(matchNo));
							}
						}
					}
					logger.info("Xpath returned for the text/attribute:"
							+ textvalue + " is " + xpath);

					return xpath;

				}
			} catch (Exception e) {
				logger.error("Error message" + e.getMessage());
				e.printStackTrace();
				logger.info("Error while returning the xpath value");
			}

		} else {
			matchNo = matchNo - 1;
			try {
				DynamicElementFinder def = new DynamicElementFinder();
				if (((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete")) {
					try {
						// if((long)((JavascriptExecutor)
						// driver).executeScript("return jQuery.active")==0)
						{
							if (textvalue.contains("=")) {
								String[] values = textvalue.split("=");

								xpath = def.getXpathforAttributeNameValueMatch(
										driver, values[0], values[1],
										Integer.toString(matchNo));
							} else {
								xpath = def.getXpathforTagValueWithMatch(
										driver, textvalue,
										Integer.toString(matchNo));
								if (xpath.equals("")) {
									xpath = def.getXpathforAttributeValueMatch(
											driver, textvalue,
											Integer.toString(matchNo));
								}
								if (xpath.equals("")) {
									xpath = def.getXpathforAttributeNameMatch(
											driver, textvalue,
											Integer.toString(matchNo));
								}

							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					logger.info("Xpath returned for the text/attribute:"
							+ textvalue + " is " + xpath);
					return xpath;
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.info("Error while returning the xpath value");
			}
		}
		return xpath;
	}

	
	// method to clcick an web element based on regex

	public void PerformActionClickonRegex(String regex) throws ParseException {
		logger.info("in PerformActionClickonRegex ext");
		String regexcontent = "var eler=new RegExp('" + regex + "');";
		String addjquerContent = "var newscript = document.createElement('script');newscript.type = 'text/javascript';newscript.async = true;newscript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js';(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(newscript);";
		String jscontent = addjquerContent
				+ regexcontent
				+ "var ele=jQuery( \"*\" );for(i=0;i<ele.length;i++){if(eler.test(ele[i])==true){ele[i].click();return true;}}return false;";
		if (((JavascriptExecutor) driver).executeScript(
				"return document.readyState").equals("complete")) {
			logger.info("in PerformActionClickonRegex int");

			try {
				long lastSec = 0;
				int i = 1;
				while (true) {
					long sec = System.currentTimeMillis() / 5000;
					if (sec != lastSec) {

						try {
							Boolean val = (Boolean) ((JavascriptExecutor) driver)
									.executeScript(jscontent);
							logger.info("clicked on the element captured with regex:"
									+ regex);
							if (val == false) {
								String[] host = CONSTANTS_AAPLNURL.split("/");
								try {
									driver.get(host[0]
											+ "//"
											+ host[2]+"/"+ host[3]+ regex.replaceAll("\\\\u0026", "&"));
									logger.info("Launching next page url directly "
											+ host[0]
											+ "//"
											+ host[2]
											+ regex.replaceAll("\\\\u0026", "&"));
								} catch (Exception e1) {

								}
							}
							break;
						} catch (Exception e) {

						}

						if (i == 3) {
							String[] host = CONSTANTS_AAPLNURL.split("/");
							driver.get(host[0] + "//" + host[2]+"/"+ host[3]+ regex.replaceAll("\\\\u0026", "&"));
							logger.info("Launching next page url directly "
									+ host[0] + "//" + host[2]
									+ regex.replaceAll("\\\\u0026", "&"));
							break;
						}
						lastSec = sec;
						i++;
						logger.info("Unable to click on next page button so waiting for 5sec");
					}
				}
			} catch (Exception e) {
				logger.error("error while waiting for 5 sec to launch the next page url");

			}
		}
	}

	
public void performActionClickonDynamicOR(String objectName) {
		DynamicElementFinder DF = new DynamicElementFinder();
		logger.info("Obj resolved" + DF.resolveJQConflict(driver));
		String xpathfound;
		try {
			long lastSec = 0;
			int i = 1;
			while (true) {
				// waiting for 2 sec
				long sec = System.currentTimeMillis() / 1000;
				if (sec != lastSec) {
					DF.resolveJQConflict(driver);
					// xpathfound=getXpathByValue(objectName);
					// modified fro reading data from next column
					try {
						xpathfound = getXpathByValue(objectExtraDetails
								.get(objectName));
					} catch (Exception e) {
						xpathfound = getXpathByValue(objectName);
					}

					if (!xpathfound.equals("")) {
						break;
					}
					if (i == 1) {
						break;
					}
					lastSec = sec;
					i++;
					logger.info("Unable to calculat"
							+ "e the xpath so waiting for 5sec");
				}
			}
			driver.findElement(By.xpath(xpathfound)).click();
			logger.info("Clicked on element " + objectName);

		} catch (Exception e) {

			logger.info("Unable to click the object using dynamic OR so launching the url directly");
			try {
				if (null != objectExtraRegexDetails.get(objectExtraRegexDetails
						.get("ObjName") + "_regex")) {
					logger.info("in obj ext");
					PerformActionClickonRegex(objectExtraRegexDetails
							.get(objectExtraRegexDetails.get("ObjName")
									+ "_regex"));
					objectExtraDetails.clear();
				}

			} catch (ParseException e1) {
				logger.info("Unable to launch the url directly");
				e1.printStackTrace();
			}

		}

	}
	
	
	/**
	 * Method to parse the data to the DataProvider
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Test
	 *            data path
	 * @return Iterator<Object[]>
	 */
	@SuppressWarnings("resource")
	public Iterator<Object[]> getTestData(String TestDataPath)
			throws FileNotFoundException {
		TestDataPath = getProjectPath() + TestDataPath;
		// System.out.println(TestDataPath);
		CSVReader reader = new CSVReader(new FileReader(TestDataPath));
		result = true;
		String[] line;
		try {
			while ((line = reader.readNext()) != null) {
				testData.add(new Object[] { line[0] });
			}

		} catch (FileNotFoundException e) {
			logger.info("Could not fetch Test data **********");
			logger.error(e);
			result = false;
		} catch (IOException e) {
			logger.info("Could not fetch Test data **********");
			logger.error(e);
			result = false;
		}
		return testData.iterator();
	}

	/**
	 * The method is used to capture the url
	 */
	public void reportUrl() {
		String url = null;
		try {
			url = driver.getCurrentUrl();
			report.log(LogStatus.INFO, "Page Url: " + url);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * The method is used to report page url
	 * 
	 * @author Kaushik It returns void
	 * @param
	 */
	public static String reportUrl1() {
		String url = null;
		try {
			url = driver.getCurrentUrl();
			report.log(LogStatus.INFO, "Page Url: " + url);

		} catch (Exception e) {
			System.out.println(e);
		}
		return url;
	}

	/**
	 * The method used to verify the broken links It accepts one parameter
	 * 
	 * @Autor Anant
	 * @param string
	 */
	@SuppressWarnings("unused")
	public void linksValidation(String pageName) {
		String pageResponse = "";
		List<WebElement> urlLink, headerLink, footerLink;
		List<String> finalList, hedrFooter;
		urlLink = new ArrayList<WebElement>();
		headerLink = new ArrayList<WebElement>();
		footerLink = new ArrayList<WebElement>();

		finalList = new ArrayList<String>();
		hedrFooter = new ArrayList<String>();
		int invalidLinks = 0;
		try {
			if (pageName.toLowerCase().contains("header")) {
				try {
					WebElement _header = driver.findElement(By
							.xpath("//*[contains(@id,'header')]"));
					headerLink = _header.findElements(By.tagName("a"));
					try {
						headerLink.addAll(_header.findElements(By
								.tagName("img")));
					} catch (Exception e) {
					}
					logger.info("the haeder size:" + headerLink.size());
				} catch (Exception e) {
					logger.error("<== The Exception occured while fetching the links from header ==>");
				}
			}

			if (pageName.toLowerCase().contains("footer")) {
				try {
					WebElement _footer = driver.findElement(By.id("footer"));
					footerLink = _footer.findElements(By.tagName("a"));
					try {
						footerLink.addAll(_footer.findElements(By
								.tagName("img")));
					} catch (Exception e) {
					}
					logger.info("the Footer size:" + footerLink.size());
				} catch (Exception e) {
					logger.error("<== The Exception occured while fetching the links from footer ==>");
				}
			}

			hedrFooter = filterUrls(headerLink);
			hedrFooter.addAll(filterUrls(footerLink));

			urlLink = driver.findElements(By.tagName("a"));
			urlLink.addAll(driver.findElements(By.tagName("img")));
			finalList = filterUrls(urlLink);

			// System.out.println("Before removing:"+finalList.size()+"
			// "+hedrFooter.size());
			for (int hf = 0; hf < hedrFooter.size(); hf++) {
				for (int f = 0; f < finalList.size(); f++) {
					if (hedrFooter.get(hf).trim()
							.equals(finalList.get(f).trim())) {
						finalList.remove(hedrFooter.get(hf));
						// System.out.println("Removed:"+hedrFooter.get(hf));
						break;
					}
				}
			}
			// System.out.println("After removing:"+finalList.size());
			pageName = pageName.toLowerCase().replace("header", "")
					.replace("footer", "").replaceAll(",", "");
			for (int i = 0; i < finalList.size(); i++) {
				URL url = null;
				HttpURLConnection connect;
				pageResponse = "";
				// HttpGet request = new HttpGet(finalList.get(i));
				try {
					String line;
					url = new URL(finalList.get(i));
					connect = (HttpURLConnection) url.openConnection();
					connect.setRequestMethod("GET");
					connect.connect();
					int _code = connect.getResponseCode();
					String _message = connect.getResponseMessage()
							.toLowerCase();
					StringBuilder builder = new StringBuilder();
					// InputStream stream = connect.getInputStream();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(connect.getInputStream()));
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					reader.close();
					if (builder.toString().contains("Technical Difficulties")) {
						pageResponse = "Technical Difficulties";
					}
					if (builder.toString().contains(
							"Service temporarily unavailable")) {
						pageResponse = "Service temporarily unavailable";
					}
					if (builder.toString().contains("All Gone")) {
						pageResponse = "All Gone";
					}
					if (builder.toString().length() == 0) {
						pageResponse = "Server Returned Blank Page";
					}

					// System.out.println("The
					// url$$$$$$$$$$$$$$:"+builder.toString().length()+" "+url);

					if (_code == 200 && _message.equalsIgnoreCase("ok")) {
						report.log(LogStatus.PASS, "LinksStatus"
								+ " Verifying condition is passed from "
								+ pageName + " page where Expected:"
								+ "200 and ok" + " " + "Actual:" + _code
								+ " and " + _message + " " + pageResponse
								+ " URl:" + url);
						logger.info("Comparingsummary parameters" + "-"
								+ "Expected" + "200 and ok" + " " + "Actual"
								+ +_code + " and " + _message + " "
								+ pageResponse + " URl:" + url);

						// System.out.println("Links Verfifying condition is
						// passed from page where Expected:200 and ok
						// Actual:"+_code+" and "+_message);
					} else {
						report.log(LogStatus.FAIL, "LinksStatus"
								+ " Verifying condition is failed from "
								+ pageName + " page where Expected:"
								+ "200 and ok" + " " + "Actual:" + _code
								+ " and " + _message + " " + pageResponse
								+ " URl:" + url);
						logger.info("Comparingsummary parameters" + "-"
								+ "Expected" + "200 and ok" + " " + "Actual"
								+ +_code + " and " + _message + " "
								+ pageResponse + " URl:" + url);
						invalidLinks++;
					}
				} catch (FileNotFoundException e) {
					report.log(LogStatus.FAIL, "LinksStatus"
							+ " Verifying condition is failed from " + pageName
							+ " page where Expected:" + "200 and ok" + " "
							+ "Actual:" + "File Not found Exception" + " URl:"
							+ url);
					logger.info("Links"
							+ " could not captured from the web page **********");
					e.printStackTrace();
					logger.error(e);
				} catch (Exception e) {
					report.log(LogStatus.FAIL, "LinksStatus"
							+ " Verifying condition is failed from " + pageName
							+ " page where Expected:" + "200 and ok" + " "
							+ "Actual:" + "" + " URl:" + url);
					logger.info("Links"
							+ " could not captured from the web page **********");
					logger.error(e);
				}

			}

		} catch (Exception e) {
			logger.info("Links"
					+ " could not captured from the web page **********");
			logger.error(e);
		}

	}

	/**
	 * The method used to filter the url It returns the list;
	 * 
	 * @param urlLink
	 */
	@SuppressWarnings("unused")
	public List<String> filterUrls(List<WebElement> urlLink) {
		int invalidLinks = 0;
		List<String> finalList = new ArrayList<String>();
		for (int i = 0; i < urlLink.size(); i++) {
			try {
				String urlVal = urlLink.get(i).getAttribute("href");
				if (urlVal != null && !urlVal.contains("javascript")) {
					if (urlVal.trim().length() > 1
							&& urlVal.trim().contains("http")) {
						if (!(urlVal.contains("youtube") || urlVal
								.contains("facebook"))) {
							if (!(urlVal.contains("twitter") || urlVal
									.contains("instagram"))) {
								if (!urlVal.contains("plus.google.com"))
									finalList.add(urlLink.get(i).getAttribute(
											"href"));
							}
						}
					}
				} else {
					invalidLinks++;
				}
			} catch (Exception e) {
				logger.error("The exception in filtering the url:" + e);
			}
		}
		return finalList;
	}

	/**
	 * @param File
	 *            Name
	 * @param Sheet
	 *            Name
	 * @return
	 */
	public String[][] getTestScriptData(String fileName, String sheetName) {
		String[][] arrayExcelData = null;
		try {
			FileInputStream fs = new FileInputStream(fileName);
			Workbook wb = Workbook.getWorkbook(fs);
			Sheet sh = wb.getSheet(sheetName);

			int totalNoOfCols = sh.getColumns();
			int totalNoOfRows = sh.getRows();

			arrayExcelData = new String[totalNoOfRows - 1][totalNoOfCols];

			for (int i = 1; i < totalNoOfRows; i++) {

				for (int j = 0; j < totalNoOfCols; j++) {
					arrayExcelData[i - 1][j] = sh.getCell(j, i).getContents();
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			e.printStackTrace();
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return arrayExcelData;
	}

	/**
	 * The method is used to read the data from property file It reads ,Script
	 * path,sheetName,Or Name,Test Data name
	 * 
	 * @return Map
	 */
	public Set<String> readPropertiesData(String prptyPath) {
		prop = new Properties();
		Set<String> keyDetail = null;
		try {
			String bndName = null;
			keyDetail = new LinkedHashSet<String>();
			prop.load(new FileInputStream(prptyPath));
			Iterator<Object> it = prop.keySet().iterator();
			while (it.hasNext()) {
				String val = it.next().toString();
				try {
					bndName = val.substring(0, val.indexOf("."));
					keyDetail.add(bndName.trim());
				} catch (StringIndexOutOfBoundsException a) {
					logger.error("String Index out of bpound execption "
							+ a.getMessage());
					System.exit(0);
				} catch (NullPointerException e) {
					logger.error("Null pointer execption in readPropertiesData method :"
							+ e.getMessage());
					System.exit(0);
				}
			}
		} catch (FileNotFoundException e) {
			logger.error("The file not found exeception:" + e.getMessage());
		} catch (Exception e) {
			logger.error("The Exception occurred while reading property: "
					+ prptyPath + "\n" + e.getMessage());
		}
		return keyDetail;
	}

	/**
	 * The method used to set the different browser values
	 * 
	 * @param brandName
	 * @return void All the properties values will store in Map
	 */
	public void setDifferentBrowserInfo(String brandName, String envPrptyPath) {
		String brName, brVersion = "", osOrPlatform, osVersion = "", deviceName = "", browerEnvPath;
		if (envPrptyPath.length() == 0) {
			browerEnvPath = getProjectPath()
					+ Constants.CONSTANTS_BROWSERENV_PATH;
		} else {
			browerEnvPath = envPrptyPath;
		}
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(browerEnvPath));
			String[] noOfBrwoser = CONSTANTS_EXECUTIONCRITERIA.split(",");
			for (int b = 0; b < noOfBrwoser.length; b++) {
				String brwser = noOfBrwoser[b].trim();
				List<String> brValues = new ArrayList<String>();
				try {
					if (brandName.toLowerCase().contains("desktop")) {
						brVersion = prop.getProperty(
								brwser + ".browser.version").trim();
						osVersion = prop.getProperty(brwser + ".os.version")
								.trim();
					}
					if (brandName.toLowerCase().contains("mobile")) {
						deviceName = prop.getProperty(brwser + ".device.name")
								.trim();
					}
					brName = prop.getProperty(brwser + ".browser.name").trim();
					osOrPlatform = prop.getProperty(
							brwser + ".osorplatform.name").trim();
					brValues.add(brName);
					brValues.add(brVersion);
					brValues.add(osOrPlatform);
					brValues.add(osVersion);
					brValues.add(deviceName);
					browserProp.put(b, brValues);
				} catch (NullPointerException n) {
					logger.error("Exception while reading the browser env properties file"
							+ n.getMessage());
					System.exit(0);
				}
			}
		} catch (FileNotFoundException f) {
			logger.error("File not found exception while reading "
					+ browerEnvPath + " " + f.getMessage());
			System.exit(0);
		} catch (Exception e) {
			logger.error("IO exception error while reading" + browerEnvPath
					+ " " + e.getMessage());
			System.exit(0);
		}
	}

	/**
	 * The function to read the test data from CSV file The values are get
	 * stored in key/value pair It returns the total rows as test data It
	 * accepts the CSV filpath as parameter
	 * 
	 * @param filPath
	 */
	public int getTestData1(String filPath) {
		int totRows = 1, i;
		try {
			@SuppressWarnings("resource")
			CSVReader reader = new CSVReader(new FileReader(filPath));
			List<String[]> lt = reader.readAll();
			List<String> contnt;
			String[] header = lt.get(0);
			for (i = 1; i < lt.size(); i++) {
				String[] st = lt.get(i);
				contnt = new ArrayList<String>();
				for (int p = 0; p < st.length; p++) {
					contnt.add(header[p] + "^^" + st[p]);
				}
				testData1.put(i, contnt);
			}
			totRows = i - 1;
		} catch (FileNotFoundException e) {
			logger.info("Could not fetch Test data from test data file**********");
			logger.error(e);
			result = false;
		} catch (IOException e) {
			logger.info("Could not fetch Test data  from test data file**********");
			logger.error(e);
			result = false;
		}
		return totRows;
	}

	/*
	 * The function to read the test data and get store in key/value pair Thsi
	 * functuion is useful to read the test data based on the test case
	 */
	public int getTestDataAtCore(String filPath) {
		try {
			List<String> contnt1 = null;
			int tot = 0;
			CSVReader reader = new CSVReader(new FileReader(filPath));
			List<Object> lVal;
			List<String[]> csvData = reader.readAll();
			List<List<Object>> contnt = new ArrayList<List<Object>>();
			String val1, val2;
			for (int i = 0; i < csvData.size(); i++) {
				String[] st = csvData.get(i);
				for (int p = 0; p < st.length; p++) {
					lVal = new ArrayList<Object>();
					if (st[p].contains("TestCase")) {
						lVal.add(st[p]);
						lVal.add((i + 1));
						lVal.add(p);
						contnt.add(lVal);
					}
				}
			}

			if (contnt.size() > 1) {
				if (contnt.size() % 2 == 0) {
					tot = contnt.size();
				} else {
					tot = contnt.size() / 2;
				}
			}

			for (int i = 0; i < tot; i++) {
				int srCt, erCt, slCt, elCt;

				try {
					List<Object> lVal1 = contnt.get(i);
					List<Object> lVal2 = contnt.get(++i);

					srCt = (int) lVal1.get(1);
					erCt = (int) lVal2.get(1);

					slCt = (int) lVal1.get(2);
					elCt = (int) lVal2.get(2);

					val1 = lVal1.get(0).toString();
					val2 = lVal2.get(0).toString();

					if (val1.equals(val2)) {
						List<List<String>> lval = new ArrayList<List<String>>();
						for (int j = srCt; j < erCt - 1; j++) {
							contnt1 = new ArrayList<String>();
							String[] header = csvData.get(srCt - 1);
							String[] st1 = csvData.get(j);
							for (int k = slCt + 1; k < elCt; k++) {
								contnt1.add(header[k] + "^^" + st1[k]);
							}
							lval.add(contnt1);
						}
						testDataAtcre.put(val1, lval);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			/*
			 * if (testDataAtcre.size() != 0) {
			 * System.out.println("The size of map:" + mp.size());
			 * Iterator<String> it2 = mp.keySet().iterator(); while
			 * (it2.hasNext()) {
			 * 
			 * List<List<String>> ltV = mp.get(it2.next()); for (int l = 0; l <
			 * ltV.size(); l++) { System.out.println(ltV.get(l)); } } }
			 */

			reader.close();

		} catch (FileNotFoundException e) {
			logger.info("Could not fetch Test data from test data file**********");
			logger.error(e);
			result = false;
		} catch (IOException e) {
			logger.info("Could not fetch Test data  from test data file**********");
			logger.error(e);
			result = false;
		}
		return testDataAtcre.size();
	}

	/**
	 * The method used to call to store the succes test data in list It accepts
	 * Two parameter
	 * 
	 * @param objectName
	 * @param string
	 *            It returns void
	 */
	public void successTestDataNumber(String objectName, String string) {
		try {
			Driver.flagRepeat = true;
			String pageSrc, response;
			pageSrc = driver.getPageSource();
			response = performActionGetText(objectName);

			if (response != null) {
				if (response.toLowerCase().contains(string.toLowerCase())) {
					logger.info("The given data presnet in the page as data can be "
							+ "used for next testcase");
					Driver.isExecute = true;

					if (successCount.size() == 0)
						successCount.add(Driver.rep);

					if (!successCount.contains(Driver.rep))
						successCount.add(Driver.rep);
				}
			} else if (pageSrc.toLowerCase().contains(string.toLowerCase())) {
				logger.info("The given data presnet in the page as data can be "
						+ "used for next testcase");
				Driver.isExecute = true;

				if (successCount.size() == 0)
					successCount.add(Driver.rep);

				if (!successCount.contains(Driver.rep))
					successCount.add(Driver.rep);
			} else {
				logger.info("The given data is not present in the page so test cant be"
						+ "used for next testcase");
				// DriverAtcore.isExecute=false;
				Driver.isExecute = false;
			}
		} catch (Exception e) {
			logger.error("The given data is not present in the page so test cant be"
					+ "used for next testcase");
			// DriverAtcore.isExecute=false;
			Driver.isExecute = false;
			logger.error("Could not proceed the execution for next test case**********");
			logger.error(e);
		}
	}

	/**
	 * The following method used to read the CSV data The values of CSV data
	 * will be stored in map It accepts the CVSV filepath as parameter It
	 * returns the no of rows as test data
	 * 
	 * @param filPath
	 * @return
	 * @throws IOException
	 */
	public int getTestDataAtCoreNew(String filPath) throws IOException {
		int totRows = 0, i, ctr = 1;
		String[] headData;
		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(filPath));
			List<String[]> lt = reader.readAll();
			List<String[]> contnt;
			if (lt.size() != 0) {
				String[] header = lt.get(0);
				for (i = 1; i < lt.size(); i++) {
					String[] st = lt.get(i);
					contnt = new ArrayList<String[]>();
					if (st.length >= header.length) {
						for (int p = 0; p < header.length; p++) {
							headData = new String[2];
							headData[0] = header[p];
							headData[1] = st[p];
							contnt.add(headData);
						}
						testDataAtcore.put(ctr, contnt);
						ctr++;
					}
				}
				// totRows = i-1;
				totRows = ctr - 1;
				reader.close();
			} else {
				logger.info("The csv file doesn't have the test data");
			}

		} catch (FileNotFoundException e) {
			logger.error("Could not fetch Test data from test data file**********"
					+ e.getMessage());
			// logger.info("Could not fetch Test data from test data
			// file**********");
			// logger.error(e);
		} catch (IOException e) {
			logger.error("Could not fetch Test data from test data file**********"
					+ e.getMessage());
		} catch (Exception e) {
			logger.error("Could not fetch Test data from test data file**********"
					+ e.getMessage());
		} finally {
			if (reader != null) {
				reader.close();
			}
		}
		return totRows;
	}

	/**
	 * Method to enter the text and then move to specified object, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type Note: Object type of first and
	 * second should be same
	 * 
	 * @author Anant Patil
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */

	public void performActionEnterTextAndMoveToElement(String objecttoMove,
			String ctry) throws InterruptedException {

		result = true;
		try {
			String[] arr = objecttoMove.split(",");

			objectDetails = getObjectDetails(arr[0].trim());
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				Actions action = new Actions(driver);
				performActionEnterText(arr[0].trim(), ctry);

				Thread.sleep(1000L);
				objectDetails2 = getObjectDetails(arr[1].trim());
				waitForElementToBeDisplayed(arr[1].trim());
				WebElement element = driver.findElement(By.xpath(objectDetails2
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				action.moveToElement(element).click().build().perform();
				report.log(LogStatus.INFO, objecttoMove + " is clicked");
				logger.info(objecttoMove + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
				performActionEnterText(arr[0].trim(), ctry);
				Thread.sleep(1000L);
				objectDetails2 = getObjectDetails(arr[1]);
				WebElement element = driver.findElement(By.id(objectDetails2
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Actions action = new Actions(driver);
				action.moveToElement(element).click().build().perform();
				report.log(LogStatus.INFO, objecttoMove + " is clicked");
				logger.info(objecttoMove + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				performActionEnterText(arr[0].trim(), ctry);
				Thread.sleep(1000L);
				objectDetails2 = getObjectDetails(arr[1]);
				WebElement element = driver.findElement(By
						.cssSelector(objectDetails2
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				Actions action = new Actions(driver);
				action.moveToElement(element).click().build().perform();
				report.log(LogStatus.INFO, objecttoMove + " is clicked");
				logger.info(objecttoMove + " is clicked");
			}
		} catch (Exception e) {
			logger.info(objecttoMove + " is not clicked **********");
			report.log(LogStatus.ERROR, objecttoMove + " is not clicked");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}
	
	/**
	 * Methods to compute child date of birth based on the number of child and
	 * their age, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @author Anant patil
	 * @param Object
	 *            name as mentioned in the object repository. Note: xpath should
	 *            be ending with the term select and object type should be
	 *            Select in OR
	 * @param Test
	 *            data value to be send date of birth
	 * @return void
	 * @throws ParseException
	 */
	public void performActionComputeDateofBirth1(String objectName) {
		PerformActionSleep("1000");
		objectDetails = getObjectDetails(objectName);
		try {
			String str = driver.findElement(By.xpath("//div[@class='fl cb paxSplit']|//div[@class='leftAlign']//ul/li[2]")).getText().trim(), currDat, dob;
			SimpleDateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			Date d = new Date();
			currDat = fmt.format(d);
			if (str.toLowerCase().contains("child")) {
				String agVal = str.substring(str.indexOf("(") + 1, str.lastIndexOf(")"));
				agVal = agVal.replace("yrs", "").replace(" ", "").replace("yr", "");
				String[] ages = agVal.split(",");
				for (int i = 0; i < ages.length; i++) {
					Integer curYear = Integer.parseInt(currDat.substring(currDat.lastIndexOf("/") + 1));
					Integer chdYr = curYear - Integer.parseInt(ages[i]);
					dob = currDat.replace(curYear.toString(), chdYr.toString());
					// System.out.println(currDat.replace(curYear.toString(),
					// chdYr.toString()));

					try {
						if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT)) {
							if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
									.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
								List<WebElement> ele = driver
										.findElements(By.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(i);
								listdob.sendKeys(dob);
							} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
									.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
								List<WebElement> ele = driver
										.findElements(By.name(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(i);
								listdob.sendKeys(dob);
							} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
									.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
								List<WebElement> ele = driver
										.findElements(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(i);
								listdob.sendKeys(dob);
							} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
									.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
								List<WebElement> ele = driver.findElements(
										By.cssSelector(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(i);
								listdob.sendKeys(dob);
							}
						}
					} catch (Exception e) {
						logger.info(objectName + " text is not entered **********");
						logger.error(e);
						result = false;
					}
				}
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Method to select aiports
	 * 
	 * @author
	 * 
	 */

	/*
	 * public void performActionRandomAirport(String objectName) { objectDetails
	 * = getObjectDetails(objectName); List<WebElement> ele; Random rand=new
	 * Random(); try { waitForElementToBeDisplayed(objectName);
	 * if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .contains(CONSTANTS_OBJECT_LOCATOR_ID)) {
	 * 
	 * ele=driver.findElements(By.id(objectDetails.
	 * get(CONSTANTS_OBJECT_LOCATOR_VALUE))); int r=rand.nextInt(ele.size());
	 * if(r==0){ r=1; } ele.get(r).click(); System.out.println(objectName +
	 * "-----is clicked"); report.log(LogStatus.INFO, objectName +
	 * " is clicked "); logger.info(objectName + " is clicked"); } else
	 * if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .contains(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
	 * ele=driver.findElements(By.xpath(objectDetails.
	 * get(CONSTANTS_OBJECT_LOCATOR_VALUE))); int r=rand.nextInt(ele.size());
	 * if(r==0){ r=1; } System.out.println("the randomValue:"+r);
	 * ele.get(r).click(); System.out.println(objectName + "-----is clicked");
	 * report.log(LogStatus.INFO, objectName + " is clicked ");
	 * logger.info(objectName + " is clicked"); } else
	 * if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .contains(CONSTANTS_OBJECT_LOCATOR_CSS)) {
	 * ele=driver.findElements(By.cssSelector(objectDetails.
	 * get(CONSTANTS_OBJECT_LOCATOR_VALUE))); int r=rand.nextInt(ele.size());
	 * if(r==0){ r=1; } ele.get(r).click(); System.out.println(objectName +
	 * "-----is clicked"); report.log(LogStatus.INFO, objectName +
	 * " is clicked "); logger.info(objectName + " is clicked"); } else
	 * if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .contains(CONSTANTS_OBJECT_LOCATOR_NAME)) {
	 * ele=driver.findElements(By.name(objectDetails.
	 * get(CONSTANTS_OBJECT_LOCATOR_VALUE))); int r=rand.nextInt(ele.size());
	 * if(r==0){ r=1; } ele.get(r).click(); System.out.println(objectName +
	 * "-----is clicked"); report.log(LogStatus.INFO, objectName +
	 * " is clicked "); logger.info(objectName + " is clicked"); } }
	 * catch(Exception e) { logger.info(objectName + " could not click");
	 * report.attachScreenshot(takeScreenShotExtentReports()); logger.error(e);
	 * } }
	 */

	/**
	 * Method to select random Month element
	 * 
	 * @return void
	 */
	/*
	 * public void performActionRandomClickMonth(String objectName) {
	 * objectDetails = getObjectDetails(objectName); List<WebElement> wd; Random
	 * rd = new Random(); try {
	 * 
	 * if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
	 * CONSTANTS_OBJECT_LOCATOR_ID)) {
	 * 
	 * wd = driver.findElements(By.id(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE))); rd = new Random(); int y =
	 * rd.nextInt(wd.size()-7); if(y==0){ y=1; } wd.get(y).click();
	 * report.log(LogStatus.INFO, objectName + " is clicked! ");
	 * logger.info(objectName + " " + " is selcted");
	 * 
	 * }
	 * 
	 * else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
	 * 
	 * wd = driver.findElements(By.name(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
	 * 
	 * rd = new Random(); int y = rd.nextInt(wd.size()-7); if(y==0){ y=1; }
	 * wd.get(y).click(); report.log(LogStatus.INFO, objectName +
	 * " is clicked! "); logger.info(objectName + " " + " is selcted");
	 * 
	 * }
	 * 
	 * else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) { //Thread.sleep(500);
	 * wd = driver.findElements(By.xpath(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
	 * 
	 * int y = rd.nextInt(wd.size()-7); if(y==0){ y=1; } wd.get(y).click();
	 * report.log(LogStatus.INFO, objectName + " is clicked! ");
	 * logger.info(objectName + " " + " is selcted");
	 * 
	 * }
	 * 
	 * else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
	 * waitForElementToBeDisplayed(objectName); wd =
	 * driver.findElements(By.cssSelector(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
	 * System.out.println("The month size:"+wd.size()); rd = new Random(); int y
	 * = rd.nextInt(wd.size()-7); if(y==0){ y=1; } wd.get(y).click();
	 * report.log(LogStatus.INFO, objectName + " is clicked! ");
	 * logger.info(objectName + " " + "is selcted");
	 * 
	 * }
	 * 
	 * } catch (Exception e) { System.out.println(objectName +
	 * " could not click"+"    "+e); logger.info(objectName +
	 * " could not click");
	 * report.attachScreenshot(takeScreenShotExtentReports()); logger.error(e);
	 * 
	 * } }
	 */

	/**
	 * Method to select random date element
	 * 
	 * @return void
	 */
	/*
	 * public void performActionRandomClickDate(String objectName) {
	 * objectDetails = getObjectDetails(objectName);
	 * 
	 * List<WebElement> wd; Random rd = new Random(); try {
	 * 
	 * if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
	 * CONSTANTS_OBJECT_LOCATOR_ID)) {
	 * 
	 * wd = driver.findElements(By.id(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE))); rd = new Random(); int y =
	 * rd.nextInt(wd.size()); wd.get(y).click(); report.log(LogStatus.INFO,
	 * objectName + " is clicked! "); logger.info(objectName + " " +
	 * " is selcted");
	 * 
	 * } else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
	 * 
	 * wd = driver.findElements(By.name(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
	 * 
	 * rd = new Random(); int y = rd.nextInt(wd.size()); wd.get(y).click();
	 * report.log(LogStatus.INFO, objectName + " is clicked! ");
	 * logger.info(objectName + " " + " is selcted");
	 * 
	 * } else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) { //Thread.sleep(500);
	 * wd = driver.findElements(By.xpath(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
	 * 
	 * int y = rd.nextInt(wd.size()); wd.get(y).click();
	 * report.log(LogStatus.INFO, objectName + " is clicked! ");
	 * logger.info(objectName + " " + " is selcted");
	 * 
	 * } else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
	 * .equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
	 * waitForElementToBeDisplayed(objectName); wd =
	 * driver.findElements(By.cssSelector(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE))); rd = new Random(); int y =
	 * rd.nextInt(wd.size()); wd.get(y).click(); report.log(LogStatus.INFO,
	 * objectName + " is clicked! "); logger.info(objectName + " " +
	 * "is selcted");
	 * 
	 * }
	 * 
	 * } catch (Exception e) { System.out.println(objectName +
	 * " could not click"+"    "+e); logger.info(objectName +
	 * " could not click");
	 * report.attachScreenshot(takeScreenShotExtentReports()); logger.error(e);
	 * 
	 * } }
	 */

	/**
	 * The method used to enter the ages It accepts the object name as parameter
	 * 
	 * @param objectName
	 * @param testData
	 */
	public void performActionEnterAges(String objectName, String testData)  {
		String[] objVal = objectName.split(",");
		List<WebElement> agVal;
		testData = fetchTestDataFromMap(testData);
		if (!testData.contains(",")) {
			performActionClick(objVal[0].trim());
			waitForElementToBeClickable(objVal[1]);
			objectDetails = getObjectDetails(objVal[1]);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				agVal = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement ags : agVal) {
					if (ags.getText().trim().contains(testData.trim())) {
						Actions act = new Actions(driver);
						act.moveToElement(ags).build().perform();
						ags.click();
						report.log(LogStatus.INFO, objVal[1]
								+ " text is set as " + testData);
						logger.info(objVal[1] + " text is set as " + testData);
						break;
					}
				}
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				agVal = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement ags : agVal) {
					if (ags.getText().trim().contains(testData.trim())) {
						Actions act = new Actions(driver);
						act.moveToElement(ags).build().perform();
						ags.click();
						report.log(LogStatus.INFO, objVal[1]
								+ " text is set as " + testData);
						logger.info(objVal[1] + " text is set as " + testData);
						break;
					}
				}
			}
		} else {
			String[] testVal = testData.split(",");
			try {
				Integer ct = 0;
				for (Integer j = 0; j < testVal.length; j++) {
					ct++;
					performActionClickBasedonIndex(objVal[0], ct.toString());
					waitForElementToBeDisplayed(objVal[1]);
					objectDetails = getObjectDetails(objVal[1]);
					if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
						agVal = driver.findElements(By.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						logger.info("the agVal and size:" + agVal.size());
						for (WebElement ele1 : agVal) {
							if (ele1.getText().trim()
									.contains(testVal[j].trim())) {
								Actions act = new Actions(driver);
								act.moveToElement(ele1).click().perform();
								report.log(LogStatus.INFO, objVal[1]
										+ " text is set as " + testData);
								logger.info(objVal[1] + " text is set as "
										+ testData);
								break;
							}
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

						agVal = driver.findElements(By
								.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						for (WebElement ele1 : agVal) {
							if (ele1.getText().trim()
									.contains(testVal[j].trim())) {
								Actions act = new Actions(driver);
								act.moveToElement(ele1).build().perform();
								ele1.click();
								report.log(LogStatus.INFO, objVal[1]
										+ " text is set as " + testData);
								logger.info(objVal[1] + " text is set as "
										+ testData);
								break;
							}
						}
					}
				}
			} catch (Exception e) {
				logger.info(objectName + " text is not entered **********");
				report.log(LogStatus.ERROR, objectName + " text is not set as "
						+ testData);
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.error(e);
				result = false;
			}
		}
	}

	/**
	 * Method to perform click operation for the specified object, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @author Anant patil
	 * @param Object
	 *            name as mentioned in the object repository
	 * @return void
	 */
	public void performActionClickBasedonIndex(String objectName, String index) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_IMAGE))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT) || (objectDetails
							.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)))) {
				waitForElementToBeClickable(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR_VALUE).replace(
									"1", index))).click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + " is clicked");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR_VALUE).replace(
									"1", index))).click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked ");
					logger.info(objectName + " is clicked");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					waitForElementToBeClickable(objectName);
					driver.findElement(
							By.xpath(objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR_VALUE).replace(
									"1", index))).click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + "is clicked");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					waitForElementToBeDisplayed(objectName);
					driver.findElement(
							By.cssSelector(objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR_VALUE).replace(
									"1", index))).click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + " is clicked");
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not clicked **********");
			report.log(LogStatus.ERROR, objectName + " is not clicked");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			// Assert.assertTrue(result, "Verifying Page Display");
		}
	}


	
	/**
	 * Method to perform click operation for the specified object, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @return void
	 * @throws
	 */
	public void performActionClick(String objectName) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_IMAGE))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT) || (objectDetails
							.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)))) {
				waitForElementToBeClickable(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + " is clicked");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked ");
					logger.info(objectName + " is clicked");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					waitForPageLoad(driver);
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + "is clicked");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + " is clicked");
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not clicked **********");
			report.log(LogStatus.ERROR, objectName + " is not clicked");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			if (null != objectExtraDetails.get(objectName)
					&& !objectExtraDetails.get(objectName).equals("")) {
				performActionClickonDynamicOR(objectName);
			}

		}
	}
	
	
	/*
	 * * Method to click the cross button for the feedback pop up opens in any
	 * page(method needs to be callen in driverresuable)
	 * 
	 * @date 18th January 2017
	 * 
	 * @author Omar
	 * 
	 * @return void
	 */
	public void closeFeedBackPopup() {
		try {
			if (driver instanceof JavascriptExecutor) {
				((JavascriptExecutor) driver)
						.executeScript("document.querySelector('.acsInviteButton acsDeclineButton').click();");
				logger.info("Feedback popup closed");
			}
		} catch (Exception e) {

			// e.printStackTrace();
		}
		try {
			if (driver instanceof JavascriptExecutor) {
				((JavascriptExecutor) driver)
						.executeScript("document.querySelector('.fsrDeclineButton').click();");
				logger.info("Feedback popup closed");
			}
		} catch (Exception e) {

			// e.printStackTrace();
		}

	}

	/**
	 * Method to perform send text operation for the specified object, method
	 * would determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @param Test
	 *            data to be sent as text
	 * @return void
	 */
	public void performActionEnterText(String objectName, String testData) {
		objectDetails = getObjectDetails(objectName);
		result = true;

		try {
			// System.out.println("The incremented value:" + Driver.rep);

			// testData = fetchDataFromMap(testData);
			if(null!=fetchTestDataFromMap(testData)){
			testData = fetchTestDataFromMap(testData);
			}
			
			// System.out.println("The object and test dat:"+objectName+"
			// "+testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				waitForElementToBeDisplayed(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {

					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();

					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.clear();
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {

			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/*
	 * public String fetchDataFromMap(String testData) { String retValue =
	 * testData; List<String> testDataVal = testData1.get(Driver.rep); for (int
	 * t = 0; t < testDataVal.size(); t++) { if
	 * (testDataVal.get(t).contains(testData)) { try { retValue =
	 * testDataVal.get(t).substring( testDataVal.get(t).indexOf("^^") + 2);
	 * break; } catch (ArrayIndexOutOfBoundsException a) { a.printStackTrace();
	 * } } } return retValue; }
	 */

	/**
	 * The method used to read the test data from Map Based on the key If the
	 * key present it will return th value from map else it will return the key
	 * as value
	 * 
	 * @param testData
	 * @returns string
	 */
	public String fetchTestDataFromMap(String testData) {
		String retValue = testData;
		try {
			if (testDataAtcore.size() != 0) {
				List<String[]> testDataVal = testDataAtcore.get(Driver.rep);
				for (int t = 0; t < testDataVal.size(); t++) {
					String[] tstData = testDataVal.get(t);
					if (tstData[0].toString().contains(testData)) {
						try {
							retValue = tstData[1].trim();
							break;
						} catch (ArrayIndexOutOfBoundsException a) {
							a.printStackTrace();
						}
					}
				}
			}
		} catch (NullPointerException n) {
			logger.error("Nullpointer exection");
		} catch (Exception e) {
		}

		return retValue;
	}

	/**
	 * Method to perform send text operation for the specified object, method
	 * would determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @param Test
	 *            data to be sent as text
	 * @return void
	 */
	public void performActionEnterTextStored(ArrayList<String> ay, String cc)  {

		performActionEnterText(cc, ay.get(0));

	}

	/**
	 * Method to perform select value from the drop down list for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository. Note: xpath should
	 *            be ending with the term select and object type should be
	 *            Select in OR
	 * @param Test
	 *            data value to be to be select
	 * @return void
	 */
	public void performActionSelectDropDown_Select(String objectName,
			String testData)  {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			testData = fetchTestDataFromMap(testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_SELECT)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					Select selectBox = new Select(driver.findElement(By
							.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByValue(testData);
					// selectBox.selectByVisibleText(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					Select selectBox = new Select(driver.findElement(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByValue(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					Select selectBox = new Select(driver.findElement(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					try{
						selectBox.selectByVisibleText(testData);
					}
					catch(Exception e){
					selectBox.selectByValue(testData);
					}
					
					// selectBox.selectByVisibleText(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByValue(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " Select dropdown is not selected **********" + testData);
			report.log(LogStatus.ERROR, objectName
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * The method is used to identify if a given component is enabled It
	 * receives 1 parameter It returns a boolean value
	 * 
	 * @author-Chaitra
	 * @param objectName
	 * @Updated By Anant
	 */
	public boolean performActionIsEnabled(String objectName) {
		objectDetails = getObjectDetails(objectName);
		String page = null;
		boolean flag = false;
		try {
			page = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			try {
				if ((objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
						CONSTANTS_OBJECT_TYPE_TEXT)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(
										CONSTANTS_OBJECT_TYPE_RADIOBUTTON) || objectDetails
						.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
								CONSTANTS_OBJECT_TYPE_CHECKBOX))) {
					waitForElementToBeDisplayed(objectName);
					if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
						if (driver.findElement(
								By.id(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isEnabled() == true) {
							logger.info(objectName + " component is enabled");
							report.log(LogStatus.PASS, objectName
									+ " component is enabled from " + page
									+ " page");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not enabled from " + page
									+ " page");
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
						if (driver.findElement(
								By.name(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isEnabled() == true) {
							report.log(LogStatus.PASS, objectName
									+ " component is enabled from " + page
									+ " page");
							logger.info(objectName + " component is enabled");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not enabled from " + page
									+ " page");
						}
					}

					else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
						if (driver.findElement(
								By.xpath(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isEnabled() == true) {
							report.log(LogStatus.PASS, objectName
									+ " component is enabled from " + page
									+ " page");
							logger.info(objectName + " component is enabled");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not enabled from " + page
									+ " page");
						}
					}

					else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
						if (driver.findElement(
								By.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isEnabled() == true) {
							report.log(LogStatus.PASS, objectName
									+ " component is enabled in from " + page
									+ " page");
							logger.info(objectName + " component is enabled");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not enabled from " + page
									+ " page");
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {
						driver.findElement(By.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						report.log(LogStatus.PASS, objectName
								+ " component is enabled from " + page
								+ " page");
						logger.info(objectName + " component is isEnabled");
					}
				}

			} catch (Exception e) {
				report.log(LogStatus.FAIL, objectName
						+ " component is not enabled from " + page + " page");
				logger.info(objectName + " component is not enabled **********");
				logger.error(e);

			}
		} catch (ArrayIndexOutOfBoundsException a) {
			report.log(LogStatus.INFO, objectName
					+ " doesnt contain the page value as execption occuring");
			logger.info(objectName + " component is not enabled **********");
			logger.error(a);
		}
		return flag;
	}

	/**
	 * Method to perform check for Radio button or CheckBox for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void isSelected(String objectName) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_RADIOBUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					if (driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						report.log(LogStatus.PASS, objectName + " is selected");
						logger.info(objectName + " is selected");
					} else {
						report.log(LogStatus.FAIL, objectName
								+ " is not selected");
						logger.info(objectName + " is not selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					if (driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						report.log(LogStatus.PASS, objectName + " is selected");
						logger.info(objectName + " is selected");
					} else {
						report.log(LogStatus.FAIL, objectName
								+ " is not selected");
						logger.info(objectName + " is not selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					if (driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						report.log(LogStatus.PASS, objectName + " is selected");
						logger.info(objectName + " is selected");
					} else {
						report.log(LogStatus.FAIL, objectName
								+ " is not selected");
						logger.info(objectName + " is not selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					if (driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						report.log(LogStatus.PASS, objectName + " is selected");
						logger.info(objectName + " is selected");
					} else {
						report.log(LogStatus.FAIL, objectName
								+ " is not selected");
						logger.info(objectName + " is not selected");
					}
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not selected **********");
			report.log(LogStatus.ERROR, objectName + " "
					+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is not selected");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to perform check for Radio button or CheckBox for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionSelectBox(String objectName) {
		objectDetails = getObjectDetails(objectName);
		// System.out.println(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_RADIOBUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					if (!driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.id(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					if (!driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.name(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					if (!driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.xpath(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected at first click");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					if (!driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
					}
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not selected **********");
			report.log(LogStatus.ERROR, objectName + " "
					+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is not selected");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * The method use to select the given test data from application It accepts
	 * tow parameters
	 * 
	 * @param objectName
	 * @param testData
	 *            Returns void
	 */
	public void performActionSelectGivenText(String objectName, String testData) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		List<WebElement> ele;
		try {
			testData = fetchTestDataFromMap(testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_DROPDOWN)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_SELECT)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElements(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Thread.sleep(1000L);
							Actions act = new Actions(driver);
							act.moveToElement(ele1).build().perform();
							ele1.click();
							break;
						}
					}
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElements(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Actions act = new Actions(driver);
							act.moveToElement(ele1).build().perform();
							ele1.click();
							Thread.sleep(1000L);
							break;
						}
					}
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					ele = driver.findElements(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Thread.sleep(1000L);
							Actions act = new Actions(driver);
							act.moveToElement(ele1).build().perform();
							ele1.click();
							break;
						}
					}
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElements(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Thread.sleep(1000L);
							Actions act = new Actions(driver);
							act.moveToElement(ele1).build().perform();
							ele1.click();
							break;
						}
					}
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to get text for the specified object, method would determine the
	 * object locator type from OR and perform the action accordingly as per the
	 * object locator type
	 * 
	 * @date April 2015
	 * @author Himaperf
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return String
	 */
	public String performActionGetText(String objectName) {
		String objectValue = null;
		objectDetails = getObjectDetails(objectName);

		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT))) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					objectValue = driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					objectValue = driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					objectValue = driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					objectValue = driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {

					objectValue = regexExtractor(
							objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE),
							driver.getPageSource());
					if (objectValue.equals("")) {
						objectValue = null;
					}

					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " could not get the text **********");
			report.log(LogStatus.INFO, objectName
					+ " could not get the text **********");
			logger.error(e);
		}
		return objectValue;
	}

	/**
	 * Method to get the multiple text for the specified object, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date oct 2015
	 * @author Anant
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return String
	 */

	public List<String> performActionMultipleGetText(String objectName)  {
		// String objectValue = null;
		objectDetails = getObjectDetails(objectName);
		List<String> multVal = new ArrayList<String>();
		List<WebElement> ele = new ArrayList<WebElement>();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElements(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElements(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					ele = driver.findElements(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElements(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " text is captured");
				}

			}
			for (WebElement ele1 : ele) {
				multVal.add(ele1.getText());
			}
		} catch (Exception e) {
			logger.info(objectName + " could not get the text **********");
			logger.error(e);
		}
		return multVal;
	}

	/**
	 * Method to get the web elements for the specified object, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return String
	 */
	public WebElement getElement(String objectName)  {
		objectDetails = getObjectDetails(objectName);
		WebElement ele = null;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElement(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " web elemenent is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElement(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + "  web elemenent is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					ele = driver.findElement(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + "  web elemenent is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElement(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + "  web elemenent is captured");
				}
			}

		} catch (Exception e) {
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info(objectName
					+ "  Couldn't get the web elemenent **********");
			logger.error(e);
		}
		return ele;
	}

	/**
	 * Method to get the multiple webelements for the specified object, method
	 * would determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return String
	 */
	public List<WebElement> getMultipleElement(String objectName) {
		objectDetails = getObjectDetails(objectName);
		List<WebElement> ele = new ArrayList<WebElement>();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElements(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " web elemenents is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElements(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + "  web elemenents is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					ele = driver.findElements(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + "  web elemenents is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElements(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

					logger.info(objectName + "  web elemenents is captured");
				}
			}

		} catch (Exception e) {
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info(objectName
					+ "  Couldn't get the web elemenents **********");
			logger.error(e);
		}
		return ele;
	}

	/**
	 * Method to verify the specified object in the page, method would determine
	 * the object locator type from OR and perform the action accordingly as per
	 * the object locator type
	 * 
	 * @author Anant patil
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public boolean performActionComponentPresent(String objectName) {
		// String objectValue = null;
		objectDetails = getObjectDetails(objectName);
		String page = null;
		boolean flag = false;
		try {
			page = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			try {

				if ((objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
						CONSTANTS_OBJECT_TYPE_TEXT)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(
										CONSTANTS_OBJECT_TYPE_DROPDOWN)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_SELECT) || objectDetails
						.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
								CONSTANTS_OBJECT_TYPE_CHECKBOX))) {
					waitForElementToBeDisplayed(objectName);
					if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
						if (driver.findElement(
								By.id(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							logger.info(objectName + " component is displayed");
							report.log(LogStatus.PASS, objectName
									+ " component is dispalyed from " + page
									+ " page");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not dispalyed from "
									+ page + " page");
							report.attachScreenshot(takeScreenShotExtentReports());
						}
					}

					else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
						if (driver.findElement(
								By.name(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.PASS, objectName
									+ " component is dispalyed from " + page
									+ " page");
							logger.info(objectName + " component is dispalyed");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not dispalyed from "
									+ page + " page");
							report.attachScreenshot(takeScreenShotExtentReports());
						}
					}

					else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
						if (driver.findElement(
								By.xpath(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.PASS, objectName
									+ " component is dispalyed from " + page
									+ " page");
							logger.info(objectName + " component is dispalyed");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not dispalyed from "
									+ page + " page");
							report.attachScreenshot(takeScreenShotExtentReports());
						}
					}

					else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
						if (driver.findElement(
								By.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.PASS, objectName
									+ " component is dispalyed in from " + page
									+ " page");
							logger.info(objectName + " component is dispalyed");
							flag = true;
						} else {
							report.log(LogStatus.FAIL, objectName
									+ " component is not dispalyed from "
									+ page + " page");
							report.attachScreenshot(takeScreenShotExtentReports());
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {
						// driver.findElement(By.cssSelector(objectDetails
						// .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						regexExtractor(
								objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE),
								driver.getPageSource());
						report.log(LogStatus.PASS, objectName
								+ " component is dispalyed from " + page
								+ " page");
						logger.info(objectName + " component is dispalyed");
					}
				}

			} catch (Exception e) {
				report.log(LogStatus.FAIL, objectName
						+ " component is not dispalyed from " + page + " page");
				logger.info(objectName
						+ " component is not dispalyed **********");
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.error(e);

			}
		} catch (ArrayIndexOutOfBoundsException a) {
			report.log(LogStatus.INFO, objectName
					+ " doesnt contain the page value as execption occuring");
			logger.info(objectName + " component is not dispalyed **********");
			logger.error(a);
		}
		return flag;
	}

	/**
	 * Method to verify the specified object clciked in the page, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @author Anant patil
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionVerifyElementClickable(String objectName,
			String page){
		// String objectValue = null;
		objectDetails = getObjectDetails(objectName);
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT))) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					logger.info(objectName + " text is captured");
					report.log(LogStatus.PASS, objectName
							+ " component is clicked from " + page + " page");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					report.log(LogStatus.PASS, objectName
							+ " component is clicked from " + page + " page");
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					driver.findElement(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					report.log(LogStatus.PASS, objectName
							+ " component is clicked from " + page + " page");
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					report.log(LogStatus.PASS, objectName
							+ " component is clicked from " + page + " page");
					logger.info(objectName + " text is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {
					driver.findElement(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					report.log(LogStatus.PASS, objectName
							+ " component is clicked from " + page + " page");
					logger.info(objectName + " text is captured");
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.FAIL, objectName
					+ " component is not clicked from " + page + " page");
			logger.info(objectName + " could not get the text **********");
			logger.error(e);
		}
		// return objectValue;
	}

	/**
	 * Method to get the html attribute value for the specified object, method
	 * would determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Anant Patil
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return String
	 */
	public String performActionGetAttributeValue(String objectName,
			String attrName) {

		String objectValue = null;
		objectDetails = getObjectDetails(objectName);
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON) || objectDetails
					.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
							CONSTANTS_OBJECT_TYPE_LINK))) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					objectValue = driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getAttribute(attrName);
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					objectValue = driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getAttribute(attrName);
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					objectValue = driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getAttribute(attrName);
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					objectValue = driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getAttribute(attrName);
					logger.info(objectName + " text is captured");
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {
					objectValue = driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getAttribute(attrName);
					logger.info(objectName + " text is captured");
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " could not get the text **********");
			logger.error(e);
		}
		return objectValue;
	}

	/**
	 * Method to wait for the page to load completely
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return void
	 */
	public void waitForPageLoad(WebDriver driver) {
		result = true;
		try {
			ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					return ((JavascriptExecutor) driver).executeScript(
							"return document.readyState").equals("complete");
				}
			};
			try {
				WebDriverWait wait = new WebDriverWait(driver, 40);
				wait.until(pageLoadCondition);
			} catch (Exception e) {
				logger.info("Could not wait until page load condition");
				/*
				 * report .log(LogStatus.WARNING,
				 * "Driver issue in waiting for page load condition due to Javascript error"
				 * );
				 */
				logger.error(e);
			}
		} catch (Exception e) {
			logger.info("Could not wait for page load ********");
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to verify whether the search result page is displayed successfully
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return true if page displayed successfully else false
	 */
	public boolean verify_Falcon_SearchResultsPageIsDisplayed() {
		boolean isSearchResultsPageDisplayed = false;
		try {
			Thread.sleep(3000);
			String holidayCount = performActionGetText("FALCON_HOLIDAY_COUNT");
			if (holidayCount != null) {
				isSearchResultsPageDisplayed = true;
			}
		} catch (Exception e) {
			logger.info("Could not verify the search results page display");
			logger.error(e);
		} finally {
			try {
				if (isSearchResultsPageDisplayed == false) {
					logger.error("Expected Page is not displayed successfully");
					report.log(LogStatus.FAIL,
							"Page has not launched successfully!");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				Assert.assertTrue(isSearchResultsPageDisplayed,
						"Verify Search Page Display");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
		return isSearchResultsPageDisplayed;
	}

	/**
	 * Method to verify whether the popup displayed and switches to alert
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return true if popup displayed and switvched to the alert else false
	 */
	public boolean verifyIsPopUpDisplayed() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException Ex) {
			logger.info("Could not verify popup display");
			logger.error(Ex);
			return false;
		}
	}

	/**
	 * It is used to click the alert It returns the void
	 */
	public void alertClick() {
		try {
			driver.switchTo().alert().accept();
			logger.info("======Alert is accepted======");

		} catch (NoAlertPresentException Ex) {
			logger.error("======Alert is not accepted======");
			logger.info("Could not verify popup display");
			logger.error(Ex);

		}
	}

	/**
	 * Method to perform select value from the drop down list for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository. Note: object type
	 *            should be DropDown in OR
	 * @param Test
	 *            data value to be to be select
	 * @return void
	 */
	public void performActionSelectDropDown(String objectName, String testData)
			throws InterruptedException {

		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			testData = fetchTestDataFromMap(testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_DROPDOWN)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					List<WebElement> listBox = driver.findElements(By
							.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement listValue : listBox) {
						if (listValue.getText().contains(testData)) {
							Actions action = new Actions(driver);
							Thread.sleep(1000);
							action.moveToElement(listValue).click().build()
									.perform();
							break;

						}
					}
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					List<WebElement> listBox = driver.findElements(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement listValue : listBox) {
						if (listValue.getText().contains(testData)) {
							Actions action = new Actions(driver);
							Thread.sleep(1000);
							action.moveToElement(listValue).click().build()
									.perform();
							break;
						}
					}
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as  "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					List<WebElement> listBox = driver.findElements(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement listValue : listBox) {
						if (listValue.getText().contains(testData)) {
							Actions action = new Actions(driver);
							Thread.sleep(1000);
							action.moveToElement(listValue).click().build()
									.perform();

							report.log(LogStatus.INFO, objectName
									+ " value is selected as" + testData);
							logger.info(objectName + " value is selected as "
									+ testData);
							break;
						}
					}

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					List<WebElement> listBox = driver.findElements(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement listValue : listBox) {
						if (listValue.getText().contains(testData)) {
							Actions action = new Actions(driver);
							Thread.sleep(1000);
							action.moveToElement(listValue).click().build()
									.perform();
							break;
						}
					}
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " could not select value from the drop down **********");
			report.log(LogStatus.ERROR, objectName
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to select first value from the drop down list for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository. Note: object type
	 *            should be DropDown in OR
	 * @return void
	 */
	public void performActionSelectFirstOption(String objectName)
			throws InterruptedException {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_DROPDOWN)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					WebElement element = driver.findElement(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

					Actions action = new Actions(driver);
					action.click(element).build().perform();
					Thread.sleep(1000);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					WebElement element = driver.findElement(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					Actions action = new Actions(driver);
					action.click(element).build().perform();
					Thread.sleep(1000);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					WebElement element = driver.findElement(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					Actions action = new Actions(driver);
					action.moveToElement(element).click().build().perform();
					Thread.sleep(1000);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					WebElement element = driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					Actions action = new Actions(driver);
					action.click(element).build().perform();
					Thread.sleep(1000);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " could not select the first option **********");
			report.log(LogStatus.ERROR, objectName
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to get Booking reference id
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return Booking reference id
	 */
	/*
	 * public String getBookingReferenceNumber() { String referenceNumber =
	 * null; String Adults = null; String Duration = null; String TotalPrice =
	 * null; result = true; try { if (verify.verifyBookingSuccess() == true) {
	 * referenceNumber =
	 * performActionGetText("FO_ACCOM_BOOKING_REFERENCE_NUMBER"); Adults =
	 * performActionGetText("FO_ACCOM_BOOKING_ADULTS") .replaceAll("[^0-9]",
	 * ""); Duration = performActionGetText("FO_ACCOM_BOOKING_DURATION")
	 * .replaceAll("[^0-9]", ""); TotalPrice =
	 * performActionGetText("FO_ACCOM_BOOKING_PRICE") .replaceAll("[^0-9.]",
	 * ""); setBookingReference(referenceNumber, Adults, Duration, TotalPrice);
	 * } logger.info("Booking Reference ID is: " + referenceNumber);
	 * report.log(LogStatus.INFO, "Booking reference ID is: " +
	 * referenceNumber); } catch (Exception e) { logger
	 * .info("Could not get the Booking reference number **********");
	 * report.log(LogStatus.FAIL,
	 * "Could not get the Booking reference number **********");
	 * report.attachScreenshot(takeScreenShotExtentReports()); logger.error(e);
	 * result = false; } return referenceNumber; }
	 */

	/**
	 * Method to verify FlighOption Page Displayed Successfully
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return true if Page displayed Successfully else false
	 */
	public boolean verify_FO_FlightOptionsPageDisplayed() {
		boolean isPageDisplayed = false;
		try {
			waitForPageLoad(driver);
			String response = driver.getPageSource();
			if (response.contains("CHOOSE YOUR FLIGHT OPTIONS")) {
				isPageDisplayed = true;
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		} finally {
			try {
				if (isPageDisplayed == false) {
					logger.error("Expected Page is not displayed successfully");
				}
				Assert.assertTrue(isPageDisplayed,
						"Verify FlighOption Page Display");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
		return isPageDisplayed;
	}

	/**
	 * Method to verify Extras Page Displayed Successfully
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return true if Page displayed Successfully else false
	 */
	public boolean verify_FO_ExtrasPageDisplayed() {
		boolean isPageDisplayed = false;
		try {
			waitForPageLoad(driver);
			String response = driver.getPageSource();
			if (response.contains("CHOOSE YOUR EXTRAS")) {
				isPageDisplayed = true;
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		} finally {
			try {
				if (isPageDisplayed == false) {
					logger.error("Expected Page is not displayed successfully");
				}
				Assert.assertTrue(isPageDisplayed, "Verify Extras Page Display");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
		return isPageDisplayed;
	}

	/**
	 * Method to verify Book Page Displayed Successfully
	 * 
	 * @date April 2015
	 * @author Hima
	 * @return true if Page displayed Successfully else false
	 */
	public boolean verify_FO_BookingPageDisplayed() {
		boolean isPageDisplayed = false;
		try {
			waitForPageLoad(driver);
			String response = driver.getPageSource();
			if (response.contains("Book Your Flight")) {
				isPageDisplayed = true;
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		} finally {
			try {
				Assert.assertTrue(isPageDisplayed, "Verify Book Page Display");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
		return isPageDisplayed;
	}

	/**
	 * Method to wait for an element until it is displayed
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param ObjectName
	 * @return void
	 */
	public void waitForElementToBeDisplayed(String objectName) {
		objectDetails = getObjectDetails(objectName);
		try {

			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	/**
	 * Method to read TestData
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param String
	 *            TestDatapath
	 * @return Properties
	 * @throws IOException
	 */
	public Properties getTestDataToParse(String testDataPath)
			throws IOException {
		Properties prop = new Properties();
		result = true;
		try {
			String fileName = getProjectPath() + testDataPath;
			InputStream is = new FileInputStream(fileName);
			prop.load(is);
		} catch (FileNotFoundException e) {
			logger.error(e);
			result = false;
		} catch (IOException e) {
			logger.error(e);
			result = false;
		}
		return prop;
	}

	/**
	 * Method to wait for an element until it is clickable
	 * 
	 * @date April 2015
	 * @author Hima
	 * @param ObjectName
	 * @return void
	 */
	public void waitForElementToBeClickable(String objectName) {
		objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.name(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				wait.until(ExpectedConditions.elementToBeClickable(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
			}
		} catch (Exception e) {
			logger.error("Error occured in waitForElementToBeClickable method:"
					+ e);
		}
	}

	/**
	 * Method to validate the object value and the test data value which we
	 * parsed
	 * 
	 * @date dec 2016
	 * @author Hima
	 * @updated by Anant
	 * @param ObjectName
	 *            , Testdata value
	 * @return boolean
	 */
	public boolean performActionTextValidation(String objectName,
			String testDataValue)  {
		String compName1, pageValue1, actualValue;
		actualValue = performActionGetText(objectName);
		boolean cmpresult = false;
		/*
		 * if (actualValue.equalsIgnoreCase(testDataValue)) {
		 * logger.info("Object value and Test data value are as expected");
		 * return true; } else { logger. error(
		 * "Object value and Test data value are not expected as Actual Value: "
		 * + actualValue + " and expected is :" + testDataValue); return false;
		 * }
		 */
		try {
			pageValue1 = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			compName1 = objectName.substring(objectName.lastIndexOf("_") + 1);
			if (actualValue != null) {
				actualValue = actualValue.trim().toLowerCase()
						.replaceAll("\\n", "").replaceAll("\\r", "");
			} else {
				actualValue = "null";
			}
			testDataValue = testDataValue.trim().toLowerCase();
			if (actualValue.replace(" ", "").contains(
					testDataValue.replace(" ", ""))
					|| testDataValue.replace(" ", "").contains(
							actualValue.replace(" ", ""))) {
				cmpresult = true;
				report.log(LogStatus.PASS, compName1
						+ " Verifying condition is passed from " + pageValue1
						+ " page where Expected:" + testDataValue + " "
						+ "Actual:" + actualValue);
				logger.info(compName1 + " Verifying condition is passed from "
						+ pageValue1 + " page where Expected:" + testDataValue
						+ " " + "Actual:" + actualValue + " Result-"
						+ cmpresult);
			} else {
				cmpresult = false;
				report.log(LogStatus.FAIL, compName1
						+ " Verifying condition is failed from " + pageValue1
						+ " page where Expected:" + testDataValue + " "
						+ "Actual:" + actualValue);
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.info(compName1 + " Verifying condition is failed from "
						+ pageValue1 + " page where Expected:" + testDataValue
						+ " " + "Actual:" + actualValue + " Result-"
						+ cmpresult);
			}
		} catch (Exception e) {
			logger.error(objectName
					+ " Object name doesn't have the proper syntax in performActionTextValidation method");
			report.log(
					LogStatus.ERROR,
					objectName
							+ " Object name doesn't have the proper syntax in performActionTextValidation method");
		}
		return cmpresult;
	}

	/**
	 * Method to validate the object value and the test data value which we
	 * parsed
	 * 
	 * @date dec 2016
	 * @author by Anant
	 * @param ObjectName
	 *            , Testdata value
	 * @return boolean
	 */
	public boolean performActionAttrValVerification(String objectName,
			String attrName, String testDataValue)  {
		String compName1, pageValue1, actualValue;
		actualValue = performActionGetAttributeValue(objectName, attrName);
		boolean cmpresult = false;
		try {
			pageValue1 = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			compName1 = objectName.substring(objectName.lastIndexOf("_") + 1);
			if (actualValue != null) {
				actualValue = actualValue.trim().toLowerCase()
						.replaceAll("\\n", "").replaceAll("\\r", "");
			} else {
				actualValue = "null";
			}
			testDataValue = testDataValue.trim().toLowerCase();
			if (actualValue.replace(" ", "").contains(
					testDataValue.replace(" ", ""))
					|| testDataValue.replace(" ", "").contains(
							actualValue.replace(" ", ""))) {
				cmpresult = true;
				report.log(LogStatus.PASS, compName1
						+ " Verifying condition is passed from " + pageValue1
						+ " page where Expected:" + testDataValue + " "
						+ "Actual:" + actualValue);
				logger.info(compName1 + " Verifying condition is passed from "
						+ pageValue1 + " page where Expected:" + testDataValue
						+ " " + "Actual:" + actualValue + " Result-"
						+ cmpresult);
			} else {
				cmpresult = false;
				report.log(LogStatus.FAIL, compName1
						+ " Verifying condition is failed from " + pageValue1
						+ " page where Expected:" + testDataValue + " "
						+ "Actual:" + actualValue);
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.info(compName1 + " Verifying condition is failed from "
						+ pageValue1 + " page where Expected:" + testDataValue
						+ " " + "Actual:" + actualValue + " Result-"
						+ cmpresult);
			}
		} catch (Exception e) {
			logger.error(objectName
					+ " Object name doesn't have the proper syntax in performActionTextValidation method");
			report.log(
					LogStatus.ERROR,
					objectName
							+ " Object name doesn't have the proper syntax in performActionTextValidation method");
		}
		return cmpresult;
	}

	public static String createScreenshot(WebDriver driver) {
		UUID uuid = UUID.randomUUID();

		// generate screenshot as a file object
		File scrFile = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		try {
			// copy file object to designated location
			FileUtils.copyFile(scrFile, new File(
					CONSTANTS_EXTENTREPORT_LOCATION
							+ CONSTANT_EXTENTREPORT_IMAGES + uuid + ".png"));
		} catch (IOException e) {
		}
		return CONSTANT_EXTENTREPORT_IMAGES + uuid + ".png";
	}

	/**
	 * Method to Take Screen Shot along with time stamp for Extent Reports
	 * 
	 * @date April 2015
	 * @author Hima
	 * @Param String name
	 * @return void
	 */
	public String takeScreenShotExtentReports() {
		Date d = new Date();
		Timestamp t = new Timestamp(d.getTime());
		String timeStamp = t.toString();
		timeStamp = timeStamp.replace(' ', '_');
		timeStamp = timeStamp.replace(':', '_');
		try {
			File myImg = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			// FileUtils.copyFile(myImg, new File(getReportLocation()
			// + getReportImageLocation() + timeStamp + ".png"));
			FileUtils.copyFile(myImg, new File(fldPath
					+ getReportImageLocation() + timeStamp + ".png"));
		} catch (IOException e) {
			logger.error(e);
		}
		/*
		 * return getReportLocation() + getReportImageLocation() + timeStamp +
		 * ".png";
		 */
		return "./" + getReportImageLocation() + timeStamp + ".png";
	}

	/**
	 * Method to set reporting details
	 * 
	 * @date May 2015
	 * @author Hima
	 * @Param String reportName
	 * @return void
	 */
	public void setReportingDetails(String reportName) {

		if (Driver.t == 0) {
			fldPath = getReportLocation();
		}
		logger.info("the folderpath:" + fldPath + "    " + Driver.t);
		report.init(fldPath + reportName + ".html", true);
		// report.init(getReportLocation() + reportName + ".html", true);
		report.config().documentTitle(reportName + " Report");
		report.config().reportHeadline(
				reportName + "execution details using ExtentReports");
		report.config().displayCallerClass(false);
		report.config().useExtentFooter(false);
		report.config().statusIcon(LogStatus.PASS, "check-circle");
		report.startTest(reportName);
		finalReportName = reportName + ".html";
		Constants.REPORT_NAME = fldPath + reportName + ".html";
	}

	/**
	 * Method to double click the specified object, method would determine the
	 * object locator type from OR and perform the action accordingly as per the
	 * object locator type
	 * 
	 * @date 15th June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionDoubleClick(String objectName)
			throws InterruptedException {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_DROPDOWN)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					WebElement element = driver.findElement(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

					Actions action = new Actions(driver);
					action.moveToElement(element).build().perform();
					action.doubleClick().build().perform();
					Thread.sleep(1000);

					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					WebElement element = driver.findElement(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					Actions action = new Actions(driver);
					action.moveToElement(element).build().perform();
					action.doubleClick().build().perform();
					Thread.sleep(1000);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					WebElement element = driver.findElement(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					Actions action = new Actions(driver);
					action.moveToElement(element).build().perform();
					action.doubleClick().build().perform();
					Thread.sleep(1000);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					WebElement element = driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					Actions action = new Actions(driver);
					action.moveToElement(element).build().perform();
					action.doubleClick().build().perform();
					Thread.sleep(1000);
					logger.info(objectName + " value is selected as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		}
	}

	/**
	 * The method is used to get the view on web element
	 * 
	 * @param objectName
	 * @return void
	 */
	public void performActiongetView(String objectName) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_IMAGE))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT) || (objectDetails
							.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)))) {

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					WebElement element = driver.findElement(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					((JavascriptExecutor) driver).executeScript(
							"arguments[0].scrollIntoView(true);", element);
					logger.info(objectName + "-----is in View");
					report.log(LogStatus.INFO, objectName + " is in View");
					logger.info(objectName + " is in View");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					WebElement element = driver.findElement(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					((JavascriptExecutor) driver).executeScript(
							"arguments[0].scrollIntoView(true);", element);
					logger.info(objectName + "-----is in View");
					report.log(LogStatus.INFO, objectName + "is in View ");
					logger.info(objectName + " is in View");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					waitForPageLoad(driver);
					WebElement element = driver.findElement(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					((JavascriptExecutor) driver).executeScript(
							"arguments[0].scrollIntoView(true);", element);
					logger.info(objectName + "-----is in View");
					report.log(LogStatus.INFO, objectName + " is in View");
					logger.info(objectName + "is in View");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					WebElement element = driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					((JavascriptExecutor) driver).executeScript(
							"arguments[0].scrollIntoView(true);", element);
					logger.info(objectName + "-----is in View");
					report.log(LogStatus.INFO, objectName + "is in View");
					logger.info(objectName + "is in View");
				}
			}
		} catch (Exception e) {
			logger.error(objectName + " is not in View **********");
			logger.info(objectName + " is not in View  **********");
			report.log(LogStatus.ERROR, objectName + " is not in View ");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;

		}
	}

	/**
	 * The method is used to click multiple web elements
	 * 
	 * @param objectName
	 * @return void
	 */
	public void performActionMultipleClick(String objectName)  {
		objectDetails = getObjectDetails(objectName);
		List<String> multVal = new ArrayList<String>();
		List<WebElement> ele = new ArrayList<WebElement>();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElements(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " element is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElements(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " element is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					ele = driver.findElements(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " element is captured with count "
							+ ele.size());
					logger.info(objectName + " element is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElements(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " element is captured");
				}

			}
			for (WebElement ele1 : ele) {
				ele1.click();
				logger.info("Element is clicked");
			}
		} catch (Exception e) {
			logger.info(objectName + " could not click **********");
			logger.error(e);
		}

	}

	/**
	 * Method to perform select value from the drop down list for multiple
	 * objects holding same object locator value, method would determine the
	 * object locator type from OR and perform the action accordingly as per the
	 * object locator type
	 * 
	 * @date May 2015
	 * @author Omar Updated BY Anant
	 * @param Object
	 *            name as mentioned in the object repository. Note: object type
	 *            should be DropDown and object locator type should be xpath in
	 *            OR
	 * @param Test
	 *            data value to be to be select
	 * @return void
	 */
	public void performActionMutipleSelectDropDown_Select(String objectName, String testData)
			throws InterruptedException {
		       objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				List<WebElement> listBox1 = driver
						.findElements(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (int j = 0; j < listBox1.size(); j++) {
					Select sel = new Select(listBox1.get(j));
					sel.selectByVisibleText(testData);
					logger.info(objectName + " value is selected as " + testData);
				}
			}
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				List<WebElement> listBox1 = driver
						.findElements(By.cssSelector(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (int j = 0; j < listBox1.size(); j++) {
					Select sel = new Select(listBox1.get(j));
					sel.selectByVisibleText(testData);
					logger.info(objectName + " value is selected as " + testData);
				}
			}
			else
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
				List<WebElement> listBox1 = driver
						.findElements(By.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (int j = 0; j < listBox1.size(); j++) {
					Select sel = new Select(listBox1.get(j));
					sel.selectByVisibleText(testData);
					logger.info(objectName + " value is selected as " + testData);
				}
			}
			else
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					List<WebElement> listBox1 = driver
							.findElements(By.name(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int j = 0; j < listBox1.size(); j++) {
						Select sel = new Select(listBox1.get(j));
						sel.selectByVisibleText(testData);
						logger.info(objectName + " value is selected as " + testData);
					}
			}
		} catch (Exception e) {
			logger.info(objectName + " could not select value from the drop down **********");
			logger.error(e);
		}
	}

/**
 * The method used to select the first value of multiple dropdown
 * This method useful only when drop down is designed by using select class 
 * @param objectName
 * @author Anant
 * @throws InterruptedException
 */
public void performActionMultipleSelectDropDown_Select(String objectName)
			throws InterruptedException {
		       objectDetails = getObjectDetails(objectName);
		       System.out.println("Contorl enter here");
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_DROPDOWN))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_SELECT)))
			 {
			  if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				List<WebElement> listBox1 = driver
						.findElements(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (int j = 0; j < listBox1.size(); j++) {
					Select sel = new Select(listBox1.get(j));
					sel.selectByIndex(1);
					logger.info(objectName + " first value is selected as " );
				}
			}
			else
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				List<WebElement> listBox1 = driver
						.findElements(By.cssSelector(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (int j = 0; j < listBox1.size(); j++) {
					Select sel = new Select(listBox1.get(j));
					sel.selectByIndex(1);
					logger.info(objectName + " first value is selected as " );
				}
			}
			else
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					List<WebElement> listBox1 = driver
							.findElements(By.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int j = 0; j < listBox1.size(); j++) {
						Select sel = new Select(listBox1.get(j));
						sel.selectByIndex(1);
						logger.info(objectName + " first value is selected as " );
				}
			}
			else
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					List<WebElement> listBox1 = driver
							.findElements(By.name(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int j = 0; j < listBox1.size(); j++) {
						Select sel = new Select(listBox1.get(j));
						sel.selectByIndex(1);
						logger.info(objectName + " first value is selected as " );
				}
			 }
		   }
		} catch (Exception e) {
			logger.info(objectName + " could not select value from the drop down **********");
			logger.error(e);
		}
	}

	/**
	 * Method to perform click operation for the specified object, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date April 2015 Updated By Anant
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @return void
	 */
	public void performActionClickinView(String objectName) {
		performActiongetView(objectName);
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_IMAGE))
					|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT) || (objectDetails
							.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)))) {
				waitForElementToBeClickable(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + " is clicked");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked ");
					logger.info(objectName + " is clicked");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					waitForPageLoad(driver);
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + "is clicked");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					waitForPageLoad(driver);
					report.log(LogStatus.INFO, objectName + " is clicked");
					logger.info(objectName + " is clicked");
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not clicked **********");
			report.log(LogStatus.ERROR, objectName + " is not clicked");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to perform select value from the drop down list for multiple
	 * objects holding same object locator value, method would determine the
	 * object locator type from OR and perform the action accordingly as per the
	 * object locator type
	 * 
	 * @date May 2015
	 * @author Madan
	 * @param Object
	 *            name as mentioned in the object repository. Note: object type
	 *            should be DropDown and object locator type should be xpath in
	 *            OR
	 * @param Test
	 *            data value to be to be select
	 * @return void
	 */
	public void performActionMutipleSelectDropDown(String objectName)
			throws InterruptedException {
		objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				List<WebElement> listBox1 = driver.findElements(By
						.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (int j = 0; j < listBox1.size(); j++) {
					listBox1.get(j).click();
					List<WebElement> listBox = driver.findElements(By
							.xpath("//div[@class='dropdownlist']"));
					for (int i = 0; i < listBox.size(); i++) {
						String c = listBox.get(i)
								.findElement(By.xpath("ul/li")).getText();
						if (!c.equals("")) {
							i++;
							List<WebElement> listBox2 = driver.findElements(By
									.xpath("//div[@class='dropdownlist']" + "["
											+ i + "]" + "/ul/li"));
							Random rd = new Random();
							int x = rd.nextInt(listBox2.size());
							WebElement listValue = listBox2.get(x);
							String DDValue = listBox2.get(x).getText();
							if (DDValue.equals("Select")
									|| DDValue.equals("Dr")
									|| DDValue.equals("Prof")
									|| DDValue.equals("Rev")
									|| DDValue.equals("Not Required")) {
								listValue = listBox2.get(1);
								Actions action = new Actions(driver);
								Thread.sleep(1000);
								action.moveToElement(listValue).doubleClick()
										.build().perform();
								break;
							} else {
								Actions action = new Actions(driver);
								Thread.sleep(1000);
								action.moveToElement(listValue).doubleClick()
										.build().perform();
								break;
							}
						}
					}
				}

				logger.info(objectName + " value is selected as " + testData);
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " could not select value from the drop down **********");
			logger.error(e);
		}
	}

	/**
	 * Methods to compute child date of birth based on the number of child and
	 * their age, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date May 2015
	 * @author Kaushik
	 * @param Object
	 *            name as mentioned in the object repository. Note: xpath should
	 *            be ending with the term select and object type should be
	 *            Select in OR
	 * @param Test
	 *            data value to be send date of birth
	 * @return void
	 * @throws ParseException
	 */

	public void performActionComputeDateofBirth(String objectName)
			throws ParseException {
		String value = "";
		String[] datenew = null;
		objectDetails = getObjectDetails(objectName);

		List<WebElement> lists = driver.findElements(By
				.xpath(".//*[contains(@name,'insurancePersonType')]"));
		List<WebElement> dob1 = driver.findElements(By
				.xpath(".//*[contains(@name,'dob')]"));
		if (lists.size() != 0) {
			for (int i = 0; i < lists.size(); i++) {
				lists.get(i)
						.findElement(By.xpath("./preceding-sibling::a/span[2]"))
						.click();
				List<WebElement> drop = driver.findElements(By
						.xpath("//*[@class='dropdownlist']/ul/li[2]"));
				for (WebElement drop2 : drop) {
					if (drop2.isDisplayed()) {
						value = drop2.getAttribute("datavalue");
						drop2.click();
						if (value.equalsIgnoreCase("ADULT")) {
							dob1.get(i).sendKeys("27/05/1985");
							break;
						}
						if (value.equalsIgnoreCase("CHILD")) {

							String childAge = dob1
									.get(i)
									.findElement(
											By.xpath("//ancestor::div[6]//*[@class='passenger-description childAge']"))
									.getText().trim();
							String childAge2 = childAge
									.replaceAll("[^0-9]", "").trim();
							Calendar now = Calendar.getInstance();
							now.add(Calendar.YEAR, -Integer.parseInt(childAge2));
							String dob = (now.get(Calendar.DATE) + 1) + "/"
									+ now.get(Calendar.MONTH) + "/"
									+ now.get(Calendar.YEAR);
							// dob = "02/06/2013";
							SimpleDateFormat formatter = new SimpleDateFormat(
									"dd/MM/yyyy");
							Date date = formatter.parse(dob);
							dob1.get(i).sendKeys(formatter.format(date));
							break;
						}
					}
				}
			}
			String adu = driver
					.findElement(
							By.xpath(".//*[@id='totalPriceComponent']/div"))
					.getText().trim();

			if (adu.contains("Children")) {
				child1 = adu.substring(adu.indexOf("("), adu.indexOf(")"))
						.replaceAll("[^0-9]+", " ");
				child = child1.replaceAll("\\s", " ").trim().split(" ");
				datenew = new String[child.length];

				for (int j = 0; j < child.length; j++) {
					Calendar now = Calendar.getInstance();
					now.add(Calendar.YEAR, -Integer.parseInt(child[j]));
					String dob = (now.get(Calendar.DATE) + 1) + "/"
							+ now.get(Calendar.MONTH) + "/"
							+ now.get(Calendar.YEAR);
					SimpleDateFormat formatter = new SimpleDateFormat(
							"dd/MM/yyyy");
					Date date = formatter.parse(dob);
					datenew[j] = formatter.format(date);
				}
			}
			int j = 0;
			int l = lists.size();
			if (l != 0) {
				l = l - 1;
			}
			int k = 0;
			for (int i = l; i < dob1.size(); i++) {

				String s = dob1
						.get(i)
						.findElement(
								By.xpath(".//ancestor::div[@class='sub-section']/div[2]/div[2]/div[3]/span[1]"))
						.getText().trim();
				if (i > 0 && s.equalsIgnoreCase("Travel insurance")) {
					j++;
				} else {
					if (i != 0 && k == 0) {
						dob1.get(i).sendKeys(datenew[j]);
						j++;
					} else {
						k++;
						dob1.get(i).sendKeys(datenew[j]);
						j++;
					}
				}
			}
		}
	}

	/**
	 * Method to perform send text operation for multiple objects, method would
	 * determine the object locator type and locator value from OR and perform
	 * the action accordingly for all the objects based on the object locator
	 * value and type
	 * 
	 * @date May 2015
	 * @author Kaushik
	 * @param Object
	 *            name as mentioned in the object repository
	 * @param Test
	 *            data to be sent as text
	 * @return void
	 */
	public void performActionEnterMultipleText(String objectName,
			String testData) {
		objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					List<WebElement> Textboxes = driver.findElements(By
							.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int i = 0; i < Textboxes.size(); i++) {
						Textboxes.get(i).click();
						Textboxes.get(i).sendKeys(testData);
					}
					logger.info(objectName + " text is set as " + testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					List<WebElement> Textboxes = driver.findElements(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int i = 0; i < Textboxes.size(); i++) {
						Textboxes.get(i).click();
						Textboxes.get(i).sendKeys(testData);
					}
					logger.info(objectName + " text is set as " + testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					List<WebElement> Textboxes = driver.findElements(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int i = 0; i < Textboxes.size(); i++) {
						Textboxes.get(i).click();
						Textboxes.get(i).sendKeys(testData);
					}
					logger.info(objectName + " text is set as " + testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					List<WebElement> Textboxes = driver.findElements(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (int i = 0; i < Textboxes.size(); i++) {
						Textboxes.get(i).click();
						Textboxes.get(i).sendKeys(testData);
					}
					logger.info(objectName + " text is set as " + testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			logger.error(e);
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
		}
	}

	/**
	 * Method to compute DOB which would be calculated as per given age and
	 * computed age would be entered in the text box
	 * 
	 * @date May 2015
	 * @author Kaushik
	 * @param Object
	 *            name as mentioned in the object repository
	 * @return void
	 */
	public void performActionComputeDateofBirthWithoutInsurance(
			String objectName) {
		String datenew;
		objectDetails = getObjectDetails(objectName);
		try {
			String adu = driver
					.findElement(
							By.xpath(".//*[@id='totalPriceComponent']/div"))
					.getText().trim();
			adu = adu.replaceAll("[^0-9]+", " ");
			adu = adu.replaceAll("\\s", " ");
			int l = 2;
			String[] arr = adu.split(" ");
			if (arr.length > 1) {
				int chdcount = Integer.parseInt(arr[1].trim());
				for (int m = 0; m < chdcount; m++) {
					int age = Integer.parseInt(arr[l].trim());
					l++;
					Calendar now = Calendar.getInstance();
					now.add(Calendar.YEAR, -age);
					String dob = (now.get(Calendar.DATE) + 1) + "/"
							+ now.get(Calendar.MONTH) + "/"
							+ now.get(Calendar.YEAR);
					SimpleDateFormat formatter = new SimpleDateFormat(
							"dd/MM/yyyy");
					Date date = formatter.parse(dob);
					datenew = formatter.format(date);
					try {
						if (objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT)) {
							if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
									.equalsIgnoreCase(
											CONSTANTS_OBJECT_LOCATOR_ID)) {
								List<WebElement> ele = driver
										.findElements(By.id(objectDetails
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(m);
								listdob.sendKeys(datenew);
							} else if (objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
									CONSTANTS_OBJECT_LOCATOR_NAME)) {
								List<WebElement> ele = driver
										.findElements(By.name(objectDetails
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(m);
								listdob.sendKeys(datenew);
							} else if (objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
									CONSTANTS_OBJECT_LOCATOR_XPATH)) {
								List<WebElement> ele = driver
										.findElements(By.xpath(objectDetails
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(m);
								listdob.sendKeys(datenew);
							} else if (objectDetails.get(
									CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
									CONSTANTS_OBJECT_LOCATOR_CSS)) {
								List<WebElement> ele = driver
										.findElements(By.cssSelector(objectDetails
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								WebElement listdob = ele.get(m);
								listdob.sendKeys(datenew);
							}
						}
					} catch (Exception e) {
						logger.info(objectName
								+ " text is not entered **********");
						logger.error(e);
						result = false;
					}
				}
			} else {
				driver.findElement(
						By.xpath("html/body/div[1]/div[3]/div[3]/div[1]/div/form/div/div[3]/div[2]/div[2]/div[9]/div/div[2]/input"))
						.sendKeys("23/04/1984");
				driver.findElement(
						By.xpath("html/body/div[1]/div[3]/div[3]/div[1]/div/form/div/div[5]/div[2]/div[2]/div[4]/div/div[2]/input"))
						.sendKeys("23/04/1984");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method used to wait specific period of time It returns the void
	 */
	public void pageToLoad() {
		result = true;
		try {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		} catch (Exception e) {
			logger.info("Could not wait for page load ********");
			logger.error(e);
			result = false;
		}
	}

	/**
	 * The method used to create the Eyes method It accepts the two parameters
	 * as ApplicationName and Test Name
	 * 
	 * @param ApplnName
	 * @param TestName
	 *            It returns void
	 */
	public void eyeOpen(String ApplnName, String TestName) {
		try {
			eyes = new Eyes();
			eyes.setApiKey("mS4Jh1023RVUuqDRdP6t60a103zF6J4t110PeUVqSQnjQIoqY110");
			driver = eyes.open(driver, ApplnName, TestName);
		} catch (Exception e) {
			report.log(LogStatus.INFO, "Exception in eyeOpen method:");
			logger.equals(e);
			logger.error("Exception in eyeOpen method:" + e);
		}
	}

	/**
	 * The method used to verify the entire Window GUI It returns void
	 */
	// @SuppressWarnings("static-access")
	public void checkWindowGUI(String windowName) {
		try {
			eyes.checkWindow(windowName);
			// eyes.setAppName(driver.getCurrentUrl());
			// eyes.getDefaultServerUrl();
			// System.out.println("The eye url:"+eyes.getDefaultServerUrl());
			// eyes.setServerUrl((URI)driver.getCurrentUrl());
		} catch (Exception e) {
			logger.equals(e);
			report.log(LogStatus.INFO, "Exception in checkWindowGUI method:");
			logger.error("Exception in checkWindowGUI method:" + e);
		}
	}

	/**
	 * The method used to verify the particular component Window GUI It accepts
	 * The one parameter as objectName
	 * 
	 * @param objectName
	 *            It returns void
	 */
	// @SuppressWarnings("static-access")
	public void checkElementGUI(String objectName) {
		String page, cmpName, stepDetail;
		objectDetails = getObjectDetails(objectName);
		try {
			page = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			cmpName = objectName.substring(objectName.lastIndexOf("_") + 1);

			result = true;
			WebElement uiElement;
			try {
				if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
						CONSTANTS_OBJECT_TYPE_TEXT)
						|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)) {
					waitForElementToBeDisplayed(objectName);
					if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
						uiElement = driver.findElement(By.id(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						stepDetail = "1." + "Launch the url==>"
								+ driver.getCurrentUrl().replace("http://", "")
								+ "<br/>2.Navigate To the page==>" + page
								+ "<br/>3.Check the component==>" + cmpName;
						// eyes.checkRegion(uiElement, objectName);
						eyes.checkRegion(uiElement, stepDetail);
						logger.info(objectName
								+ " Component is displayed from WEB");
						report.log(LogStatus.INFO, objectName
								+ " Coponent is displayed From WEB ");
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
						uiElement = driver.findElement(By.name(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						stepDetail = "1." + "Launch the url==>"
								+ driver.getCurrentUrl().replace("http://", "")
								+ "<br/>2.Navigate To the page==>" + page
								+ "<br/>3.Check the component==>" + cmpName;
						// eyes.checkRegion(uiElement, objectName);
						eyes.checkRegion(uiElement, stepDetail);
						logger.info(objectName
								+ " Component is displayed from WEB");
						report.log(LogStatus.INFO, objectName
								+ " Component is displayed From WEB ");
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
						uiElement = driver.findElement(By.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						// eyes.checkRegion(uiElement, objectName);
						stepDetail = "1." + "Launch the url==>"
								+ driver.getCurrentUrl().replace("http://", "")
								+ "<br/>2.Navigate To the page==>" + page
								+ "<br/>3.Check the component==>" + cmpName;
						eyes.checkRegion(uiElement, stepDetail);
						logger.info(objectName
								+ " Component is captured from WEB");
						report.log(LogStatus.INFO, objectName
								+ " Component is displayed From WEB ");
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
						uiElement = driver.findElement(By
								.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						stepDetail = "1." + "Launch the url==>"
								+ driver.getCurrentUrl().replace("http://", "")
								+ "<br/>2.Navigate To the page==>" + page
								+ "<br/>3.Check the component==>" + cmpName;
						// eyes.checkRegion(uiElement, objectName);
						eyes.checkRegion(uiElement, stepDetail);
						logger.info(objectName
								+ " Component is displayed from WEB");
						report.log(LogStatus.INFO, objectName
								+ " Component is displayed From WEB ");
					}
				}
			} catch (Exception e) {
				result = false;
				// eyes.getDefaultServerUrl();
				// System.out.println("The eye
				// url:"+eyes.getDefaultServerUrl());
				logger.info(objectName
						+ " Component is not displayed from the WEB **********");
				report.log(LogStatus.INFO, objectName
						+ " Component is not displayed from the WEB **********");
				logger.error(e);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Method to move to specified object and click, method would determine the
	 * object locator type from OR and perform the action accordingly as per the
	 * object locator type Note: Object type of first and second should be same
	 * 
	 * @date 23rd June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionMoveToElementAndClick(String objecttoMove,
			String objectToClick) throws InterruptedException {

		result = true;
		try {
			objectDetails = getObjectDetails(objecttoMove);
			waitForElementToBeDisplayed(objecttoMove);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				WebElement element = driver.findElement(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				Thread.sleep(1000);
				objectDetails = getObjectDetails(objectToClick);
				WebElement element2 = driver.findElement(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				executor.executeScript("arguments[0].click();", element2);
				Thread.sleep(1000);
				logger.info(objectToClick + " value is clicked");
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				WebElement element = driver.findElement(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				Thread.sleep(1000);
				objectDetails = getObjectDetails(objectToClick);
				WebElement element2 = driver.findElement(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				executor.executeScript("arguments[0].click();", element2);
				Thread.sleep(1000);
				logger.info(objectToClick + " value is clicked");
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				WebElement element = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				Thread.sleep(1000);
				objectDetails = getObjectDetails(objectToClick);
				WebElement element2 = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				executor.executeScript("arguments[0].click();", element2);
				Thread.sleep(1000);
				logger.info(objectToClick + " value is clicked");
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				WebElement element = driver.findElement(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				Thread.sleep(1000);
				objectDetails = getObjectDetails(objectToClick);
				WebElement element2 = driver.findElement(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				executor.executeScript("arguments[0].click();", element2);
				Thread.sleep(1000);
				logger.info(objectToClick + " value is clicked");
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to move to specified object and hover over it, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly Objective of writing this method was that sometimes
	 * performActionMoveToElementAndClick did not function properly for
	 * Responsive pages.
	 * 
	 * @date 9th March 2017
	 * @author Samson
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionMouseOver(String objecttoMove)
			throws InterruptedException{
		result = true;
		try {
			objectDetails = getObjectDetails(objecttoMove);
			waitForElementToBeDisplayed(objecttoMove);

			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				Actions builder = new Actions(driver);
				WebElement element = driver.findElement(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				builder.moveToElement(element).build().perform();

				Thread.sleep(1000);
				logger.info("Mouse-over successful on " + objecttoMove);
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				Actions builder = new Actions(driver);
				WebElement element = driver.findElement(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				builder.moveToElement(element).build().perform();

				Thread.sleep(1000);
				logger.info("Mouse-over successful on " + objecttoMove);
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

				Actions builder = new Actions(driver);
				WebElement element = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				builder.moveToElement(element).build().perform();

				Thread.sleep(1000);
				logger.info("Mouse-over successful on " + objecttoMove);
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				Actions builder = new Actions(driver);
				WebElement element = driver.findElement(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				builder.moveToElement(element).build().perform();

				Thread.sleep(1000);
				logger.info("Mouse-over successful on " + objecttoMove);
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		}
	}

	public void performActiononmouseover(String objecttoMove)
			throws InterruptedException{

		result = true;
		try {
			objectDetails = getObjectDetails(objecttoMove);
			waitForElementToBeDisplayed(objecttoMove);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				WebElement element = driver.findElement(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				/*
				 * Thread.sleep(1000); objectDetails =
				 * getObjectDetails(objectToClick); WebElement element2 =
				 * driver.findElement(By.id(objectDetails
				 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				 * executor.executeScript("arguments[0].click();", element2);
				 * Thread.sleep(1000); logger.info(objectToClick +
				 * " value is clicked");
				 */
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				WebElement element = driver.findElement(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				/*
				 * Thread.sleep(1000); objectDetails =
				 * getObjectDetails(objectToClick); WebElement element2 =
				 * driver.findElement(By.name(objectDetails
				 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				 * executor.executeScript("arguments[0].click();", element2);
				 * Thread.sleep(1000); logger.info(objectToClick +
				 * " value is clicked");
				 */
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				WebElement element = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				/*
				 * Thread.sleep(1000); objectDetails =
				 * getObjectDetails(objectToClick); WebElement element2 =
				 * driver.findElement(By.xpath(objectDetails
				 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				 * executor.executeScript("arguments[0].click();", element2);
				 * Thread.sleep(1000); logger.info(objectToClick +
				 * " value is clicked");
				 */
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				WebElement element = driver.findElement(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].onmouseover();", element);
				/*
				 * Thread.sleep(1000); objectDetails =
				 * getObjectDetails(objectToClick); WebElement element2 =
				 * driver.findElement(By .cssSelector(objectDetails
				 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				 * executor.executeScript("arguments[0].click();", element2);
				 * Thread.sleep(1000); logger.info(objectToClick +
				 * " value is clicked");
				 */
			}
		} catch (Exception e) {
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to click the required data from the table method would determine
	 * the element to click from the table
	 * 
	 * @date 23rd June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @param which
	 *            column to be clicked
	 * @param test
	 *            data value to be searched in table
	 * @return void
	 */
	public boolean performActionOnTable(String objectName, int column,
			String data) {

		objectDetails = getObjectDetails(objectName);
		WebElement table = driver.findElement(By.xpath(objectDetails
				.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		List<WebElement> rows = table.findElements(By.tagName("tr"));

		for (int rowNum = 0; rowNum < rows.size(); rowNum++) {
			List<WebElement> columns = rows.get(rowNum).findElements(
					By.tagName("td"));

			for (int colNum = 0; colNum < columns.size(); colNum++) {
				if (columns.get(colNum).getText().equalsIgnoreCase(data)) {
					columns.get(column - 1).click();
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * The method to reset the cookie It accepts The parameter and value
	 * 
	 * @author ananth
	 */
	public void cookieReset(String paramName, String value) {
		try {
			String appCookieName, appCookieVal;
			Iterator<Cookie> applCookie = driver.manage().getCookies()
					.iterator();
			Cookie ck;
			boolean found = false;
			while (applCookie.hasNext()) {
				Cookie appCookie = applCookie.next();
				appCookieName = appCookie.getName();
				if (appCookieName.trim().equals(paramName)) {
					found = true;
					appCookieVal = appCookie.getValue();
					if (!appCookieVal.trim().equals(value)) {
						ck = new Cookie(paramName, value);
						driver.manage().addCookie(ck);
						driver.navigate().refresh();
					} else {
						logger.info("The app Cookie val same:" + appCookieVal
								+ "    " + value);
					}
					break;
				}
			}
			if (found == false) {
				ck = new Cookie(paramName, value);
				driver.manage().addCookie(ck);
				driver.navigate().refresh();
			}
			// Cookie ck = new Cookie(paramName,value);
			// driver.manage().addCookie(ck);
			// driver.navigate().refresh();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method to click the required data from the table method would determine
	 * the element to click from the table
	 * 
	 * @date 23rd June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @param column
	 *            number to be clicked for the first entry
	 * @return void
	 */
	@SuppressWarnings("unused")
	public boolean performActionOnTable(String objectName, int column) {

		objectDetails = getObjectDetails(objectName);
		WebElement table = driver.findElement(By.xpath(objectDetails
				.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		List<WebElement> rows = table.findElements(By.tagName("tr"));

		for (int rowNum = 0; rowNum < rows.size(); rowNum++) {
			List<WebElement> columns = rows.get(rowNum).findElements(
					By.tagName("td"));

			for (int colNum = 0; colNum < columns.size(); colNum++) {
				columns.get(column - 1).click(); // Table column size would
				// be started from 0, hence
				// performing column-1
				return true;
			}
		}
		return false;
	}

	/**
	 * Method to click the required element by invoking Java script
	 * 
	 * @date 25th June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 * @throws
	 */
	public void performActionJavaScriptClick(String objectName)
			throws InterruptedException {
		try {
			objectDetails = getObjectDetails(objectName);
			WebElement element = driver.findElement(By.xpath(objectDetails
					.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", element);
			logger.info(objectName + " is clicked using javascript!");
			Thread.sleep(1000);
		} catch (Exception e) {
			logger.error(objectName + " is not clicked !");
			if (null != objectExtraDetails.get(objectName)
					&& !objectExtraDetails.get(objectName).equals("")) {
				performActionClickonDynamicOR(objectName);
			}
		}
	}

	/**
	 * Method to perform random search click on multiple objects, method would
	 * determine the object locator type from OR and perform the action
	 * accordingly as per the object locator type
	 * 
	 * @date 26th June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository. Note: object type
	 *            should be DropDown in OR
	 * @return void
	 */
	public boolean performFirstSearchClick(String objectName)
			throws InterruptedException {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				List<WebElement> elements = driver.findElements(By
						.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement element : elements) {
					element.click();
					report.log(LogStatus.INFO, objectName
							+ " Continue Button is clicked! ");
					logger.info(objectName + "  Continue Button is clicked! ");
					return true;
				}
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				List<WebElement> elements = driver
						.findElements(By.name(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement element : elements) {
					element.click();
					report.log(LogStatus.INFO, objectName
							+ " Continue Button is clicked! ");
					logger.info(objectName + "  Continue Button is clicked! ");
					return true;
				}
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				List<WebElement> elements = driver.findElements(By
						.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement element : elements) {
					element.click();
					report.log(LogStatus.INFO, objectName
							+ " Continue Button is clicked! ");
					logger.info(objectName + "  Continue Button is clicked! ");
					return true;
				}
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				List<WebElement> elements = driver.findElements(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement element : elements) {
					element.click();
					report.log(LogStatus.INFO, objectName
							+ " Continue Button is clicked! ");
					logger.info(objectName + "  Continue Button is clicked! ");
					return true;
				}

			}
		} catch (Exception e) {
			logger.info(objectName + " could not click Continue Button");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
		return false;
	}

	/**
	 * Method to wait for payment gateway to disappear within specified seconds
	 * 
	 * @date 26th June 2015
	 * @author Hima
	 * @return void
	 */
	public void waitForPaymentGateWayProcessingDone(Integer _seconds) {
		WebElement waitMsg = driver.findElement(By
				.xpath(".//div[@class='wait-message']"));
		for (int second = 0;; second++) {
			if (second >= _seconds)
				try {
					if (!waitMsg.isDisplayed())
						break;
				} catch (Exception e) {
					logger.info("Waiting for Payment Gateway Error due to " + e);
				}
		}
	}

	/**
	 * Method to get frames , used while developing script
	 * 
	 * @date 26th June 2015
	 * @author Hima
	 * @return void
	 */
	public void getFrames() {
		List<WebElement> ele = driver.findElements(By.tagName("frame"));
		List<WebElement> ele2 = driver.findElements(By.tagName("iframe"));
		logger.info("**************Number of frames in a page :" + ele.size());
		// SYSOUT statements needed for debugging as this method would be used
		// at debug level not at test level
		logger.info("**************Number of iframes in a page :" + ele2.size());
		for (WebElement el : ele) {
			// Returns the Id of a frame.
			logger.info("Frame Id :" + el.getAttribute("id"));
			// Returns the Name of a frame.
			logger.info("Frame name :" + el.getAttribute("name"));
		}
		for (WebElement el2 : ele2) {
			// Returns the Id of a frame.
			logger.info("IFrame Id :" + el2.getAttribute("id"));
			// Returns the Name of a frame.
			logger.info("IFrame name :" + el2.getAttribute("name"));
		}
	}

	/**
	 * Method to set Booking Reference Id to a file
	 * 
	 * @date 29th June 2015
	 * @author Hima
	 * @param String
	 *            referenceId
	 * @return void
	 */
	public void setBookingReference(String referenceId, String Adults,
			String Duration, String TotalPrice) throws IOException {

		FileOutputStream out = new FileOutputStream(getProjectPath()
				+ CONSTANTS_BOOKING_REFERENCE_PROPERTY);
		FileInputStream in = new FileInputStream(getProjectPath()
				+ CONSTANTS_BOOKING_REFERENCE_PROPERTY);
		Properties props = new Properties();
		props.load(in);
		in.close();

		props.setProperty("bookingReference", referenceId);
		props.setProperty("NoOfAdults", Adults);
		props.setProperty("NoOfNights", Duration);
		props.setProperty("TotalPrice", TotalPrice);

		props.store(out, null);
		out.close();
	}

	public boolean isPresent(String objectName) {
		objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE).contains("/")) {
				return driver.findElement(
						By.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
						.isDisplayed();
			} else {
				return driver.findElement(
						By.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
						.isDisplayed();
			}
		} catch (NoSuchElementException e) {
			return false;
		} catch (Exception e1) {
			return false;
		}
	}

	/**
	 * Method to get the search Test Data Details
	 * 
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @return Hashtable<String,String>
	 */
	@SuppressWarnings("resource")
	public Object[][] getSearchTestData(String path) {

		String csvFile = getProjectPath() + path;
		BufferedReader br = null;
		try {
			CSVReader reader = new CSVReader(new FileReader(csvFile));
			List<String[]> list = reader.readAll();

			// Convert to 2D array
			searchTestData = new String[list.size()][];
			searchTestData = list.toArray(searchTestData);

		} catch (FileNotFoundException e) {
			logger.info("Could not fetch object details from OR **********");
			logger.error(e);
		} catch (IOException e) {
			logger.info("Could not fetch object details from OR **********");
			logger.error(e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					logger.info("Could not fetch object details from OR **********");
					logger.error(e);
				}
			}
		}
		return searchTestData;
	}

	/**
	 * Method to perform check for Radio button or CheckBox for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date 6th July 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionRandomSelectBox(String objectName) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_RADIOBUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					List<WebElement> checkBoxes = driver.findElements(By
							.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					int size = checkBoxes.size();
					String checkValues[] = new String[size];
					for (int j = 0; j < size; j++) {
						String cvalue = checkBoxes.get(j).getAttribute("name");
						checkValues[j] = cvalue;
						Thread.sleep(2000);
					}
					for (int k = checkValues.length - 1; k > 0; k--) {
						Random rnd = new Random();
						if (rnd.nextInt(k + 1) == k) {
							driver.findElement(By.id(checkValues[k])).click();
							Thread.sleep(3000);
							report.log(LogStatus.INFO, objectName + " "
									+ CONSTANTS_OBJECT_TYPE_CHECKBOX
									+ " is selected as " + checkValues[k]);
							logger.info(objectName + " "
									+ CONSTANTS_OBJECT_TYPE_CHECKBOX
									+ " is selected");
						}
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					if (!driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.name(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					List<WebElement> checkBoxes = driver.findElements(By
							.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					int size = checkBoxes.size();
					Random rd = new Random();
					int x = rd.nextInt(size);
					WebElement listValue = checkBoxes.get(x);
					listValue.click();
					report.log(LogStatus.INFO, objectName + " "
							+ CONSTANTS_OBJECT_TYPE_CHECKBOX
							+ " is selected as " + listValue.getText());
					logger.info(objectName + " "
							+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is selected");
				}
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				if (!driver.findElement(
						By.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
						.isSelected()) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					report.log(LogStatus.INFO, objectName + " "
							+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is selected");
					logger.info(objectName + " "
							+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is selected");
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not selected **********");
			report.log(LogStatus.ERROR, objectName + " "
					+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is not selected");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to perform uncheck for Radio button or CheckBox for the specified
	 * object, method would determine the object locator type from OR and
	 * perform the action accordingly as per the object locator type
	 * 
	 * @date 13th July 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionUnSelectBox(String objectName) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_RADIOBUTTON)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					if (driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.id(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					if (driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.name(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					if (driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.xpath(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
					}
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					if (driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isSelected()) {
						driver.findElement(
								By.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.click();
						report.log(LogStatus.INFO, objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
						logger.info(objectName + " "
								+ CONSTANTS_OBJECT_TYPE_CHECKBOX
								+ " is un selected");
					}
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " is not un selected **********");
			report.log(LogStatus.ERROR, objectName + " "
					+ CONSTANTS_OBJECT_TYPE_CHECKBOX + " is not un selected");
			logger.error(e);
			result = false;
		}
	}

	// Methods by Madan

	/**
	 * Method to get packs count and worldcare fund price
	 * 
	 * @param String
	 *            ApplicationName,Extraspage
	 * @author Madan
	 * @return double value
	 */
	@SuppressWarnings("unused")
	public double packsCount(String App, String ExtrasPage) {
		int countt = 0;
		double cnt = 0;
		int counttAsAdult = 0;
		String s = driver.getCurrentUrl();
		int childcount;
		String noOfAdults;
		int noOfAdultss = 0;
		String noOfSeniors;
		int noOfSeniorss = 0;
		String ChildrenAge = "";
		String noOfChildren = "";
		int noOfChildrens;
		Double AdultPax;
		Double ChildPax;
		String[] x;
		Pattern findUrl, findUrl1, findUrl2;

		if (App.equalsIgnoreCase("FO")) {

			findUrl = Pattern.compile("&children=(.+?)");
		}

		else {
			findUrl = Pattern.compile("&noOfChildren=(.+?)");
		}

		Matcher matcher = findUrl.matcher(s);
		if (matcher.find()) {
			String childrencount = matcher.group(1);
			childcount = Integer.parseInt(childrencount);
			if (childcount >= 1) {
				if (App.equalsIgnoreCase("FO")) {
					findUrl1 = Pattern
							.compile("&adults=(.+?)&children=(.+?)&.+?&childAge=(.+?)&isFlexible");
				}

				else {

					findUrl1 = Pattern
							.compile("&noOfAdults=(.+?)&noOfChildren=(.+?)&childrenAge=(.+?)&duration");

				}

				Matcher matcher1 = findUrl1.matcher(s);
				while (matcher1.find()) {
					noOfAdults = matcher1.group(1);
					noOfAdultss = Integer.parseInt(noOfAdults);

					ChildrenAge = matcher1.group(3);
					if (App.equalsIgnoreCase("FO")) {
						x = ChildrenAge.split("%2C");
					}

					else {

						x = ChildrenAge.split(",");
					}

					for (int i = 0; i < x.length; i++) {
						if (ExtrasPage.equalsIgnoreCase("worldcarefund")) {
							if (Integer.parseInt(x[i]) > 12) {
								counttAsAdult = counttAsAdult + 1;
							} else {
								if ((Integer.parseInt(x[i]) >= 2))
									countt = countt + 1;
							}
						}

						else {
							if ((Integer.parseInt(x[i]) >= 2))
								countt = countt + 1;
						}

					}
					noOfChildren = matcher1.group(2);
					noOfChildrens = Integer.parseInt(noOfChildren);

				}

			} else {

				if (App.equalsIgnoreCase("FO")) {
					findUrl2 = Pattern
							.compile("&adults=(.+?)&children=(.+?)&infants");
				}

				else {

					findUrl2 = Pattern
							.compile("&noOfAdults=(.+?)&noOfChildren=(.+?).+?noOfSeniors=(.+?)");
				}

				Matcher matcher2 = findUrl2.matcher(s);
				while (matcher2.find()) {

					noOfAdults = matcher2.group(1);
					noOfAdultss = Integer.parseInt(noOfAdults);

					noOfChildren = matcher2.group(2);
					noOfChildrens = Integer.parseInt(noOfChildren);

				}
			}
		}

		if (ExtrasPage.equalsIgnoreCase("taxi")) {
			if (countt >= 2) {
				cnt = (noOfAdultss + noOfSeniorss + countt) / 4.0;
				String yy = Double.toString(cnt);
				Pattern findUrl3 = Pattern.compile(".+\\.(.+?)");
				Matcher matcher2 = findUrl3.matcher(yy);
				if (matcher2.find()) {
					yy = matcher2.group(1);

				}

				int xx = Integer.parseInt(yy);
				if (xx >= 1) {
					cnt = Math.ceil(cnt);

				} else {
					cnt = Math.floor(cnt);

				}
			} else if (countt >= 1 || countt == 0) {
				cnt = (noOfAdultss + noOfSeniorss + countt) / 3.0;
				String yy = Double.toString(cnt);
				Pattern findUrl3 = Pattern.compile(".+\\.(.+?)");
				Matcher matcher2 = findUrl3.matcher(yy);
				if (matcher2.find()) {
					yy = matcher2.group(1);

				}

				int xx = Integer.parseInt(yy);
				if (xx >= 1) {
					cnt = Math.ceil(cnt);

				} else {
					cnt = Math.floor(cnt);

				}
			}

		}
		if (ExtrasPage.equalsIgnoreCase("")) {
			cnt = noOfAdultss + noOfSeniorss + countt;
		}
		if (ExtrasPage.equalsIgnoreCase("worldcarefund")) {
			cnt = (noOfAdultss + noOfSeniorss + counttAsAdult) + (countt * 0.5);
		}
		if (ExtrasPage.equalsIgnoreCase("Insurance")) {
			cnt = countt;
		}

		return cnt;
	}

	/**
	 * The following method to capture the data from response using regex It
	 * fetches the data based on occurrence number we will provide to it Fetches
	 * the data based on the number of group will provide in the regex Fetches
	 * the based on the number of matches it finds in the response and
	 * concatenate it. and converts the the date into dd/MM/yyyy format
	 * Author-AnantPatil
	 */
	public String regexExtractor(String regex, String response)
			throws ParseException {
		StringBuffer sb = new StringBuffer();
		int pos = 0, occur = 0;
		SimpleDateFormat fmt = new SimpleDateFormat();
		boolean flag = false;
		String regValue = null, val = "";
		try {
			if (regex.contains("^^")) {
				regValue = regex.substring(0, regex.indexOf("^^"));
				pos = Integer
						.parseInt(regex.substring(regex.indexOf("^^") + 2));
				flag = true;
			} else
				regValue = regex;
			try {
				Pattern pat = Pattern.compile(regValue);
				Matcher mat = pat.matcher(response);
				while (mat.find()) {
					if (flag)
						occur++;
					if (occur == pos) {
						for (int j = 1; j <= mat.groupCount(); j++) {
							val = val + mat.group(j).trim().replace(" ", "");
						}
						if (val.matches("\\d{2}\\-*\\w{3}\\-*\\d{2}\\w*")
								&& !val.matches("[0-9]+")) {
							val = val.replace("-", "");
							fmt.applyPattern("ddMMMyy");
							Date dt = fmt.parse(val);
							fmt.applyPattern("ddMMMyyyy");
							val = fmt.format(dt);
						}
						sb.append(val);
						val = "";
					}

				}
			} catch (PatternSyntaxException p) {
				logger.error("Pattern syntax exeception occured in regexExtractor:"
						+ p.getMessage());
			} catch (Exception e) {
				logger.error("Exeception occured in regexExtractor:" + e);
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("ArrayIndexOutOfBoundsException in regexExtractor" + a);
		} catch (Exception e) {
			logger.error("ArrayIndexOutOfBoundsException in regexExtractor" + e);
		}
		// System.out.println("the val:"+sb.toString());
		return sb.toString().replace("\r", "");
	}

	/**
	 * Method to verify the page display
	 * @date April 2015
	 * @param PageName
	 *            as mentioned in the object repository.
	 * @return boolean value
	 */
	public boolean pageDisplayedSuccessfully() {
		boolean isPageLaunched = false;
		try {
			waitForPageLoad(driver);
			String response = driver.getPageSource();
			if (response.contains("All Gone")) {
				report.log(LogStatus.FAIL, "Page diplayed with All Gone");
				Assert.assertTrue(false, "Page Not Displayed");
			} else if (response.contains("Service temporarily unavailable")) {
				report.log(LogStatus.FAIL,
						"Page diplayed with Service temporarily unavailable");
				Assert.assertTrue(false, "Page Not Displayed");
			} else if (response.contains("Technical Difficulties")) {
				report.log(LogStatus.FAIL,
						"Page diplayed with Technical Difficulties");
				Assert.assertTrue(false, "Page Not Displayed");
			} else if (response.contains("We're really sorry")) {
				report.log(LogStatus.FAIL,
						"Page diplayed with We're really sorry");
				Assert.assertTrue(false, "Page Not Displayed");
			} else if (response.contains("Page Not Found")) {
				report.log(LogStatus.FAIL, "Page diplayed with Page not found");
				Assert.assertTrue(false, "Page Not Displayed");
			} else if (response.contains("We don't know what you are looking")) {
				report.log(LogStatus.FAIL,
						"Page diplayed with We don't know what you are looking");
				Assert.assertTrue(false, "Page Not Displayed");
			} else if (response.contains("Internal Server Error")) {
				report.log(LogStatus.FAIL,
						"Page diplayed with Internal Server Error");
				Assert.assertTrue(false, "Page Not Displayed");
			}  else if (response.contains("Please select a card type.")) {
				report.log(LogStatus.FAIL,
						"Page diplayed with <b>Please select a card type.<b>");
				Assert.assertTrue(false, "Page Not Displayed");
			}
			else {
				report.log(LogStatus.PASS, "Page was rendered properly");
				isPageLaunched = true;
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Could not get the page response **********");
			logger.info("Could not get the page response **********");
			logger.error(e);
			isPageLaunched = false;
			Assert.assertTrue(isPageLaunched, "Page Not Displayed");
		} finally {
			try {
				if (isPageLaunched == false) {
					logger.error("Expected Page is not displayed successfully");
					report.log(LogStatus.FAIL,
							"Page has not launched successfully!");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				Assert.assertTrue(isPageLaunched, "Page Not Displayed");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
		return isPageLaunched;
	}
	
	/**
	 * Method to select Flight Teaser
	 * 
	 * @author Madan
	 * @return double value
	 */
	public Double performActionRandomClickTeaser(String objectName) {
		objectDetails = getObjectDetails(objectName);
		Double d1 = null;
		List<WebElement> wd = driver.findElements(By.cssSelector(objectDetails
				.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		for (int s = 0; s < wd.size(); s++) {
			String d = wd.get(s).getText();
			if (!d.isEmpty()) {
				String dd = wd.get(s).getText();
				d1 = Double.parseDouble(dd.replace(pound, "").replace(plusEuro,
						""));

				wd.get(s).click();
				break;
			}
		}
		return d1;
	}

	/**
	 * Method to select random element
	 * 
	 * @author Madan
	 * @return double value
	 */
	public Double performActionRandomClick(String objectName) {
		objectDetails = getObjectDetails(objectName);
		Double d = null;
		String value;
		List<WebElement> wd1;
		Random rd = new Random();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd1.get(y).getText();
				if (!value.isEmpty()) {
					try {
						value = value.replace(plusPound, "").replace(plusEuro,
								"");
						d = Double.parseDouble(value);
					} catch (Exception e) {
					}
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd1.get(y).getText();
				if (!value.isEmpty()) {
					try {
						value = value.replace(pound, "").replace(plusEuro, "");
						d = Double.parseDouble(value);
					} catch (Exception e) {
					}

				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int y = rd.nextInt(wd1.size());
				value = wd1.get(y).getText();
				if (!value.isEmpty()) {
					try {
						value = value.replace(pound, "").replace(plusEuro, "");
						d = Double.parseDouble(value);
					} catch (Exception e) {
						logger.error("exception in performActionRandomClick method:"
								+ e);
					}
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd1.get(y).getText();
				if (!value.isEmpty()) {
					try {
						value = value.replace(pound, "").replace(plusEuro, "");
						d = Double.parseDouble(value);
					} catch (Exception e) {
					}
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + "is selcted as" + "<strong>" + d
						+ "</strong>");
				report.log(LogStatus.INFO, objectName + " " + "is selcted as"
						+ "<strong>" + d + "</strong>");
			}

		} catch (Exception e) {
			logger.info(objectName + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
		return d;
	}

	/**
	 * The method is used to click randomely
	 * 
	 * @author Kaushika It returns void
	 * @param objectName
	 */
	public void performActionRandomClickinView(String objectName) {
		objectDetails = getObjectDetails(objectName);
		Double d = null;
		List<WebElement> wd1;
		Random rd = new Random();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int y = rd.nextInt(wd1.size());
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				Thread.sleep(500);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				Thread.sleep(500);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + "is selcted as" + "<strong>" + d
						+ "</strong>");
				report.log(LogStatus.INFO, objectName + " " + "is selcted as"
						+ "<strong>" + d + "</strong>");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				Thread.sleep(500);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + "is selcted as" + "<strong>" + d
						+ "</strong>");
				report.log(LogStatus.INFO, objectName + " " + "is selcted as"
						+ "<strong>" + d + "</strong>");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				Thread.sleep(500);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + "is selcted as" + "<strong>" + d
						+ "</strong>");
				report.log(LogStatus.INFO, objectName + " " + "is selcted as"
						+ "<strong>" + d + "</strong>");
			}
		} catch (Exception e) {
			logger.info(objectName + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
	}

	/**
	 * Author Swati The method is used to scroll to view and select the dropdown
	 * value where Select class will not work
	 * 
	 * @param objectName
	 */
	public void performActionRandomSelect_Dropdowninview(String objectName) {
		objectDetails = getObjectDetails(objectName);
		List<WebElement> ele;
		Random rand = new Random();
		try {
			waitForElementToBeDisplayed(objectName);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				ele = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", ele.get(r));
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				ele = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", ele.get(r));
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_CSS)) {
				ele = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", ele.get(r));
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_NAME)) {
				ele = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", ele.get(r));
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			}
		} catch (Exception e) {
			logger.info(objectName + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
	}

	/**
	 * The method is used to select the dropdown value where Slecte class will
	 * not work
	 * 
	 * @param objectName
	 */
	public void performActionRandomSelect_Dropdown(String objectName) {
		objectDetails = getObjectDetails(objectName);
		List<WebElement> ele;
		Random rand = new Random();
		try {
			waitForElementToBeDisplayed(objectName);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				ele = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				ele = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_CSS)) {
				ele = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).contains(
					CONSTANTS_OBJECT_LOCATOR_NAME)) {
				ele = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				int r = rand.nextInt(ele.size());
				if (r == 0) {
					r = 1;
				}
				ele.get(r).click();
				report.log(LogStatus.INFO, objectName + " is clicked ");
				report.log(LogStatus.INFO, ele.get(r).getText()
						+ " is Selected ");
				logger.info(objectName + " is clicked");
			}
		} catch (Exception e) {
			logger.info(objectName + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
	}

	/**
	 * Method to select random element
	 * 
	 * @author Anant
	 * @return double value
	 */
	public String performActionRandomClickandGetText(String objectName1,
			String objectName2) {
		objectDetails = getObjectDetails(objectName1);
		// Double d = null;
		String value = null;
		List<WebElement> wd1;
		List<String> wd2;
		Random rd = new Random();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "");
					// d = Double.parseDouble(value);
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {

				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "");
					// d = Double.parseDouble(value);
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				Thread.sleep(100);
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				logger.info("the wd1 and wd2:" + wd1.size() + "    "
						+ wd2.size() + "    " + y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "");
					// d = Double.parseDouble(value);
				}
				Actions actions = new Actions(driver);
				actions.moveToElement(wd1.get(y)).click().perform();
				// wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "");
					// d = Double.parseDouble(value);
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + "is selcted as" + "<strong>"
						+ value + "</strong>");
				report.log(LogStatus.INFO, objectName1 + " " + "is selcted as"
						+ "<strong>" + value + "</strong>");
			}
		} catch (Exception e) {
			logger.info(objectName1 + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
		return value;
	}

	/**
	 * The method is used to click random and return Muliple value
	 * 
	 * @param objectName1
	 * @param objectName2
	 * @param objectName3
	 * @return
	 */
	public List<String> randomClickandGetMultipleText(String objectName1,
			String objectName2, String objectName3){
		objectDetails = getObjectDetails(objectName1);
		String value1 = null, value2 = null;
		List<WebElement> wd1;
		List<String> wd2, wd3, wd4;
		wd4 = new ArrayList<String>();
		Random rd = new Random();
		try {

			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				wd3 = performActionMultipleGetText(objectName3);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				value2 = wd3.get(y);
				if (!(value1.isEmpty() && value2.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
					value2 = value2.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {

				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				wd3 = performActionMultipleGetText(objectName3);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				value2 = wd3.get(y);
				if (!(value1.isEmpty() && value2.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
					value2 = value2.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				Thread.sleep(100);
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				wd3 = performActionMultipleGetText(objectName3);
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				value2 = wd3.get(y);

				if (!(value1.isEmpty() && value2.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
					value2 = value2.replace(pound, "").replace(euro, "");
				}
				Actions actions = new Actions(driver);
				// actions.moveToElement(wd1.get(y)).click().perform();
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				wd3 = performActionMultipleGetText(objectName3);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				value2 = wd3.get(y);
				if (!(value1.isEmpty() && value2.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
					value2 = value2.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + "is selcted as" + "<strong>"
						+ value1 + "</strong>");
				report.log(LogStatus.INFO, objectName1 + " " + "is selcted as"
						+ "<strong>" + value1 + "</strong>");
			}
			wd4.add(value1);
			wd4.add(value2);
		} catch (Exception e) {
			logger.info(objectName1 + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
		return wd4;

	}

	/**
	 * The method is used to verify the element present It accepts one parameter
	 * as object name
	 * 
	 * @param objectName
	 * @return boolean
	 */
	public boolean IsElementPresent(String objectName)  {
		boolean isElementDisplayed = false;
		objectDetails = getObjectDetails(objectName);
		String page = null;
		try {
			page = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			try {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					if (driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isDisplayed() == true) {
						report.log(LogStatus.PASS, objectName
								+ " Componet is displayed from " + page
								+ " page");
						logger.info(objectName + " Componet is displayed from "
								+ page + " page");
						isElementDisplayed = true;
					} else {
						report.log(LogStatus.INFO, objectName
								+ " Component is not displayed from " + page
								+ " page");
						logger.info(objectName
								+ " Componet is not displayed from " + page
								+ " page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					if (driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isDisplayed() == true) {
						report.log(LogStatus.PASS, objectName
								+ " Componet is displayed from " + page
								+ " page");
						logger.info(objectName + " Componet is displayed from "
								+ page + " page");
						isElementDisplayed = true;
					} else {
						report.log(LogStatus.INFO, objectName
								+ " Component is not displayed from " + page
								+ " page");
						logger.info(objectName
								+ " Componet is not displayed from " + page
								+ " page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					if (driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isDisplayed() == true) {
						report.log(LogStatus.PASS, objectName
								+ " Componet is displayed from " + page
								+ " page");
						logger.info(objectName + " Componet is displayed from "
								+ page + " page");
						isElementDisplayed = true;
					} else {
						report.log(LogStatus.INFO, objectName
								+ " Component is not displayed from " + page
								+ " page");
						logger.info(objectName
								+ " Componet is not displayed from " + page
								+ " page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					if (driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.isDisplayed() == true) {
						report.log(LogStatus.PASS, objectName
								+ " Componet is displayed from " + page
								+ " page");
						logger.info(objectName + " Componet is displayed from "
								+ page + " page");
						isElementDisplayed = true;
					} else {
						report.log(LogStatus.INFO, objectName
								+ " Component is not displayed from " + page
								+ " page");
						logger.info(objectName
								+ " Componet is not displayed from " + page
								+ " page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
			} catch (Exception e) {
				logger.error(objectName + "*******" + e.getMessage());
				report.log(LogStatus.INFO, objectName
						+ " Component is not displayed from " + page + " page");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, objectName
					+ " Componet doesnt contain page");
		}
		return isElementDisplayed;
	}

	/**
	 * The method is used to setup the browser stack
	 * 
	 * @throws IOException
	 */
	public void BrowserSetupBrowserStack() throws IOException {
		String URL = "https://" + browserStack_UserName + ":"
				+ browserStack_Password + "@hub-cloud.browserstack.com/wd/hub";
		String brwserName, browserVersion, osOrPlat, osVers, deviceVal;
		brwserName = browserName;
		browserVersion = browserVer;
		osOrPlat = osOrPlatform;
		osVers = osVersion;
		deviceVal = device;
		/*System.setProperty("http.proxyHost", "10.40.1.98");
		System.setProperty("http.proxyPort", "8080");
		System.setProperty("https.proxyHost", "10.40.1.98");
		System.setProperty("https.proxyPort", "8080");*/
		DesiredCapabilities caps = new DesiredCapabilities();
		result = true;
		try {
			// cap1=new DesiredCapabilities();
			if (brwserName.equalsIgnoreCase("firefox")) {
				logger.info("Firefox browser");
				caps.setCapability("browser", brwserName);
				caps.setCapability("browser_version", browserVersion);
				caps.setCapability("os", osOrPlat);
				caps.setCapability("os_version", osVers);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Firefox driver has started");
			} else if (brwserName.equalsIgnoreCase("Firefox50")) {
				logger.info("Firefox browser");
				caps.setCapability("browser", brwserName);
				caps.setCapability("browser_version", browserVersion);
				caps.setCapability("os", osOrPlat);
				caps.setCapability("os_version", osVers);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Firefox driver has started");
			} else if (brwserName.equalsIgnoreCase("Chrome")) {
				logger.info("======In Chrome=====");
				caps.setCapability("browser", brwserName);
				caps.setCapability("browser_version", browserVersion);
				caps.setCapability("os", osOrPlat);
				caps.setCapability("os_version", osVers);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Chrome driver has started");
			} else if (brwserName.equalsIgnoreCase("Safari")) {
				logger.info("++++++++Safari++++++");
				caps.setCapability("browser", brwserName);
				caps.setCapability("browser_version", browserVersion);
				caps.setCapability("os", osOrPlat);
				caps.setCapability("os_version", osVers);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Safari driver has started");
			} else if (brwserName.equalsIgnoreCase("IE")) {
				caps.setCapability("browser", brwserName);
				caps.setCapability("browser_version", browserVersion);
				caps.setCapability("os", osOrPlat);
				caps.setCapability("os_version", osVers);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO,"Internet Explorer driver has started");
			} else if (brwserName.equalsIgnoreCase("Iphone5S")) {
				logger.info("++++++++In Iphone++++++");
				caps.setCapability("browserName", "iphone");
				caps.setCapability("platform", osOrPlat);
				caps.setCapability("device", device);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("realMobile", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Iphone5s driver has started");
			} else if (brwserName.equalsIgnoreCase("Iphone6S")) {
				logger.info("++++++++In Iphone++++++");
				caps.setCapability("browserName", "iphone");
				caps.setCapability("platform", osOrPlat);
				caps.setCapability("device", deviceVal);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("realMobile", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Iphone6s driver has started");
			} else if (brwserName.equalsIgnoreCase("Android")) {
				logger.info("++++++++in Andriod++++++");
				caps.setCapability("browserName", brwserName);
				caps.setCapability("platform", osOrPlat);
				caps.setCapability("device", deviceVal);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("realMobile", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Android driver has started");
			} else if (brwserName.equalsIgnoreCase("Ipad")) {
				logger.info("++++++++in iPad++++++");
				caps.setCapability("browserName", brwserName);
				caps.setCapability("platform", osOrPlat);
				caps.setCapability("device", deviceVal);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("realMobile", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "Ipad driver has started");
			} else if (brwserName.equalsIgnoreCase("AndriodTab")) {
				caps.setCapability("browserName", brwserName);
				caps.setCapability("platform", osOrPlat);
				caps.setCapability("device", deviceVal);
				caps.setCapability("browserstack.debug", "true");
				caps.setCapability("browserstack.local", "true");
				caps.setCapability("realMobile", "true");
				caps.setCapability("project", "P2");
				caps.setCapability("build", "2.0");
				driver = new RemoteWebDriver(new URL(URL), caps);
				report.log(LogStatus.INFO, "AndroidTab driver has started");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				Assert.assertTrue(result, "Set up Test");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	/**
	 * The method is used to create the browser instance Which accepts one
	 * parameter as browser name
	 * 
	 * @param browser
	 * @throws MalformedURLException
	 */
	public void BrowserSetup() throws MalformedURLException {
		result = true;
		try {
			/*
			 * org.openqa.selenium.Proxy proxy=new org.openqa.selenium.Proxy();
			 * proxy
			 * .setSslProxy("10.40.1.98:8080").setHttpProxy("10.40.1.98:8080");
			 */
			String browser = browserName;
			cap1 = new DesiredCapabilities();
			// cap1.setCapability(CapabilityType.PROXY, proxy);
			if (browser.equalsIgnoreCase("Firefox")) {
				/*
				 * cap1 = new DesiredCapabilities();
				 * cap1.setBrowserName("firefox");
				 * cap1.setPlatform(Platform.XP); driver=new RemoteWebDriver(new
				 * URL("http://10.174.16.156:5566/wd/hub"),cap1);
				 */
				System.setProperty("webdriver.gecko.driver",
						CONSTANTS_GECKO_DRIVER_PATH);
				cap1.setCapability("marionette", true);
				driver = new FirefoxDriver(cap1);
				wait = new WebDriverWait(driver, 25);
				report.log(LogStatus.INFO, "Firefox driver has started");
				logger.info("Firefox driver has started");

			} else if (browser.equalsIgnoreCase("Chrome")) {
				/*
				 * System.setProperty(CONSTANTS_CHROME_PROPERTY,
				 * getProjectPath() + CONSTANTS_CHROME_DRIVER_PATH);
				 */
				System.setProperty(CONSTANTS_CHROME_PROPERTY,
						CONSTANTS_CHROME_DRIVER_PATH);
				driver = new ChromeDriver();
				wait = new WebDriverWait(driver, 25);
				report.log(LogStatus.INFO, "Chrome driver has started");
				logger.info("Chrome driver has started");
			} else if (browser.equalsIgnoreCase("PhantomJS")) {
				/*
				 * System.setProperty(CONSTANTS_PHANTOM_PROPERTY,
				 * getProjectPath() + CONSTANTS_PHANTOM_DRIVER_PATH);
				 */
				System.setProperty(CONSTANTS_PHANTOM_PROPERTY,
						CONSTANTS_PHANTOM_DRIVER_PATH);
				cap1.setJavascriptEnabled(true);
				cap1.setCapability("takesScreenshot", true);
				cap1.setCapability(
						PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
						getProjectPath() + CONSTANTS_PHANTOM_DRIVER_PATH);
				driver = new PhantomJSDriver(cap1);
				driver.manage().window().setSize(new Dimension(1920, 1200));

				wait = new WebDriverWait(driver, 25);
				report.log(LogStatus.INFO, "PhantomJS/Ghost driver has started");
				logger.info("PhantomJS/Ghost driver has started");
			} else if (browser.equalsIgnoreCase("IE")) {
				/*
				 * System.setProperty(CONSTANTS_IE_PROPERTY, getProjectPath() +
				 * CONSTANTS_IE_DRIVER_PATH);
				 */
				System.setProperty(CONSTANTS_IE_PROPERTY,
						CONSTANTS_IE_DRIVER_PATH);
				cap1.internetExplorer();
				cap1.setCapability(
						InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				driver = new InternetExplorerDriver(cap1);
				// driver = new InternetExplorerDriver();
				wait = new WebDriverWait(driver, 25);

				report.log(LogStatus.INFO,
						"Internet Explorer driver has started");
				logger.info("Internet Explorer driver has started");
			}

		} finally {
			try {
				Assert.assertTrue(result, "Set up Test");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	/**
	 * The method used for running the scripts on remote machines
	 * 
	 * @param browserandnodeUrl
	 * @throws MalformedURLException
	 */
	public void BrowserSetupgrid() throws MalformedURLException {
		String browser, nodeUrl;
		// String browserandnodeUrl = browserName;
		// String[] temp=browserandnodeUrl.split(",");
		// browser=temp[0];
		// nodeUrl=temp[1];
		browser = browserName;
		nodeUrl = CONSTANTS_IPDETAILS;
		logger.info("in browser set up " + browser + "--" + nodeUrl + "Thread"
				+ Thread.currentThread().getName());
		result = true;
		try {
			cap1 = new DesiredCapabilities();
			if (browser.equalsIgnoreCase("Firefox")) {
				System.setProperty("webdriver.gecko.driver",
						CONSTANTS_GECKO_DRIVER_PATH);
				cap1.setCapability("marionette", true);
				cap1 = DesiredCapabilities.firefox();
				cap1.setBrowserName("firefox");
				cap1.setPlatform(Platform.WINDOWS);
				driver = new RemoteWebDriver(new URL(nodeUrl), cap1);
				wait = new WebDriverWait(driver, 40);
				report.log(LogStatus.INFO, "Firefox driver has started");
				logger.info("Firefox driver has started");

			} else if (browser.equalsIgnoreCase("Chrome")) {
				/*
				 * System.setProperty(CONSTANTS_CHROME_PROPERTY,
				 * getProjectPath() + CONSTANTS_CHROME_DRIVER_PATH);
				 */
				System.setProperty(CONSTANTS_CHROME_PROPERTY,
						CONSTANTS_CHROME_DRIVER_PATH);
				cap1 = DesiredCapabilities.chrome();
				cap1.setBrowserName("chrome");
				cap1.setPlatform(Platform.WINDOWS);
				driver = new RemoteWebDriver(new URL(nodeUrl), cap1);
				wait = new WebDriverWait(driver, 40);
				report.log(LogStatus.INFO, "Chrome driver has started");
				logger.info("Chrome driver has started");
			} else if (browser.equalsIgnoreCase("PhantomJS")) {
				System.setProperty(CONSTANTS_PHANTOM_PROPERTY,
						CONSTANTS_PHANTOM_DRIVER_PATH);
				cap1.setJavascriptEnabled(true);
				cap1.setCapability("takesScreenshot", true);
				cap1.setCapability(
						PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
						getProjectPath() + CONSTANTS_PHANTOM_DRIVER_PATH);
				driver = new PhantomJSDriver(cap1);
				driver.manage().window().setSize(new Dimension(1920, 1200));

				wait = new WebDriverWait(driver, 40);
				report.log(LogStatus.INFO, "PhantomJS/Ghost driver has started");
				logger.info("PhantomJS/Ghost driver has started");
			} else if (browser.equalsIgnoreCase("IE")) {
				/*
				 * System.setProperty(CONSTANTS_IE_PROPERTY, getProjectPath() +
				 * CONSTANTS_IE_DRIVER_PATH);
				 */
				System.setProperty(CONSTANTS_IE_PROPERTY,
						CONSTANTS_IE_DRIVER_PATH);
				cap1 = DesiredCapabilities.internetExplorer();
				cap1.setBrowserName("internet explorer");
				cap1.setPlatform(Platform.WINDOWS);
				cap1.setCapability(
						InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
				cap1.setCapability(
						InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
				driver = new RemoteWebDriver(new URL(nodeUrl), cap1);
				wait = new WebDriverWait(driver, 40);
				report.log(LogStatus.INFO,
						"Internet Explorer driver has started");
				logger.info("Internet Explorer driver has started");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				Assert.assertTrue(result, "Set up Test");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	/**
	 * The method used to switch the frames It accepts one parameter as object
	 * name
	 * 
	 * @param objectName
	 */
	public void SwitchToFrames(String objectName){
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				WebElement element = driver.findElement(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				driver.switchTo().frame(element);
				report.log(LogStatus.INFO, objectName + " is clicked");
				logger.info(objectName + " is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				WebElement element = driver.findElement(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				driver.switchTo().frame(element);
				report.log(LogStatus.INFO, objectName + " is clicked ");
				logger.info(objectName + " is clicked");
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				WebElement element = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				driver.switchTo().frame(element);
				report.log(LogStatus.INFO, objectName + " is clicked");
				logger.info(objectName + "is clicked");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				WebElement element = driver.findElement(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				driver.switchTo().frame(element);

				report.log(LogStatus.INFO, objectName + " is clicked");
				logger.info(objectName + " is clicked");
			}

		} catch (Exception e) {
			logger.info(objectName + " is not clicked **********");
			report.log(LogStatus.ERROR, objectName + " is not clicked");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			// Assert.assertTrue(result, "Verifying Page Display");
		}
	}

	/**
	 * Method to perform send text operation for the specified object by
	 * clearing the existing text, method would determine the object locator
	 * type from OR and perform the action accordingly as per the object locator
	 * type
	 * 
	 * @date 18th September 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @param Test
	 *            data to be sent as text
	 * @return void 
	 */
	public void performActionSingleBackSpaceAndEnterText(String objectName,
			String testData){
		result = true;
		objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.BACK_SPACE));

					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.BACK_SPACE));
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.BACK_SPACE));
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.BACK_SPACE));
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to perform send text operation for the specified object by
	 * clearing the existing text, method would determine the object locator
	 * type from OR and perform the action accordingly as per the object locator
	 * type
	 * 
	 * @date 18th September 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository
	 * @param Test
	 *            data to be sent as text
	 * @return void
	 */
	public void performActionSelectAllAndEnterText(String objectName,
			String testData) {
		result = true;
		objectDetails = getObjectDetails(objectName);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.CONTROL, "a"),
									Keys.DELETE);

					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.CONTROL, "a"),
									Keys.DELETE);
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.CONTROL, "a"),
									Keys.DELETE);
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(Keys.chord(Keys.CONTROL, "a"),
									Keys.DELETE);
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to move to specified object, method would determine the object
	 * locator type from OR and perform the action accordingly as per the object
	 * locator type Note: Object type of first and second should be same
	 * 
	 * @date 23rd June 2015
	 * @author Hima
	 * @param Object
	 *            name as mentioned in the object repository.
	 * @return void
	 */
	public void performActionMoveToElement(String objecttoMove)
			throws InterruptedException{
		result = true;
		try {
			objectDetails = getObjectDetails(objecttoMove);
			waitForElementToBeDisplayed(objecttoMove);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				WebElement element = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Actions action = new Actions(driver);
				action.moveToElement(element).build().perform();
				report.log(LogStatus.INFO, objecttoMove
						+ " Mousoever is happened");
				logger.info(objecttoMove + "  Mousoever is happened");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				WebElement element = driver.findElement(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Actions action = new Actions(driver);
				action.moveToElement(element).build().perform();
				report.log(LogStatus.INFO, objecttoMove
						+ " Mousoever is happened");
				logger.info(objecttoMove + "  Mousoever is happened");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
				WebElement element = driver.findElement(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Actions action = new Actions(driver);
				action.moveToElement(element).build().perform();
				report.log(LogStatus.INFO, objecttoMove
						+ " Mousoever is happened");
				logger.info(objecttoMove + "  Mousoever is happened");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				WebElement element = driver.findElement(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Actions action = new Actions(driver);
				action.moveToElement(element).build().perform();
				report.log(LogStatus.INFO, objecttoMove
						+ " Mousoever is happened");
				logger.info(objecttoMove + "  Mousoever is happened");
			}
		} catch (Exception e) {
			report.log(LogStatus.INFO, objecttoMove
					+ " Mousoever is not happened");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info(objecttoMove + "  Mousoever is not happened");
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method to Verify Actual and Expected values
	 * 
	 * @date Sept 2015
	 * @param String
	 *            Actual,String Expected
	 * @author Madan
	 * @return void
	 */
	public void AssertEquals(String Actual, String Expected)
			throws InvocationTargetException {
		try {
			String Actual1 = performActionGetText(Actual);
			Assert.assertEquals(Actual1, Expected);

		} catch (Exception e) {
			logger.error("===assert failed===");
		}
	}

	/**
	 * Method to Verify the whether the expected page is displayed or not
	 * 
	 * @date Sept 2015
	 * @param String
	 *            ObjectName,String PageDisplay
	 * @author Madan Updated By Anant
	 * @return void
	 */
	public void PageDisplay(String ObjectName, String pageDisplay)  {
		// objectDetails = getObjectDetails(ObjectName);
		pageDisplay = pageDisplay.toUpperCase();
		String pageSrc = driver.getPageSource(), errMsg;
		String page = null;
		try {
			page = ObjectName.substring(ObjectName.indexOf("_") + 1,
					ObjectName.lastIndexOf("_"));
			String element = performActionGetText(ObjectName);
			report.log(LogStatus.INFO, "the element value of page:" + element);
			logger.info("the element value of page:" + element + "    "
					+ pageDisplay);

			if (element != null) {
				if (element
						.toUpperCase()
						.replace(" ", "")
						.replace(space, "")
						.trim()
						.contains(
								pageDisplay.replace(" ", "").replace(space, "")
										.trim())) {
					report.log(LogStatus.PASS, ObjectName
							+ " Component is Displayed from " + page
							+ " Successfully");
				} else if (pageSrc.toUpperCase().contains(pageDisplay.trim())) {
					report.log(LogStatus.PASS, ObjectName
							+ " Component is Displayed from " + page
							+ " Successfully");
				} else {
					report.log(LogStatus.FAIL, ObjectName
							+ " Component is not displayed from " + page
							+ " page as page displayed ");
					report.attachScreenshot(takeScreenShotExtentReports());
					Assert.assertTrue(false, ObjectName + "Page Not Displayed");
				}
			} else if (pageSrc.contains(pageDisplay)) {
				report.log(LogStatus.PASS, ObjectName
						+ " Component is Displayed from " + page
						+ " Successfully");
			} else if (pageSrc.contains("Technical Difficulties")) {
				errMsg = "Technical Difficulties";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page diaplayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");

			} else if (pageSrc.contains("Service temporarily unavailable")) {
				errMsg = "Service temporarily unavailable";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page displayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			} else if (pageSrc.contains("All Gone")) {
				errMsg = "All Gone";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page displayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			} else if (pageSrc.toLowerCase().contains("Please select a card type.".toLowerCase())) {
				errMsg = "Please select a card type.";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page displayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			} else if (pageSrc.toLowerCase().contains("Card type doesn't match with the Card Number.".toLowerCase())) {
				errMsg = "Card type doesn't match with the Card Number.";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page displayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			}
			
			else {
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page hence pagenot loaded properly");
				logger.info("The Given text is not present in the page as page not loaded properly");
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The array index out of bound execption in pageDisplay method:"
					+ a.getMessage());
		} catch (Exception e) {
			logger.error("The execption occured as given object " + ObjectName
					+ " doesnt have the page value " + e.getMessage());
			report.log(LogStatus.FAIL, ObjectName
					+ " Component is not displayed from " + page
					+ " page hence pagenot loaded properly");
			report.attachScreenshot(takeScreenShotExtentReports());
			Assert.assertTrue(false, ObjectName + "Page Not Displayed");
		}
	}

	/**
	 * The method is used to enter child ages It receives 2 parameters It
	 * returns of void
	 * 
	 * @author-Chaitra
	 * @param objectName
	 *            , testData
	 */
	public void performActionEnterAges_MC(String objectName, String testData)  {
		objectDetails = getObjectDetails(objectName);
		List<WebElement> textBoxes;

		try {
			int j = 0;
			testData = fetchTestDataFromMap(testData);
			String[] testVal = testData.split(",");
			waitForElementToBeDisplayed(objectName);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				textBoxes = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement ele1 : textBoxes) {
					ele1.clear();
					ele1.sendKeys(testVal[j]);
					Thread.sleep(4000L);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testVal[j]);
					j++;
				}
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				textBoxes = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				for (WebElement ele1 : textBoxes) {
					ele1.clear();
					ele1.sendKeys(testVal[j]);
					Thread.sleep(4000L);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testVal[j]);
					j++;
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * The method is used to scroll the window bar It returns void No paramater
	 * required
	 */
	public void scrollWindow() {
		try {
			((JavascriptExecutor) driver).executeScript(
					"window.scrollBy(0,250)", "");
			// report.log(LogStatus.INFO, objectName + " is clicked");
			logger.info("The window bar scrolled");
		} catch (Exception e) {
			// report.log(LogStatus.INFO, objectName + " is clicked");
			logger.info("The window bar is not scrolled");
			logger.error(e);
		}
	}

	/**
	 * Method to move the control to the active window
	 * 
	 * @date Sept 2015
	 * @author Madan
	 * @return void
	 */
	public void ActiveWindow() throws InterruptedException {
		String element = null;
		AllWindowHandles = new ArrayList<String>(driver.getWindowHandles());
		Iterator<String> itr = AllWindowHandles.iterator();
		while (itr.hasNext()) {
			element = itr.next();
			logger.info(element + " ");
		}
		driver.switchTo().window(element);
	}

	/**
	 * Method to take the screenshot
	 * 
	 * @date Sept 2015
	 * @author Madan
	 * @return void
	 */
	public void Screenshot() {
		report.attachScreenshot(takeScreenShotExtentReports());
	}

	/**
	 * Method to move to ParentFrame from the existing frame
	 * 
	 * @date Sept 2015
	 * @author Madan
	 * @return void
	 */
	public void ParentFrame() {
		driver.switchTo().defaultContent();
	}

	/**
	 * Method to launch the browser on the mobile with the required
	 * configuration
	 * 
	 * @date Sept 2015
	 * @author Suresh
	 * @return void
	 */
	/*
	 * public void MobileBrowserSetup(String browser) throws Exception { //
	 * ffdriver = new FirefoxDriver();
	 * 
	 * FirefoxProfile profile = new FirefoxProfile();
	 * 
	 * profile.setPreference( "general.useragent.override",
	 * "Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_0 like Mac OS X; en-us) AppleWebKit/528.18 (KHTML, like Gecko) Version/4.0 Mobile/7A341 Safari/528.16"
	 * ); driver = new FirefoxDriver(profile); Dimension dim = new
	 * Dimension(360, 540); driver.manage().window().setSize(dim);
	 *//**
	 * Set Desired capabilities to test on real android device. Author:
	 * Suresh Jashti.
	 */

	/*
	 * DesiredCapabilities capabilities = DesiredCapabilities.android();
	 * capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
	 * capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
	 * "Android"); capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,
	 * "Galaxy S4"); capabilities.setCapability(MobileCapabilityType.VERSION,
	 * "4.4.2"); //capabilities.setCapability(key, value); // appWaitActivity =
	 * .DispatchActivity capabilities.setCapability("appWaitActivity",
	 * ".DispatchActivity"); capabilities.setCapability("appWaitActivity",
	 * ".StartActivity"); capabilities.setCapability("udid","04a78a4409bc0dd2");
	 * capabilities.setCapability("autoLaunch", true); //create a
	 * RemoteWebDriver, the default port for Appium is 4723 driver = new
	 * RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
	 * driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
	 * driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
	 * 
	 * }
	 */
	/**
	 * The method is used to verify th element not to be displayed
	 * 
	 * @param objectName
	 * @return boolean
	 */
	public boolean IsElementNotPresent(String objectName) {
		boolean isElementDisplayed = false;
		objectDetails = getObjectDetails(objectName);
		String page = null;

		try {
			page = objectName.substring(objectName.indexOf("_") + 1,
					objectName.lastIndexOf("_"));
			try {
				if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
						.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON))
						|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_IMAGE))
						|| (objectDetails.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT) || (objectDetails
								.get(CONSTANTS_OBJECT_TYPE)
								.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_LINK)))) {
					if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_CHECKBOX)) {
						if (driver.findElement(
								By.id(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.FAIL, objectName
									+ " Component is  displayed from " + page
									+ " page");
							logger.info(objectName
									+ " Component is  displayed from " + page
									+ " page");
							isElementDisplayed = true;
						} else {
							report.log(LogStatus.PASS, objectName
									+ " Component is not displayed from "
									+ page + " page");
							logger.info(objectName
									+ " Component is not displayed from "
									+ page + " page");
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
						if (driver.findElement(
								By.name(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.FAIL, objectName
									+ " Component is  displayed from " + page
									+ " page");
							logger.info(objectName
									+ " Component is  displayed from " + page
									+ " page");
							isElementDisplayed = true;
						} else {
							report.log(LogStatus.PASS, objectName
									+ " Component is not displayed from "
									+ page + " page");
							logger.info(objectName
									+ " Component is not displayed from "
									+ page + " page");
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
						if (driver.findElement(
								By.xpath(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.FAIL, objectName
									+ " Component is  displayed from " + page
									+ " page");
							logger.info(objectName
									+ " Component is  displayed from " + page
									+ " page");
							isElementDisplayed = true;
						} else {
							report.log(LogStatus.PASS, objectName
									+ " Component is not displayed from "
									+ page + " page");
							logger.info(objectName
									+ " Component is not displayed from "
									+ page + " page");
						}
					} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
							.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
						if (driver.findElement(
								By.cssSelector(objectDetails
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
								.isDisplayed() == true) {
							report.log(LogStatus.FAIL, objectName
									+ " Component is  displayed from " + page
									+ " page");
							logger.info(objectName
									+ " Component is  displayed from " + page
									+ " page");
							isElementDisplayed = true;
						} else {
							report.log(LogStatus.PASS, objectName
									+ " Component is not displayed from "
									+ page + " page");
							logger.info(objectName
									+ " Component is not displayed from "
									+ page + " page");
						}
					}
				}
			} catch (NoSuchElementException e) {
				report.log(LogStatus.PASS, objectName
						+ " Component is not displayed from " + page + " page");
				logger.info(objectName + " Component is not displayed from "
						+ page + " page");
			} catch (Exception e) {
				logger.error(objectName + "*******" + e.getMessage());
				report.log(LogStatus.PASS, objectName
						+ " Component is not displayed from " + page + " page");
			}
		} catch (Exception e) {
			logger.error(objectName + "*******" + e.getMessage());
			report.log(LogStatus.INFO, objectName + "*******"
					+ "Doesnt have the page vale");
		}
		return isElementDisplayed;
	}

	/*
	 * public boolean verifyIsElementPresent(String objectName) { boolean
	 * isElementDisplayed = false; objectDetails = getObjectDetails(objectName);
	 * String page = null;
	 * 
	 * try{ page=objectName.substring(objectName.indexOf("_")+1,
	 * objectName.lastIndexOf("_"));
	 * 
	 * try{ waitForElementToBeDisplayed(objectName);
	 * if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR
	 * ).equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
	 * if(driver.findElement(By
	 * .id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE
	 * ))).isDisplayed()==true){ report.log(LogStatus.PASS, objectName+
	 * " Component is displayed from "+page+" page"); isElementDisplayed=true;}
	 * 
	 * else{report.log(LogStatus.FAIL,
	 * objectName+" Component is not displayed from "+page+" page");
	 * report.attachScreenshot(takeScreenShotExtentReports()); } } else
	 * if(objectDetails .get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
	 * CONSTANTS_OBJECT_LOCATOR_NAME )) {
	 * if(driver.findElement(By.name(objectDetails.get(
	 * CONSTANTS_OBJECT_LOCATOR_VALUE))).isDisplayed()==true){
	 * report.log(LogStatus.PASS, objectName+
	 * " Component is displayed from "+page+" page"); isElementDisplayed=true;}
	 * else{report.log(LogStatus.FAIL,
	 * objectName+" Component is not displayed from "+page+" page");
	 * report.attachScreenshot(takeScreenShotExtentReports()); } }
	 * 
	 * else if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
	 * CONSTANTS_OBJECT_LOCATOR_XPATH)) {
	 * if(driver.findElement(By.xpath(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE))).isDisplayed()==true){
	 * report.log(LogStatus.PASS, objectName+
	 * " Component is displayed from "+page+" page"); isElementDisplayed=true;}
	 * else{report.log(LogStatus.FAIL,
	 * objectName+" Component is not displayed from "+page+" page");
	 * report.attachScreenshot(takeScreenShotExtentReports()); } }
	 * 
	 * else if(objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
	 * CONSTANTS_OBJECT_LOCATOR_CSS)) {
	 * if(driver.findElement(By.cssSelector(objectDetails
	 * .get(CONSTANTS_OBJECT_LOCATOR_VALUE))).isDisplayed()==true){
	 * report.log(LogStatus.PASS, objectName+
	 * " Component is displayed from "+page+" page"); isElementDisplayed=true;}
	 * else{report.log(LogStatus.FAIL,
	 * objectName+" Component is not displayed from "+page+" page");
	 * report.attachScreenshot(takeScreenShotExtentReports()); } } }
	 * catch(Exception e){ logger.error(objectName +"*******"+e.getMessage()); }
	 * }catch(Exception e){ logger.error(objectName +"*******"+e.getMessage());
	 * report.log(LogStatus.INFO,objectName
	 * +"*******"+"Doesnt have the page vale"); } return isElementDisplayed; }
	 */

	public void pageloadandback() {
		String storevalue = "Technical difficulties.";
		String pageload = driver.getPageSource();
		if (pageload.contains(storevalue)) {
			logger.error("Expected Page is not displayed successfully");
			report.log(LogStatus.FAIL, "Page has not launched successfully!");
			report.log(LogStatus.INFO,
					"Page has not launched successfully,Navigating back to Holiday Summary page");

			report.attachScreenshot(takeScreenShotExtentReports());
			driver.navigate().back();
		}
	}

	// ////////
	public void PassMessage(String Message) {
		logger.info(Message);
		report.log(LogStatus.INFO, Message);
	}

	// ////////////////
	/**
	 * Method to wait by using sleep
	 * 
	 * @date nov 2015
	 * @author dheeraj
	 */
	public void PerformActionSleep(String a) {
		try {
			long l = Long.parseLong(a);
			Thread.sleep(l);
			report.log(LogStatus.INFO, "waiting for " + l + " milliseconds");
			logger.info(a);
			logger.info(l);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// //////////
	public void FindBrand() throws InterruptedException {
		try {
			String BeforeUrl = driver.getCurrentUrl();
			String[] splittedURL = BeforeUrl.split("\\.");
			String BeforeUrlBrand = splittedURL[1].replace("prjuat", "")
					.toUpperCase();
			performActionClick("THOMSONANC_LOGINPAGE_LOGINBUTTON");
			Thread.sleep(3000);
			String AfterUrl = driver.getCurrentUrl();
			String[] splittedURL1 = AfterUrl.split("\\.");

			String AfterUrlBrand = splittedURL1[1].replace("prjuat", "")
					.toUpperCase();
			if (AfterUrlBrand.equalsIgnoreCase(BeforeUrlBrand)) {
				if (IsElementPresent("THOMSONANC_SUMMARYPAGE_MYBOOKING") == true) {
					logger.info("Navigation from " + "<strong>" + BeforeUrl
							+ "<strong>" + " to " + AfterUrl
							+ " as Booking Reference Number belongs to "
							+ AfterUrlBrand);
					report.log(LogStatus.INFO, "Navigation from " + "<strong>"
							+ BeforeUrl + "<strong>" + " to " + AfterUrl
							+ " as Booking Reference Number belongs to "
							+ AfterUrlBrand);
					report.log(LogStatus.INFO, "In Short Navigation from "
							+ BeforeUrlBrand + " to " + AfterUrlBrand
							+ " as Booking Reference Number belongs to "
							+ AfterUrlBrand);
				} else {
					String Errormessage = performActionGetText("THOMSONANC_LOGINPAGE_ERRORMESSAGE");
					report.log(LogStatus.FAIL,
							"Unable to Login into Application Beacuse of Error message being displyes as"
									+ Errormessage);

				}

			} else {
				performActionClick("THOMSONANC_LOGINPAGE_LOGINBUTTON");
				if (IsElementPresent("THOMSONANC_SUMMARYPAGE_MYBOOKING") == true) {
					logger.info("Navigation from " + BeforeUrl + " to "
							+ AfterUrl
							+ " as Booking Reference Number belongs to "
							+ AfterUrlBrand);
					report.log(LogStatus.INFO, "Navigation from " + BeforeUrl
							+ " to " + AfterUrl
							+ " as Booking Reference Number belongs to "
							+ AfterUrlBrand);
					report.log(LogStatus.INFO, "In Short Navigation from "
							+ BeforeUrlBrand + " to " + AfterUrlBrand
							+ " as Booking Reference Number belongs to "
							+ AfterUrlBrand);
				} else {
					String Errormessage = ("THOMSONANC_LOGINPAGE_ERRORMESSAGE");
					report.log(LogStatus.FAIL,
							"Unable to Login into Application Beacuse of Error message being displyes as"
									+ Errormessage);
				}
			}
		}

		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method is used to perform select from dropdown based on visible text
	 * for the specified object, method would determine the object locator type
	 * from OR and perform the action accordingly as per the object locator type
	 * 
	 * name as mentioned in the object repository. Note: xpath should be ending
	 * with select or with id of select and object type should be Select in OR
	 * 
	 * @param Test
	 *            data value to be selected
	 * @return void
	 */
	public void performActionSelectDropDown_SelectByVisibleText(
			String objectName, String testData) {

		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			testData = fetchTestDataFromMap(testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_SELECT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)) {

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {

					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByVisibleText(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					Select selectBox = new Select(driver.findElement(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByVisibleText(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByVisibleText(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByVisibleText(testData);
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " Select dropdown is not selected **********");
			report.log(LogStatus.ERROR, objectName
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * This method is used to perform select from dropdown based on id for the
	 * specified object, method would determine the object locator type from OR
	 * and perform the action accordingly as per the object locator type
	 * 
	 * name as mentioned in the object repository. Note: xpath should be ending
	 * with the select or with id of select and object type should be Select in
	 * OR
	 * 
	 * @param Test
	 *            data value to be selected
	 * @return void
	 */
	public void performActionSelectDropDown_SelectByIndex(String objectName,
			String testData){
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_SELECT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)) {

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByIndex(Integer.parseInt(testData));
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					Select selectBox = new Select(driver.findElement(By
							.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByIndex(Integer.parseInt(testData));
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + " value is selected as "
							+ testData);
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByIndex(Integer.parseInt(testData));
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

					Select selectBox = new Select(driver.findElement(By
							.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
					selectBox.selectByIndex(Integer.parseInt(testData));
					report.log(LogStatus.INFO, objectName
							+ " value is selected as " + testData);
					logger.info(objectName + "  value is selected as "
							+ testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " Select dropdown is not selected **********");
			report.log(LogStatus.ERROR, objectName
					+ " value is not selected as " + testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	// ****************************************** Mobile Methods
	// *****************************************

	/**
	 * Method to Browser Setup For Mobile.
	 * 
	 * @date 22nd/Jan/2016
	 * @author Suresh Jashti
	 * @return void
	 */
	public void MobileBrowserSetup() throws Exception {
		String DeviceID, Browser;
		result = true;
		DeviceID = device;
		Browser = browserName;
		DesiredCapabilities capabilities = DesiredCapabilities.android();
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, Browser);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,
				"Android");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus-5");
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,
				"5.0.1");
		capabilities.setCapability("appWaitActivity", ".DispatchActivity");
		capabilities.setCapability("appWaitActivity", ".StartActivity");
		capabilities.setCapability("udid", DeviceID);
		capabilities.setCapability("autoLaunch", true);
		driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"),
				capabilities);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 20);
	}

	/**
	 * Method to Verifiy the background color of the components
	 * 
	 * @date 22nd/March/2016
	 * @author Suresh Jashti
	 * @return void
	 */
	public void ColorVerificationForComponents(String objectName,
			String CheckColor) {
		objectDetails = getObjectDetails(objectName);
		result = true;

		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				WebElement X = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				String Y = Color.fromString(X.getCssValue("background-color"))
						.asHex();
				logger.info(Color.fromString(X.getCssValue("background-color"))
						.asHex());
				if (Y.equalsIgnoreCase(CheckColor)) {
					report.log(LogStatus.PASS, objectName
							+ "Expected Colorcode : " + CheckColor
							+ " is matching with Actual Colorcode : " + Y);
					logger.info(objectName + " Color is verified with"
							+ CheckColor);
				} else {
					report.log(LogStatus.FAIL, objectName
							+ " Expected color Code " + CheckColor
							+ "is not macting with Actual ColorCode");
				}
				logger.info(objectName + " is not matching with" + CheckColor);
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				WebElement X = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				String Y = Color.fromString(X.getCssValue("background-color"))
						.asHex();
				logger.info(Color.fromString(X.getCssValue("background-color"))
						.asHex());
				if (Y.equalsIgnoreCase(CheckColor)) {
					report.log(LogStatus.PASS, objectName
							+ "Expected Colorcode : " + CheckColor
							+ " is matching with Actual Colorcode : " + Y);
					logger.info(objectName + " Color is verified with "
							+ CheckColor);
				} else {
					report.log(LogStatus.FAIL, objectName
							+ " Expected color Code " + CheckColor
							+ "is not macting with Actual ColorCode");
				}
				logger.info(objectName + " is not matching with" + CheckColor);
			}

		} catch (Exception e) {
			logger.info(objectName + " Color Not Matching");
			// System.out.println(objectName + " value is not selected as "+
			// testData);
			report.log(LogStatus.ERROR, objectName + " Expected color Code "
					+ CheckColor + "is not macting with Actual ColorCode");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	/**
	 * Method for text color verifications
	 * 
	 * @date 22nd/March/2016
	 * @author Suresh Jashti
	 * @return void
	 */
	public void ColorVerificationForText(String objectName, String CheckColor)  {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				WebElement X = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				String Y = Color.fromString(X.getCssValue("color")).asHex();
				logger.info(Color.fromString(X.getCssValue("color")).asHex());
				if (Y.equalsIgnoreCase(CheckColor)) {
					report.log(LogStatus.PASS, objectName
							+ "Expected text Color Text : " + CheckColor
							+ " is matching with Actual text Color Text : " + Y);
					logger.info(objectName + " Text Color is verified with"
							+ CheckColor);
				} else {
					report.log(LogStatus.FAIL, objectName
							+ " Expected color Text " + CheckColor
							+ "is not macting with Actual Color Text" + Y);
				}
				logger.info(objectName + " is not matching with" + CheckColor);
			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				WebElement X = driver.findElement(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				String Y = Color.fromString(X.getCssValue("color")).asHex();
				logger.info(Color.fromString(X.getCssValue("color")).asHex());
				if (Y.equalsIgnoreCase(CheckColor)) {
					report.log(LogStatus.PASS, objectName
							+ "Expected Colorcode : " + CheckColor
							+ " is matching with Actual Color code : " + Y);
					logger.info(objectName + " Text Color is verified with "
							+ CheckColor);
				} else {
					report.log(LogStatus.FAIL, objectName
							+ " Expected color Text " + CheckColor
							+ "is not macting with Actual Color Text" + Y);
				}
				logger.info(objectName + " is not matching with" + CheckColor);
			}

		} catch (Exception e) {
			logger.info(objectName + " Text Color Not Matching");
			report.log(LogStatus.ERROR, objectName
					+ " Expected  text color Code " + CheckColor
					+ "is not macting with Actual text Color Code");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	public void MobileBrowserSetupChrome() throws Exception {
		//String DeviceName = "Google Nexus 5";
		String DeviceName = device;
		DesiredCapabilities capabilities;

		//String ChromeDriverPath = System.getProperty("user.dir")
			//	+ "/lib/chromedriver.exe";
	try{
		//String ChromeDriverPath = System.getProperty("user.dir").substring(0, 
				 // System.getProperty("user.dir").lastIndexOf(File.separator))+File.separator+"Jars"+File.separator;
		//System.setProperty("webdriver.chrome.driver", getProjectPath()
				//+ CONSTANTS_MOBILECHROMEDRIVER_PATH);
		System.setProperty("webdriver.chrome.driver",  CONSTANTS_CHROME_DRIVER_PATH);

		Map<String, String> mobileEmulation = new HashMap<String, String>();
		mobileEmulation.put("deviceName", DeviceName);

		Map<String, Object> chromeOptions = new HashMap<String, Object>();
		chromeOptions.put("mobileEmulation", mobileEmulation);

		capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
		driver = new ChromeDriver(capabilities);
	 }catch(Exception e){
		 logger.error("The chrome driver couldn't find in Jars folder:"+e);
	 }
	}

	/**
	 * Method to scroll down
	 * 
	 * @date 22nd/March/2016
	 * @author Suresh Jashti
	 * @return void
	 */
	public void Scroll(String Objectname) {
		String String = driver.findElement(
				By.xpath(objectDetails.get(Objectname))).getText();
		((JavascriptExecutor) driver)
				.executeScript("arguments[0].scrollIntoView();");
	}

	/**
	 * The method is used to scroll the window bar It returns void No paramater
	 * required
	 * 
	 * @author Samson
	 */
	public void scrollUp() {
		try {
			((JavascriptExecutor) driver).executeScript(
					"window.scrollBy(0,-250)", "");
			logger.info("The window bar scrolled");
		} catch (Exception e) {
			logger.info("The window bar is not scrolled");
			logger.error(e);
		}
	}

	/**
	 * Method to scroll down to a given element
	 * 
	 * @date 12 June 2016
	 * @author Samson
	 * @return void
	 */

	public void scrollIntoViewElement(String Objectname) {
		objectDetails = getObjectDetails(Objectname);
		WebElement element = driver.findElement(By.xpath(objectDetails
				.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView(true);", element);
		report.log(LogStatus.INFO, "Scrolled into view");
		logger.info("Scrolled into view");
	}

	/**
	 * Method to get atttribute size
	 * 
	 * @author dheersj
	 * @return int
	 */
	public int performActionGetSize(String objectName) {
		int size = 0;
		objectDetails = getObjectDetails(objectName);
		try {
			waitForElementToBeDisplayed(objectName);
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				size = driver.findElements(
						By.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))).size();
				logger.info("The size of " + objectName + "is : " + size);
				report.log(LogStatus.INFO, "The size of " + objectName
						+ "is : " + size);
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				size = driver.findElements(
						By.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))).size();
				logger.info("The size of " + objectName + "is : " + size);
				report.log(LogStatus.INFO, "The size of " + objectName
						+ "is : " + size);
			}
		} catch (Exception e) {
			logger.info(objectName + " could not get the size **********");
			report.log(LogStatus.INFO, objectName
					+ " could not get size **********");
			logger.error(e);
		}
		return size;
	}
/**
	 * The following method to capture the data from response using regex It
	 * fetches the data based on occurrence number we will provide to it Fetches
	 * the data based on the number of group will provide in the regex Fetches
	 * the based on the number of matches it finds in the response and
	 * concatenate it. and converts the the date into dd/MM/yyyy format
	 * Modified by Omar
	 */
	public String regexExtractorR(String regex, String response) throws ParseException {
		StringBuffer sb = new StringBuffer();
		int pos = 0, occur = 0;
		SimpleDateFormat fmt = new SimpleDateFormat();
		boolean flag = false;
		String regValue = null, val = "",singleVal="";
		try {
			if (regex.contains("^^")) {
				regValue = regex.substring(0, regex.indexOf("^^"));
				pos = Integer.parseInt(regex.substring(regex.indexOf("^^") + 2));
				flag = true;
			} else
				regValue = regex;
			try {
				Pattern pat = Pattern.compile(regValue);
				Matcher mat = pat.matcher(response);
				while (mat.find()) {
					if (flag){
						occur++;
					if (occur == pos) {
						sb.append(mat.group(1));
						return sb.toString();
						}	
					}
					else{
						sb.append(mat.group(1));
						return sb.toString();
					}
				}
			} catch (PatternSyntaxException p) {
				logger.error("Pattern syntax exeception occured in regexExtractor:" + p.getMessage());
			} catch (Exception e) {
				logger.error("Exeception occured in regexExtractor:" + e);
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("ArrayIndexOutOfBoundsException in regexExtractor" + a);
		} catch (Exception e) {
			logger.error("ArrayIndexOutOfBoundsException in regexExtractor" + e);
		}
		// System.out.println("the val:"+sb.toString());
		return sb.toString().replace("\r", "");
	}
	
	/**
	 * The method is used to switch control to the new tab on the browser
	 * 
	 * @author Samson
	 */
	public void moveToNewTab() {
		try {

			List<String> browserTabs = new ArrayList<String>(
					driver.getWindowHandles());
			// switch to new tab
			driver.switchTo().window(browserTabs.get(1));
			report.log(LogStatus.INFO, "New tab opened");
			logger.info("*********New tab opened*********");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "New tab not opened");
			logger.error("Error occured :" + e.getMessage());
			report.attachScreenshot(takeScreenShotExtentReports());
		}
	}

	/**
	 * The method is used to close current tab and switch control to the
	 * previous tab on the browser
	 * 
	 * @author Samson
	 */
	public void closeTab() {
		try {

			List<String> browserTabs = new ArrayList<String>(
					driver.getWindowHandles());
			// switch to new tab
			driver.close();
			driver.switchTo().window(browserTabs.get(0));
			report.log(LogStatus.INFO, "Closed tab");
			logger.info("*********Closed tab*********");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "Tab not closed");
			logger.error("Error occured :" + e.getMessage());
			report.attachScreenshot(takeScreenShotExtentReports());
		}
	}
	
	
	//Jmeter API Function startes here
	
	/**
	 * <p>The method to add the attributes 
	 * to parent tag </p>
	 * @author Anant
	 */
	private void addAttributesToParentTag(Document doc,Element arguments,String guiClass,String testClass,
			String testName,String enableStatus){
		try{
			createAttribute(doc, arguments, "guiclass", guiClass);
			createAttribute(doc, arguments, "testclass", testClass);
			createAttribute(doc, arguments, "testname", testName);
			createAttribute(doc, arguments, "enabled", enableStatus);
			logger.info("Successfully added attributes to Parent tag:");
		}catch(Exception err){
			logger.error("Exception in while "
					+ "adding the attributes to parent tag:"+err.getMessage());
		}
	}

	/**
	 * The method to verify the general page error
	 * @param varName
	 * @param pageText
	 * @return String
	 * @author Anant
	 */
	private String generalPageVerification(String varName,String pageText){
		String finalString  = "import org.apache.commons.lang.StringUtils;\n"+
				"responseResult=vars.get(\"responsebody\");\n"+
				"if(StringUtils.containsIgnoreCase(responseResult,"+"\""+pageText+"\""+")){\n" +
				"str2=\"[Page was rendered properly]\";"+"\n"+"vars.put("+"\""+varName+"\""+",str2);"+"\n"+"vars.put(\"error\",\"continue\");\n}\n" +
				"else\n"+
				"if(StringUtils.containsIgnoreCase(responseResult,\"Technical Difficulties\")){\n" +
				"str2=\"[Technical Difficulties]\";"+"\n"+"vars.put("+"\""+varName+"\""+",str2);"+"\n"+"vars.put(\"error\",\"stop\");\n}\n"+
				"else\n"+
				"if(StringUtils.containsIgnoreCase(responseResult,\"Service temporarily unavailable\")){\n"+
				"str2=\"[Service temporarily unavailable]\";"+"\n"+"vars.put("+"\""+varName+"\""+",str2);"+"\n"+"vars.put(\"error\",\"stop\");\n}\n"+
				"else\n"+
				"if(StringUtils.containsIgnoreCase(responseResult,\"All Gone\")){\n"+
				"str2=\"[All Gone]\";"+"\n"+"vars.put("+"\""+varName+"\""+",str2);"+"\n"+"vars.put(\"error\",\"stop\");\n}\n"+
				"else\n"+
				"str2=\"[page was not rendered properly]\";"+"\n"+"vars.put("+"\""+varName+"\""+",str2);"+"\n"+"vars.put(\"error\",\"stop\");\n}\n";	
		return finalString;
	}

	/**
	 * The method to create attribute
	 * @param doc
	 * @param root
	 * @param attrName
	 * @param attrValue#
	 * @author Anant
	 */
	private void createAttribute(Document doc,Element root,String attrName,String attrValue){
		try{
			Attr attr = doc.createAttribute(attrName);
			attr.setValue(attrValue);
			root.setAttributeNode(attr);
		}catch(Exception e){
			logger.error("Attribute didnt create as "
					+ "value not set for this:"+e.getMessage());
		}
	}

	/**
	 * The method to create element
	 * @param doc
	 * @param ele
	 * @param tagName
	 * @return
	 * @author Anant
	 */
	private Element createElement(Document doc,Element ele,String tagName){
		Element hashTree = null;
		try{
			hashTree = doc.createElement(tagName);
			ele.appendChild(hashTree);
			logger.info("Element was created:"+tagName);
			report.log(LogStatus.INFO, "Element created:"+tagName);
		}catch(Exception e){
			logger.error("Element didn't created:"+e.getMessage());
		}
		return hashTree;
	}

	/**
	 * The method used t
	 * @param searchText
	 * @author Anant
	 */
	private Element searchTagByText(Document doc,String searchText){ 
		boolean flag = false;
		Element element = null;
		try{	
			Element root = doc.getDocumentElement();
			NodeList listElement = root.getElementsByTagName("*");
			for(int j=0;j<listElement.getLength();j++){
				Element ele = (Element)listElement.item(j);
				if(ele.getAttribute("testname").toLowerCase().equals(searchText.toLowerCase())){
					flag = true;
					Node node = ele.getNextSibling();
					element = (Element)node.getNextSibling();
					break;
				}
			}
			if(flag == false){
				logger.info("The given search text was not present in the xml:");
			}
		}catch(Exception e){
			logger.error("Exception occred while searching the tag "
					+ "By given text:"+e.getMessage()); 
		}
		return element;
	}

	/**
	 * This method is used to add the plus one to string contain
	 * integer value
	 * ]
	 * @param intVal
	 * @return
	 * @author Anant
	 */
	private String addIntVal(String intVal){
		String retVal="";
		if(intVal.matches("\\-*\\d+")){
			Integer inVal = Integer.parseInt(intVal);
			inVal = inVal + 1;
			retVal = inVal.toString();
		}else{
			logger.error("String doesn't contain integer value:");
		}
		return retVal;
	}

	/**
	 * <p> The method to write content to xml file</p>
	 * @author Anant
	 */
	private void writeContentToXml(Document doc){
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(JMX_FILE_NAME));
			transformer.transform(source, result);
		} catch (TransformerConfigurationException e) {
			logger.error("Tranformer configur Exception while "
					+ "writting the content to XML file:"+e.getMessage());
			report.log(LogStatus.ERROR, "Tranformer configur Exception while "
					+ "writting the content to XML file:"+e.getMessage());
		} catch (TransformerException e) {
			logger.error("Tranformer exception while "
					+ "writting the content to XML file:"+e.getMessage());
			report.log(LogStatus.ERROR,"Tranformer exception while "
					+ "writting the content to XML file:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the empty hash tree </p>
	 * @author Anant
	 */
	public void createHashTree(){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element root = doc.getDocumentElement();
				Element hashTree = (Element)root.getElementsByTagName("hashTree").item(0);
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, hashTree, "hashTree");
				writeContentToXml(doc);
			}else
				logger.error("JMX file doesn't exist:");
		}catch(Exception e){
			logger.error("Exception occured while creating the Hah");
		}
	}

	/**
	 * The method to create the test plan.
	 * @author Anant
	 */
	public void createTestPlan(String testName){
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("jmeterTestPlan");
			doc.appendChild(rootElement);

			createAttribute(doc, rootElement, "version", "1.2");
			createAttribute(doc, rootElement, "properties", "2.8");
			createAttribute(doc, rootElement, "jmeter", "2.13 r1665067");

			Element hashTree = createElement(doc, rootElement, "hashTree");
			Element testPlan = createElement(doc, hashTree, "TestPlan");
			addAttributesToParentTag(doc, testPlan, "TestPlanGui", "TestPlan", testName, "true");

			Element stringProp = createElement(doc, testPlan, "stringProp");		
			createAttribute(doc, stringProp, "name", "TestPlan.comments");
			stringProp.appendChild(doc.createTextNode(""));

			Element boolProp = createElement(doc, testPlan, "boolProp");
			createAttribute(doc, boolProp, "name", "TestPlan.functional_mode");
			boolProp.appendChild(doc.createTextNode("false"));

			Element boolProp1 = createElement(doc, testPlan, "boolProp");
			createAttribute(doc, boolProp1, "name", "TestPlan.serialize_threadgroups");
			boolProp1.appendChild(doc.createTextNode("false"));

			Element elementProp = createElement(doc, testPlan, "elementProp");
			createAttribute(doc, elementProp, "name", "TestPlan.user_defined_variables");
			createAttribute(doc, elementProp, "elementType", "Arguments");
			addAttributesToParentTag(doc, elementProp, "ArgumentsPanel", "Arguments", "User Defined Variables", "true");

			Element collectionProp = createElement(doc, elementProp, "collectionProp");
			createAttribute(doc, collectionProp, "name", "Arguments.arguments");

			Element stringProp1 = createElement(doc, testPlan, "stringProp");
			createAttribute(doc, stringProp1, "name", "TestPlan.user_define_classpath");
			stringProp1.appendChild(doc.createTextNode(""));
			createElement(doc, hashTree, "hashTree");

			// write the content into xml file
			writeContentToXml(doc);
			logger.info("File Saved");
			report.log(LogStatus.INFO, "File Saved");
		} catch (ParserConfigurationException e) {
			logger.error("Parse configure exception in "
					+ "creating the Test Plan:"+e.getMessage());
			report.log(LogStatus.ERROR, "Parse configure exception in "
					+ "creating the Test Plan:"+e);
		} catch(Exception e){
			logger.error("Exception in creating the Test Plan:"+e);
			report.log(LogStatus.ERROR, "Exception in creating the Test Plan:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the user defined<p>
	 * @author Anant
	 */
	public void createUserDefinedVariable(String searchTextToAdd,String variableName,String variableValue){
		     String mapValue;
		    Map<String,String> pathNameAndValue = new LinkedHashMap<String,String>();
		     pathNameAndValue.put("path", jmeterHome);
		     pathNameAndValue.put("conffldpath", jmeterConfFldpath);
		     pathNameAndValue.put("tstfldpath", jmeterTstFldPath);
		     pathNameAndValue.put("rsltfldpath", jmeterConfFldpath);	    
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element arguments = createElement(doc, element, "Arguments");
				addAttributesToParentTag(doc,arguments,"ArgumentsPanel","Arguments",
						"User Defined Variables","true");

				Element collectionProp = createElement(doc, arguments, "collectionProp");
				createAttribute(doc, collectionProp, "name", "Arguments.arguments");

				String[] varNames = variableName.split("\\^");
				String[] varValues = variableValue.split("\\^");

				for(int v=0;v<varNames.length;v++){
					String keyText1,keyText2;
					keyText1 = varNames[v];
					if((keyText1.equalsIgnoreCase("path")||keyText1.equalsIgnoreCase("rsltfldpath"))||
							(keyText1.equalsIgnoreCase("conffldpath")||keyText1.equalsIgnoreCase("tstfldpath"))){
						mapValue = pathNameAndValue.get(keyText1.toLowerCase().trim());
						keyText2 = mapValue;
					}else{
					     try{
						      keyText2 = varValues[v];
					       }catch(Exception e){
						      keyText2 = "";
					      }
					    }
					if (keyText1.length() != 0) {
						Element elementProp = createElement(doc, collectionProp, "elementProp");
						createAttribute(doc, elementProp, "name", keyText1);
						createAttribute(doc, elementProp, "elementType", "Argument");

						Element stringProp1 = createElement(doc, elementProp, "stringProp");
						createAttribute(doc, stringProp1, "name", "Argument.name");
						stringProp1.setTextContent(keyText1);

						Element stringProp2 = createElement(doc, elementProp, "stringProp");
						createAttribute(doc, stringProp2, "name", "Argument.value");
						stringProp2.setTextContent(keyText2);

						Element stringProp3 = createElement(doc, elementProp, "stringProp");
						createAttribute(doc, stringProp3, "name", "Argument.metadata");
						stringProp3.setTextContent("=");
					} 
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("User Defined varaibale component created:");
				report.log(LogStatus.INFO, "User Defined varaibale component created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.INFO, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "User Defined Varaiable:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "User Defined Varaiable:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the header Manager
	 * components<p>
	 * @author Anant
	 */
	public void createHeaderManager(String searchTextToAdd,String testname,String variableName,String variableValue){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element arguments = createElement(doc, element, "HeaderManager");
				addAttributesToParentTag(doc, arguments, "HeaderPanel", "HeaderManager", testname, "true");

				Element collectionProp = createElement(doc, arguments, "collectionProp");
				createAttribute(doc, collectionProp, "name", "HeaderManager.headers");

				String[] varNames = variableName.split("\\^");
				String[] varValues = variableValue.split("\\^");

				for(int v=0;v<varNames.length;v++){
					String keyText1,keyText2;
					keyText1 = varNames[v];

					try{
						keyText2 = varValues[v];
					}catch(Exception e){
						keyText2 = "";
					}
					if (keyText1.length() != 0) {
						Element elementProp = createElement(doc, collectionProp, "elementProp");
						createAttribute(doc, elementProp, "name", "\"\"");
						createAttribute(doc, elementProp, "elementType", "Argument");

						Element stringProp1 = createElement(doc, elementProp, "stringProp");
						createAttribute(doc, stringProp1, "name", "Header.name");
						stringProp1.setTextContent(keyText1);

						Element stringProp2 = createElement(doc, elementProp, "stringProp");
						createAttribute(doc, stringProp2, "name", "Header.value");
						stringProp2.setTextContent(keyText2);
					} 
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created Header manager component:");
				report.log(LogStatus.INFO, "Created Header manager component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.INFO, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "HTTP Header manager:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "HTTP Header manager:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the debug sampler<p>
	 * @author Anant
	 */
	public void createDebugSampler(String searchTextToAdd,String testname){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element debugSampler = createElement(doc, element, "DebugSampler");
				addAttributesToParentTag(doc, debugSampler, "TestBeanGUI", "DebugSampler", testname, "true");

				String[] tagNames = {"displayJMeterProperties","displayJMeterVariables","displaySystemProperties"};
				String[] tagValues = {"false","true","false"};

				for (int t = 0; t < tagNames.length; t++) {
					Element boolProp = createElement(doc, debugSampler,"boolProp");
					createAttribute(doc, boolProp, "name", tagNames[t]);
					boolProp.setTextContent(tagValues[t]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Debug Sampler components created:");
				report.log(LogStatus.INFO, "Debug Sampler components created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Debug Sampler:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Debug Sampler:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the while controller<p>
	 * @author Anant
	 */
	public void createWhileController(String searchTextToAdd,String testname,String conditionExpression){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element whileController = createElement(doc, element, "WhileController");
				addAttributesToParentTag(doc, whileController, "WhileControllerGui", "WhileController", testname, "true");

				Element stringProp = createElement(doc, whileController,"stringProp");
				createAttribute(doc, stringProp, "name", "WhileController.condition");
				stringProp.setTextContent(conditionExpression);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("While controller component is created:");
				report.log(LogStatus.INFO, "While controller component is created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "while controller:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "while controller:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the Through put controller<p>
	 * @author Anant
	 */
	public void createThroughPutController(String searchTextToAdd,String testname,String maxThroughPut){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element throughputController = createElement(doc, element, "ThroughputController");
				addAttributesToParentTag(doc, throughputController, "ThroughputControllerGui", "ThroughputController", testname, "true");

				Element intProp1 = createElement(doc, throughputController,"intProp");
				createAttribute(doc, intProp1, "name", "ThroughputController.style");
				intProp1.setTextContent("0");

				Element boolProp = createElement(doc, throughputController,"boolProp");
				createAttribute(doc, boolProp, "name", "ThroughputController.perThread");
				boolProp.setTextContent("true");

				Element intProp2 = createElement(doc, throughputController,"intProp");
				createAttribute(doc, intProp2, "name", "ThroughputController.maxThroughput");
				intProp2.setTextContent(maxThroughPut);

				Element floatProperty = createElement(doc, throughputController,"FloatProperty");			
				Element name = createElement(doc, floatProperty,"name");
				name.setTextContent("ThroughputController.percentThroughput");
				Element value = createElement(doc, floatProperty,"value");
				value.setTextContent("100.0");
				Element savedValue = createElement(doc, floatProperty,"savedValue");
				savedValue.setTextContent("0.0");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Through put controller created:");
				report.log(LogStatus.INFO, "Through put controller created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "through put controller:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "through put controller:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the Aggregate report
	 *  components <p>
	 * @author Anant
	 */
	public void createAggregateReport(String searchTextToAdd,String fileName,String status){
		try{
			Map<String,String> tagNameAndValue =  new LinkedHashMap<String,String>();
			tagNameAndValue.put("time", "true");
			tagNameAndValue.put("latency", "true");
			tagNameAndValue.put("timestamp", "true");
			tagNameAndValue.put("success", "true");
			tagNameAndValue.put("label", "true");
			tagNameAndValue.put("code", "true");
			tagNameAndValue.put("message", "true");
			tagNameAndValue.put("threadName", "true");
			tagNameAndValue.put("dataType", "true");
			tagNameAndValue.put("encoding", "false");
			tagNameAndValue.put("assertions", "true");
			tagNameAndValue.put("subresults", "true");
			tagNameAndValue.put("responseData", "false");
			tagNameAndValue.put("samplerData", "false");
			tagNameAndValue.put("xml", "false");
			tagNameAndValue.put("fieldNames", "false");
			tagNameAndValue.put("responseHeaders", "false");
			tagNameAndValue.put("requestHeaders", "false");
			tagNameAndValue.put("responseDataOnError", "false");
			tagNameAndValue.put("saveAssertionResultsFailureMessage", "false");
			tagNameAndValue.put("assertionsResultsToSave", "0");
			tagNameAndValue.put("bytes", "true");

			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element resultCollector = createElement(doc, element, "ResultCollector");
				addAttributesToParentTag(doc, resultCollector, "StatVisualizer", "ResultCollector", "Aggregate Report", "true");

				Element boolProp = createElement(doc, resultCollector,"boolProp");
				createAttribute(doc, boolProp, "name", "ResultCollector.error_logging");
				if(status.trim().equalsIgnoreCase("error")) 
					boolProp.setTextContent("true");
				else
					boolProp.setTextContent("false");

				Element objProp = createElement(doc, resultCollector,"objProp");
				Element name = createElement(doc, objProp,"name");
				name.setTextContent("saveConfig");
				Element value = createElement(doc, objProp,"value");
				createAttribute(doc, value, "class", "SampleSaveConfiguration");

				for ( Map.Entry<String, String> entry : tagNameAndValue.entrySet()) {
					String key = entry.getKey();
					String Value = entry.getValue();
					Element ele = createElement(doc, value, key);
					ele.setTextContent(Value);
				}

				Element stringProp = createElement(doc, resultCollector,"stringProp");
				createAttribute(doc, stringProp, "name", "filename");
				stringProp.setTextContent(fileName.trim());
				if(status.equalsIgnoreCase("success")){
					Element boolProp1 = createElement(doc, resultCollector,"boolProp");
					createAttribute(doc, boolProp1, "name", "ResultCollector.success_only_logging");
					boolProp1.setTextContent("true");
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				report.log(LogStatus.INFO, "Through put controller created:");
				logger.info("Through Put controller created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "through put controller:"+e.getMessage());
			report.log(LogStatus.INFO, "Exception occred while creating the "
					+ "through put controller:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the Simple Data Writer
	 * components <p>
	 * @author Anant
	 */
	public void createSimpleDataWriter(String searchTextToAdd,String fileName,String status){
		try{
			Map<String,String> tagNameAndValue =  new LinkedHashMap<String,String>();
			tagNameAndValue.put("time", "true");
			tagNameAndValue.put("latency", "true");
			tagNameAndValue.put("timestamp", "true");
			tagNameAndValue.put("success", "true");
			tagNameAndValue.put("label", "true");
			tagNameAndValue.put("code", "true");
			tagNameAndValue.put("message", "true");
			tagNameAndValue.put("threadName", "true");
			tagNameAndValue.put("dataType", "true");
			tagNameAndValue.put("encoding", "false");
			tagNameAndValue.put("assertions", "true");
			tagNameAndValue.put("subresults", "true");
			tagNameAndValue.put("responseData", "false");
			tagNameAndValue.put("samplerData", "false");
			tagNameAndValue.put("xml", "false");
			tagNameAndValue.put("fieldNames", "false");
			tagNameAndValue.put("responseHeaders", "false");
			tagNameAndValue.put("requestHeaders", "false");
			tagNameAndValue.put("responseDataOnError", "false");
			tagNameAndValue.put("saveAssertionResultsFailureMessage", "false");
			tagNameAndValue.put("assertionsResultsToSave", "0");
			tagNameAndValue.put("bytes", "true");
		
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element resultCollector = createElement(doc, element, "ResultCollector");
				createAttribute(doc, resultCollector, "guiclass", "SimpleDataWriter");
				createAttribute(doc, resultCollector, "testclass", "ResultCollector");
				createAttribute(doc, resultCollector, "testname", "Simple Data Writer");
				createAttribute(doc, resultCollector, "enabled", "true");

				Element boolProp = createElement(doc, resultCollector,"boolProp");
				createAttribute(doc, boolProp, "name", "ResultCollector.error_logging");
				if(status.trim().equalsIgnoreCase("error")) 
				   boolProp.setTextContent("true");
				else
				   boolProp.setTextContent("false");
				
				Element objProp = createElement(doc, resultCollector,"objProp");
				Element name = createElement(doc, objProp,"name");
				name.setTextContent("saveConfig");
				Element value = createElement(doc, objProp,"value");
				createAttribute(doc, value, "class", "SampleSaveConfiguration");

				for ( Map.Entry<String, String> entry : tagNameAndValue.entrySet()) {
				    String key = entry.getKey();
				    String Value = entry.getValue();
				    Element ele = createElement(doc, value, key);
					ele.setTextContent(Value);
				}
				
				Element stringProp = createElement(doc, resultCollector,"stringProp");
				createAttribute(doc, stringProp, "name", "filename");
				stringProp.setTextContent(fileName.trim());
			    if(status.equalsIgnoreCase("success")){
				   Element boolProp1 = createElement(doc, resultCollector,"boolProp");
				   createAttribute(doc, boolProp1, "name", "ResultCollector.success_only_logging");
				   boolProp1.setTextContent("true");
			    }
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Simple Data Writer components:");
				report.log(LogStatus.INFO, "Created the Simple Data Writer components:");
			}else{
				logger.error("Jmx file doesn't exist:");
			    report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			  }
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Simple Data Writer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Simple Data Writer:"+e.getMessage());
		}
	}
	
	/**
	 * <p>The method to create the debug sampler<p>
	 * @author Anant
	 */
	public void createHttpRequestDefault(String searchTextToAdd,String serverName,String portVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element configTestElement = createElement(doc, element, "ConfigTestElement");
				addAttributesToParentTag(doc, configTestElement, "HttpDefaultsGui", "ConfigTestElement", "HTTP Request Defaults", "true");

				Element elementProp = createElement(doc, configTestElement,"elementProp");
				createAttribute(doc, elementProp, "name", "HTTPsampler.Arguments");
				createAttribute(doc, elementProp, "elementType", "Arguments");
				addAttributesToParentTag(doc, elementProp, "HTTPArgumentsPanel", "Arguments", "User Defined Variables", "true");

				Element collectionProp = createElement(doc, elementProp,"collectionProp");
				createAttribute(doc, collectionProp, "name", "Arguments.arguments");

				String[] attrNames = {"HTTPSampler.domain","HTTPSampler.port","HTTPSampler.connect_timeout","HTTPSampler.response_timeout",
						"HTTPSampler.protocol","HTTPSampler.contentEncoding","HTTPSampler.path","HTTPSampler.concurrentPool"};
				String[] tagValues = {serverName,portVal,"","","","","",""};

				for (int t = 0; t < attrNames.length; t++) {
					Element stringProp = createElement(doc, configTestElement,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[t]);
					stringProp.setTextContent(tagValues[t]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("HTTP Request Default Component created:");
				report.log(LogStatus.INFO, "HTTP Request Default Component created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "HTTP Request default:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "HTTP Request default:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the debug sampler<p>
	 * @author Anant
	 */
	public void createJdbcConnectConf(String searchTextToAdd,String dataSource,String dbURL,String dbDriver,String username,String password){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element jdbcDataSource = createElement(doc, element, "JDBCDataSource");
				addAttributesToParentTag(doc, jdbcDataSource, "TestBeanGUI", "JDBCDataSource", 
						"JDBC Connection Configuration", "true");

				Element boolProp1 = createElement(doc, jdbcDataSource,"boolProp");
				createAttribute(doc, boolProp1, "name", "autocommit");
				boolProp1.setTextContent("true");

				Element boolProp2 = createElement(doc, jdbcDataSource,"boolProp");
				createAttribute(doc, boolProp2, "name", "keepAlive");
				boolProp2.setTextContent("true");

				String[] attrNames = {"checkQuery","connectionAge","dataSource","dbUrl","driver",
						"password","poolMax","timeout","transactionIsolation","trimInterval","username"};
				String[] tagValues = {"Select 1","5000",dataSource,dbURL,dbDriver,password,"10","10000","DEFAULT","60000",username};

				for (int t = 0; t < attrNames.length; t++) {
					Element stringProp = createElement(doc, jdbcDataSource,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[t]);
					stringProp.setTextContent(tagValues[t]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("JDBC Conncetion configuration compoenent created:");
				report.log(LogStatus.INFO, "JDBC Conncetion configuration compoenent created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "JDBC Connection Conf:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "JDBC Connection Conf:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the debug sampler<p>
	 * @author Anant
	 */
	public void createJdbcSampler(String searchTextToAdd,String testname,String query,String varsName){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element jdbcSampler = createElement(doc, element, "JDBCSampler");
				addAttributesToParentTag(doc, jdbcSampler, "TestBeanGUI", "JDBCSampler", testname, "true");

				String[] attrNames = {"dataSource","query","queryArguments","queryArgumentsTypes","queryTimeout",
						"queryType","resultSetHandler","resultVariable","variableNames"};
				String[] tagValues = {"",query,"","","","Select Statement","Store as String","",varsName};

				for (int t = 0; t < attrNames.length; t++) {
					Element stringProp = createElement(doc, jdbcSampler,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[t]);
					stringProp.setTextContent(tagValues[t]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("JDBC sampler component created:");
				report.log(LogStatus.INFO, "JDBC sampler component created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "JDBC Sampler:"+e.getMessage());
			report.log(LogStatus.INFO, "Exception occred while creating the "
					+ "JDBC Sampler:"+e.getMessage());
		}
	}


	/**
	 * <p>The method to create the JP@GC Dummy sampler<p>
	 * @author Anant
	 */
	public void createJpgcDummySampler(String searchTextToAdd,String testname,String responseData){
		String request;
		request = "Dummy Sampler used to simulate requests and responses without actual network activity. This helps debugging tests.";
		if(responseData.length()==0){
			responseData = "Dummy Sampler used to simulate requests and responses without actual network activity. This helps debugging tests.";
		}
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element dummySampler = createElement(doc, element, "kg.apc.jmeter.samplers.DummySampler");
				addAttributesToParentTag(doc, dummySampler, "kg.apc.jmeter.samplers.DummySamplerGui", 
						"kg.apc.jmeter.samplers.DummySampler", testname, "true");

				Element boolProp1 = createElement(doc, dummySampler, "boolProp");
				createAttribute(doc, boolProp1, "name", "WAITING");
				boolProp1.setTextContent("true");

				Element boolProp2 = createElement(doc, dummySampler, "boolProp");
				createAttribute(doc, boolProp2, "name", "SUCCESFULL");
				boolProp2.setTextContent("true");

				String[] attrNames = {"RESPONSE_CODE","RESPONSE_MESSAGE","REQUEST_DATA","RESPONSE_DATA","RESPONSE_TIME","LATENCY","CONNECT"};
				String[] tagValues = {"200","OK",request,responseData,"${__Random(50,500)}","${__Random(1,50)}","${__Random(1,5)}"};

				for (int a = 0; a < attrNames.length; a++) {
					Element stringProp = createElement(doc, dummySampler,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[a]);
					stringProp.setTextContent(tagValues[a]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("JP@GC Dummay Sampler component created:");
				report.log(LogStatus.INFO, "JP@GC Dummay Sampler component created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "JP@GC dummy Sampler:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "JP@GC dummy Sampler:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the Flexible 
	 * File Writer </p>
	 * @author Anant
	 */
	public void createJpgcFlexibleFileWriter(String searchTextToAdd,String fileName,String headerNames){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element flexibleFileWriter = createElement(doc, element, "kg.apc.jmeter.reporters.FlexibleFileWriter");
				addAttributesToParentTag(doc, flexibleFileWriter, "kg.apc.jmeter.reporters.FlexibleFileWriterGui",
						"kg.apc.jmeter.reporters.FlexibleFileWriter", "jp@gc - Flexible File Writer", "true");

				String[] attrNames = {"filename","columns","header","footer"};
				String[] tagValues = {fileName,"responseData|\r\n",headerNames,""};

				for (int a = 0; a < attrNames.length; a++) {
					Element stringProp = createElement(doc, flexibleFileWriter,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[a]);
					stringProp.setTextContent(tagValues[a]);
				}
				Element boolProp = createElement(doc, flexibleFileWriter,"boolProp");
				createAttribute(doc, boolProp, "name", "overwrite");
				boolProp.setTextContent("true");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("JP@GC Flexible File Writer component created:");
				report.log(LogStatus.INFO, "JP@GC Flexible File Writer component created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occured while creating the "
					+ "JP@GC Flexible File Writer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occured while creating the "
					+ "JP@GC Flexible File Writer:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the Through put shape 
	 * Timer
	 */
	public void createThroughPutShapeTimer(String searchTextToAdd,String strtRPS,String endRPS,String durationVal){
		String cllctPrpID="1586752",strtRpsID="1",endRpsID="48625",durID="48628";
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element variableThroughputTimer = createElement(doc, element, "kg.apc.jmeter.timers.VariableThroughputTimer");
				addAttributesToParentTag(doc, variableThroughputTimer, "kg.apc.jmeter.timers.VariableThroughputTimerGui",
						"kg.apc.jmeter.timers.VariableThroughputTimer", "jp@gc - Throughput Shaping Timer", "true");

				Element collectionProp = createElement(doc, variableThroughputTimer,"collectionProp");
				createAttribute(doc, collectionProp, "name", "load_profile");

				String[] startRps = strtRPS.split("\\^");
				String[] endRps = endRPS.split("\\^");
				String[] durVal = durationVal.split("\\^");

				for (int a = 0; a < startRps.length; a++) {
					try{ 
						Element collectionProp1 = createElement(doc, collectionProp,"collectionProp");
						createAttribute(doc, collectionProp1, "name", cllctPrpID);

						Element stringProp1 = createElement(doc, collectionProp1,"stringProp");
						createAttribute(doc, stringProp1, "name", strtRpsID);
						stringProp1.setTextContent(startRps[a]);

						Element stringProp2 = createElement(doc, collectionProp1,"stringProp");
						createAttribute(doc, stringProp2, "name", endRpsID);
						stringProp2.setTextContent(endRps[a]);

						Element stringProp3 = createElement(doc, collectionProp1,"stringProp");
						createAttribute(doc, stringProp3, "name", durID);
						stringProp3.setTextContent(durVal[a]);

						cllctPrpID = addIntVal(cllctPrpID);
						strtRpsID = addIntVal(strtRpsID);
						endRpsID = addIntVal(endRpsID);
						durID = addIntVal(durID);
					}catch(Exception e){}
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Through Put Shape Timer component Created:");
				report.log(LogStatus.INFO, "Through Put Shape Timer component Created:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Through put shape timer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Through put shape timer:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the Counter Element
	 * @author Anant </p>
	 */
	public void createCounter(String searchTextToAdd,String stratIndex,String endIndex,String incrVal,String refName){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element counterConfig = createElement(doc, element, "CounterConfig");
				addAttributesToParentTag(doc, counterConfig, "CounterConfigGui", "CounterConfig", 
						"Counter", "true");

				String[] attrNames = {"CounterConfig.start","CounterConfig.end","CounterConfig.incr",
						"CounterConfig.name","CounterConfig.format"};
				String[] tagValues = {stratIndex,endIndex,incrVal,refName,""};

				for (int a = 0; a < attrNames.length; a++) {
					Element stringProp = createElement(doc, counterConfig,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[a]);
					stringProp.setTextContent(tagValues[a]);
				}
				Element boolProp = createElement(doc, counterConfig,"boolProp");
				createAttribute(doc, boolProp, "name", "CounterConfig.per_user");
				boolProp.setTextContent("false");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Crated Counter component:");
				report.log(LogStatus.INFO, "Crated Counter component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Counter element:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Counter element:"+e.getMessage());
		}
	}


	/**
	 * <p> The method to create the Bean shell
	 * sampler </p>
	 * @author Anant
	 */
	public void createBeanShellSampler(String searchTextToAdd,String testName,String code){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element beanShellSampler = createElement(doc, element, "BeanShellSampler");
				addAttributesToParentTag(doc, beanShellSampler, "BeanShellSamplerGui", 
						"BeanShellSampler", testName, "true");

				String[] attrNames = {"BeanShellSampler.query","BeanShellSampler.filename",
				"BeanShellSampler.parameters"};
				String[] tagValues = {code,"",""};

				for (int a = 0; a < attrNames.length; a++) {
					Element stringProp = createElement(doc, beanShellSampler,"stringProp");
					createAttribute(doc, stringProp, "name", attrNames[a]);
					stringProp.setTextContent(tagValues[a]);
				}
				Element boolProp = createElement(doc, beanShellSampler,"boolProp");
				createAttribute(doc, boolProp, "name", "BeanShellSampler.resetInterpreter");
				boolProp.setTextContent("false");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created Bean shell sampler component:");
				report.log(LogStatus.INFO, "Created Bean shell sampler component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Bean shell sampler:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Bean shell sampler:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the Response assertion <p>
	 * @author Anant
	 */
	public void createResponseAssertion(String searchTextToAdd,String respText,String matchType){
		try{
			Random r1 = new Random();
			String attrVal = "92955290";Integer val1,val2; boolean flag = false;
			Map<String,Integer> assertType = new LinkedHashMap<String,Integer>();
			assertType.put("matches", 1);
			assertType.put("contains", 2);
			assertType.put("equals", 8);
			assertType.put("substring", 16);

			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element responseAssertion = createElement(doc, element, "ResponseAssertion");
				addAttributesToParentTag(doc, responseAssertion, "AssertionGui", "ResponseAssertion", 
						"Response Assertion", "true");

				Element collectionProp = createElement(doc, responseAssertion,"collectionProp");
				createAttribute(doc, collectionProp, "name", "Asserion.test_strings");
				String[] respVal = respText.split("\\^");

				for (int r = 0; r < respVal.length; r++) {
					if (r > 0) {
						val1 = (Integer.parseInt(attrVal) + 1) + (r1.nextInt(100) + 1);
						attrVal = val1.toString();
					}
					Element stringProp = createElement(doc, collectionProp, "stringProp");
					createAttribute(doc, stringProp, "name", attrVal);
					stringProp.setTextContent(respVal[r]);
				}

				Element stringProp = createElement(doc, responseAssertion,"stringProp");
				createAttribute(doc, stringProp, "name", "Assertion.test_field");
				stringProp.setTextContent("Assertion.response_data");

				Element boolProp = createElement(doc, responseAssertion,"boolProp");
				createAttribute(doc, boolProp, "name", "Assertion.assume_success");
				boolProp.setTextContent("false");

				if(matchType.toLowerCase().contains("not")){
					flag = true;
					matchType = matchType.toLowerCase().replace("not", "");
				}

				if(assertType.containsKey(matchType.toLowerCase().trim()))
					val2 =   assertType.get(matchType.toLowerCase().trim());
				else{
					val2 = 2;
				}

				Element intProp = createElement(doc, responseAssertion,"intProp");
				createAttribute(doc, intProp, "name", "Assertion.test_type");
				if(flag)
					val2 = (4 + val2);

				intProp.setTextContent(val2.toString());
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created Response Assertion component");
				report.log(LogStatus.INFO, "Created Response Assertion component");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Response assertion:"+e.getMessage());
			report.log(LogStatus.INFO , "Exception occred while creating the "
					+ "Response assertion:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the Constant Timer <p>
	 * @author Anant
	 */
	public void createConstantTimer(String searchTextToAdd,String timeInMili){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element constantTimer = createElement(doc, element, "ConstantTimer");
				addAttributesToParentTag(doc, constantTimer, "ConstantTimerGui", "ConstantTimer", "Constant Timer", "true");

				Element stringProp = createElement(doc, constantTimer,"stringProp");
				createAttribute(doc, stringProp, "name", "ConstantTimer.delay");
				stringProp.setTextContent(timeInMili);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the constant time component:");
				report.log(LogStatus.INFO, "Created the constant time component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Constant Timer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Constant Timer:"+e.getMessage());
		}
	}


	/**
	 * <p>The method to create the Constant 
	 * Through Put Timer<p>
	 * 
	 */
	public void createConstantThroughPutTimer(String searchTextToAdd,String timeInMin){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element constantThroughputTimer = createElement(doc, element, "ConstantThroughputTimer");
				addAttributesToParentTag(doc, constantThroughputTimer, "TestBeanGUI", "ConstantThroughputTimer", 
						"Constant Throughput Timer", "true");

				Element intProp = createElement(doc, constantThroughputTimer,"intProp");
				createAttribute(doc, intProp, "name", "calcMode");
				intProp.setTextContent("0");

				Element doubleProp = createElement(doc, constantThroughputTimer,"doubleProp");
				Element name = createElement(doc, doubleProp,"name");
				name.setTextContent("throughput");
				Element value = createElement(doc, doubleProp,"value");
				value.setTextContent(timeInMin);
				Element savedValue = createElement(doc, doubleProp,"savedValue");
				savedValue.setTextContent("0.0");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Constant Through Put Timer:");
				report.log(LogStatus.INFO, "Created the Constant Through Put Timer:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Constant Through put Timer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Constant Through put Timer:"+e.getMessage());
		}
	}


	/**
	 * <p>The method to create the Constant Timer<p>
	 * @author Anant
	 */
	public void createGaussianRandomTimer(String searchTextToAdd,String deviationInMili,String delayInMili){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element gaussianRandomTimer = createElement(doc, element, "GaussianRandomTimer");
				addAttributesToParentTag(doc, gaussianRandomTimer, "GaussianRandomTimerGui", "GaussianRandomTimer",
						"Gaussian Random Timer", "true");

				Element stringProp1 = createElement(doc, gaussianRandomTimer,"stringProp");
				createAttribute(doc, stringProp1, "name", "ConstantTimer.delay");
				stringProp1.setTextContent(deviationInMili);

				Element stringProp2 = createElement(doc, gaussianRandomTimer,"stringProp");
				createAttribute(doc, stringProp2, "name", "RandomTimer.range");
				stringProp2.setTextContent(delayInMili);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Gaussian Random Timer Component:");
				report.log(LogStatus.INFO, "Created the Gaussian Random Timer Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Gaussian Random Timer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Gaussian Random Timer:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the Constant Timer<p>
	 * @author Anant
	 */
	public void createSyncTimer(String searchTextToAdd,String noOfUsers,String timeInMili){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element syncTimer = createElement(doc, element, "SyncTimer");
				addAttributesToParentTag(doc, syncTimer, "TestBeanGUI", "SyncTimer", "Synchronizing Timer", "true");

				Element intProp = createElement(doc, syncTimer,"intProp");
				createAttribute(doc, intProp, "name", "ConstantTimer.delay");
				intProp.setTextContent(noOfUsers);

				Element longProp = createElement(doc, syncTimer,"longProp");
				createAttribute(doc, longProp, "name", "RandomTimer.range");
				longProp.setTextContent(timeInMili);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.error("Crated the Synchronizing Timer component:");
				report.log(LogStatus.INFO, "Crated the Synchronizing Timer component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Synchronizing Timer:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Synchronizing Timer:"+e.getMessage());
		}
	}

	/**
	 * The method to create the user defined
	 * @author Anant
	 */
	public void createViewResultTree(String searchTextToAdd){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){

				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element resultCollector = createElement(doc, element, "ResultCollector");
				addAttributesToParentTag(doc, resultCollector, "ViewResultsFullVisualizer", "ResultCollector",
						"View Results Tree", "true");
				Element boolProp = createElement(doc, resultCollector, "boolProp");
				createAttribute(doc, boolProp, "name", "ResultCollector.error_logging");
				boolProp.setTextContent("false");
				Element objProp = createElement(doc, resultCollector, "objProp");

				Element name = createElement(doc, objProp, "name");
				name.setTextContent("saveConfig");

				Element value = createElement(doc, objProp, "value");
				createAttribute(doc, value, "class", "SampleSaveConfiguration");

				String[] tagNames = {"time","latency","timestamp","success","label","code","message","threadName",
						"dataType","encoding","assertions","subresults","samplerData","xml","fieldNames",
						"responseHeaders","requestHeaders","responseDataOnError","saveAssertionResultsFailureMessage","assertionsResultsToSave","bytes"};
				String[] tagValues = {"true","true","true","true","true","true","true","true","true","false","true",
						"true","false","false","false","false","false","false","false","0","true"};

				for(int v=0;v<tagNames.length;v++){
					Element tagName = createElement(doc, value, tagNames[v]);
					tagName.setTextContent(tagValues[v]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the View Result Tree component:");
				report.log(LogStatus.ERROR, "Created the View Result Tree component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "view result tree:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "view result tree:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the HTTP cookie manager </p>
	 * @author Anant
	 */
	public void createCacheManager(String searchTextToAdd,String boolVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element cacheManager = createElement(doc, element, "CacheManager");
				addAttributesToParentTag(doc, cacheManager, "CacheManagerGui", "CacheManager", "HTTP Cache Manager", "true");

				Element boolProp1 = createElement(doc, cacheManager, "boolProp");
				createAttribute(doc, boolProp1, "name", "clearEachIteration");
				boolProp1.setTextContent(boolVal);

				Element boolProp2 = createElement(doc, cacheManager, "boolProp");
				createAttribute(doc, boolProp2, "name", "useExpires");
				boolProp2.setTextContent(boolVal);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Creted the Cache Manager Component:");
				report.log(LogStatus.INFO, "Creted the Cache Manager Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occured while creating the "
					+ "Cache manager:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occured while creating the "
					+ "Cache manager:"+e.getMessage());
		}
	}


	/**
	 * <p>The method to create the HTTP cookie manager <p>
	 * @author Anant
	 */
	public void createCookieManager(String searchTextToAdd,String boolVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element cookieManager = createElement(doc, element, "CookieManager");
				addAttributesToParentTag(doc, cookieManager,  "CookiePanel", "CookieManager", 
						"HTTP Cookie Manager", "true");

				Element collectionProp = createElement(doc, cookieManager, "collectionProp");
				createAttribute(doc, collectionProp, "name", "CookieManager.cookies");

				Element boolProp = createElement(doc, cookieManager, "boolProp");
				createAttribute(doc, boolProp, "name", "CookieManager.clearEachIteration");
				boolProp.setTextContent(boolVal);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the cookie manage component:");
				report.log(LogStatus.INFO, "Created the cookie manage component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occured while creating the "
					+ "HTTP cookie manager:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occured while creating the "
					+ "HTTP cookie manager:"+e.getMessage());
		}
	}

	/**
	 * <p> The method to create the CSV Data set configuration</p>
	 * @author Anant
	 */
	public void createCSVDatasetConfig(String searchTextToAdd,String fileName,String varsName,String delimeter){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc,searchTextToAdd);

				Element csvDataSet = createElement(doc, element, "CSVDataSet");
				addAttributesToParentTag(doc, csvDataSet, "TestBeanGUI", "CSVDataSet", "CSV Data Set Config", "true");

				String[] strAttrName = { "filename","fileEncoding", "variableNames","delimiter","shareMode"};
				String[] strTagValue = { fileName,"", varsName, delimeter,"shareMode.all"};

				for (int i = 0; i < strAttrName.length; i++) {
					Element stringProp = createElement(doc, csvDataSet, "stringProp");
					createAttribute(doc, stringProp, "name", strAttrName[i]);
					stringProp.setTextContent(strTagValue[i]);
				}

				String[] boolAttrName = { "quotedData","recycle", "stopThread"};
				String[] boolTagValue = { "true", "false", "true"};

				for (int i = 0; i < boolAttrName.length; i++) {
					Element boolProp = createElement(doc, csvDataSet, "boolProp");
					createAttribute(doc, boolProp, "name", boolAttrName[i]);
					boolProp.setTextContent(boolTagValue[i]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the CSV Data Set Config Component:");
				report.log(LogStatus.INFO, "Created the CSV Data Set Config Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occured while creating the "
					+ "CSV data set config:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occured while creating the "
					+ "CSV data set config:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create loop controller<p>
	 * @author Anant
	 */
	public void createRegexExtractor(String searchTextToAdd,String refName,String regex,String template,String defVal,String matchNo){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element regexExtractor = createElement(doc, element, "RegexExtractor");
				addAttributesToParentTag(doc, regexExtractor, "RegexExtractorGui", "RegexExtractor",
						"Regular Expression Extractor", "true");

				String[] attrName={"RegexExtractor.useHeaders","RegexExtractor.refname","RegexExtractor.regex","RegexExtractor.template"
						,"RegexExtractor.default","RegexExtractor.match_number"};
				String[] tagValue={"false",refName,regex,template,defVal,matchNo};

				for(int i=0;i<attrName.length;i++){
					Element stringProp = createElement(doc, regexExtractor, "stringProp");
					createAttribute(doc, stringProp, "name", attrName[i]);
					stringProp.setTextContent(tagValue[i]);
				}

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Regular Expression Component:");
				report.log(LogStatus.INFO, "Created the Regular Expression Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "regex components:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "regex components:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create CSS Extractor<p>
	 * @author Anants
	 */
	public void createCSSExtractor(String searchTextToadd,String refName,String cssVal,String atrName,String defVal,String matchNo){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToadd);

				Element htmlExtractor = createElement(doc, element, "HtmlExtractor");
				addAttributesToParentTag(doc, htmlExtractor, "HtmlExtractorGui", 
						"HtmlExtractor", "CSS/JQuery Extractor", "true");

				String[] attrName={"HtmlExtractor.refnames","HtmlExtractor.expr","HtmlExtractor.attribute","HtmlExtractor.default"
						,"HtmlExtractor.match_number","HtmlExtractor.extractor_impl"};
				String[] tagValue={refName,cssVal,atrName,defVal,matchNo,""};

				for(int i=0;i<attrName.length;i++){
					Element stringProp = createElement(doc, htmlExtractor, "stringProp");
					createAttribute(doc, stringProp, "name", attrName[i]);
					stringProp.setTextContent(tagValue[i]);
				}

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the CSV Data Set Config Components:");
				report.log(LogStatus.INFO, "Created the CSV Data Set Config Components:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "css selector components:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "css selector components:"+e.getMessage());
		}
	}


	/**
	 * The method to create the XPATH extractor
	 * @author Anant
	 */
	public void createXpathExtractor(String searchTextToAdd,String refName,String xpathquery,String defVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element htmlExtractor = createElement(doc, element, "HtmlExtractor");
				addAttributesToParentTag(doc, htmlExtractor, "XPathExtractorGui", "XPathExtractor", 
						"XPath Extractor", "true");

				String[] strAttrName={"XPathExtractor.default","XPathExtractor.refname","XPathExtractor.xpathQuery"};
				String[] strTagValue={defVal,refName,xpathquery};

				for(int i=0;i<strAttrName.length;i++){
					Element stringProp = createElement(doc, htmlExtractor, "stringProp");
					createAttribute(doc, stringProp, "name", strAttrName[i]);
					stringProp.setTextContent(strTagValue[i]);
				}

				String[] boolAttrName={"XPathExtractor.validate","XPathExtractor.tolerant","XPathExtractor.namespace"};
				String[] boolTagValue={"false","false","false"};
				for(int i=0;i<boolAttrName.length;i++){
					Element stringProp = createElement(doc, htmlExtractor, "boolProp");
					createAttribute(doc, stringProp, "name", boolAttrName[i]);
					stringProp.setTextContent(boolTagValue[i]);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Xpath Extractor Component:");
				report.log(LogStatus.INFO, "Created the Xpath Extractor Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "XPATH extractor components:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "XPATH extractor components:"+e.getMessage());
		}
	}

	/**
	 * The method to create loop controller
	 * @author Anant
	 */
	public void createLoopController(String searchTextToAdd,String testName,String boolval,String loopVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){ 
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element LoopController = createElement(doc, element, "LoopController");
				addAttributesToParentTag(doc, LoopController, "LoopControlPanel", "LoopController", 
						testName, "true");

				Element boolProp = createElement(doc, LoopController, "boolProp");
				createAttribute(doc, boolProp, "name", "LoopController.continue_forever");
				boolProp.setTextContent("false");

				if(boolval.equalsIgnoreCase("true")){
					Element stringProp = createElement(doc, LoopController, "stringProp");
					createAttribute(doc, stringProp, "name", "LoopController.loops");
					stringProp.setTextContent("-1");
				}else{
					Element stringProp = createElement(doc, LoopController, "stringProp");
					createAttribute(doc, stringProp, "name", "LoopController.loops");
					stringProp.setTextContent(loopVal);
				}
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Loop Controller Component:");
				report.log(LogStatus.INFO, "Created the Loop Controller Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "loop controller:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "loop controller:"+e.getMessage());
		}
	}

	/**
	 * The method to create the For each controller
	 * @param inputVal
	 * @param returnval
	 * @param strtIndex
	 * @param endIndex
	 * @param boolVal
	 * @author Anant
	 */
	public void createForEachController(String searchTextToAdd,String testName,String inputVal,String returnval,String strtIndex,String endIndex,String boolVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element foreachController = createElement(doc, element, "ForeachController");
				addAttributesToParentTag(doc, foreachController, "ForeachControlPanel", 
						"ForeachController", testName, "true");

				Element stringProp = createElement(doc, foreachController, "stringProp");
				createAttribute(doc, stringProp, "name", "ForeachController.inputVal");
				stringProp.setTextContent(inputVal);

				Element stringProp1 = createElement(doc, foreachController, "stringProp");
				createAttribute(doc, stringProp1, "name", "ForeachController.returnVal");
				stringProp.setTextContent(returnval);

				Element boolProp = createElement(doc, foreachController, "boolProp");
				createAttribute(doc, boolProp, "name", "ForeachController.useSeparator");
				stringProp.setTextContent(boolVal);

				Element stringProp2 = createElement(doc, foreachController, "stringProp");
				createAttribute(doc, stringProp2, "name", "ForeachController.startIndex");
				stringProp.setTextContent(strtIndex);

				Element stringProp3 = createElement(doc, foreachController, "stringProp");
				createAttribute(doc, stringProp3, "name", "ForeachController.endIndex");
				stringProp.setTextContent(endIndex);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the For Each Controller Component:");
				report.log(LogStatus.INFO, "Created the For Each Controller Component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "For each controller:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "For each controller:"+e.getMessage());
		}
	}

	/**
	 * The method to create the simple controller
	 * @author Anant
	 */
	public void createSimpleController(String searchTextToAdd,String testName){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element genericController = createElement(doc, element, "GenericController");
				addAttributesToParentTag(doc, genericController, "LogicControllerGui",
						"GenericController", testName, "true");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the simple controller:");
				report.log(LogStatus.INFO, "Created the simple controller:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "simple controller:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "simple controller:"+e.getMessage());
		}
	}

	/**
	 * The method to create the transaction controller
	 * @param inputVal
	 * @author Anant
	 */
	public void createTransactionController(String searchTextToAdd,String testName,String inputVal){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element transactionController = createElement(doc, element, "TransactionController");
				addAttributesToParentTag(doc, transactionController, "TransactionControllerGui",
						"TransactionController", testName, "true");

				Element boolProp1 = createElement(doc, transactionController, "boolProp");
				createAttribute(doc, boolProp1, "name", "ForeachController.inputVal");
				boolProp1.setTextContent(inputVal);

				Element boolProp2 = createElement(doc, transactionController, "boolProp");
				createAttribute(doc, boolProp2, "name", "ForeachController.useSeparator");
				boolProp2.setTextContent("");

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Transaction Controller component:");
				report.log(LogStatus.INFO, "Created the Transaction Controller component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occred while creating the "
					+ "Transaction controller:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occred while creating the "
					+ "Transaction controller:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the post processor code </p>
	 * @author Anant
	 */
	public void createPreProcessor(String searchTextToAdd,String code){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element BeanShellPreProcessor = createElement(doc, element, "BeanShellPreProcessor");
				addAttributesToParentTag(doc, BeanShellPreProcessor, "TestBeanGUI", "BeanShellPreProcessor",
						"BeanShell PreProcessor", "true");

				Element boolProp = createElement(doc, BeanShellPreProcessor, "boolProp");
				createAttribute(doc, boolProp, "name", "resetInterpreter");
				createAttribute(doc, boolProp, "elementType", "Arguments");

				Element parameters = createElement(doc, BeanShellPreProcessor, "stringProp");
				createAttribute(doc, parameters, "name", "parameters");

				Element filename = createElement(doc, BeanShellPreProcessor, "stringProp");
				createAttribute(doc, filename, "name", "filename");

				Element script = createElement(doc, BeanShellPreProcessor, "script");
				createAttribute(doc, script, "name", "script");
				script.setTextContent(code);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Beanshell pre processor component:");
				report.log(LogStatus.INFO, "Created the Beanshell pre processor component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception in creating the "
					+ "Beanshell pre processor:"+e.getMessage()); 
			report.log(LogStatus.ERROR, "Exception in creating the "
					+ "Beanshell pre processor:"+e.getMessage());
		}
	}


	/**
	 * <p>The method to create the post processor code </p>
	 * @author Anant
	 */
	public void createPostProcessor(String searchTextToAdd,String code){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element BeanShellPostProcessor = createElement(doc, element, "BeanShellPostProcessor");
				addAttributesToParentTag(doc, BeanShellPostProcessor, "TestBeanGUI", "BeanShellPostProcessor", 
						"BeanShell PostProcessor", "true");

				Element boolProp = createElement(doc, BeanShellPostProcessor, "boolProp");
				createAttribute(doc, boolProp, "name", "resetInterpreter");

				Element parameters = createElement(doc, BeanShellPostProcessor, "stringProp");
				createAttribute(doc, parameters, "name", "parameters");

				Element filename = createElement(doc, BeanShellPostProcessor, "stringProp");
				createAttribute(doc, filename, "name", "filename");

				Element script = createElement(doc, BeanShellPostProcessor, "stringProp");
				createAttribute(doc, script, "name", "script");
				script.setTextContent(code);

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the Beanshell Post Processor Component");
				report.log(LogStatus.INFO, "Created the Beanshell Post Processor Component");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception in creating the Beanshell"
					+ " post processor:"+e.getMessage()); 
			report.log(LogStatus.ERROR, "Exception in creating the Beanshell"
					+ " post processor:"+e.getMessage());
		}
	}



	/**
	 * <p>The method to create the post processor code <p>
	 * @author Anant
	 */
	public void createPostProcessorForGeneralError_Validation(String searchTextToAdd,String varName,String pageTextToVerify){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element BeanShellPostProcessor = createElement(doc, element, "BeanShellPostProcessor");
				addAttributesToParentTag(doc, BeanShellPostProcessor, "TestBeanGUI", "BeanShellPostProcessor",
						"BeanShell PostProcessor", "true");

				Element boolProp = createElement(doc, BeanShellPostProcessor, "boolProp");
				createAttribute(doc, boolProp, "name", "resetInterpreter");

				Element parameters = createElement(doc, BeanShellPostProcessor, "stringProp");
				createAttribute(doc, parameters, "name", "parameters");

				Element filename = createElement(doc, BeanShellPostProcessor, "stringProp");
				createAttribute(doc, filename, "name", "filename");

				Element script = createElement(doc, BeanShellPostProcessor, "stringProp");
				createAttribute(doc, script, "name", "script");
				script.setTextContent(generalPageVerification(varName, pageTextToVerify));

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Added the general error validation "
						+ "code to post processor");
				report.log(LogStatus.INFO, "Added the general error "
						+ "validation code to post processor");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception in creating the Beanshell post "
					+ "processor for general error validation:"+e.getMessage()); 
			report.log(LogStatus.ERROR, "Exception in creating the Beanshell post "
					+ "processor for general error validation:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the HTTP get sampler<p>
	 * @author Anant
	 */
	public void createHttpGetSampler(String searchTextToAdd,String testname,String server,String port,String protocol,String url){
		String serverName = server,serverPort = port,protocolName = protocol;

		if(server.toLowerCase().equals("default"))
			serverName = "";
		if(port.toLowerCase().equals("default"))
			serverPort = "";
		if(protocol.toLowerCase().equals("default"))
			protocolName = "";

		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element httpSamplerProxy = createElement(doc, element, "HTTPSamplerProxy");
				addAttributesToParentTag(doc, httpSamplerProxy, "HttpTestSampleGui", 
						"HTTPSamplerProxy", testname, "true");

				Element elementProp = createElement(doc, httpSamplerProxy, "elementProp");
				createAttribute(doc, elementProp, "name", "HTTPsampler.Arguments");
				createAttribute(doc, elementProp, "elementType", "Arguments");
				createAttribute(doc, elementProp, "guiclass", "HTTPArgumentsPanel");
				createAttribute(doc, elementProp, "testclass", "HTTPArgumentsPanel");
				createAttribute(doc, elementProp, "enabled", "true");

				Element collectionProp = createElement(doc, elementProp, "collectionProp");
				createAttribute(doc, collectionProp, "name", "Arguments.arguments");

				String[] strgProp={"HTTPSampler.domain","HTTPSampler.port","HTTPSampler.proxyHost",
						"HTTPSampler.proxyPort","HTTPSampler.proxyUser","HTTPSampler.proxyPass","HTTPSampler.connect_timeout",
						"HTTPSampler.response_timeout","HTTPSampler.protocol","HTTPSampler.contentEncoding","HTTPSampler.path",
						"HTTPSampler.method","HTTPSampler.embedded_url_re"};
				String[] strgPrpVal={serverName,serverPort,"","","","","","",protocolName,"",url,"",""};

				for(int s=0;s<strgProp.length;s++){  
					Element domain = createElement(doc, httpSamplerProxy, "stringProp");
					createAttribute(doc, domain, "name", strgProp[s]);
					domain.appendChild(doc.createTextNode(strgPrpVal[s]));
				}

				String[] boolProp={"HTTPSampler.follow_redirects","HTTPSampler.auto_redirects","HTTPSampler.use_keepalive",
						"HTTPSampler.DO_MULTIPART_POST","HTTPSampler.monitor"};
				String[] boolPropVal={"true","false","true","false","false"};

				for(int b=0;b<boolProp.length;b++){  
					Element domain = createElement(doc, httpSamplerProxy, "boolProp");
					createAttribute(doc, domain, "name", boolProp[b]);
					domain.appendChild(doc.createTextNode(boolPropVal[b]));
				}  
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the HTTP GET Sampler component:");
				report.log(LogStatus.INFO, "Created the HTTP GET Sampler component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occured in while creating "
					+ "the HTTP GET sampler:"+e.getMessage());
			report.log(LogStatus.ERROR,"Exception occured in while creating "
					+ "the HTTP GET sampler:"+e.getMessage());
		}
	}


	/**
	 * <p>The method to create the HTTP sampler<p>
	 * @author Anant
	 */
	public void createHttpPostSampler(String searchTextToAdd,String testname,String server,
			String port,String protocol,String url,String paramNameAndValues,String postType){
		String serverName = server,serverPort = port,protocolName = protocol;

		if(server.toLowerCase().equals("default"))
			serverName = "";
		if(port.toLowerCase().equals("default"))
			serverPort = "";
		if(protocol.toLowerCase().equals("default"))
			protocolName = "";

		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element httpSamplerProxy = createElement(doc, element, "HTTPSamplerProxy");
				addAttributesToParentTag(doc, httpSamplerProxy,"HttpTestSampleGui", "HTTPSamplerProxy", 
						testname, "true");

				if(postType.toLowerCase().contains("param")){
					Element elementProp = createElement(doc, httpSamplerProxy, "elementProp");
					createAttribute(doc, elementProp, "name", "HTTPsampler.Arguments");
					createAttribute(doc, elementProp, "elementType", "Arguments");
					createAttribute(doc, elementProp, "guiclass", "HTTPArgumentsPanel");
					createAttribute(doc, elementProp, "testclass", "HTTPArgumentsPanel");
					createAttribute(doc, elementProp, "enabled", "true");

					Element collectionProp = createElement(doc, elementProp, "collectionProp");
					createAttribute(doc, collectionProp, "name", "Arguments.arguments");
					try{
						String[] paramNameValues = paramNameAndValues.split("\\|\\|");
						String[] params = paramNameValues[0].split("\\^");
						String[] values = paramNameValues[1].split("\\^");
						for(int j=0;j<params.length;j++){
							Element httpArgument = createElement(doc, collectionProp, "elementProp");
							createAttribute(doc, httpArgument, "name", "");
							createAttribute(doc, httpArgument, "elementType", "HTTPArgument");

							String[] strAttrName = {"Argument.name","Argument.value","Argument.metadata"};
							String[] strAttrVal = {params[j],values[j],"="};

							for(int k=0;k<strAttrName.length;k++){
								Element stringProp = createElement(doc, httpArgument, "stringProp");
								createAttribute(doc, stringProp, "name", strAttrName[k]);
								stringProp.setTextContent(strAttrVal[k]);
							}

							String[] boolAttrName = {"HTTPArgument.always_encode","HTTPArgument.use_equals"};
							String[] boolAttrVal = {"false","true"};
							for(int k=0;k<boolAttrName.length;k++){
								Element boolProp = createElement(doc, httpArgument, "boolProp");
								createAttribute(doc, boolProp, "name", boolAttrName[k]);
								boolProp.setTextContent(boolAttrVal[k]);
							}
						}
					}catch(Exception e){
						logger.error("Error in HttpPost request:");
					}
				}
				if(postType.toLowerCase().contains("body")){
					Element boolProp = createElement(doc, httpSamplerProxy, "boolProp");
					createAttribute(doc, boolProp, "name", "HTTPSampler.postBodyRaw");
					boolProp.setTextContent("true");
					Element elementProp = createElement(doc, httpSamplerProxy, "elementProp");
					createAttribute(doc, elementProp, "name", "HTTPsampler.Arguments");
					createAttribute(doc, elementProp, "elementType", "Arguments");
					Element collectionProp = createElement(doc, elementProp, "collectionProp");
					createAttribute(doc, collectionProp, "name", "Arguments.arguments");
					Element elementProp1 = createElement(doc, collectionProp, "elementProp");
					createAttribute(doc, elementProp1, "name", "");
					createAttribute(doc, elementProp1, "elementType", "HTTPArgument");

					Element boolProp1 = createElement(doc, elementProp1, "boolProp");
					createAttribute(doc, boolProp1, "name", "HTTPArgument.always_encode");
					boolProp1.setTextContent("false");

					Element stringProp = createElement(doc, elementProp1, "stringProp");
					createAttribute(doc, stringProp, "name", "Argument.value");
					stringProp.setTextContent(paramNameAndValues);

					Element stringProp1 = createElement(doc, elementProp1, "stringProp");
					createAttribute(doc, stringProp1, "name", "Argument.metadata");
					stringProp1.setTextContent("=");
				}

				String[] strgProp={"HTTPSampler.domain","HTTPSampler.port","HTTPSampler.proxyHost",
						"HTTPSampler.proxyPort","HTTPSampler.proxyUser","HTTPSampler.proxyPass","HTTPSampler.connect_timeout",
						"HTTPSampler.response_timeout","HTTPSampler.protocol","HTTPSampler.contentEncoding","HTTPSampler.path",
						"HTTPSampler.method","HTTPSampler.embedded_url_re"};
				String[] strgPrpVal={serverName,serverPort,"","","","","","",protocolName,"",url,"POST",""};

				for(int s=0;s<strgProp.length;s++){  
					Element domain = createElement(doc, httpSamplerProxy, "stringProp");
					createAttribute(doc, domain, "name", strgProp[s]);
					domain.appendChild(doc.createTextNode(strgPrpVal[s]));
				}

				String[] boolProp={"HTTPSampler.follow_redirects","HTTPSampler.auto_redirects","HTTPSampler.use_keepalive",
						"HTTPSampler.DO_MULTIPART_POST","HTTPSampler.monitor"};
				String[] boolPropVal={"true","false","true","false","false"};

				for(int b=0;b<boolProp.length;b++){  
					Element domain = createElement(doc, httpSamplerProxy, "boolProp");
					createAttribute(doc, domain, "name", boolProp[b]);
					domain.appendChild(doc.createTextNode(boolPropVal[b]));
				}  
				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the HTTP POST sampler component");
				report.log(LogStatus.INFO, "Created the HTTP POST sampler component");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception occured while creating "
					+ "the HTTP POST sampler:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception occured while creating "
					+ "the HTTP POST sampler:"+e.getMessage());
		}
	}

	/**
	 * <p>The method to create the IF condition
	 */
	public void createIfCondition(String searchTextToAdd, String testname,String ifCondtionExpression){
		try{
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element element = searchTagByText(doc, searchTextToAdd);

				Element IfController = createElement(doc, element, "IfController");
				addAttributesToParentTag(doc, IfController, "IfControllerPanel", "IfController", 
						testname, "true");

				Element stringProp = createElement(doc, IfController, "stringProp");
				createAttribute(doc, stringProp, "name", "IfController.condition");
				stringProp.appendChild(doc.createTextNode(ifCondtionExpression));

				Element boolProp = createElement(doc, IfController, "boolProp");
				createAttribute(doc, boolProp, "name", "IfController.evaluateAll");
				boolProp.appendChild(doc.createTextNode("false"));

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Created the IF condtion component:");
				report.log(LogStatus.INFO,"Created the IF condtion component:");
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		}catch(Exception e){
			logger.error("Exception in creating If condtion:"+e.getMessage());
			report.log(LogStatus.ERROR, "Exception in creating If condtion:"+e.getMessage());
		}
	}

	/**
	 * <p> the method create the thread group
	 */
	public void createThreadGroup(String searchTextToAdd , String testname){
		try {
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			File file =  new File(JMX_FILE_NAME);
			if(file.exists()){
				Document doc = builder.parse(file);
				Element root = doc.getDocumentElement();
				Element element = searchTagByText(doc, searchTextToAdd);

				Element threadGrp = createElement(doc, element, "ThreadGroup");
				addAttributesToParentTag(doc, threadGrp, "ThreadGroupGui", "ThreadGroup", testname, "true");

				Element stringProp = createElement(doc, threadGrp, "stringProp");
				createAttribute(doc, stringProp, "name", "ThreadGroup.on_sample_error");
				stringProp.appendChild(doc.createTextNode("continue"));

				Element elementProp = createElement(doc, threadGrp, "elementProp");
				createAttribute(doc, elementProp, "name", "ThreadGroup.main_controller");
				createAttribute(doc, elementProp, "elementType", "LoopController");
				createAttribute(doc, elementProp, "guiclass", "LoopControlPanel");
				createAttribute(doc, elementProp, "testclass", "LoopController");
				createAttribute(doc, elementProp, "testname", "Loop Controller");
				createAttribute(doc, elementProp, "enabled", "true");

				Element boolProp = createElement(doc, elementProp, "boolProp");
				createAttribute(doc, boolProp, "name", "LoopController.continue_forever");
				boolProp.appendChild(doc.createTextNode("false"));

				Element stringProp1 = createElement(doc, elementProp, "stringProp");
				createAttribute(doc, stringProp1, "name", "LoopController.loops");
				stringProp1.appendChild(doc.createTextNode("1"));

				Element noThreads = createElement(doc, threadGrp, "stringProp");
				createAttribute(doc, noThreads, "name", "ThreadGroup.num_threads");
				noThreads.appendChild(doc.createTextNode("1"));

				Element rampTime = createElement(doc, threadGrp, "stringProp");
				createAttribute(doc, rampTime, "name", "ThreadGroup.ramp_time");
				rampTime.appendChild(doc.createTextNode("1"));

				Element strtTime = createElement(doc, threadGrp, "longProp");
				createAttribute(doc, strtTime, "name", "ThreadGroup.start_time");
				strtTime.appendChild(doc.createTextNode("1510076590000"));

				Element endTime = createElement(doc, threadGrp, "longProp");
				createAttribute(doc, endTime, "name", "ThreadGroup.end_time");
				endTime.appendChild(doc.createTextNode("1510076590000"));

				Element scheduler = createElement(doc, threadGrp, "boolProp");
				createAttribute(doc, scheduler, "name", "ThreadGroup.scheduler");
				scheduler.appendChild(doc.createTextNode("false"));

				Element duration = createElement(doc, threadGrp, "stringProp");
				createAttribute(doc, duration, "name", "ThreadGroup.duration");
				duration.appendChild(doc.createTextNode(""));

				Element delay = createElement(doc, threadGrp, "stringProp");
				createAttribute(doc, delay, "name", "ThreadGroup.delay");
				delay.appendChild(doc.createTextNode(""));

				@SuppressWarnings("unused")
				Element hashTree1 = createElement(doc, element, "hashTree");
				writeContentToXml(doc);
				logger.info("Cretaed the Thread Group Component:"+root);
				report.log(LogStatus.INFO, "Cretaed the Thread Group Component:"+root);
			}else{
				logger.error("Jmx file doesn't exist:");
				report.log(LogStatus.ERROR, "Jmx file doesn't exist:");
			}
		} catch (ParserConfigurationException e) {
			logger.error("ParserConfigurationException while "
					+ "creating the Thread Group:"+e.getMessage());
		} catch (SAXException e) {
			logger.error("SAXException while "
					+ "creating the Thread Group:"+e.getMessage());
		} catch (IOException e) {
			logger.error("IOException while "
					+ "creating the Thread Group:"+e.getMessage());
		}
	}

	

}// End Of Class
