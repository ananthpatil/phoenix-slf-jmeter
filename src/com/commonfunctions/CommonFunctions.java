package com.commonfunctions;

import io.appium.java_client.android.AndroidDriver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.apache.commons.lang3.time.DateUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.collect.Ordering;
import com.relevantcodes.extentreports.LogStatus;
//import java.util.Random;
//import bsh.ParseException;
//import com.utilities.Common_Bean;


public class CommonFunctions extends Action {

	// Common_Bean bean = new Common_Bean();

	public static HashMap<String, String> valuesMap = new HashMap<String, String>();
	public static int bookingRefID = 2439380;
	public static String pound = "\u00a3";
	public static String euro = "\u20ac";
	public static String multiply = "\u00D7";
	public static String customerid;
	public static String packageNameMC;
	public static String childAgeMC;
	public static FileOutputStream out;
	public static FileInputStream in;
	
	
	public static ArrayList<String> BCPagevalues = new ArrayList<String>();
	public static ArrayList<String> DBvalues = new ArrayList<String>();
	public static Connection dbconn;
	public static String dynamicurl;

	public static String UrlDetails;
	public static FileOutputStream out1;
	public static FileInputStream in1;
	public static File file;
	public static String bookingref;
    
	public static String Gallerybookflowimage, flightextrasBookflowimage;
	public static boolean Video, Gallery;
	public static List<String> BaggageWeightValues = new ArrayList<String>();
	public static Set<String> SeatsList = new LinkedHashSet<String>();
	public static Hashtable<String, String> bookingDetails = new Hashtable<String, String>();

	/**
	 * FC Brand filter text verification
	 * 
	 * @param searchResultCount
	 * @param typFilter
	 * @param brandSelction
	 * @param fltClseButton
	 * @param brandFilterText
	 * @throws InterruptedException
	 */
	public void fcBrandFilterVerification(String searchResultCount,
			String typFilter, String brandSelction, String fltClseButton,
			String brandFilterText) throws InterruptedException {
		String brandText, accmCt, brandName;
		List<String> listOfAccomSection;
		// PerformActionSleep("5000");
		performActionClick(typFilter);
		PerformActionSleep("3000");
		brandText = performActionGetText(brandSelction);
		performActionClick(brandSelction);

		Thread.sleep(5000L);
		performActionMoveToElement(fltClseButton);
		performActionClick(fltClseButton);
		String res = performActionGetText(searchResultCount);
		if (res.matches("\\d+")) {
			try {
				if (brandText != null) {
					logger.info("Theselected accom feature from search filter panel: "
							+ brandText);
					report.log(LogStatus.INFO,
							"The selected accom feature from search filter panel: "
									+ brandText);
					accmCt = brandText.replaceAll("[^0-9]+", "").trim();
					if (accmCt.matches("\\d+")) {
						scrollSearchPage(accmCt);
						PerformActionSleep("1000");
						listOfAccomSection = performActionMultipleGetText(brandFilterText);
						if (listOfAccomSection.size() == Integer
								.parseInt(accmCt)) {
							compareStringTwoPage(
									brandSelction.replace("Name", "Count"),
									brandFilterText, accmCt,
									String.valueOf(listOfAccomSection.size()));
						} else {
							compareStringTwoPage(
									brandSelction.replace("Name", "Count"),
									brandFilterText, accmCt,
									String.valueOf(listOfAccomSection.size()));
						}
						brandText = brandText.replaceAll("[0-9]+", "")
								.replace("(", "").replace(")", "").trim();
						for (int r = 0; r < listOfAccomSection.size(); r++) {
							brandName = listOfAccomSection.get(r).trim();
							compareStringTwoPage(brandSelction,
									brandFilterText, brandText, brandName);
						}
					}
				}
			} catch (Exception e) {
				report.log(
						LogStatus.INFO,
						"Exception in fcBrandFilterVerification method:"
								+ e.getMessage());
			}
		}
	}

	/**
	 * The method is used to verify the flexible dates based on selected date
	 * 
	 * @param enteredDate
	 * @param displayDateForflexible
	 * @returns void
	 * @author Anant
	 */
	public void dateVerificationForFlexibleSelection(String enteredDate,
			String displayDateForflexible)  {
		String slctdDate;
		PerformActionSleep("4000");
		slctdDate = performActionGetText(enteredDate);
		List<String> flexibleDates = performActionMultipleGetText(displayDateForflexible);
		if (slctdDate != null) {
			slctdDate = slctdDate.replace(" ", "").replaceAll("\\(.+?\\)", "")
					.trim();
			SimpleDateFormat fmt = new SimpleDateFormat();
			Calendar cal = Calendar.getInstance();
			if (slctdDate.matches("\\w{3}\\d{1,2}\\w{3,}\\d{4}"))
				fmt.applyPattern("EEEddMMMyyyy");
			else if (slctdDate.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2}"))
				fmt.applyPattern("yyyy-MM-dd");
			else if (slctdDate.matches("\\d{1,2}\\w{3,}\\d{4}"))
				fmt.applyPattern("ddMMMyyyy");
			else {
				fmt.applyPattern("dd-MM-yyyy");
			}
			try {
				fmt.applyPattern("EEEddMMM");
				Date seldDate = fmt.parse(slctdDate);
				cal.setTime(seldDate);
				cal.add(Calendar.DATE, 3);
				Date addedDat = cal.getTime();

				cal.setTime(seldDate);
				cal.add(Calendar.DATE, -3);
				Date subDate = cal.getTime();

				for (int d = 0; d < flexibleDates.size(); d++) {
					String flexDate = flexibleDates.get(d).replace(" ", "")
							.replace("\n", "").trim();
					if (flexDate.matches("\\w{3}\\d{1,2}\\w{3,}\\d{4}"))
						fmt.applyPattern("EEEddMMMyyyy");
					else if (flexDate.matches("\\w{3}\\d{1,2}\\w{3,}"))
						fmt.applyPattern("EEEddMMM");
					else {
						fmt.applyPattern("ddMMMyyyy");
					}
					addedDat = fmt.parse(fmt.format(addedDat));
					subDate = fmt.parse(fmt.format(subDate));
					Date applnDat = fmt.parse(flexDate);
					if ((applnDat.after(subDate) && applnDat.before(addedDat))
							|| (applnDat.equals(subDate) || applnDat
									.equals(addedDat))) {
						compareStringOnePage(displayDateForflexible, true,
								addedDat.toString() + "or" + subDate,
								applnDat.toString());
					} else {
						compareStringOnePage(displayDateForflexible, false,
								addedDat.toString() + "or" + subDate,
								applnDat.toString());
					}
				}
			} catch (java.text.ParseException e) {
				report.log(
						LogStatus.INFO,
						"Exception occured in the dateVerification method:"
								+ e.getMessage());
				logger.error("Exception occured in the dateVerification method:"
						+ e);
			}
		} else {
			report.log(LogStatus.ERROR, enteredDate
					+ "Could't get the text for this object");
		}
	}

	/**
	 * The method is used to select the duration randomly and verify the search
	 * results displayed with selected duration or not
	 * 
	 * @param durationVal
	 * @param resultCount
	 * @param searchResultsDutaion
	 * @throws InterruptedException
	 */
	public void durationFilterVerification(String durationVal,
			String resultCount, String searchResultsDutaion)
			throws InterruptedException {
		String randSelDuration, totResCt, searchDurVal;
		randSelDuration = performActionRandomClickandGetText(durationVal,
				durationVal);
		if (randSelDuration != null) {
			totResCt = performActionGetText(resultCount);
			if (totResCt.matches("\\d+")) {
				scrollSearchPage(totResCt);
				List<String> durVal = performActionMultipleGetText(searchResultsDutaion);
				for (int d = 0; d < durVal.size(); d++) {
					try {
						searchDurVal = durVal.get(d)
								.substring(durVal.get(d).indexOf("-") + 1)
								.replace("nights", "").replace("night", "");
						searchDurVal = searchDurVal.trim();
						if (Integer.parseInt(randSelDuration) == Integer
								.parseInt(searchDurVal)) {
							compareStringTwoPage(durationVal,
									searchResultsDutaion, randSelDuration,
									searchDurVal);
						} else {
							compareStringTwoPage(durationVal,
									searchResultsDutaion, randSelDuration,
									searchDurVal);
						}
					} catch (Exception e) {
						report.log(LogStatus.INFO,
								"Exception occured in durationFilterVerification method:"
										+ e.getMessage());
						logger.error("Exception occured in durationFilterVerification method:"
								+ e.getMessage());
					}
				}
			} else {
				logger.error(resultCount
						+ " Couldn't the value for this compoent from application source");
			}
		} else {
			report.log(
					LogStatus.ERROR,
					durationVal
							+ " Couldnt get the value for this component from application");
			logger.error(durationVal
					+ " Couldnt get the value for this component from application");
		}
	}

	/**
	 * The method is used to move to the new tab and verify any text from the
	 * page
	 * 
	 * @param objectName
	 * @param objectText
	 * @author Samson
	 */
	public void newTabTextUrlValidation(String objectName, String objectText,
			String expectedUrl) {
		String url = null;
		String objectValue = null;
		String expected = objectText;
		objectDetails = getObjectDetails(objectName);
		List<String> browserTabs = new ArrayList<String>(
				driver.getWindowHandles());
		// switch to new tab
		driver.switchTo().window(browserTabs.get(1));

		try {

			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT))) {
				waitForElementToBeDisplayed(objectName);
				url = driver.getCurrentUrl();
				report.log(LogStatus.INFO, "Page Url is captured as: " + url);
				if (url.equalsIgnoreCase(expectedUrl)) {
					logger.info("URL captured is the same as expected");
					report.log(LogStatus.PASS,
							"URL captured is the same as expected");
				} else {
					report.log(LogStatus.FAIL,
							"URL captured is NOT the same as expected."
									+ " Expected: " + expectedUrl + " Actual: "
									+ url);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info("URL captured is NOT the same as expected."
							+ " Expected: " + expectedUrl + " Actual: " + url);
				}
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					objectValue = driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					objectValue = driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					objectValue = driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					objectValue = driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {

					objectValue = regexExtractor(
							objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE),
							driver.getPageSource());
					if (objectValue.equals("")) {
						objectValue = null;
					}

					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");
				}

				if (objectValue.equalsIgnoreCase(expected)) {
					logger.info(objectName
							+ "Text captured is the same as expected");
					report.log(LogStatus.PASS, objectName
							+ "Text captured is the same as expected");
				} else {
					report.log(LogStatus.FAIL,
							"Text captured is NOT the same as expected."
									+ " Expected: " + expected + " Actual: "
									+ objectValue);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(objectName
							+ "Text captured is NOT the same as expected."
							+ " Expected: " + expected + " Actual: "
							+ objectValue);
				}

			}

		} catch (Exception e) {
			report.log(LogStatus.FAIL, "Text could not be captured");
			logger.error("Error occured :" + e.getMessage());
			report.attachScreenshot(takeScreenShotExtentReports());
		}
		driver.close();
		driver.switchTo().window(browserTabs.get(0));
	}

	/**
	 * The method is used to move to the new tab and verify any text from the
	 * page
	 * 
	 * @param objectName
	 * @param objectText
	 * @author Samson
	 */
	public void newTabTextValidation(String objectName, String objectText) {
		String url = null;
		String objectValue = null;
		String expected = objectText;
		objectDetails = getObjectDetails(objectName);
		List<String> browserTabs = new ArrayList<String>(
				driver.getWindowHandles());
		// switch to new tab
		driver.switchTo().window(browserTabs.get(1));

		try {

			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT))) {
				waitForElementToBeDisplayed(objectName);
				url = driver.getCurrentUrl();
				report.log(LogStatus.INFO, "Page Url is captured as: " + url);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					objectValue = driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					objectValue = driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					objectValue = driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					objectValue = driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.getText();
					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");

				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_REGEX)) {

					objectValue = regexExtractor(
							objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE),
							driver.getPageSource());
					if (objectValue.equals("")) {
						objectValue = null;
					}

					logger.info(objectName + " text is captured as"
							+ objectValue);
					report.log(LogStatus.INFO, objectName
							+ " text is captured as " + "<strong>"
							+ objectValue + "</strong>");
				}

				if (objectValue.equalsIgnoreCase(expected)) {
					logger.info(objectName
							+ "Text captured is the same as expected");
					report.log(LogStatus.PASS, objectName
							+ "Text captured is the same as expected");
				} else {
					report.log(LogStatus.FAIL,
							"Text captured is NOT the same as expected."
									+ " Expected: " + expected + " Actual: "
									+ objectValue);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(objectName
							+ "Text captured is NOT the same as expected."
							+ " Expected: " + expected + " Actual: "
							+ objectValue);
				}

			}

		} catch (Exception e) {
			report.log(LogStatus.FAIL, "Text could not be captured");
			logger.error("Error occured :" + e.getMessage());
			report.attachScreenshot(takeScreenShotExtentReports());
		}
		driver.close();
		driver.switchTo().window(browserTabs.get(0));
	}

	/**
	 * Method to select verify deposit as per days and Select deposit type
	 * 
	 * @date Dec 2016
	 * @param String
	 * @Updated by Anant
	 * @author Swati
	 * @return void
	 */
	public void daysCalculationAndDeposit(String depDate, String lowDeposit,
			String standDeposit, String fullDeposit) throws Exception {
		SimpleDateFormat fmt = new SimpleDateFormat();
		String depDat = performActionGetText(depDate);
		if (depDat != null) {
			depDat = depDat.trim().replace(" ", "");
			try {
				if (depDat.matches("\\w{3}\\d{1,2}\\w{3,}\\d{4}")) {
					fmt.applyPattern("EEEddMMMyyyy");
				} else if (depDat.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2}")) {
					fmt.applyPattern("yyyy-MM-dd");
				} else if (depDat.matches("\\d{1,2}\\w{3,}\\d{4}")) {
					fmt.applyPattern("ddMMMyyyy");
				} else {
					fmt.applyPattern("dd-MM-yyyy");
				}
				Date depD = fmt.parse(depDat);
				Date currDate = new Date();

				long diff = depD.getTime() - currDate.getTime();
				long dif = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
				if (dif > 90) {
					performActionComponentPresent(lowDeposit);
					performActionComponentPresent(standDeposit);
					performActionComponentPresent(fullDeposit);
				} else if (dif >= 57 && diff <= 84) {
					performActionComponentPresent(standDeposit);
					performActionComponentPresent(fullDeposit);
				} else if (diff < 57) {
					performActionComponentPresent(fullDeposit);
				}
			} catch (Exception e) {
				logger.error("The exception occured in daysCalculationAndDeposit method:"
						+ e.getMessage());
			}
		} else {
			report.log(LogStatus.ERROR,
					"The departure date not capture in the daysCalculationAndDeposit method:");
		}
	}

	public void browserRefresh() {
		driver.navigate().refresh();
		report.log(LogStatus.INFO, "Refreshed the browser");
	}

	/**
	 * Method to compare price from previous page to current page(If difference
	 * will be les than equal to 2 then it will not throw error)
	 * 
	 * @date OCT 2016
	 * @param String
	 * @author Swati
	 * @return void
	 * @updated By Anant
	 */
	public void ValidatingPrice(String ExpectedPrice, String ActualPrice) {
		try {
			String expected = valuesMap.get(ExpectedPrice)
					.substring(valuesMap.get(ExpectedPrice).indexOf("^^") + 2)
					.replaceAll("[^0-9.]", "");

			String Actual = valuesMap.get(ActualPrice)
					.substring(valuesMap.get(ActualPrice).indexOf("^^") + 2)
					.replaceAll("[^0-9.]", "");

			if (!(Actual.equals("") || expected.equals(""))) {
				if (Actual.contains(expected) || expected.contains(Actual)) {
					report.log(LogStatus.PASS, "Expected Text" + " " + expected
							+ " " + "Actual Text" + " " + Actual);
				} else {
					if ((Double.parseDouble(Actual)
							- Double.parseDouble(expected) <= 2 && Double
							.parseDouble(Actual) - Double.parseDouble(expected) >= 0)
							|| (Double.parseDouble(expected)
									- Double.parseDouble(Actual) <= 2 && Double
									.parseDouble(expected)
									- Double.parseDouble(Actual) >= 0)) {
						report.log(LogStatus.PASS,
								"Actual and expected price diffrence is less than equal to 2. Expected Text"
										+ " " + expected + " " + "Actual Text"
										+ " " + Actual);
					} else {
						report.log(LogStatus.FAIL, "Expected Text" + " "
								+ expected + " " + "Actual Text" + " " + Actual);
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
			} else {
				report.log(LogStatus.FAIL,
						"unable to get the actual text or expected text");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error While Validating text due to "
					+ e);
		}
	}

	/**
	 * @author Swati The method is used to select the Single insurance for
	 *         cruise
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param GetaquateButtonClick
	 * @param agreeChk
	 * @param diffPrc
	 * @param inSelection
	 * @param totPrcAftAlt
	 */
	public void CruisesingleInsuranceSelection(String totPrcBfAlt,
			String insBtnClick, String GetaquateButtonClick, String agreeChk,
			String diffPrc, String inSelection, String totPrcAftAlt)  {
		String ttPrcBfAlt, dfTotPrc, addedTp, ttPrcAftAlt;
		boolean flag;
		ttPrcBfAlt = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO,
				"The total price before selecting alternate: " + ttPrcBfAlt);
		performActionClick(insBtnClick);
		PerformActionSleep("1500");

		performActionClick(GetaquateButtonClick);
		PerformActionSleep("800");

		performActionClick(agreeChk);
		PerformActionSleep("800");
		dfTotPrc = performActionRandomClickandGetText(inSelection, diffPrc);
		report.log(LogStatus.INFO,
				"The difference total price for selected alternate: "
						+ dfTotPrc);

		if (ttPrcBfAlt != null && dfTotPrc != null) {
			addedTp = prcAdder("+" + dfTotPrc, ttPrcBfAlt);
			try {
				PerformActionSleep("1000");
				ttPrcAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
						"[^0-9\\.]+", "");
				if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(ttPrcAftAlt)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(totPrcAftAlt, flag, addedTp,
							ttPrcAftAlt);
				} else {
					flag = false;
					compareStringOnePage(totPrcAftAlt, flag, addedTp,
							ttPrcAftAlt);
				}
			} catch (Exception e) {
				logger.info("The exeception occured in the singleInsuranceSelection: "
						+ e.getMessage());
			}
		}

	}

	/**
	 * The method is used to select the Single insurance for cruise
	 * 
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param GetaquateButtonClick
	 * @param AddExcessWeiver
	 * @param agreeChk
	 * @param diffPrc
	 * @param inSelection
	 * @param totPrcAftAlt
	 * @author Swathi
	 * @Updated by Anant
	 * @return void
	 */
	public void CruisesingleInsuranceSelectionExcessWeiver(String totPrcBfAlt,
			String insBtnClick, String getaquateButtonClick,
			String excessWeiverChK, String agreeChk, String diffPrc,
			String inSelection, String totPrcAftAlt)   {
		String ttPrcBfAlt, dfTotPrc, addedTp, ttPrcAftAlt;
		boolean flag;
		ttPrcBfAlt = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO,
				"The total price before selecting alternate: " + ttPrcBfAlt);
		performActionClick(insBtnClick);
		PerformActionSleep("1500");

		performActionClick(getaquateButtonClick);
		PerformActionSleep("1500");

		performActionClick(excessWeiverChK);
		PerformActionSleep("800");

		performActionClick(agreeChk);
		PerformActionSleep("800");
		dfTotPrc = performActionRandomClickandGetText(inSelection, diffPrc);
		report.log(LogStatus.INFO,
				"The difference total price for selected alternate: "
						+ dfTotPrc);

		if (ttPrcBfAlt != null && dfTotPrc != null) {
			addedTp = prcAdder("+" + dfTotPrc, ttPrcBfAlt);
			try {
				PerformActionSleep("1000");
				ttPrcAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
						"[^0-9\\.]+", "");
				// if(Double.parseDouble(addedTp) ==
				// Double.parseDouble(ttPrcAftAlt)){
				if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(ttPrcAftAlt)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(totPrcAftAlt, flag, addedTp,
							ttPrcAftAlt);
				} else {
					flag = false;
					compareStringOnePage(totPrcAftAlt, flag, addedTp,
							ttPrcAftAlt);
				}
			} catch (Exception e) {
				logger.info("The exeception occured in the singleInsuranceSelection: "
						+ e.getMessage());
			}
		}

	}

	/**
	 * The method is used to select the family insurance with excess weiver
	 * 
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param diffPrc
	 * @param insSelection
	 * @param addButton
	 * @param totPrcAftAlt
	 * @author Swati Updated By Anant
	 */
	public void cruiseFamilyInsSelectionwithExcessweiver(String totPrcBfAlt,
			String insBtnClick, String excessWeiverChk, String agrChkBox,
			String diffPrc, String addButton, String totPrcAftAlt)  {
		String totPrcBfSel, dfTotPrc, addedTp, tpAftAlt;
		boolean flag;
		totPrcBfSel = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO, "The total price before select alternate: "
				+ totPrcBfAlt);
		PerformActionSleep("500");
		performActionClick(insBtnClick);
		PerformActionSleep("1500");

		performActionClick(excessWeiverChk);
		PerformActionSleep("1500");

		performActionClick(agrChkBox);
		dfTotPrc = performActionRandomClickandGetText(addButton, diffPrc);

		PerformActionSleep("1500");
		report.log(LogStatus.INFO,
				"The difference price for selected alternate: " + dfTotPrc);
		if (totPrcBfSel != null) {
			addedTp = prcAdder(dfTotPrc, totPrcBfSel);
			try {
				tpAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
						"[^0-9\\.]+", "");
				// if(Double.parseDouble(addedTp) ==
				// Double.parseDouble(tpAftAlt)){
				if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(tpAftAlt)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
				} else {
					flag = false;
					compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
				}
			} catch (Exception e) {
				logger.info("Exception occured in familyInsuranceSelection method");
			}
		} else {
			logger.info("Total price captured null:");
		}

	}

	/**
	 * Method to calculate total holiday price from PP price(Previous page to
	 * current page)
	 * 
	 * @date OCT 2016
	 * @param String
	 * 
	 * @author Swati
	 * @return void
	 */

	public void holidayPriceCalculation(String adult, String child, String price)
			throws InterruptedException {
		try {
			int aduCount = Integer.parseInt(adult);
			int chCount = Integer.parseInt(child);
			int ppPrice = Integer.parseInt(valuesMap.get(price).substring(
					valuesMap.get(price).indexOf("^^") + 2));

			int totPrice = (aduCount * ppPrice) + (chCount * ppPrice);
			String totalPrice = "TOTALPRICE" + "^^"
					+ Integer.toString(totPrice);
			logger.info("price " + totalPrice);
			valuesMap.put("totalPrice", totalPrice);
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error While Validating text due to "
					+ e);
		}

	}

	/**
	 * The method is used to scroll the window bar up
	 * 
	 * @return void
	 * @author Anant
	 */
	public void scrollWindowUp() {
		try {
			((JavascriptExecutor) driver).executeScript(
					"window.scrollBy(0,-4000)", "");
			// report.log(LogStatus.INFO, objectName + " is clicked");
			logger.info("The window bar scrolled");
		} catch (Exception e) {
			// report.log(LogStatus.INFO, objectName + " is clicked");
			logger.info("The window bar is not scrolled");
			logger.error(e);
		}
	}

	/**
	 * The method is used to set the adultCount,childCount and childernAges
	 * Returns void
	 */
	/*
	 * public void paxComposition() { String ad,chd,chdAg; ad =
	 * regexExtractor("noOfAdults=(.+?)\u0026"); chd =
	 * regexExtractor("noOfChildren=(.+?)\u0026"); chdAg =
	 * regexExtractor("childrenAge=(.+?)\u0026"); if(ad != null){
	 * bean.setAdults(Integer.parseInt(ad)); } if(chd != null){
	 * bean.setAdults(Integer.parseInt(chd)); } if(chdAg != null){
	 * bean.setChildAges(chdAg); }
	 * 
	 * }
	 */

	/**
	 * The method is used to extract the desired string from page source
	 * 
	 * @param regex
	 * @return String
	 * @author Anant
	 */
	public String regexExtractorPattern(String regex) {
		String px = null;
		int ct = 1;
		Pattern pat = Pattern.compile(driver.getPageSource());
		Matcher mat = pat.matcher(regex);
		try {
			while (mat.find()) {
				if (ct == 1) {
					px = mat.group(1);
					break;
				}
				ct++;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return px;
	}

	/**
	 * Method to click Insurance option
	 * 
	 * @date Oct 2016
	 * @param String
	 *            Scenario,Boolean flag,String Expected, String Actual
	 * @return void
	 */
	@SuppressWarnings("unused")
	public void clickAcceptOwnInsurance(String objectName) {
		try {
			performActionClick(objectName);
			if (verifyIsPopUpDisplayed() == true) {
				performActionClick(objectName);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method used to add
	 * 
	 * @param value
	 * @param prc
	 * @author Anant
	 * @return
	 */
	public String prcAdder(String value, String prc) {
		double sum = 0;
		String val = "", retval;
		DecimalFormat dt = new DecimalFormat("#.00");
		if (value != null) {
			if (!value.equals("")) {
				retval = value.replaceAll("[^0-9\\.\\-\\+]+", "").trim();
				prc = prc.replaceAll("[^0-9\\.]+", "").trim();
				try {
					if (retval.contains("+")) {
						retval = retval.replace("+", "");
						sum = Double.parseDouble(retval)
								+ Double.parseDouble(prc);
					} else if (retval.contains("-")) {
						retval = retval.replace("-", "");
						sum = Double.parseDouble(prc)
								- Double.parseDouble(retval);
					} else {
						retval = retval.replace("+", "");
						sum = Double.parseDouble(retval)
								+ Double.parseDouble(prc);
					}
				} catch (Exception e) {
					logger.info("Price addition not happend properly:");
					report.log(LogStatus.INFO,
							"Price addition not happend properly:");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
		}
		val = dt.format(sum);
		return val;
	}

	/**
	 * @author Anant The method is used to select the prebookInfant option
	 * @param ppPrice
	 * @param totPrice
	 * @param preBkInfBtn
	 * @param drpDown
	 * @param drpValue
	 * @param addButton
	 * @param diffTotPrc
	 * @throws InterruptedException
	 */
	public void preBookInfant(String ppPrice, String totPrice,
			String preBkInfBtn, String drpDown, String drpValue,
			String addButton, String diffTotPrc) throws InterruptedException {
		// int adutlt,child,inf;
		String totPrc, ppPrc, diffPrc;
		totPrc = performActionGetText(totPrice);
		ppPrc = performActionGetText(ppPrice);
		performActionClick(preBkInfBtn);
		PerformActionSleep("1200");
		performActionClick(drpDown);
		PerformActionSleep("2000");
		performActionSelectDropDown(drpValue, "1");
		diffPrc = performActionGetText(diffTotPrc);
		performActionClick(addButton);
	}

	public void preBookInfantNursery(String ppPrice, String totPrice,
			String preBkNurBtn, String radioButton, String diffTotPrc,
			String addButton) throws InterruptedException {
		// int adutlt,child,inf;

		String totPrc, ppPrc, diffPrc;
		totPrc = performActionGetText(totPrice);
		ppPrc = performActionGetText(ppPrice);
		PerformActionSleep("3000");
		performActionClick(preBkNurBtn);
		PerformActionSleep("2000");
		performActionClick(radioButton);
		PerformActionSleep("1000");
		diffPrc = performActionGetText(diffTotPrc);
		performActionClick(addButton);
	}

	/**
	 * The method used to verify the text of meals from page to popup
	 * 
	 * @param mlText
	 * @param adMlTxt
	 * @param inFltMealTextBfAlt
	 * @param adMealBfText
	 * @author Anant
	 */
	public void verifyMeals(List<String> mlText, List<String> adMlTxt,
			String inFltMealTextBfAlt, String adMealBfText) {
		boolean flag;
		String mlTxt, mlpTxt = null;
		logger.info("The meal size:" + mlText.size() + "\t" + adMlTxt.size());
		for (int i = 0; i < mlText.size(); i++) {
			flag = false;
			mlTxt = mlText.get(i).replaceAll("\\d+", "").replace(multiply, "");
			for (int j = 0; j < adMlTxt.size(); j++) {
				mlpTxt = adMlTxt.get(j).replace("Children's inflight meal",
						"Child Meal");
				if (mlTxt.contains(mlpTxt)) {
					flag = true;
					compareStringTwoPage(inFltMealTextBfAlt, adMealBfText,
							mlTxt, mlpTxt);
					logger.info("In flight meals verification passed:"
							+ mlText.get(i));
					break;
				}
			}
			if (flag == false) {
				mlpTxt = null;
				logger.info("In flight meals verification passed:"
						+ mlText.get(i));
				compareStringTwoPage(inFltMealTextBfAlt, adMealBfText, mlTxt,
						mlpTxt);
			}
		}
	}

	/**
	 * The method is used to verify the alternate meals upgrdation
	 * 
	 * @author Anant
	 * @param inFltMealTextBfAlt
	 * @param inFltMealBtn
	 * @param adMealBfText
	 * @param chdMealText
	 * @param arrowClick
	 * @param altMlVal
	 * @param mlTextAfterSelFromPpup
	 * @param mlchdTextAftrSelFromPpup
	 * @param Okbutton
	 * @param mlTextAfSelfrmPage
	 */
	public void inFlightMealsUpgradation(String inFltMealTextBfAlt,
			String inFltMealBtn, String adMealBfText, String chdMealText,
			String arrowClick, String altMlVal, String mlTextAfterSelFromPpup,
			String mlchdTextAftrSelFromPpup, String Okbutton,
			String mlTextAfSelfrmPage) {
		List<String> mlText, adMlTxt, chdMlTxt, mlTextAfter, adMlTxtAftSel, chdMlTxtAftSel;
		mlText = performActionMultipleGetText(inFltMealTextBfAlt);
		performActionClick(inFltMealBtn);
		adMlTxt = performActionMultipleGetText(adMealBfText);
		chdMlTxt = performActionMultipleGetText(chdMealText);
		adMlTxt.addAll(chdMlTxt);
		verifyMeals(mlText, adMlTxt, inFltMealTextBfAlt, adMealBfText);
		List<WebElement> ele = getMultipleElement(arrowClick);
		for (WebElement ele1 : ele) {
			String[] arr = { "Gluten Free Meal", "Healthy Flight Meal",
					"Vegan Flight Meal" };
			try {
				Random r1 = new Random();
				int ind = r1.nextInt(arr.length);
				ele1.click();
				PerformActionSleep("1000");
				List<WebElement> altMeallVal = getMultipleElement(altMlVal);
				for (WebElement str : altMeallVal) {
					if (str.getText().contains(arr[ind])) {
						Actions act = new Actions(driver);
						act.moveToElement(str).click().perform();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			PerformActionSleep("1000");
			adMlTxtAftSel = performActionMultipleGetText(mlTextAfterSelFromPpup);
			chdMlTxtAftSel = performActionMultipleGetText(mlchdTextAftrSelFromPpup);
			adMlTxtAftSel.addAll(chdMlTxtAftSel);
			performActionClick(Okbutton);
			PerformActionSleep("2000");
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollBy(0,-180)", "");
			mlTextAfter = performActionMultipleGetText(mlTextAfSelfrmPage);
			verifyMeals(mlTextAfter, adMlTxtAftSel, mlTextAfSelfrmPage,
					mlchdTextAftrSelFromPpup);
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occure in inFlightMealsUpgradation :"
							+ e.getMessage());
			logger.error("Error occure in inFlightMealsUpgradation :"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select alternate board in accom page
	 * 
	 * @author Anant
	 * @param totPrice
	 * @param altBdButton
	 * @param altDiffTotPrc
	 * @throws InterruptedException
	 */
	public void boardUpgradationInAccm(String totPrice, String altBdButton,
			String altDiffTotPrc, String totPrcaftrUpgrade)
			throws InterruptedException {
		String totPrcBfrAlt, diffBdPrc, added, totPrcAfrAlt, tot;
		boolean flag;
		totPrcBfrAlt = performActionGetText(totPrice);
		report.log(LogStatus.INFO,
				"The total price before selecting alternate:" + totPrcBfrAlt);

		Thread.sleep(1000L);
		diffBdPrc = performActionRandomClickandGetText(altBdButton,
				altDiffTotPrc);
		report.log(LogStatus.INFO,
				"The diffrence price for selected alternate:" + diffBdPrc);
		try {
			added = prcAdder(diffBdPrc, totPrcBfrAlt);
			PerformActionSleep("1000");

			totPrcAfrAlt = performActionGetText(totPrcaftrUpgrade).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(added)==Double.parseDouble(totPrcAfrAlt)){
			if (Math.round(Double.parseDouble(added)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(totPrcAfrAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcaftrUpgrade, flag, added,
						totPrcAfrAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcaftrUpgrade, flag, added,
						totPrcAfrAlt);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error occure in boardUpgradationInAccm :" + e.getMessage());
			logger.error("Error occure in boardUpgradationInAccm :"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select the single insurance without excess viewer
	 * for cruise
	 * 
	 * @author Anant
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param unSelChkbox
	 * @param getQuote
	 * @param agreeChk
	 * @param indDiffPrc
	 * @param addButton
	 * @param totPrcAftAlt
	 * @throws InterruptedException
	 */
	public void indInsuranceForCruise(String totPrcBfAlt, String insBtnClick,
			String unSelChkbox, String getQuote, String agreeChk,
			String indDiffPrc, String addButton, String totPrcAftAlt)
			throws InterruptedException {
		String tpBfIns, dfTotPrc, addedTp, ttPrcAftAlt;
		boolean flag;

		tpBfIns = performActionGetText(totPrcBfAlt);
		performActionClick(insBtnClick);
		waitForElementToBeDisplayed(unSelChkbox);
		List<WebElement> ele = getMultipleElement(unSelChkbox);
		for (WebElement ele1 : ele) {
			ele1.click();
			Thread.sleep(1000L);
		}

		performActionClick(getQuote);
		Thread.sleep(1000L);
		performActionClick(agreeChk);
		dfTotPrc = performActionGetText(indDiffPrc);
		report.log(LogStatus.INFO,
				"The difference total price for selected alternate: "
						+ dfTotPrc);
		Thread.sleep(2000L);
		performActionClick(addButton);

		try {
			addedTp = prcAdder(dfTotPrc, tpBfIns);
			PerformActionSleep("1000");
			ttPrcAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) ==
			// Double.parseDouble(ttPrcAftAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(ttPrcAftAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, ttPrcAftAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, ttPrcAftAlt);
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error occure in indInsuranceForCruise :" + e.getMessage());
			logger.error("Error occure in indInsuranceForCruise :"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select the single insurnce with excess viwer
	 * 
	 * @author Anant
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param unSelChkbox
	 * @param getQuote
	 * @param excessCheck
	 * @param agreeChk
	 * @param indDiffPrc
	 * @param addButton
	 * @param totPrcAftAlt
	 * @throws InterruptedException
	 */
	public void indInsuranceWithExcessForCruise(String totPrcBfAlt,
			String insBtnClick, String unSelChkbox, String getQuote,
			String excessCheck, String agreeChk, String indDiffPrc,
			String addButton, String totPrcAftAlt) throws InterruptedException{
		String tpBfIns, dfTotPrc, addedTp, ttPrcAftAlt;
		boolean flag;

		tpBfIns = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO, "The total price before alternate: "
				+ tpBfIns);
		performActionClick(insBtnClick);
		waitForElementToBeDisplayed(unSelChkbox);
		List<WebElement> ele = getMultipleElement(unSelChkbox);
		for (WebElement ele1 : ele) {
			ele1.click();
			Thread.sleep(1000L);
		}

		performActionClick(getQuote);
		Thread.sleep(1000L);
		performActionClick(excessCheck);
		Thread.sleep(1000L);
		performActionClick(agreeChk);
		dfTotPrc = performActionGetText(indDiffPrc);
		report.log(LogStatus.INFO,
				"The difference total price for selected alternate: "
						+ dfTotPrc);
		performActionClick(addButton);

		try {
			addedTp = prcAdder(dfTotPrc, tpBfIns);
			PerformActionSleep("1000");
			ttPrcAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) ==
			// Double.parseDouble(ttPrcAftAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(ttPrcAftAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, ttPrcAftAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, ttPrcAftAlt);
			}

		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occure in indInsuranceWithExcessForCruise :"
							+ e.getMessage());
			logger.error("Error occure in indInsuranceWithExcessForCruise :"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select the family insurance without excess viewer
	 * for cruise
	 * 
	 * @author Anant
	 * @param totPrcBfAlt
	 * @param famBtnClick
	 * @param agrChkBox
	 * @param diffPrc
	 * @param addButton
	 * @param totPrcAftAlt
	 * @throws InterruptedException
	 */
	public void familyInsuranceForCruise(String totPrcBfAlt,
			String famBtnClick, String agrChkBox, String diffPrc,
			String addButton, String totPrcAftAlt) throws InterruptedException {
		String totPrcBfSel, dfTotPrc, addedTp, tpAftAlt;
		boolean flag;
		totPrcBfSel = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO, "The total price before select alternate: "
				+ totPrcBfAlt);
		performActionClick(famBtnClick);
		Thread.sleep(1000L);

		performActionClick(agrChkBox);
		dfTotPrc = performActionGetText(diffPrc);
		Thread.sleep(4000L);
		performActionClick(addButton);

		PerformActionSleep("1500");
		report.log(LogStatus.INFO,
				"The difference price for selected alternate: " + dfTotPrc);
		try {
			addedTp = prcAdder(dfTotPrc, totPrcBfSel);
			tpAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) == Double.parseDouble(tpAftAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(tpAftAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occure in familyInsuranceForCruise :"
							+ e.getMessage());
			logger.error("Error occure in familyInsuranceForCruise :"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select the alternate flights by clicking change
	 * flights overlay
	 * 
	 * @author Anant
	 * @param ppBfAlt
	 * @param tpBfAlt
	 * @param difPpprice
	 * @param difTpPrice
	 * @param altButton
	 * @throws InterruptedException
	 */
	public void changeFlightOverLay(String ppBfAlt, String tpBfAlt,
			String difPpprice, String difTpPrice, String altButton)
			throws InterruptedException {
		String tpPrcBfAlt, ppPrcBfAlt, addedTp, addedP;
		List<String> difVal;
		boolean flag;

		tpPrcBfAlt = performActionGetText(tpBfAlt);
		ppPrcBfAlt = performActionGetText(ppBfAlt);
		Thread.sleep(2000L);
		report.log(LogStatus.INFO,
				"The Per person and total price before selecting alternate: "
						+ ppPrcBfAlt + "\t" + tpPrcBfAlt);
		difVal = randomClickandGetMultipleText(altButton, difTpPrice,
				difPpprice);
		report.log(LogStatus.INFO, difPpprice + ": " + difVal.get(1));
		report.log(LogStatus.INFO, difTpPrice + ": " + difVal.get(0));

		try {
			addedTp = prcAdder(difVal.get(0), tpPrcBfAlt);
			addedP = prcAdder(difVal.get(0), tpPrcBfAlt);
			// if(Double.parseDouble(addedTp) ==
			// Double.parseDouble(tpPrcBfAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(tpPrcBfAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage("TH_Flights_TotalPriceAftrAltFlights",
						flag, addedTp, tpPrcBfAlt);
			} else {
				flag = false;
				compareStringOnePage("TH_Flights_TotalPriceAftrAltFlights",
						flag, addedTp, tpPrcBfAlt);
			}

			// if(Double.parseDouble(addedP) == Double.parseDouble(ppPrcBfAlt)){
			if (Math.round(Double.parseDouble(addedP)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(ppPrcBfAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage("TH_Flights_PerPriceAftrAltFlights", flag,
						addedP, ppPrcBfAlt);
			} else {
				flag = false;
				compareStringOnePage("TH_Flights_PerPriceAftrAltFlights", flag,
						addedP, ppPrcBfAlt);
			}

		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occured in changeFlightOverLay method : "
							+ e.getMessage());
			logger.error("Error occured in changeFlightOverLay method : "
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select the family insurance excess viewer for
	 * cruise
	 * 
	 * @author Anant
	 * @param totPrcBfAlt
	 * @param famBtnClick
	 * @param agrChkBox
	 * @param diffPrc
	 * @param addButton
	 * @param totPrcAftAlt
	 * @throws InterruptedException
	 */
	public void familyInsuranceWithExcessForCruise(String totPrcBfAlt,
			String famBtnClick, String excessCheck, String agrChkBox,
			String diffPrc, String addButton, String totPrcAftAlt)
			throws InterruptedException {
		String totPrcBfSel, dfTotPrc, addedTp, tpAftAlt;
		boolean flag;
		totPrcBfSel = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO, "The total price before select alternate: "
				+ totPrcBfAlt);
		performActionClick(famBtnClick);
		Thread.sleep(1000L);

		performActionClick(excessCheck);
		Thread.sleep(1000L);
		performActionClick(agrChkBox);
		dfTotPrc = performActionGetText(diffPrc);
		Thread.sleep(1000L);
		performActionClick(addButton);

		PerformActionSleep("1500");
		report.log(LogStatus.INFO,
				"The difference price for selected alternate: " + dfTotPrc);
		try {
			addedTp = prcAdder(dfTotPrc, totPrcBfSel);
			tpAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) == Double.parseDouble(tpAftAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(tpAftAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
			}

		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occure in familyInsuranceWithExcessForCruise :"
							+ e.getMessage());
			logger.error("Error occure in familyInsuranceWithExcessForCruise :"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to select the Single insurance
	 * 
	 * @author Anant
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param unSelChkbox
	 * @param diffPrc
	 * @param inSelection
	 * @param addButton
	 * @param totPrcAftAlt
	 */
	public void singleInsuranceSelection(String totPrcBfAlt,
			String insBtnClick, String unSelChkbox, String agreeChk,
			String diffPrc, String inSelection, String totPrcAftAlt)  {
		String ttPrcBfAlt, dfTotPrc, addedTp, ttPrcAftAlt;
		boolean flag;
		ttPrcBfAlt = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO,
				"The total price before selecting alternate: " + ttPrcBfAlt);
		performActionClick(insBtnClick);
		PerformActionSleep("1500");

		List<WebElement> ele = getMultipleElement(unSelChkbox);
		for (WebElement ele1 : ele) {
			ele1.click();
			PerformActionSleep("1000");
		}
		performActionClick(agreeChk);
		PerformActionSleep("800");
		dfTotPrc = performActionRandomClickandGetText(inSelection, diffPrc);
		report.log(LogStatus.INFO,
				"The difference total price for selected alternate: "
						+ dfTotPrc);

		try {
			addedTp = prcAdder("+" + dfTotPrc, ttPrcBfAlt);
			PerformActionSleep("1000");
			ttPrcAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) ==
			// Double.parseDouble(ttPrcAftAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(ttPrcAftAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, ttPrcAftAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, ttPrcAftAlt);
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occured in singleInsuranceSelection method: "
							+ e.getMessage());
			logger.error("Error occured in singleInsuranceSelection method: "
					+ e.getMessage());
		}

	}

	/**
	 * The method is used to select the family insurance
	 * 
	 * @author Anant
	 * @param totPrcBfAlt
	 * @param insBtnClick
	 * @param diffPrc
	 * @param insSelection
	 * @param addButton
	 * @param totPrcAftAlt
	 * @throws InterruptedException
	 */
	public void familyInsuranceSelection(String totPrcBfAlt,
			String insBtnClick, String agrChkBox, String diffPrc,
			String addButton, String totPrcAftAlt) throws InterruptedException{
		String totPrcBfSel, dfTotPrc, addedTp, tpAftAlt;
		boolean flag;
		totPrcBfSel = performActionGetText(totPrcBfAlt);
		report.log(LogStatus.INFO, "The total price before select alternate: "
				+ totPrcBfAlt);
		Thread.sleep(1000L);
		performActionClick(insBtnClick);
		Thread.sleep(1000L);
		performActionClick(agrChkBox);
		Thread.sleep(1000L);
		dfTotPrc = performActionRandomClickandGetText(addButton, diffPrc);

		PerformActionSleep("1500");
		report.log(LogStatus.INFO,
				"The difference price for selected alternate: " + dfTotPrc);
		try {
			addedTp = prcAdder(dfTotPrc, totPrcBfSel);
			tpAftAlt = performActionGetText(totPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) == Double.parseDouble(tpAftAlt)){
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(tpAftAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftAlt, flag, addedTp, tpAftAlt);
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occured in familyInsuranceSelection method: "
							+ e.getMessage());
		}

	}

	/**
	 * The method is used to upgrade the taxi
	 * 
	 * @author Anant
	 * @param totPrc
	 * @param diffTotPrice
	 * @param taxiBtn
	 * @throws InterruptedException
	 */
	public void taxiUpgradationInExtras(String totPrc, String diffTotPrice,
			String taxiBtn, String totPrcAftrAlt) throws InterruptedException {
		String totPrcBfAlt, tpDiffPrc, addedTp, totPrcAfAlt;
		boolean flag = true;

		totPrcBfAlt = performActionGetText(totPrc);
		report.log(LogStatus.INFO,
				"The toal price before select alternate taxi:" + totPrcBfAlt);
		Thread.sleep(4000L);
		tpDiffPrc = performActionRandomClickandGetText(taxiBtn, diffTotPrice);
		report.log(LogStatus.INFO, "The total diff price for alternate taxi:"
				+ tpDiffPrc);
		PerformActionSleep("1000");
		try {
			addedTp = prcAdder(tpDiffPrc, totPrcBfAlt);
			totPrcAfAlt = performActionGetText(totPrcAftrAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedTp) ==
			// Double.parseDouble(totPrcAfAlt))
			if (Math.round(Double.parseDouble(addedTp)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(totPrcAfAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(totPrcAftrAlt, flag, addedTp, totPrcAfAlt);
			} else {
				flag = false;
				compareStringOnePage(totPrcAftrAlt, flag, addedTp, totPrcAfAlt);
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error ocuured in taxiUpgradationInExtras method: "
							+ e.getMessage());
			logger.error("Error ocuured in taxiUpgradationInExtras method: "
					+ e.getMessage());
		}
	}

	/**
	 * The method used to select the alternate flights Based on the
	 * date,duration and airports
	 * 
	 * @author Anant
	 * @param ppPrc
	 * @param ttPrice
	 * @param altClick
	 * @param dfPpPrc
	 * @param dfTpPrc
	 * @param ppPriceSftAlt
	 * @param tpPrcAftAlt
	 */
	public void altFlightsBasedOnDurDateAirpot(String ppPrc, String ttPrice,
			String altClick, String dfPpPrc, String dfTpPrc,
			String ppPriceSftAlt, String tpPrcAftAlt, String selButton) 

	{
		List<String> dffVal;
		String ppPrcBfAlt, tpPrcBfAlt, addedTotal, addedPP, ppPrcAfAlt, tpPrcAfAlt;
		boolean flag;
		ppPrcBfAlt = performActionGetText(ppPrc);
		tpPrcBfAlt = performActionGetText(ttPrice);

		report.log(LogStatus.INFO, "The pp price before alternate: "
				+ ppPrcBfAlt);
		report.log(LogStatus.INFO, "The total pricebefore alternate: "
				+ tpPrcBfAlt);
		dffVal = randomClickandGetMultipleText(altClick, dfTpPrc, dfPpPrc);
		PerformActionSleep("3000");
		performActionClick(selButton);
		PerformActionSleep("20000");
		report.log(LogStatus.INFO,
				"The Difference per person and total price for selected alternate: "
						+ dffVal.get(0) + "\t" + dffVal.get(1));
		addedTotal = prcAdder(dffVal.get(0), tpPrcBfAlt);
		addedPP = prcAdder(dffVal.get(1), ppPrcBfAlt);
		try {
			PerformActionSleep("3000");
			ppPrcAfAlt = performActionGetText(ppPriceSftAlt).replaceAll(
					"[^0-9\\.]+", "");
			tpPrcAfAlt = performActionGetText(tpPrcAftAlt).replaceAll(
					"[^0-9\\.]+", "");
			// if(Double.parseDouble(addedPP) ==
			// Double.parseDouble(ppPrcAfAlt)){
			if (Math.round(Double.parseDouble(addedPP)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(ppPrcAfAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(ppPriceSftAlt, flag, addedPP, ppPrcAfAlt);
			} else {
				flag = false;
				compareStringOnePage(ppPriceSftAlt, flag, addedPP, ppPrcAfAlt);
			}

			// if(Double.parseDouble(addedTotal) ==
			// Double.parseDouble(tpPrcAfAlt)){
			if (Math.round(Double.parseDouble(addedTotal)) * 100.0 / 100.0 == Math
					.round(Double.parseDouble(tpPrcAfAlt)) * 100.0 / 100.0) {
				flag = true;
				compareStringOnePage(tpPrcAftAlt, flag, addedTotal, tpPrcAfAlt);
			} else {
				flag = false;
				compareStringOnePage(tpPrcAftAlt, flag, addedTotal, tpPrcAfAlt);
			}

		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error occured altFlightsBasedOnDurDateAirpot method:"
							+ e.getMessage());
			logger.error("Error occured altFlightsBasedOnDurDateAirpot method:"
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to filter the room&Boardbasis
	 * 
	 * @author Anant
	 * @param ppPrice
	 * @param tpPrice
	 * @param altButton
	 * @param diffTotalPrice
	 * @param diffPerPrice
	 * @param duration
	 */
	public void roomAndBoardAlternateBookflow(String ppPrice, String tpPrice,
			String altButton, String diffTotalPrice, String diffPerPrice,
			String duration)  {
		DecimalFormat fmt = new DecimalFormat("#.00");
		String totPrcBfrSel, ppPrcBfrSel, addedTotal = null, addedPP = null, pp = null, tpp = null, totPrcAftrSel, ppPrcAfrSel, duraVal;
		boolean flag;

		totPrcBfrSel = performActionGetText(tpPrice);
		ppPrcBfrSel = performActionGetText(ppPrice);
		report.log(LogStatus.INFO,
				"The Per pesron and total price before selecting alternate: "
						+ totPrcBfrSel + "\t" + ppPrcBfrSel);

		List<String> prcDetails = randomClickandGetMultipleText(altButton,
				diffTotalPrice, diffPerPrice);
		try {
			tpp = prcDetails.get(0).replaceAll("[^0-9\\.\\-\\+]+", "");
			pp = prcDetails.get(1).replaceAll("[^0-9\\.\\-\\+]+", "");
			report.log(LogStatus.INFO,
					"Diffrence total and per person price after selecting alternate: "
							+ tpp + "\t" + pp);

			duraVal = performActionGetText(duration);
			if (duraVal != null) {
				duraVal = duraVal.replaceAll("[^0-9]+", "");
				Double val = Double
						.parseDouble(pp.replaceAll("[^0-9\\.]+", ""))
						* Integer.parseInt(duraVal);
				if (pp.contains("+"))
					pp = "+" + val.toString();
				else
					pp = "-" + val.toString();
			}

			if (totPrcBfrSel != null) {
				addedTotal = prcAdder(tpp, totPrcBfrSel);
				report.log(
						LogStatus.INFO,
						"The alternate differe total price and total price before select "
								+ prcDetails.get(0) + "+"
								+ totPrcBfrSel.replaceAll("[^0-9\\.]+", "")
								+ "=" + addedTotal);
			}
			if (ppPrcBfrSel != null) {
				addedPP = prcAdder(pp, ppPrcBfrSel);
				report.log(
						LogStatus.INFO,
						"The alternate differe price and total price before select "
								+ prcDetails.get(1) + "+"
								+ ppPrcBfrSel.replaceAll("[^0-9\\.]+", "")
								+ "=" + addedPP);
			}

			PerformActionSleep("1000");

			totPrcAftrSel = performActionGetText(tpPrice);
			ppPrcAfrSel = performActionGetText(ppPrice);
			try {
				totPrcAftrSel = totPrcAftrSel.replaceAll("[^0-9\\.]+", "");
				ppPrcAfrSel = ppPrcAfrSel.replaceAll("[^0-9\\.]+", "");
				// if(Double.parseDouble(addedPP)==Double.parseDouble(ppPrcAfrSel)){
				if (Math.round(Double.parseDouble(addedPP)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(ppPrcAfrSel)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(ppPrice, flag, addedPP, ppPrcAfrSel);
				} else {
					flag = false;
					compareStringOnePage(ppPrice, flag, addedPP, ppPrcAfrSel);
				}
				// if(Double.parseDouble(addedTotal)==Double.parseDouble(totPrcAftrSel)){
				if (Math.round(Double.parseDouble(addedTotal)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(totPrcAftrSel)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(tpPrice, flag, addedTotal,
							totPrcAftrSel);
				} else {
					flag = false;
					compareStringOnePage(tpPrice, flag, addedTotal,
							totPrcAftrSel);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error occured in the roomAndBoardAlternateBookflow method :");
		}
	}

	/**
	 * The method is use to scroll the search rseults
	 * 
	 * @author Anant
	 * @param res
	 * @return void
	 * @throws InterruptedException
	 */
	public void scrollSearchPage(String res) throws InterruptedException {
		int resCt, qt, pgInd;
		resCt = Integer.parseInt(res.trim());
		qt = resCt / 10;
		if (resCt % 10 == 0) {
			pgInd = qt;
		} else {
			pgInd = qt + 1;
		}
		for (int index = 0; index < pgInd; index++) {
			logger.info("the index value:" + index);
			JavascriptExecutor js = ((JavascriptExecutor) driver);
			js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
			Thread.sleep(6000L);
		}
	}

	/**
	 * The method is used to sort the dates based on date sort option
	 * 
	 * @author Anant
	 * @param searchRsltCt
	 * @param srtFlterClk
	 * @param srtOptSelection
	 * @param accmNames
	 * @throws InterruptedException
	 */
	public void dateSort(String searchRsltCt, String srtFlterClk,
			String srtOptSelection, String datVal) throws InterruptedException {
		List<String> datesBfSrt, datesAftSort;
		String srtDtValPg = "", srtdDtVlfmcd = "";
		List<Date> datValue = new ArrayList<Date>();
		SimpleDateFormat fmt = new SimpleDateFormat("EEEddMMMyyyy");
		Date d;
		boolean flag;

		String res = performActionGetText(searchRsltCt);
		if (res.matches("\\d+")) {

			scrollSearchPage(res);
			datesBfSrt = performActionMultipleGetText(datVal);
			logger.info("The disc value in search page before selecting the sorting: "
					+ datesBfSrt.size());
			for (String dat : datesBfSrt) {
				if (dat.length() > 0 && dat != null) {
					try {
						dat = dat.substring(0, dat.indexOf("-")).trim()
								.replace(" ", "");
						d = fmt.parse(dat);
						datValue.add(d);
					} catch (Exception e) {
						logger.error("The execption occuren in date sort method as unable to parse the stirng to date:"
								+ e.getMessage());
					}
				}
			}

			Collections.sort(datValue);
			performActionClick(srtFlterClk);
			performActionClick(srtOptSelection);
			Thread.sleep(1500L);
			scrollSearchPage(res);
			datesAftSort = performActionMultipleGetText(datVal);
			logger.info("The disc value in search page after selecting the sorting: "
					+ datesAftSort.size());
			for (int k = 0; k < datesAftSort.size(); k++) {
				if (datesAftSort.get(k) != null) {
					srtDtValPg = srtDtValPg
							+ datesAftSort
									.get(k)
									.substring(0,
											datesAftSort.get(k).indexOf("-"))
									.trim().replace(" ", "") + "|";
				}
			}
			try {
				fmt.applyLocalizedPattern("EEEdMMMyyyy");
				for (int k = 0; k < datValue.size(); k++) {
					try {
						String val = fmt.format(datValue.get(k));
						srtdDtVlfmcd = srtdDtVlfmcd + val + "|";
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				logger.info("the value:" + srtDtValPg + "\t" + srtdDtVlfmcd);
				if (srtDtValPg.equals(srtdDtVlfmcd)) {
					flag = true;
					compareStringOnePage(datVal, flag, srtdDtVlfmcd, srtDtValPg);
				} else {
					flag = false;
					compareStringOnePage(datVal, flag, srtdDtVlfmcd, srtDtValPg);
				}
			} catch (Exception e) {
				report.log(LogStatus.ERROR,
						"Unable to parse the date to string in dateSort method: "
								+ e.getMessage());
			}
		}
	}

	/**
	 * The method is used to sort the accom names based on A to Z in search
	 * panel
	 * 
	 * @author Anant
	 * @param searchRsltCt
	 * @param srtFlterClk
	 * @param srtOptSelection
	 * @param accmNames
	 * @throws InterruptedException
	 */
	public void aTozSorting(String searchRsltCt, String srtFlterClk,
			String srtOptSelection, String accmNames)
			throws InterruptedException {
		List<String> accmNamesBfSrt, accmNamesAftSort;
		String srtAccValPg = "", srtdAccVlfmcd = "";
		List<String> accN = new ArrayList<String>();
		boolean flag;

		String res = performActionGetText(searchRsltCt);
		if (res.matches("\\d+")) {

			scrollSearchPage(res);
			accmNamesBfSrt = performActionMultipleGetText(accmNames);
			logger.info("The disc value in search page before selecting the sorting: "
					+ accmNamesBfSrt.size());
			for (String acc : accmNamesBfSrt) {
				if (acc.length() > 0 && acc != null) {
					try {
						acc = acc.trim();
						accN.add(acc);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			Collections.sort(accN);
			performActionClick(srtFlterClk);
			Thread.sleep(500);
			performActionClick(srtOptSelection);
			Thread.sleep(1500L);
			scrollSearchPage(res);
			accmNamesAftSort = performActionMultipleGetText(accmNames);
			logger.info("The disc value in search page after selecting the sorting: "
					+ accmNamesAftSort.size());
			for (int k = 0; k < accmNamesAftSort.size(); k++) {
				if (accmNamesAftSort.get(k) != null) {
					srtAccValPg = srtAccValPg + accmNamesAftSort.get(k) + "|";
				}
			}
			for (int k = 0; k < accN.size(); k++) {
				srtdAccVlfmcd = srtdAccVlfmcd + accN.get(k) + "|";
			}
			logger.info("the value:" + srtAccValPg + "\t" + srtdAccVlfmcd);
			if (srtAccValPg.equals(srtdAccVlfmcd)) {
				flag = true;
				compareStringOnePage(accmNames, flag, srtdAccVlfmcd,
						srtAccValPg);
			} else {
				flag = false;
				compareStringOnePage(accmNames, flag, srtdAccVlfmcd,
						srtAccValPg);
			}
		}
	}

	/**
	 * The method is used to sort based on saving amount
	 * 
	 * @author Anant
	 * @param searchRsltCt
	 * @param ppDiscVal
	 * @param srtFlterClk
	 * @param srtOptSelection
	 * @throws InterruptedException
	 */
	public void sortingFilterSavingAmount(String searchRsltCt,
			String srtFlterClk, String srtOptSelection, String ppDiscVal)
			throws InterruptedException {
		List<String> dscLstBfSrt, dscLstAftSort;
		String srtDscpValPg = "", srtdDscVlfmcd = "";
		List<Integer> disc = new ArrayList<Integer>();
		boolean flag;

		String res = performActionGetText(searchRsltCt);
		if (res.matches("\\d+")) {

			scrollSearchPage(res);
			dscLstBfSrt = performActionMultipleGetText(ppDiscVal);
			logger.info("The disc value in search page before selecting the sorting: "
					+ dscLstBfSrt.size());
			for (String dsc : dscLstBfSrt) {
				if (dsc.length() > 0 && dsc != null) {
					try {
						dsc = dsc.replaceAll("[^0-9\\.]+", "").trim();
						disc.add(Integer.valueOf(dsc.trim()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			Collections.sort(disc);
			Collections.reverse(disc);
			performActionClick(srtFlterClk);
			performActionClick(srtOptSelection);
			Thread.sleep(1500L);
			scrollSearchPage(res);
			dscLstAftSort = performActionMultipleGetText(ppDiscVal);
			logger.info("The disc value in search page after selecting the sorting: "
					+ dscLstBfSrt.size());
			for (int k = 0; k < dscLstAftSort.size(); k++) {
				if (dscLstAftSort.get(k) != null) {
					srtDscpValPg = srtDscpValPg
							+ dscLstAftSort.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
				}
			}
			for (int k = 0; k < disc.size(); k++) {
				srtdDscVlfmcd = srtdDscVlfmcd + disc.get(k) + "|";
			}
			logger.info("the value:" + srtDscpValPg + "\t" + srtdDscVlfmcd);
			if (srtDscpValPg.equals(srtdDscVlfmcd)) {
				flag = true;
				compareStringOnePage(ppDiscVal, flag, srtdDscVlfmcd,
						srtDscpValPg);
			} else {
				flag = false;
				compareStringOnePage(ppDiscVal, flag, srtdDscVlfmcd,
						srtDscpValPg);
			}
		}
	}

	/**
	 * The method is used to sort the price value from low to high
	 * 
	 * @author Anant
	 * @param resultCount
	 * @param srtFltArrwClick
	 * @param fltSelection
	 * @param perPrcVal
	 * @param toggelClick
	 * @param totPrcVal
	 * @throws InterruptedException
	 */
	public void sortingFilterLowToHigh(String resultCount,
			String srtFltArrwClick, String fltSelection, String perPrcVal,
			String toggelClick, String totPrcVal) throws InterruptedException {
		List<String> prcAftrSrt, ppBeforeSrt, tpBeforeSort;
		List<Integer> ppBfsrt, tpBfsrt;
		ppBfsrt = new ArrayList<Integer>();
		tpBfsrt = new ArrayList<Integer>();
		String srtdppValPg = "", srtdppVlfmcd = "", srtdtpValPg = "", srtdtpVlfmcd = "";
		boolean flag;

		String res = performActionGetText(resultCount);
		if (res.matches("\\d+")) {
			scrollSearchPage(res);
			ppBeforeSrt = performActionMultipleGetText(perPrcVal);
			for (String pp : ppBeforeSrt) {
				if (pp != null && pp.length() > 0) {
					ppBfsrt.add(Integer.valueOf(pp.replaceAll("[^0-9\\.]+", "")
							.trim()));
				}
			}
			logger.info("The pp value in search page before sorting :"
					+ ppBfsrt.size());
			Collections.sort(ppBfsrt);
			performActionClick(srtFltArrwClick);
			PerformActionSleep("1000");
			performActionClick(fltSelection);
			PerformActionSleep("1500");
			scrollSearchPage(res);
			prcAftrSrt = performActionMultipleGetText(perPrcVal);
			logger.info("The pp value in search page after sorting :"
					+ prcAftrSrt.size());

			for (int k = 0; k < prcAftrSrt.size(); k++) {
				if (prcAftrSrt.get(k) != null)
					srtdppValPg = srtdppValPg
							+ prcAftrSrt.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
			}
			for (int k = 0; k < ppBfsrt.size(); k++) {
				srtdppVlfmcd = srtdppVlfmcd + ppBfsrt.get(k) + "|";
			}
			logger.info("the value:" + srtdppValPg + "\t" + srtdppVlfmcd);
			if (srtdppValPg.equals(srtdppVlfmcd)) {
				flag = true;
				compareStringOnePage(perPrcVal, flag, srtdppVlfmcd, srtdppValPg);
			} else {
				flag = false;
				compareStringOnePage(perPrcVal, flag, srtdppVlfmcd, srtdppValPg);
			}
			//
			PerformActionSleep("800");
			performActionClick(toggelClick);
			tpBeforeSort = performActionMultipleGetText(totPrcVal);
			for (String tpp : tpBeforeSort) {
				if (tpp != null && tpp.length() > 0) {
					tpBfsrt.add(Integer.valueOf(tpp
							.replaceAll("[^0-9\\.]+", "").trim()));
				}
			}

			logger.info("The tpp value in search page before sorting: "
					+ tpBeforeSort.size());
			Collections.sort(tpBfsrt);
			performActionClick(srtFltArrwClick);
			PerformActionSleep("1000");
			performActionClick(fltSelection);
			PerformActionSleep("1500");
			scrollSearchPage(res);
			prcAftrSrt = performActionMultipleGetText(totPrcVal);
			logger.info("The tpp value in search page before sorting: "
					+ prcAftrSrt.size());
			for (int k = 0; k < prcAftrSrt.size(); k++) {
				if (prcAftrSrt.get(k) != null)
					srtdtpValPg = srtdtpValPg
							+ prcAftrSrt.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
			}
			for (int k = 0; k < tpBfsrt.size(); k++) {
				srtdtpVlfmcd = srtdtpVlfmcd + tpBfsrt.get(k) + "|";
			}
			logger.info("the value:" + srtdtpValPg + "\t" + srtdtpVlfmcd);
			if (srtdtpValPg.equals(srtdtpVlfmcd)) {
				flag = true;
				compareStringOnePage(totPrcVal, flag, srtdtpVlfmcd, srtdtpValPg);
			} else {
				flag = false;
				compareStringOnePage(totPrcVal, flag, srtdtpVlfmcd, srtdtpValPg);
			}
		}
		PerformActionSleep("800");
		performActionClick(toggelClick);
		// logger.info("the price val:"+srtdtpValPg+"\n"+srtdtpVlfmcd);
	}

	/**
	 * The method is used to sort the price value from high to low
	 * 
	 * @author Anant
	 * @param resultCount
	 * @param srtFltArrwClick
	 * @param fltSelection
	 * @param perPrcVal
	 * @param toggelClick
	 * @param totPrcVal
	 * @throws InterruptedException
	 */
	public void sortingFilterHighToLow(String resultCount,
			String srtFltArrwClick, String fltSelection, String perPrcVal,
			String toggelClick, String totPrcVal) throws InterruptedException {
		List<String> prcAftrSrt, ppBeforeSrt, tpBeforeSort;
		List<Integer> ppBfsrt, tpBfsrt;
		ppBfsrt = new ArrayList<Integer>();
		tpBfsrt = new ArrayList<Integer>();
		String srtdppValPg = "", srtdppVlfmcd = "", srtdtpValPg = "", srtdtpVlfmcd = "";
		boolean flag;
		String res = performActionGetText(resultCount);
		if (res.matches("\\d+")) {
			scrollSearchPage(res);
			logger.info("the index val:" + res);
			ppBeforeSrt = performActionMultipleGetText(perPrcVal);
			for (String pp : ppBeforeSrt) {
				if (pp != null && pp.length() > 0) {
					ppBfsrt.add(Integer.valueOf(pp.replaceAll("[^0-9\\.]+", "")));
				}
			}
			logger.info("The pp value in search page before sort: "
					+ ppBeforeSrt.size());
			Collections.sort(ppBfsrt);
			Collections.reverse(ppBfsrt);
			performActionClick(srtFltArrwClick);
			PerformActionSleep("1000");
			performActionClick(fltSelection);
			PerformActionSleep("1500");
			scrollSearchPage(res);
			prcAftrSrt = performActionMultipleGetText(perPrcVal);
			logger.info("The pp value in search page before sort: "
					+ prcAftrSrt.size());

			for (int k = 0; k < prcAftrSrt.size(); k++) {
				if (prcAftrSrt.get(k) != null)
					srtdppValPg = srtdppValPg
							+ prcAftrSrt.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
			}
			for (int k = 0; k < ppBfsrt.size(); k++) {
				srtdppVlfmcd = srtdppVlfmcd + ppBfsrt.get(k) + "|";
			}
			if (srtdppValPg.equals(srtdppVlfmcd)) {
				flag = true;
				compareStringOnePage(perPrcVal, flag, srtdppVlfmcd, srtdppValPg);
			} else {
				flag = false;
				compareStringOnePage(perPrcVal, flag, srtdppVlfmcd, srtdppValPg);
			}
			//
			performActionClick(toggelClick);
			tpBeforeSort = performActionMultipleGetText(totPrcVal);
			for (String tpp : tpBeforeSort) {
				if (tpp != null && tpp.length() > 0) {
					tpBfsrt.add(Integer.valueOf(tpp
							.replaceAll("[^0-9\\.]+", "")));
				}
			}
			Collections.sort(tpBfsrt);
			Collections.reverse(tpBfsrt);
			logger.info("The tpp value in search page before sort: "
					+ tpBeforeSort.size());
			performActionClick(srtFltArrwClick);
			PerformActionSleep("1000");
			performActionClick(fltSelection);
			PerformActionSleep("1500");
			scrollSearchPage(res);
			prcAftrSrt = performActionMultipleGetText(totPrcVal);
			logger.info("The tpp value in search page before sort: "
					+ prcAftrSrt.size());
			for (int k = 0; k < prcAftrSrt.size(); k++) {
				if (prcAftrSrt.get(k) != null)
					srtdtpValPg = srtdtpValPg
							+ prcAftrSrt.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
			}
			for (int k = 0; k < tpBfsrt.size(); k++) {
				srtdtpVlfmcd = srtdtpVlfmcd + tpBfsrt.get(k) + "|";
			}
			logger.info("the value:" + srtdtpValPg + "\t" + srtdtpVlfmcd);
			if (srtdtpValPg.equals(srtdtpVlfmcd)) {
				flag = true;
				compareStringOnePage(totPrcVal, flag, srtdtpVlfmcd, srtdtpValPg);
			} else {
				flag = false;
				compareStringOnePage(totPrcVal, flag, srtdtpVlfmcd, srtdtpValPg);
			}
			performActionClick(toggelClick);
		}
	}

	/**
	 * The method is used to filter the price by selecting the toggle button It
	 * accepts on priceFiletr,prcSection and closeButton as parameters
	 * 
	 * @returns void
	 */
	public void priceFilter(String priceFiletr, String prcSection,
			String closeButton) {
		String dataValue;
		List<String> listOfPrcSection;
		int totResult;
		performActionClick(priceFiletr);
		dataValue = performActionGetAttributeValue(priceFiletr, "data-value");
		try {
			if (dataValue.equals("total")) {
				dataValue = "Total Price".toLowerCase();
				logger.info("Thee total price toggel button is selected");
			} else
				dataValue = "Per Person".toLowerCase();
		} catch (Exception e) {
			e.printStackTrace();
		}

		PerformActionSleep("500");
		listOfPrcSection = performActionMultipleGetText(prcSection);
		if (listOfPrcSection.size() >= 10)
			totResult = 10;
		else
			totResult = listOfPrcSection.size();

		for (int i = 0; i < totResult; i++) {
			if (listOfPrcSection.get(i).toString().toLowerCase()
					.equals(dataValue)) {
				compareStringContent(prcSection, dataValue, listOfPrcSection
						.get(i).toString().toLowerCase());
			} else {
				compareStringContent(prcSection, dataValue, listOfPrcSection
						.get(i).toString().toLowerCase());
			}
		}
	}

	/**
	 * The method is used to to filter the boardbasis
	 * 
	 * @author Anant It accepts on boardFilter,bdFltChkbx,seldBrdname,brdNameVal
	 *         and closeButton as parameters returns void
	 * @throws InterruptedException
	 */
	public void boardFilter(String searchResultCount, String boardFilter,
			String bdFltChkbx, String seldBrdname, String brdNameVal,
			String fltClseButton) throws InterruptedException {
		String brdName, brd = null, brdCt;
		List<String> listOfbrdSection = new ArrayList<String>();
		int totResult;
		PerformActionSleep("3000");
		performActionClick(boardFilter);
		PerformActionSleep("2000");
		brdName = performActionRandomClickandGetText(bdFltChkbx, seldBrdname);

		Thread.sleep(5000L);
		performActionMoveToElement(fltClseButton);
		Thread.sleep(5000L);
		performActionClick(fltClseButton);
		// objectDetails = getObjectDetails(fltClseButton);
		// WebElement ele =
		// driver.findElement(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		// Actions act = new Actions(driver);
		// act.moveToElement(ele).click().perform();

		PerformActionSleep("6000");
		String res = performActionGetText(searchResultCount);
		if (res.matches("\\d+")) {
			// scrollSearchPage(res);
			// listOfbrdSection = performActionMultipleGetText(brdNameVal);
			// logger.info("The list of board size:"+listOfbrdSection.size());
			try {
				if (brdName != null) {
					brd = brdName.replaceAll("[^A-Za-z& ]+", "");
					brdCt = brdName.replaceAll("[A-Za-z() ]+", "").trim();
					logger.info("Thee selected boradbasis from search filter panel: "
							+ brdName + "\t" + brdCt);
					report.log(LogStatus.INFO,
							"Thee selected boradbasis from search filter panel: "
									+ brdName + "\t" + brdCt);
					if (brdCt.matches("\\d+")) {
						scrollSearchPage(brdCt);
						listOfbrdSection = performActionMultipleGetText(brdNameVal);
						logger.info("The list of board size:"
								+ listOfbrdSection.size());
						if (listOfbrdSection.size() == Integer.parseInt(brdCt)) {
							compareStringTwoPage("TH_SearchFilter_BoardCount",
									"TH_SearchResults_BoardCount",
									String.valueOf(listOfbrdSection.size()),
									brdCt);
						} else {
							compareStringTwoPage("TH_SearchFilter_BoardCount",
									"TH_SearchResults_BoardCount",
									String.valueOf(listOfbrdSection.size()),
									brdCt);
						}
					}
				}
			} catch (Exception e) {
				report.log(
						LogStatus.ERROR,
						"Error occured in boardFilter method: "
								+ e.getMessage());
				logger.error("Error occured in boardFilter method: "
						+ e.getMessage());
			}

			if (listOfbrdSection.size() >= 10)
				totResult = 10;
			else
				totResult = listOfbrdSection.size();
			for (int i = 0; i < totResult; i++) {
				if (listOfbrdSection.get(i).toString().toLowerCase()
						.equals(brd.toLowerCase())) {
					compareStringTwoPage(seldBrdname, brdNameVal, brd,
							listOfbrdSection.get(i).toString());
				} else {
					compareStringTwoPage(seldBrdname, brdNameVal, brd,
							listOfbrdSection.get(i).toString());
				}
			}
		}
	}

	/**
	 * The method is used to filter the type
	 * 
	 * @author Anant
	 * @param searchResultCount
	 * @param typFilter
	 * @param typChkBox
	 * @param typFeature
	 * @param accmTextCt
	 * @param fltClseButton
	 * @throws InterruptedException
	 */
	public void destnFilter(String searchResultCount, String destFilter,
			String destChkBox, String destnSeldVal, String accmTextCt,
			String fltClseButton) throws InterruptedException {
		String destFeatur, accmCt;
		List<String> listOfAccomSection;
		PerformActionSleep("4000");
		performActionClick(destFilter);
		PerformActionSleep("3000");
		destFeatur = performActionRandomClickandGetText(destChkBox,
				destnSeldVal);

		Thread.sleep(5000L);
		performActionMoveToElement(fltClseButton);
		Thread.sleep(5000L);
		performActionClick(fltClseButton);

		Thread.sleep(4000L);
		String res = performActionGetText(searchResultCount);
		if (res.matches("\\d+")) {
			try {
				if (destFeatur != null) {
					logger.info("Thee selected accom feature from search filter panel: "
							+ destFeatur);
					report.log(LogStatus.INFO,
							" Thee selected accom feature from search filter panel: "
									+ destFeatur);
					accmCt = destFeatur.replaceAll("[^0-9]+", "").trim();
					if (accmCt.matches("\\d+")) {
						scrollSearchPage(accmCt);
						PerformActionSleep("1000");
						listOfAccomSection = performActionMultipleGetText(accmTextCt);
						if (listOfAccomSection.size() == Integer
								.parseInt(accmCt)) {
							compareStringTwoPage(destnSeldVal, accmTextCt,
									accmCt,
									String.valueOf(listOfAccomSection.size()));
						} else {
							compareStringTwoPage(destnSeldVal, accmTextCt,
									accmCt,
									String.valueOf(listOfAccomSection.size()));
						}
					}
				}
			} catch (Exception e) {
				report.log(
						LogStatus.ERROR,
						"Error occured in destnFilter method: "
								+ e.getMessage());
				logger.error("Error occured in destnFilter method: "
						+ e.getMessage());
			}
		}
	}

	/**
	 * The method is used to filter the type
	 * 
	 * @author Anant
	 * @param searchResultCount
	 * @param typFilter
	 * @param typChkBox
	 * @param typFeature
	 * @param accmTextCt
	 * @param fltClseButton
	 * @throws InterruptedException
	 */
	public void rateFilter(String searchResultCount, String rateFilter,
			String rateChkBox, String rateVal, String accmTextCt,
			String fltClseButton) throws InterruptedException {
		String rateFeatur, accmCt;
		List<String> listOfAccomSection;
		PerformActionSleep("1500");
		performActionClick(rateFilter);
		PerformActionSleep("3000");
		rateFeatur = performActionRandomClickandGetText(rateChkBox, rateVal);

		Thread.sleep(5000L);
		performActionMoveToElement(fltClseButton);
		Thread.sleep(5000L);
		performActionClick(fltClseButton);

		// String res =performActionGetText(searchResultCount);
		/*
		 * if(res.matches("\\d+")) { // scrollSearchPage(res); //
		 * PerformActionSleep("1000"); // listOfAccomSection =
		 * performActionMultipleGetText(accmTextCt); try{ if(rateFeatur !=
		 * null){
		 * logger.info("Thee selected accom feature from search filter panel: "
		 * +rateFeatur); accmCt = rateFeatur.replaceAll("[^0-9]+", "").trim();
		 * if(accmCt.matches("\\d+")) { scrollSearchPage(accmCt);
		 * PerformActionSleep("1000"); listOfAccomSection =
		 * performActionMultipleGetText(accmTextCt);
		 * if(listOfAccomSection.size() == Integer.parseInt(accmCt)){
		 * compareStringTwoPage(rateVal, accmTextCt,accmCt,
		 * String.valueOf(listOfAccomSection.size())); }else{
		 * compareStringTwoPage(rateVal,accmTextCt,
		 * accmCt,String.valueOf(listOfAccomSection.size())); } } }
		 * }catch(Exception e){ e.printStackTrace(); } }
		 */
	}

	/**
	 * The method is used to filter the type
	 * 
	 * @author Anant
	 * @param searchResultCount
	 * @param typFilter
	 * @param typChkBox
	 * @param typFeature
	 * @param accmTextCt
	 * @param fltClseButton
	 * @throws InterruptedException
	 */
	public void typeFilter(String searchResultCount, String typFilter,
			String typChkBox, String typFeature, String accmTextCt,
			String fltClseButton) throws InterruptedException {
		String typFeatur, accmCt;
		List<String> listOfAccomSection;
		PerformActionSleep("5000");
		performActionClick(typFilter);
		PerformActionSleep("3000");
		typFeatur = performActionRandomClickandGetText(typChkBox, typFeature);

		Thread.sleep(5000L);
		performActionMoveToElement(fltClseButton);
		Thread.sleep(5000L);
		performActionClick(fltClseButton);
		// objectDetails = getObjectDetails(fltClseButton);
		// WebElement ele =
		// driver.findElement(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		// Actions act = new Actions(driver);
		// act.moveToElement(ele).click().perform();

		String res = performActionGetText(searchResultCount);
		if (res.matches("\\d+")) {
			// scrollSearchPage(res);
			// PerformActionSleep("1000");
			// listOfAccomSection = performActionMultipleGetText(accmTextCt);
			try {
				if (typFeatur != null) {
					logger.info("Theselected accom feature from search filter panel: "
							+ typFeatur);
					report.log(LogStatus.INFO,
							"The selected accom feature from search filter panel: "
									+ typFeatur);
					accmCt = typFeatur.replaceAll("[^0-9]+", "").trim();
					if (accmCt.matches("\\d+")) {
						scrollSearchPage(accmCt);
						PerformActionSleep("1000");
						listOfAccomSection = performActionMultipleGetText(accmTextCt);
						if (listOfAccomSection.size() == Integer
								.parseInt(accmCt)) {
							compareStringTwoPage(typFeature, accmTextCt,
									accmCt,
									String.valueOf(listOfAccomSection.size()));
						} else {
							compareStringTwoPage(typFeature, accmTextCt,
									accmCt,
									String.valueOf(listOfAccomSection.size()));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * The method is used to filter the accom feature
	 * 
	 * @author Anant
	 * @param accmFilter
	 * @param accmChkBox
	 * @param accmFeature
	 * @param brdNameVal
	 * @param fltClseButton
	 *            returns void
	 * @throws InterruptedException
	 */
	public void accomFilter(String searchResultCount, String accmFilter,
			String accmChkBox, String accmFeature, String accmTextCt,
			String fltClseButton) throws InterruptedException {
		String accFeature, accmCt;
		List<String> listOfAccomSection;
		PerformActionSleep("5000");
		performActionClick(accmFilter);
		PerformActionSleep("3000");
		accFeature = performActionRandomClickandGetText(accmChkBox, accmFeature);
		report.log(LogStatus.INFO, "The Selected feature: " + accFeature);

		Thread.sleep(5000L);
		performActionMoveToElement(fltClseButton);
		Thread.sleep(5000L);
		performActionClick(fltClseButton);
		// objectDetails = getObjectDetails(fltClseButton);
		// WebElement ele =
		// driver.findElement(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		// Actions act = new Actions(driver);
		// act.moveToElement(ele).click().perform();
		Thread.sleep(1000L);
		String res = performActionGetText(searchResultCount);
		if (res.matches("\\d+")) {
			// scrollSearchPage(res);
			// PerformActionSleep("1000");
			// listOfAccomSection = performActionMultipleGetText(accmTextCt);
			try {
				if (accFeature != null) {
					logger.info("Thee selected accom feature from search filter panel: "
							+ accFeature);
					accmCt = accFeature.replaceAll("[^0-9]+", "").trim();
					if (accmCt.matches("\\d+")) {
						scrollSearchPage(accmCt);
						PerformActionSleep("1000");
						listOfAccomSection = performActionMultipleGetText(accmTextCt);
						if (listOfAccomSection.size() == Integer
								.parseInt(accmCt)) {
							compareStringTwoPage(accmFeature, accmTextCt,
									accmCt,
									String.valueOf(listOfAccomSection.size()));
						} else {
							compareStringTwoPage(accmFeature, accmTextCt,
									accmCt,
									String.valueOf(listOfAccomSection.size()));
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * The method is used to filter the budget
	 * 
	 * @author Anant
	 * @param bdgFilter
	 * @param bdgChkBox
	 * @param bdgPrice
	 * @param prcValue
	 * @param fltClseButton
	 * @throws InterruptedException
	 */
	public void budgetFilter(String bdgFilter, String bdgChkBox,
			String bdgPrice, String prcValue, String fltClseButton)
			throws InterruptedException {
		String bdgPrc;
		List<String> listOfPriceSection;
		int totResult;

		PerformActionSleep("5000");
		performActionClick(bdgFilter);
		PerformActionSleep("500");
		bdgPrc = performActionRandomClickandGetText(bdgChkBox, bdgPrice);
		objectDetails = getObjectDetails(fltClseButton);

		Thread.sleep(3000L);
		performActionMoveToElement(fltClseButton);
		Thread.sleep(3000L);
		performActionClick(fltClseButton);

		// WebElement ele =
		// driver.findElement(By.xpath(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		// Actions act = new Actions(driver);
		// act.moveToElement(ele).click().perform();
		PerformActionSleep("1000");
		listOfPriceSection = performActionMultipleGetText(prcValue);

		try {
			if (bdgPrc != null) {
				bdgPrc = bdgPrc.replaceAll("[^0-9]+", "");
				logger.info("Thee selected bugdet filter price from search filter panel: "
						+ bdgPrc);
				if (bdgPrc.matches("\\d+")) {
					if (listOfPriceSection.size() >= 10)
						totResult = 10;
					else
						totResult = listOfPriceSection.size();
					for (int i = 0; i < totResult; i++) {
						if (Integer.parseInt(listOfPriceSection.get(i)) <= Integer
								.parseInt(bdgPrc)) {
							report.log(LogStatus.PASS, "PrcValue"
									+ " Verifying condition is passed from "
									+ "SearchPanel" + " to " + "SearchResults"
									+ " page where Expected:"
									+ listOfPriceSection.get(i) + " "
									+ "Actual:" + bdgPrc);
						} else {
							report.log(LogStatus.FAIL, "PrcValue"
									+ " Verifying condition is passed from "
									+ "SearchPanel" + " to " + "SearchResults"
									+ " page where Expected:"
									+ listOfPriceSection.get(i) + " "
									+ "Actual:" + bdgPrc);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method is used to sselect the alternate duration
	 * 
	 * @author Anant
	 * @param durClickFltr
	 * @param durTextFiled
	 * @param durSearchButton
	 * @param durErrorMessge
	 * @throws InterruptedException
	 */
	public void durFilter(String durClickFltr, String durTextFiled,
			String durSearchButton, String durErrorMessge)
			throws InterruptedException {
		String durErr;
		boolean flag1;
		PerformActionSleep("5000");
		performActionClick(durClickFltr);

		String[] durVal = { "1", "9", "13", "12", "15", "16", "17", "18", "8" };
		for (int j = 0; j < durVal.length; j++) {
			Thread.sleep(1000L);
			performActionEnterText(durTextFiled, durVal[j]);
			performActionClick(durSearchButton);
			Thread.sleep(1000L);
			durErr = performActionGetText(durErrorMessge);
			if (durErr != null) {
				if (durErr
						.contains("Sorry! No holidays available. Choose another duration.")) {
					flag1 = true;
					compareStringOnePage(
							durErrorMessge,
							flag1,
							"Sorry! No holidays available. Choose another duration.",
							durErr);
				} else {
					flag1 = false;
					compareStringOnePage(
							durErrorMessge,
							flag1,
							"Sorry! No holidays available. Choose another duration.",
							durErr);
				}
			} else {
				break;
			}
		}
	}

	/**
	 * The method is used to select the different flights on different date
	 * 
	 * @author Anant
	 * @param difFlights
	 */
	public void selectFlightsOnDiffDate(String difFlights)   {
		String pageSource;
		pageSource = driver.getPageSource();

		int counter = 0;
		if (pageSource
				.contains("Sorry, there are no flights  for your selected date.")) {
			List<WebElement> diffFlights = getMultipleElement(difFlights);
			for (WebElement ele : diffFlights) {
				try {
					Actions act = new Actions(driver);
					act.moveToElement(ele).click().perform();
					PerformActionSleep("3000");
					pageSource = driver.getPageSource();
					if (!pageSource
							.contains("Sorry, there are no flights  for your selected date."))
						break;
				} catch (Exception e) {
					logger.info("The differenet flights are not clicked: " + e);
				}
				counter++;
			}
			if (diffFlights.size() == counter - 1) {
				if (pageSource
						.contains("Sorry, there are no flights for your selected date.")) {
					report.log(LogStatus.INFO,
							"There are no different flights to travel");
					Assert.assertTrue(false, "Page Not Displayed");
				}
			}
		}
	}

	/**
	 * author Anant
	 * 
	 * @param ppPriceBfAlt
	 * @param totBfPrc
	 * @param selctedAlternate
	 * @param diffPpPrice
	 * @param diffTotPrice
	 * @param selectButton
	 * @param ppPrcAftAlt
	 * @param tpPrcAftAlt
	 * @throws InterruptedException
	 * @return void
	 */
	public void alternateSeatSelection(String ppPriceBfAlt, String totBfPrc,
			String selctedAlternate, String diffPpPrice, String diffTotPrice,
			String selectButton, String ppPrcAftAlt, String tpPrcAftAlt)
			throws InterruptedException {
		String ppPrcBfalt, tpPrcBfAlt, ppPrcAfalt, tpPrcAfAlt, addedPP, addedTP, sldAlt = "";
		List<String> diffVal;
		boolean flag;

		ppPrcBfalt = performActionGetText(ppPriceBfAlt);
		tpPrcBfAlt = performActionGetText(totBfPrc);
		report.log(LogStatus.INFO, "The per person and total price before"
				+ " selecting alternate: " + ppPrcBfalt + "\t" + tpPrcBfAlt);

		diffVal = randomClickandGetMultipleText(selectButton, diffTotPrice,
				diffPpPrice);
		report.log(LogStatus.INFO,
				"The difference per person and total price for "
						+ "selected alternate : " + diffVal.get(1) + "\t"
						+ diffVal.get(0));
		PerformActionSleep("1000");
		sldAlt = performActionGetText(selctedAlternate);
		if (sldAlt != null) {
			sldAlt = sldAlt.trim().replaceAll("[^0-9.]+", "");
			report.log(LogStatus.INFO,
					"The selected seat differe price in flight summay panel "
							+ selctedAlternate);
		}
		if (ppPrcBfalt != null && tpPrcBfAlt != null) {
			addedPP = prcAdder(diffVal.get(1), ppPrcBfalt);
			addedTP = prcAdder(diffVal.get(0), tpPrcBfAlt);

			Thread.sleep(1000L);
			try {
				ppPrcAfalt = performActionGetText(ppPrcAftAlt).replaceAll(
						"[^0-9\\.]+", "");
				tpPrcAfAlt = performActionGetText(tpPrcAftAlt).replaceAll(
						"[^0-9\\.]+", "");

				if (Math.round(Double.parseDouble(addedPP)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(ppPrcAfalt)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(ppPrcAftAlt, flag, addedPP, ppPrcAfalt);
				} else {
					flag = false;
					compareStringOnePage(ppPrcAftAlt, flag, addedPP, ppPrcAfalt);
				}

				if (Math.round(Double.parseDouble(addedTP)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(tpPrcAfAlt)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(tpPrcAftAlt, flag, addedTP, tpPrcAfAlt);
				} else {
					flag = false;
					compareStringOnePage(tpPrcAftAlt, flag, addedTP, tpPrcAfAlt);
				}
			} catch (Exception e) {
				logger.info("The execption occured in alternateSeatSelectio method: "
						+ e.getMessage());
			}
		}
		compareStringTwoPage(diffTotPrice, selctedAlternate, diffVal.get(0)
				.replaceAll("[^0-9.]+", ""), sldAlt);
	}

	/**
	 * @author ananth The method is used to select the alternate baggage
	 * @param totalPrice
	 * @param selctedAlternate
	 * @param diffTotalPrice
	 * @param selectButton
	 * @throws InterruptedException
	 */
	public void alternateBaggageSelection(String totalPrice,
			String selctedAlternate, String diffTotalPrice, String selectButton)
			throws InterruptedException {
		String totPrcBeforeSelct, diffPrc, totAfterSelct, sldAlt, addedP;
		boolean flag = false;

		totPrcBeforeSelct = performActionGetText(totalPrice).replaceAll(
				"[^0-9.]", "");
		diffPrc = performActionRandomClickandGetText(selectButton,
				diffTotalPrice);
		report.log(LogStatus.INFO, "The alternate seat differe price "
				+ diffPrc);
		Thread.sleep(500L);
		sldAlt = performActionGetText(selctedAlternate);
		if (sldAlt != null) {
			sldAlt = sldAlt.trim().replaceAll("[^0-9.]+", "");
			report.log(LogStatus.INFO,
					"The selected seat differe price in flight summay panel "
							+ selctedAlternate);
		}

		PerformActionSleep("500");
		if (diffPrc != null && totPrcBeforeSelct != null) {
			report.log(LogStatus.INFO,
					"The alternate baggage differe price before select: "
							+ diffPrc + "+" + totPrcBeforeSelct);
			addedP = prcAdder(diffPrc, totPrcBeforeSelct);
			try {
				totAfterSelct = performActionGetText(totalPrice).replaceAll(
						"[^0-9.]", "");
				// if(Double.parseDouble(addedP)==Double.parseDouble(totAfterSelct))
				if (Math.round(Double.parseDouble(addedP)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(totAfterSelct)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(totalPrice, flag, addedP,
							totAfterSelct);
				} else {
					flag = false;
					compareStringOnePage(totalPrice, flag, addedP,
							totAfterSelct);
				}
			} catch (Exception e) {
				logger.info("The execption occured in alternateBaggage method: "
						+ e.getMessage());
			}
		}
		compareStringTwoPage(diffTotalPrice, selctedAlternate,
				diffPrc.replace("+", "").replace("-", ""), sldAlt);
	}

	/**
	 * The method used to upgrdae the luggage in amend and cancel
	 * 
	 */
	public void priceUpgradeCompareLuggage(String Totalprice,
			String EditSeatingLink, String RandomSeatSelection,
			String randomSelectedPrice, String AddButton, String bagDiffPrice,
			String confirmButton, String confrmPayButton, String cpsTotalPrice)
			throws InterruptedException {

		String compName = "", pageValue = "", bagDifPrc = "", bagDiffPrce = "", comp1 = "", page1 = "", page2 = "";

		try {
			pageValue = randomSelectedPrice.substring(
					randomSelectedPrice.indexOf("_") + 1,
					randomSelectedPrice.lastIndexOf("_"));
			compName = randomSelectedPrice.substring(randomSelectedPrice
					.lastIndexOf("_") + 1);
			page1 = bagDiffPrice.substring(bagDiffPrice.indexOf("_") + 1,
					bagDiffPrice.lastIndexOf("_"));
			comp1 = bagDiffPrice.substring(bagDiffPrice.lastIndexOf("_") + 1);
			page2 = cpsTotalPrice.substring(cpsTotalPrice.indexOf("_") + 1,
					cpsTotalPrice.lastIndexOf("_"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		double totalPriceBeforeAddon = Double.parseDouble(performActionGetText(
				Totalprice).replace(pound, "").replace(euro, "")
				.replaceAll("[^0-9.]", ""));
		logger.info(totalPriceBeforeAddon);

		performActionClick(EditSeatingLink);

		String SeatPrice = performActionRandomClickandGetText(
				RandomSeatSelection, randomSelectedPrice);
		logger.info("The seleceted price:" + SeatPrice);
		DecimalFormat fmt = new DecimalFormat("#.00");

		Thread.sleep(300);
		performActionClick(AddButton);

		// bagDifPrc = SeatPrice.toString().replace(pound, "").replace(euro,
		// "");
		bagDifPrc = fmt.format(Double.parseDouble(SeatPrice));
		bagDiffPrce = performActionGetText(bagDiffPrice).replace(pound, "")
				.replace(euro, "");
		logger.info("The dfirrence price value:" + bagDifPrc + "\t"
				+ bagDiffPrce);
		if (bagDifPrc.equalsIgnoreCase(bagDiffPrce)) {
			report.log(LogStatus.PASS, comp1
					+ " Verifying condition is passed from " + page1
					+ " page where Expected:" + bagDifPrc + " " + "Actual:"
					+ bagDiffPrce);
		} else {
			report.log(LogStatus.FAIL, comp1
					+ " Verifying condition is passed from " + page1
					+ " page where Expected:" + bagDifPrc + " " + "Actual:"
					+ bagDiffPrce);
		}

		double PriceAfterAdding = Double.parseDouble(SeatPrice)
				+ totalPriceBeforeAddon;
		logger.info("Price after adding:" + PriceAfterAdding);

		logger.info(PriceAfterAdding = Double.parseDouble(SeatPrice)
				+ totalPriceBeforeAddon);
		report.log(LogStatus.INFO, PriceAfterAdding + "=" + SeatPrice + "+"
				+ totalPriceBeforeAddon);

		Thread.sleep(200);
		performActionClick(confirmButton);
		Thread.sleep(300);
		performActionClick(confrmPayButton);

		double totalPriceAfterAddon = Double.parseDouble(performActionGetText(
				cpsTotalPrice).replaceAll("[^0-9.]", ""));
		logger.info("Totela price after addon:" + totalPriceAfterAddon);

		if (totalPriceAfterAddon == PriceAfterAdding) {
			logger.info("Actaul price:" + totalPriceAfterAddon
					+ " and expected price:" + PriceAfterAdding + " are same");
			report.log(LogStatus.PASS, compName
					+ " Verifying condition is passed from " + pageValue
					+ " to " + page2 + " page where Expected:"
					+ PriceAfterAdding + " " + "Actual:" + totalPriceAfterAddon);
		} else {
			logger.info("Actaul price:" + totalPriceAfterAddon
					+ " and expected price:" + PriceAfterAdding
					+ " are not same");
			report.log(LogStatus.FAIL, compName
					+ " Verifying condition is failed from " + pageValue
					+ " to " + page2 + " page where Expected:"
					+ PriceAfterAdding + " " + "Actual:" + totalPriceAfterAddon);
		}
	}

	/**
	 * The method is to calculate the refund due based on the duration Which is
	 * specifically for Amend and cancel
	 * 
	 * @param periodVal
	 * @param departureDate
	 * @param totalPrice
	 * @param dueAmt
	 */
	public void priceComparisonBasedOnperiod(String periodVal,
			String departureDate, String totalPrice, String dueAmt) {
		String depDate, expected, actual, pageVal1;
		Date dpDat, curDate;
		long noOfDays;
		Integer totDays;
		DecimalFormat dt = new DecimalFormat("#.00");
		try {
			depDate = performActionGetText(departureDate).replace(" ", "");
			SimpleDateFormat fmt = new SimpleDateFormat("EEEddMMMyyyy");
			dpDat = fmt.parse(depDate);

			Calendar cal = Calendar.getInstance();
			curDate = cal.getTime();

			noOfDays = dpDat.getTime() - curDate.getTime();
			totDays = (int) (noOfDays / (1000 * 24 * 60 * 60));
			List<String> period = performActionMultipleGetText(periodVal);

			if (totDays < 70) {
				for (int j = 1; j < period.size(); j++) {
					try {
						String prdVal = period.get(j).substring(0,
								period.get(j).indexOf(" DAYS"));
						String[] prd = prdVal.split("-");

						if (totDays >= Integer.parseInt(prd[1].trim())
								&& totDays <= Integer.parseInt(prd[0].trim())) {
							int per = Integer.parseInt(period.get(j)
									.substring(period.get(j).indexOf(":") + 1)
									.replaceAll("[^\\d]+", "").trim());
							String tot = performActionGetText(totalPrice)
									.replace(pound, "").replace("", "");
							double val = Double.parseDouble(tot) * per / 100;

							expected = dt.format(val);
							actual = performActionGetText(dueAmt).replace(
									pound, "").replace("", "");
							pageVal1 = dueAmt;

							if (expected.equalsIgnoreCase(actual)) {
								logger.info("trueeeeee");
								compareStringContent(pageVal1, expected, actual);
							} else {
								logger.info("falseeeeee");
								compareStringContent(pageVal1, expected, actual);
							}
							break;
						}
					} catch (ArrayIndexOutOfBoundsException a) {
						logger.error("The ArrayIndexOutOfBoundsException in "
								+ "priceComparisonBasedOnperiod method " + a);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The method is used to unselect the late checkout and validate the price
	 * 
	 * @param Totalprice
	 * @param latecChecoutCheckbox
	 * @param lateCheckoutPrice
	 * @throws InterruptedException
	 */
	public void priceUpgradeCompareLate(String Totalprice,
			String latecChecoutCheckbox, String lateCheckoutPrice)
			throws InterruptedException {
		String totalPriceAfterAddon;
		Double priceAfterAdding;
		try {

			double totalPriceBeforeAddon = Double
					.parseDouble(performActionGetText(Totalprice).replaceAll(
							"[^0-9.]", ""));
			report.log(LogStatus.INFO,
					"The total price before deselecting the late checkout: "
							+ totalPriceBeforeAddon);
			Thread.sleep(500);
			performActionClick(latecChecoutCheckbox);

			Thread.sleep(1000);
			String latePrice = performActionGetText(lateCheckoutPrice).replace(
					pound, "").replace(pound, "");
			if (latePrice.contains("+")) {
				priceAfterAdding = totalPriceBeforeAddon
						+ Double.parseDouble(latePrice.replace("+", ""));
			} else {
				priceAfterAdding = totalPriceBeforeAddon
						- Double.parseDouble(latePrice.replace("-", ""));
			}
			logger.info("Price after substracting:" + priceAfterAdding);

			logger.info(priceAfterAdding
					+ "="
					+ Double.parseDouble(latePrice.replace("+", "").replace(
							"-", "")) + "+" + totalPriceBeforeAddon);
			report.log(
					LogStatus.INFO,
					priceAfterAdding
							+ "="
							+ Double.parseDouble(latePrice.replace("+", "")
									.replace("-", "")) + "+"
							+ totalPriceBeforeAddon);

			Thread.sleep(500);
			totalPriceAfterAddon = performActionGetText(Totalprice).replaceAll(
					"[^0-9.]", "");
			logger.info("Totela price after addon:" + totalPriceAfterAddon);
			priceComparisonForOnePage(Totalprice, priceAfterAdding.toString(),
					totalPriceAfterAddon);
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Exception occured in priceUpgradeCompareLate due to this problem:"
							+ e);
			logger.error("Exception occured in priceUpgradeCompareLate due to this problem:"
					+ e);
		}
	}

	/**
	 * The method for changing the seat and validating the price for Amend nad
	 * cancel
	 * 
	 * @param totalPrc
	 * @param seatDescBefore
	 * @param EditSeatingLink
	 * @param popupText
	 * @param randomSeatSelection
	 * @param randomSelectedPrice
	 * @param seatDiffPrice
	 * @param seatDescAfter
	 * @param AddButton
	 * @param confirmChangesButton
	 * @param confrmButton
	 * @param bookOverviewLink
	 * @param seatLink
	 * @param totalPrcAftAlt
	 */
	public void changeSeatAC(String totalPrc, String seatDescBefore,
			String EditSeatingLink, String popupText,
			String randomSeatSelection, String randomSelectedPrice,
			String seatDiffPrice, String seatDescAfter, String AddButton,
			String confirmChangesButton, String confrmButton,
			String bookOverviewLink, String seatLink, String totalPrcAftAlt) {
		String seatDiff, totalPriceAfterAddon;
		try {

			report.log(LogStatus.INFO,
					"The seat Description before selecting the alternate seat "
							+ performActionGetText(seatDescBefore));

			double totalPriceBeforeAddon = Double
					.parseDouble(performActionGetText(totalPrc).replaceAll(
							"[^0-9.]", ""));
			report.log(LogStatus.INFO,
					"The total price before select alternate:"
							+ totalPriceBeforeAddon);
			logger.info("The total price before select alternate:"
					+ totalPriceBeforeAddon);

			performActionClick(EditSeatingLink);
			Thread.sleep(1000);
			performActionComponentPresent(popupText);

			String seatPrice = performActionRandomClickandGetText(
					randomSeatSelection, randomSelectedPrice);
			seatPrice = seatPrice.replaceAll("[^0-9.]", "");
			report.log(LogStatus.INFO, "The difference price for selected seat"
					+ seatPrice);
			logger.info("The difference price for selected seat:" + seatPrice);

			Thread.sleep(100);
			performActionClick(AddButton);

			seatDiff = performActionGetText(seatDiffPrice).replaceAll(
					"[^0-9.]", "");
			priceComparisonForOnePage(seatDiffPrice, seatPrice, seatDiff);

			Double PriceAfterAdding = Double.parseDouble(seatPrice)
					+ totalPriceBeforeAddon;
			logger.info("Price after adding:" + PriceAfterAdding);

			logger.info(PriceAfterAdding = Double.parseDouble(seatPrice)
					+ totalPriceBeforeAddon);
			report.log(LogStatus.INFO, PriceAfterAdding + "=" + seatPrice + "+"
					+ totalPriceBeforeAddon);

			report.log(LogStatus.INFO,
					"The seat Description after selecting the alternate seat"
							+ performActionGetText(seatDescAfter));

			Thread.sleep(1000);
			performActionClick(confirmChangesButton);

			Thread.sleep(1000);
			performActionClick(confrmButton);

			Thread.sleep(1000);
			performActionClick(bookOverviewLink);

			Thread.sleep(1000);
			performActionClick(seatLink);

			totalPriceAfterAddon = performActionGetText(totalPrcAftAlt)
					.replaceAll("[^0-9.]", "");
			logger.info("Total price after addon:" + totalPriceAfterAddon);
			priceComparisonForTwoPage(randomSelectedPrice, totalPrcAftAlt,
					PriceAfterAdding.toString(), totalPriceAfterAddon);

		} catch (Exception e) {
			logger.error("The execption occured in changeSeatAC method:" + e);
			report.log(LogStatus.ERROR,
					"The execption occured in changeSeatAC method:" + e);
		}
	}

	/**
	 * The method is used to change the luggage options and validate the price
	 * 
	 * @param's totPrcBfAlt,editLuggageLink,randomSeatSelection
	 * @param's randomSelectedPrice,addButton,bagDiffPrice,confirmChangeButton
	 * @param confButton
	 *            ,bookOverviewLink,seatLuglink
	 * @param totPrcAftAlt
	 * @throws InterruptedException
	 */
	public void changeLuggageAC(String totPrcBfAlt, String editLuggageLink,
			String randomSeatSelection, String randomSelectedPrice,
			String addButton, String bagDiffPrice, String confirmChangeButton,
			String confButton, String bookOverviewLink, String seatLuglink,
			String totPrcAftAlt) throws InterruptedException {

		String bagDiffPrce = "", totalPriceAfterAddon;

		try {
			double totalPriceBeforeAddon = Double
					.parseDouble(performActionGetText(totPrcBfAlt).replaceAll(
							"[^0-9.]", ""));
			report.log(LogStatus.INFO,
					"The total price before selecting the alternate: "
							+ totalPriceBeforeAddon);
			logger.info("The total price before selecting the alternate: "
					+ totalPriceBeforeAddon);

			performActionClick(editLuggageLink);

			String bagPrice = performActionRandomClickandGetText(
					randomSeatSelection, randomSelectedPrice);
			bagPrice = bagPrice.replaceAll("[^0-9.]", "");
			report.log(LogStatus.INFO,
					"The difference price for selected baggage" + bagPrice);
			logger.info("The difference price for selected baggage:" + bagPrice);

			Thread.sleep(1000);
			performActionClick(addButton);

			bagDiffPrce = performActionGetText(bagDiffPrice).replaceAll(
					"[^0-9.]", "");
			priceComparisonForOnePage(bagDiffPrice, bagPrice, bagDiffPrce);

			Double priceAfterAdding = Double.parseDouble(bagPrice)
					+ totalPriceBeforeAddon;
			logger.info("Price after adding:" + priceAfterAdding);

			logger.info(priceAfterAdding = Double.parseDouble(bagPrice)
					+ totalPriceBeforeAddon);
			report.log(LogStatus.INFO, priceAfterAdding + "=" + bagPrice + "+"
					+ totalPriceBeforeAddon);

			Thread.sleep(1000);
			performActionClick(confirmChangeButton);
			Thread.sleep(1000);
			performActionClick(confButton);

			Thread.sleep(1000);
			performActionClick(bookOverviewLink);

			Thread.sleep(1000);
			performActionClick(seatLuglink);

			totalPriceAfterAddon = performActionGetText(totPrcAftAlt)
					.replaceAll("[^0-9.]", "");
			logger.info("Totela price after addon:" + totalPriceAfterAddon);
			priceComparisonForTwoPage(totPrcAftAlt, randomSelectedPrice,
					priceAfterAdding.toString(), totalPriceAfterAddon);
		} catch (Exception e) {
			logger.error("The execption occured in changeLuggageAC method:" + e);
			report.log(LogStatus.ERROR,
					"The execption occured in changeLuggageAC method:" + e);
		}

	}

	/**
	 * The method used to change the transfer and validate the price for AC
	 * 
	 * @param Totalprice
	 *            ,transferDescBefore
	 * @param randomTransferSelection
	 *            ,randomTransferSelectPrice
	 * @param confirmChangesButton
	 *            ,tranDifPrc
	 * @param confirmButton
	 *            ,bookingOverview
	 * @param transLink
	 * @param totPrcAftalt
	 * @throws InterruptedException
	 */
	public void changeTranferAC(String Totalprice, String transferDescBefore,
			String randomTransferSelection, String randomTransferSelectPrice,
			String confirmChangesButton, String tranDifPrc,
			String confirmButton, String bookingOverview, String transLink,
			String totPrcAftalt) throws InterruptedException {
		String transPrice, totalPriceAfterAddon;
		try {
			report.log(LogStatus.INFO,
					"The Transfer Description before selecting the alternate transfer "
							+ performActionGetText(transferDescBefore));

			double totalPriceBeforeAddon = Double
					.parseDouble(performActionGetText(Totalprice).replaceAll(
							"[^0-9.]", ""));
			report.log(LogStatus.INFO,
					"The total price before select alternate:"
							+ totalPriceBeforeAddon);
			logger.info("The total price before select alternate:"
					+ totalPriceBeforeAddon);

			Thread.sleep(500);
			String transSelPrice = performActionRandomClickandGetText(
					randomTransferSelection, randomTransferSelectPrice);
			transSelPrice = transSelPrice.replaceAll("[^0-9.]", "");
			report.log(LogStatus.INFO,
					"The difference price for selected Transfer"
							+ transSelPrice);
			logger.info("The difference price for selected Transfer"
					+ transSelPrice);

			transPrice = performActionGetText(tranDifPrc).replaceAll("[^0-9.]",
					"");
			priceComparisonForOnePage(tranDifPrc, transSelPrice, transPrice);

			Double priceAfterAdding = Double.parseDouble(transSelPrice)
					+ totalPriceBeforeAddon;
			logger.info("Price after adding:" + priceAfterAdding);

			logger.info(priceAfterAdding = Double.parseDouble(transSelPrice)
					+ totalPriceBeforeAddon);
			report.log(LogStatus.INFO, priceAfterAdding + "=" + transSelPrice
					+ "+" + totalPriceBeforeAddon);

			Thread.sleep(1000);
			performActionClick(confirmChangesButton);

			Thread.sleep(1000);
			performActionClick(confirmButton);

			Thread.sleep(1000);
			performActionClick(bookingOverview);

			Thread.sleep(1000);
			performActionClick(transLink);

			Thread.sleep(1000);

			totalPriceAfterAddon = performActionGetText(totPrcAftalt)
					.replaceAll("[^0-9.]", "");
			logger.info("Toatal price of application after seleceting alternate transfer:"
					+ totalPriceAfterAddon);
			priceComparisonForTwoPage(totPrcAftalt, randomTransferSelectPrice,
					priceAfterAdding.toString(), totalPriceAfterAddon);
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Excpetion occured in changeTranferAC method due to this problem:"
							+ e);
			logger.error("Excpetion occured in changeTranferAC method due to this problem:"
					+ e);
		}
	}

	/**
	 * The method used to select the alternate seat in amend and cancel 
	 * 
	 */
	public void priceUpgradeCompareSeating(String Totalprice,
			String setDescBefore, String EditSeatingLink, String popupText,
			String RandomSeatSelection, String randomSelectedPrice,
			String seatDiffPrice, String seatDescAfter, String AddButton,
			String confirmButton, String confrmPayButton, String cpsTotalPrice)
			throws InterruptedException {

		String compName = "", pageValue = "", seatVal, seatDiff, page1 = "", comp1 = "", page2 = "";

		try {
			pageValue = randomSelectedPrice.substring(
					randomSelectedPrice.indexOf("_") + 1,
					randomSelectedPrice.lastIndexOf("_"));
			compName = randomSelectedPrice.substring(randomSelectedPrice
					.lastIndexOf("_") + 1);

			page1 = seatDiffPrice.substring(seatDiffPrice.indexOf("_") + 1,
					seatDiffPrice.lastIndexOf("_"));
			comp1 = seatDiffPrice.substring(seatDiffPrice.lastIndexOf("_") + 1);
			page2 = cpsTotalPrice.substring(cpsTotalPrice.indexOf("_") + 1,
					cpsTotalPrice.lastIndexOf("_"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		report.log(LogStatus.INFO,
				"The seat Description before selecting the alternate seat "
						+ performActionGetText(setDescBefore));

		double totalPriceBeforeAddon = Double.parseDouble(performActionGetText(
				Totalprice).replace(pound, "").replace(euro, "")
				.replaceAll("[^0-9.]", ""));
		logger.info("the total price before addon:" + totalPriceBeforeAddon);

		performActionClick(EditSeatingLink);

		Thread.sleep(100);

		performActionComponentPresent(popupText);

		// logger.info("The before seleceted
		// price:"+RandomSeatSelection+"\t"+randomSelectedPrice);
		String SeatPrice = performActionRandomClickandGetText(
				RandomSeatSelection, randomSelectedPrice);
		DecimalFormat dt = new DecimalFormat("#.00");
		report.log(LogStatus.INFO, "The difference price for selected seat"
				+ totalPriceBeforeAddon);
		logger.info("The seleceted price:" + SeatPrice);

		Thread.sleep(100);
		performActionClick(AddButton);

		// seatVal = SeatPrice.toString().replace(pound,"");
		seatVal = dt.format(Double.parseDouble(SeatPrice));
		seatDiff = performActionGetText(seatDiffPrice).replace(pound, "");
		if (seatVal.equalsIgnoreCase(seatDiff)) {
			report.log(LogStatus.PASS, comp1
					+ " Verifying condition is passed from " + page1
					+ " page where Expected:" + seatVal + " " + "Actual:"
					+ seatDiff);
		} else {
			report.log(LogStatus.FAIL, comp1
					+ " Verifying condition is passed from " + page1
					+ " page where Expected:" + seatVal + " " + "Actual:"
					+ seatDiff);
		}

		double PriceAfterAdding = Double.parseDouble(SeatPrice)
				+ totalPriceBeforeAddon;
		logger.info("Price after adding:" + PriceAfterAdding);

		logger.info(PriceAfterAdding = Double.parseDouble(SeatPrice)
				+ totalPriceBeforeAddon);
		report.log(LogStatus.INFO, PriceAfterAdding + "=" + SeatPrice + "+"
				+ totalPriceBeforeAddon);

		report.log(LogStatus.INFO,
				"The seat Description after selecting the alternate seat"
						+ performActionGetText(seatDescAfter));

		Thread.sleep(200);
		performActionClick(confirmButton);

		Thread.sleep(300);
		performActionClick(confrmPayButton);

		Thread.sleep(300);
		logger.info("The totl value:"
				+ performActionGetText(cpsTotalPrice).replaceAll("[^0-9.]", ""));
		double totalPriceAfterAddon = Double.parseDouble(performActionGetText(
				cpsTotalPrice).replaceAll("[^0-9.]", ""));
		logger.info("Totela price after addon:" + totalPriceAfterAddon);

		if (totalPriceAfterAddon == PriceAfterAdding) {
			logger.info("Actaul price:" + totalPriceAfterAddon
					+ " and expected price:" + PriceAfterAdding + " are same");
			report.log(LogStatus.PASS, compName
					+ " Verifying condition is passed from " + pageValue
					+ " to " + page2 + " page where Expected:"
					+ PriceAfterAdding + " " + "Actual:" + totalPriceAfterAddon);
		} else {
			logger.info("Actaul price:" + totalPriceAfterAddon
					+ " and expected price:" + PriceAfterAdding
					+ " are not same");
			report.log(LogStatus.FAIL, compName
					+ " Verifying condition is failed from " + pageValue
					+ " to " + page2 + " page where Expected:"
					+ PriceAfterAdding + " " + "Actual:" + totalPriceAfterAddon);
		}
	}

	/**
	 * The method use to upgrade the transfer in amend and cancel
	 * 
	 */

	/*
	 * public void priceUpgradeCompareTransfers(String Totalprice, String
	 * transferDescBefore, String randomTransferSelection, String
	 * randomTransferSelectPrice)
	 */

	public void priceUpgradeCompareTransfers(String Totalprice,
			String transferDescBefore, String randomTransferSelection,
			String randomTransferSelectPrice, String confirmButton,
			String tranDifPrc, String confirmPayButton, String cpsTotalPrice)
			throws InterruptedException {
		String compName = "", pageValue = "", comp1 = "", page1 = "", transPrc, transPrice, page2 = "";

		try {
			pageValue = randomTransferSelectPrice.substring(
					randomTransferSelectPrice.indexOf("_") + 1,
					randomTransferSelectPrice.lastIndexOf("_"));
			compName = randomTransferSelectPrice
					.substring(randomTransferSelectPrice.lastIndexOf("_") + 1);

			page1 = tranDifPrc.substring(tranDifPrc.indexOf("_") + 1,
					tranDifPrc.lastIndexOf("_"));
			comp1 = tranDifPrc.substring(tranDifPrc.lastIndexOf("_") + 1);
			page2 = cpsTotalPrice.substring(cpsTotalPrice.indexOf("_") + 1,
					cpsTotalPrice.lastIndexOf("_"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		report.log(LogStatus.INFO,
				"The Transfer Description before selecting the alternate transfer "
						+ performActionGetText(transferDescBefore));

		double totalPriceBeforeAddon = Double.parseDouble(performActionGetText(
				Totalprice).replaceAll("[^0-9.]", ""));
		logger.info("totalPriceBeforeAddon:" + totalPriceBeforeAddon);

		Thread.sleep(500);
		// double SeatPrice=performActionRandomClick(randomTransferSelection);
		String SeatPrice = performActionRandomClickandGetText(
				randomTransferSelection, randomTransferSelectPrice);
		report.log(LogStatus.INFO, "The difference price for selected Transfer"
				+ SeatPrice);
		logger.info("The seleceted price:" + SeatPrice);
		DecimalFormat fmt = new DecimalFormat("#.00");

		// transPrc=SeatPrice.toString().replace(pound, "");
		transPrc = fmt.format(Double.parseDouble(SeatPrice));
		transPrice = performActionGetText(tranDifPrc).replace(pound, "");
		if (transPrc.equalsIgnoreCase(transPrice)) {
			report.log(LogStatus.PASS, comp1
					+ " Verifying condition is passed from " + page1
					+ " page where Expected:" + transPrc + " " + "Actual:"
					+ transPrice);
		} else {
			report.log(LogStatus.FAIL, comp1
					+ " Verifying condition is passed from " + page1
					+ " page where Expected:" + transPrc + " " + "Actual:"
					+ transPrice);
		}

		double PriceAfterAdding = Double.parseDouble(SeatPrice)
				+ totalPriceBeforeAddon;
		logger.info(PriceAfterAdding = Double.parseDouble(SeatPrice)
				+ totalPriceBeforeAddon);
		report.log(LogStatus.INFO, PriceAfterAdding + "=" + SeatPrice + "+"
				+ totalPriceBeforeAddon);
		logger.info("Price after adding:" + PriceAfterAdding);

		Thread.sleep(100);
		performActionClick(confirmButton);
		Thread.sleep(100);
		performActionClick(confirmPayButton);

		double totalPriceAfterAddon = Double.parseDouble(performActionGetText(
				cpsTotalPrice).replaceAll("[^0-9.]", ""));
		logger.info("Toatal price of application after seleceting alternate transfer:"
				+ totalPriceAfterAddon);

		if (totalPriceAfterAddon == PriceAfterAdding) {
			logger.info("Actaul price:" + totalPriceAfterAddon
					+ " and expected price:" + PriceAfterAdding + " are same");
			// report.log(LogStatus.PASS,"Actaul price:"+totalPriceAfterAddon+"
			// and expected price:"+PriceAfterAdding+" are same");
			report.log(LogStatus.PASS, compName
					+ " Verifying condition is passed from " + pageValue
					+ " to " + page2 + " page where Expected:"
					+ PriceAfterAdding + " " + "Actual:" + totalPriceAfterAddon);
		} else {
			logger.info("Actaul price:" + totalPriceAfterAddon
					+ " and expected price:" + PriceAfterAdding
					+ " are not same");
			// report.log(LogStatus.FAIL,"Actaul price:"+totalPriceAfterAddon+"
			// and expected price:"+PriceAfterAdding+" are not same");
			report.log(LogStatus.FAIL, compName
					+ " Verifying condition is failed from " + pageValue
					+ " to " + page2 + " page where Expected:"
					+ PriceAfterAdding + " " + "Actual:" + totalPriceAfterAddon);
		}
	}

	/**
	 * The method is used to compare the price value from single page
	 * 
	 * @param Scenario
	 * @param expected
	 * @param actual
	 * @author ananth
	 */
	public void priceComparisonForOnePage(String Scenario, String expected,
			String actual) {

		String compName, pageValue;
		try {
			pageValue = Scenario.substring(Scenario.indexOf("_") + 1,
					Scenario.lastIndexOf("_"));
			compName = Scenario.substring(Scenario.lastIndexOf("_") + 1);
			boolean cmpresult = false;

			try {
				if (Math.round(Double.parseDouble(expected)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(actual)) * 100.0 / 100.0) {
					cmpresult = true;
					report.log(LogStatus.PASS, compName
							+ " Verifying condition is passed from "
							+ pageValue + " page where Expected:" + expected
							+ " " + "Actual:" + actual);
					logger.info(compName
							+ " Verifying condition is passed from "
							+ pageValue + " page where Expected:" + expected
							+ " " + "Actual:" + actual + " Result-" + cmpresult);

				} else {

					cmpresult = false;
					report.log(LogStatus.FAIL, compName
							+ " Verifying condition is failed from "
							+ pageValue + " page where Expected:" + expected
							+ " " + "Actual:" + actual);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(compName
							+ " Verifying condition is failed from "
							+ pageValue + " page where Expected:" + expected
							+ " " + "Actual:" + actual + "Result-" + cmpresult);
				}

			} catch (Exception e) {
				logger.info(expected + " is not selected **********");
				logger.error(e);
				logger.error(actual + "*******" + e.getMessage());

			}
		} catch (Exception e) {
			logger.error("Senario name doesnt have proper syntax in priceComparisonForOnePage : "
					+ e.getMessage());
		}
	}

	/**
	 * The method is used to compare the price capture from multiple page
	 * 
	 * @param pageVal1
	 * @param pageVal2
	 * @param expected
	 * @param actual
	 * @autor Anant
	 */
	public void priceComparisonForTwoPage(String pageVal1, String pageVal2,
			String expected, String actual) {
		String compName1, pageValue1, pageValue2;
		try {
			pageValue1 = pageVal1.substring(pageVal1.indexOf("_") + 1,
					pageVal1.lastIndexOf("_"));
			compName1 = pageVal1.substring(pageVal1.lastIndexOf("_") + 1);

			if (pageVal2.contains("_")) {
				pageValue2 = pageVal2.substring(pageVal2.indexOf("_") + 1,
						pageVal2.lastIndexOf("_"));
			} else {
				pageValue2 = pageVal2;
			}

			boolean cmpresult = false;
			try {
				if (Math.round(Double.parseDouble(expected)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(actual)) * 100.0 / 100.0) {
					cmpresult = true;
					report.log(LogStatus.PASS, compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + expected + " "
							+ "Actual:" + actual);
					logger.info(compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + expected + " "
							+ "Actual:" + actual + "\t" + cmpresult);

				} else {
					cmpresult = false;
					report.log(LogStatus.FAIL, compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + expected + " "
							+ "Actual:" + actual);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + expected + " "
							+ "Actual:" + actual + "\t" + cmpresult);

				}
			} catch (Exception e) {
				logger.info(expected + " is not selected **********"
						+ e.getMessage());
				logger.error(e);
				logger.error(actual + "*******" + e.getMessage());

			}
		} catch (Exception e) {
			logger.error("Senario name doesnt have proper syntax in priceComparisonForOnePage : "
					+ e.getMessage());
			logger.error("Error occured in priceComparisonForTwoPage method: "
					+ e);
		}
	}

	/**
	 * Method to compare the values in the different page Updated By Anant
	 * 
	 * @date Sept 2015
	 * @param String
	 *            Scenario,Boolean flag,String Expected, String Actual
	 * @return void
	 */
	@SuppressWarnings("unused")
	public void compareStringTwoPage(String pageVal1, String pageVal2,
			String Expected, String Actual) {
		String compName1, pageValue1, compName2, pageValue2;
		try {
			pageValue1 = pageVal1.substring(pageVal1.indexOf("_") + 1,
					pageVal1.lastIndexOf("_"));
			compName1 = pageVal1.substring(pageVal1.lastIndexOf("_") + 1);

			if (pageVal2.contains("_")) {
				pageValue2 = pageVal2.substring(pageVal2.indexOf("_") + 1,
						pageVal2.lastIndexOf("_"));
				compName2 = pageVal2.substring(pageVal2.lastIndexOf("_") + 1);
			} else {
				pageValue2 = pageVal2;
			}

			boolean cmpresult = false;

			try {
				if (Expected.contains("\u00A3")) {
					Expected = Expected.replace("\u00A3", "");
				}
				if (Actual.contains("\u00A3")) {
					Actual = Actual.replace("\u00A3", "");
				}

				// if (Expected.replace(":", "").equalsIgnoreCase(Actual)) {
				if (Expected.contains(Actual) && Actual.length() > 0
						|| Actual.contains(Expected) && Expected.length() > 0) {
					cmpresult = true;
					report.log(LogStatus.PASS, compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual);
					logger.info(compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual + " Result-" + cmpresult);

				} else {
					cmpresult = false;
					report.log(LogStatus.FAIL, compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual + " Result-" + cmpresult);
				}
			} catch (Exception e) {
				logger.info(Expected + " is not selected **********");
				logger.error(e);
				logger.error(Actual + "*******" + e.getMessage());

			}
		} catch (Exception e) {
			logger.error("exception occured in compareStringTwoPage:" + e);
		}
	}

	/**
	 * Method to compare the values in the same page
	 * 
	 * @date Sept 2015
	 * @param String
	 *            Scenario,Boolean flag,String Expected, String Actual
	 * @author Madan
	 * @return void
	 */

	public void compareStringOnePage(String Scenario, boolean flag,
			String Expected, String Actual) {
		String compName, pageValue;
		pageValue = Scenario.substring(Scenario.indexOf("_") + 1,
				Scenario.lastIndexOf("_"));
		compName = Scenario.substring(Scenario.lastIndexOf("_") + 1);
		boolean cmpresult = false;

		try {
			if (flag) {

				cmpresult = true;
				report.log(LogStatus.PASS, compName
						+ " Verifying condition is passed from " + pageValue
						+ " page where Expected:" + Expected + " " + "Actual:"
						+ Actual);
				logger.info(compName + " Verifying condition is passed from "
						+ pageValue + " page where Expected:" + Expected + " "
						+ "Actual:" + Actual + " Result-" + cmpresult);

			} else {

				cmpresult = false;
				report.log(LogStatus.FAIL, compName
						+ " Verifying condition is failed from " + pageValue
						+ " page where Expected:" + Expected + " " + "Actual:"
						+ Actual);
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.info(compName + " Verifying condition is failed from "
						+ pageValue + " page where Expected:" + Expected + " "
						+ "Actual:" + Actual + " Result-" + cmpresult);
			}

		} catch (Exception e) {
			logger.info(Expected + " is not selected **********");
			logger.error(e);
			logger.error(Actual + "*******" + e.getMessage());

		}
	}

	/**
	 * The method is used to compare the hybris with anite content
	 * 
	 * @param pageVal1
	 * @param pageVal2
	 * @param Expected
	 * @param Actual
	 */
	@SuppressWarnings("unused")
	public void compareStringTwoPageAnite(String pageVal1, String pageVal2,
			String Expected, String Actual) {
		String compName1, pageValue1, compName2, pageValue2;
		try {
			pageValue1 = pageVal1.substring(pageVal1.indexOf("_") + 1,
					pageVal1.lastIndexOf("_"));
			compName1 = pageVal2.substring(pageVal2.lastIndexOf("_") + 1);

			pageValue2 = pageVal2.substring(pageVal2.indexOf("_") + 1,
					pageVal2.lastIndexOf("_"));
			compName2 = pageVal2.substring(pageVal2.lastIndexOf("_") + 1);

			boolean cmpresult = false;

			try {
				if (Expected.contains("\u00A3")) {
					Expected = Expected.replace("\u00A3", "");
				}
				if (Actual.contains("\u00A3")) {
					Actual = Actual.replace("\u00A3", "");
				}

				if (Expected.contains(Actual) && Actual.length() > 0
						|| Actual.contains(Expected) && Expected.length() > 0) {

					cmpresult = true;
					report.log(LogStatus.PASS, compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual);
					logger.info(compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual + " Result-" + cmpresult);

				} else {
					cmpresult = false;
					report.log(LogStatus.FAIL, compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " to " + pageValue2
							+ " page where Expected:" + Expected + " "
							+ "Actual:" + Actual + " Result-" + cmpresult);

				}
			} catch (Exception e) {
				logger.info(Expected + " is not selected **********");
				logger.error(e);
				logger.error(Actual + "*******" + e.getMessage());

			}
		} catch (Exception e) {
			logger.error("exception occured in compareStringTwoPage:" + e);
		}
	}

	@SuppressWarnings("unused")
	public void compareStringContent(String pageVal1, String Expected,
			String Actual) {
		String compName1, pageValue1, compName2, pageValue2;
		try {
			pageValue1 = pageVal1.substring(pageVal1.indexOf("_") + 1,
					pageVal1.lastIndexOf("_"));
			compName1 = pageVal1.substring(pageVal1.lastIndexOf("_") + 1);
			boolean cmpresult = false;
			try {
				if (Expected.contains("\u00A3")) {
					Expected = Expected.replace("\u00A3", "");
				}
				if (Actual.contains("\u00A3")) {
					Actual = Actual.replace("\u00A3", "");
				}
				if (Expected.equalsIgnoreCase(Actual) && Actual.length() > 0) {
					cmpresult = true;
					report.log(LogStatus.PASS, compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " page where Expected:" + Expected
							+ " " + "Actual:" + Actual);
					logger.info(compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " page where Expected:" + Expected
							+ " " + "Actual:" + Actual + " Result-" + cmpresult);
				} else {
					cmpresult = false;
					report.log(LogStatus.FAIL, compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " page where Expected:" + Expected
							+ " " + "Actual:" + Actual);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info(compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " page where Expected:" + Expected
							+ " " + "Actual:" + Actual + " Result-" + cmpresult);
				}
			} catch (Exception e) {
				logger.info(Expected + " is not selected **********");
				logger.error(e);
				logger.error(Actual + "*******" + e.getMessage());

			}
		} catch (Exception e) {
			logger.error("exception occured in compareStringTwoPage:" + e);
		}
	}

	// //////////////////////////// kaushik/////////////////////////////////
	public void CalculateTotalAmountAfterandBeforeAdding(String TBefore,
			String Extraprice, String TAtter) throws IOException {

		try {
			String pageVal1 = valuesMap.get(TBefore).substring(0,
					valuesMap.get(TBefore).indexOf("^^"));
			String pageVal2 = valuesMap.get(Extraprice).substring(0,
					valuesMap.get(Extraprice).indexOf("^^"));
			String pageVal3 = valuesMap.get(TAtter).substring(0,
					valuesMap.get(TAtter).indexOf("^^"));
			double TotalpriceBefore = Double.parseDouble(valuesMap.get(TBefore)
					.substring(valuesMap.get(TBefore).indexOf("^^") + 2)
					.replaceAll("[^\\.0123456789]", ""));
			double AmountAdded = Double.parseDouble(valuesMap.get(Extraprice)
					.substring(valuesMap.get(Extraprice).indexOf("^^") + 2)
					.replaceAll("[^\\.0123456789]", ""));
			double TotalPriceAter = Double.parseDouble(valuesMap.get(TAtter)
					.substring(valuesMap.get(TAtter).indexOf("^^") + 2)
					.replaceAll("[^\\.0123456789]", ""));
			report.log(LogStatus.INFO, pageVal1 + " : " + TotalpriceBefore);
			report.log(LogStatus.INFO, pageVal2 + " : " + AmountAdded);
			report.log(LogStatus.INFO, pageVal3 + " : " + TotalPriceAter);

			if (TotalPriceAter == Math
					.round((TotalpriceBefore + AmountAdded) * 100.0) / 100.0) {
				report.log(LogStatus.PASS, pageVal3 + " : ( " + TotalPriceAter
						+ ") = ( " + TotalpriceBefore + " + " + AmountAdded
						+ " )(" + pageVal1 + "+" + pageVal2 + ")");
			} else {
				report.log(LogStatus.FAIL, pageVal3 + " : ( " + TotalPriceAter
						+ ") is not equal to ( " + TotalpriceBefore + " + "
						+ AmountAdded + " )(" + pageVal1 + "+" + pageVal2 + ")");
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error while Calculating TotalAmount After and Before Adding Required extras due to "
							+ e.getMessage());
		}
	}

	public void calculateExtrasPriceBasedonPax(String aduPrice,
			String chiPrice, String adult, String Child, String key) {
		if (!Child.equals("0")) {
			double adultPrice = Double.parseDouble(valuesMap.get(aduPrice)
					.substring(valuesMap.get(aduPrice).indexOf("^^") + 2)
					.replaceAll("[^\\.0123456789]", ""));
			double childPrice = Double.parseDouble(valuesMap.get(chiPrice)
					.substring(valuesMap.get(chiPrice).indexOf("^^") + 2)
					.replaceAll("[^\\.0123456789]", ""));
			double aduCount = Double.parseDouble("adult");
			double chiCount = Double.parseDouble(Child);
			String ExtrasPrice = Double.toString((adultPrice * aduCount)
					+ (childPrice * chiCount));
			String price = "Calculated Extras Price" + "^^" + ExtrasPrice;
			valuesMap.put(key, price);
		} else {
			double adultPrice = Double.parseDouble(valuesMap.get(aduPrice)
					.substring(valuesMap.get(aduPrice).indexOf("^^") + 2)
					.replaceAll("[^\\.0123456789]", ""));

			double aduCount = Double.parseDouble("adult");

			String ExtrasPrice = Double.toString((adultPrice * aduCount));
			String price = "Calculated Extras Price" + "^^" + ExtrasPrice;
			valuesMap.put(key, price);
		}
	}

	public void selectBoardBasis(String BB) throws IOException {
		try {
			String AfterPrice = null, beforePrice = null, boardPrice = null;
			performActionClick("THOMSON_ACCOM_BOARDBASIS");
			captureTextValuesandHold("THOMSON_ACCOM_TOTALPARICE", beforePrice);
			String defaultBoard = performActionGetText("THOMSON_ACCOM_DEFAULTBOARD");
			if (defaultBoard.equalsIgnoreCase(BB)) {
				report.log(LogStatus.INFO, "By default, board is selected as "
						+ defaultBoard);
			} else if (BB.equalsIgnoreCase("Half Board")) {
				captureTextValuesandHold("THOMSON_ACCOM_HALFBOARDCOST",
						boardPrice);
				performActionClick("THOMSON_ACCOM_SELECTHALFBOARD");
			} else if (BB.equalsIgnoreCase("Full Board")) {
				captureTextValuesandHold("THOMSON_ACCOM_FULLBOARDCOST",
						boardPrice);
				performActionClick("THOMSON_ACCOM_SELECTFULLBOARD");

			} else if (BB.equalsIgnoreCase("All Inclusive")) {
				captureTextValuesandHold("THOMSON_ACCOM_ALLINCLUSIVECOST",
						boardPrice);
				performActionClick("THOMSON_ACCOM_SELECTALLINCLUSIVE");

			} else if (BB.equalsIgnoreCase("Self Catering")) {
				captureTextValuesandHold("THOMSON_ACCOM_SELFCATERINGCOST",
						boardPrice);
				performActionClick("THOMSON_ACCOM_SELECTSELFCATERING");

			}
			if (BB.equalsIgnoreCase("Bed & Breakfast")) {
				captureTextValuesandHold("THOMSON_ACCOM_BEDANDBREAKFASTCOST",
						boardPrice);
				performActionClick("THOMSON_ACCOM_BEDANDBREAKFAST");
			}

			captureTextValuesandHold("THOMSON_ACCOM_TOTALPARICE", AfterPrice);
			CalculateTotalAmountAfterandBeforeAdding(beforePrice, boardPrice,
					AfterPrice);
		} catch (Exception e) {
			logger.error("Error in selecting board basis " + e);
		}
	}

	public void deselectWCF()  {
		String befPrice = performActionGetText("Cruise_AllPage_TotPrice")
				.replaceAll("[^\\.0123456789]", "");
		String wcfPrice = performActionGetText("Cruise_Extras_WCF");
		logger.info("Total price " + befPrice + "wcf " + wcfPrice);
		performActiongetView("Cruise_Extras_WCFSection");
		performActionClick("Cruise_Extras_WCFUnselect");
		String aftPrice = performActionGetText("Cruise_AllPage_TotPriceAfter")
				.replaceAll("[^\\.0123456789]", "");
		logger.info("after price " + aftPrice);
		double expected = Double.parseDouble(befPrice)
				- Double.parseDouble(wcfPrice);
		if (Double.parseDouble(aftPrice) == Double.parseDouble(befPrice)
				- Double.parseDouble(wcfPrice)) {
			report.log(LogStatus.PASS, "Expected Price: " + expected
					+ " Actual Price:" + aftPrice);
		} else {
			report.log(LogStatus.FAIL, "Expected Price: " + expected
					+ " Actual Price:" + aftPrice);
			// report.log(LogStatus.FAIL,pageVal3+" : ( "+TotalPriceAter+") is
			// not equal to ( "+TotalpriceBefore+" + "+AmountAdded+"
			// )("+pageVal1+"+"+pageVal2+")");
		}

	}

	public String performActionRandomClickandGetTextandvalidatePrice(
			String objectName1, String objectName2, String beforePrice,
			String afterPrice) throws InterruptedException, NumberFormatException {
		Thread.sleep(3000);
		double totPriceBef = Double.parseDouble(performActionGetText(
				beforePrice).replace(pound, ""));
		objectDetails = getObjectDetails(objectName1);

		// Double d = null;
		String value = null;
		List<WebElement> wd1;
		List<String> wd2;
		Random rd = new Random();
		try {

			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "")
							.replaceAll("[^\\.0123456789]", "");
					// d = Double.parseDouble(value);
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {

				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "")
							.replaceAll("[^\\.0123456789]", "");
					// d = Double.parseDouble(value);
				}
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				Thread.sleep(100);
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				int y = rd.nextInt(wd1.size());
				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "")
							.replaceAll("[^\\.0123456789]", "");
					// d = Double.parseDouble(value);
				}
				// ((JavascriptExecutor)
				// driver).executeScript("arguments[0].scrollIntoView(true);",
				// wd1.get(y));
				Thread.sleep(2000);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());

				value = wd2.get(y);
				if (!value.isEmpty()) {
					value = value.replace(pound, "").replace(euro, "")
							.replaceAll("[^\\.0123456789]", "");
					// d = Double.parseDouble(value);

				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + "is selcted as" + "<strong>"
						+ value + "</strong>");
				report.log(LogStatus.INFO, objectName1 + " " + "is selcted as"
						+ "<strong>" + value + "</strong>");
			}

		} catch (Exception e) {
			logger.info(objectName1 + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);

		}
		try {
			Thread.sleep(4000);
			double toAfterPrice = Double.parseDouble(performActionGetText(
					afterPrice).replace(pound, ""));
			double extraPrice = Double.parseDouble(value);
			if (toAfterPrice == Math.round((totPriceBef + extraPrice) * 100.0) / 100.0) {
				report.log(LogStatus.PASS, afterPrice + " : ( " + toAfterPrice
						+ ") = ( " + totPriceBef + " + " + extraPrice + " )("
						+ beforePrice + "+" + objectName2 + ")");
			} else {
				report.log(LogStatus.FAIL, afterPrice + " : ( " + toAfterPrice
						+ ") is not equal to ( " + totPriceBef + " + "
						+ extraPrice + " )(" + beforePrice + "+" + objectName2
						+ ")");
			}

		} catch (Exception e1) {
			report.log(
					LogStatus.ERROR,
					"Error while Calculating TotalAmount After and Before Adding Required extras due to "
							+ e1.getMessage());

		}

		return value;

	}

	/**
	 * Method to verify the page display @author Madan updated by Swati * @date
	 * April 2015/Jan 2017
	 * 
	 * @param PageName
	 *            as mentioned in the object repository. * @return boolean value
	 */

	public void checkPageDisplayedSuccessfully(String PageName) {
		// boolean isPageLaunched = false;
		try {
			waitForPageLoad(driver);
			Thread.sleep(4000);
			String response = driver.getPageSource();

			if (!(response.contains("All Gone")
					|| response.contains("Oh no...")
					|| response.contains("Service temporarily unavailable")
					|| response.contains("We're really sorry")
					|| response.contains("All gone!")
					|| response.contains("Service Temporarily Unavailable")
					|| response.contains("NO FLIGHTS FOUND!")
					|| response.contains("Server not found") || response
						.contains("The page cannot be displayed "))) {
				report.log(LogStatus.PASS, PageName
						+ " Page has launched successfully!");
				logger.info("Page has launched successfully!");
				// isPageLaunched = true;
			} else {
				logger.info("Page has not launched successfully!");

				report.log(LogStatus.FAIL,
						"Page has not launched successfully!");
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, PageName + "Page Not Displayed");

			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The array index out of bound execption in pageDisplay method:"
					+ a.getMessage());
		} catch (Exception e) {
			logger.error("The execption occured as given object " + PageName
					+ " doesnt have the page value " + e.getMessage());
			report.log(LogStatus.FAIL, PageName
					+ " Component is not displayed from " + PageName
					+ " page hence pagenot loaded properly");
			report.attachScreenshot(takeScreenShotExtentReports());
			Assert.assertTrue(false, PageName + "Page Not Displayed");
		}
	}

	// //////////////////// dheeraj//////////////////////

	public void sleep(String time) throws InterruptedException {
		Thread.sleep(Long.parseLong(time));
		logger.info("Thread sleep for " + time + " milli sec over");
		report.log(LogStatus.INFO, "Thread sleep for " + time
				+ " milli sec over");
	}

	// testcase name reporting
	public void ReportScenarioName(String a) {
		report.log(LogStatus.INFO, "<strong>" + a + "</strong>");
	}

	public void priceValidation(String Actual, String Expected) {
		try {
			String pageVal1 = valuesMap.get(Actual).substring(0,
					valuesMap.get(Actual).indexOf("^^"));
			String pageVal2 = valuesMap.get(Expected).substring(0,
					valuesMap.get(Expected).indexOf("^^"));
			double actual = Double.parseDouble(valuesMap.get(Actual)
					.substring(valuesMap.get(Actual).indexOf("^^") + 2)
					.replaceAll("[^.0-9]", ""));
			double expected = Double.parseDouble(valuesMap.get(Expected)
					.substring(valuesMap.get(Expected).indexOf("^^") + 2)
					.replaceAll("[^.0-9]", ""));
			logger.info(actual + "\t" + expected);
			report.log(LogStatus.INFO, pageVal1 + " : " + actual);
			report.log(LogStatus.INFO, pageVal2 + " : " + expected);

			double difference = Math.abs(expected - actual);
			if ((difference == 0) || (difference < 1)) {
				report.log(LogStatus.PASS, "Price in " + pageVal1 + " is : "
						+ actual + " is equal or nearer to Price in "
						+ pageVal2 + " : " + expected);
			} else {
				report.log(LogStatus.FAIL, "Price in " + pageVal1 + " is : "
						+ actual + "is not equal to Price in " + pageVal2
						+ " : " + expected);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error while Checking prices due to "
					+ e.getMessage());
		}
	}

	public void compareStringfromHold(String scenarioList, String actualList,
			String expectedList) {
		String[] Scenarioarray = scenarioList.split(",");
		String[] Actualarray = actualList.split(",");
		String[] Expectedarray = expectedList.split(",");
		List<String> ActualList = new ArrayList<String>();
		List<String> ExpectedList = new ArrayList<String>();
		List<String> ScenarioList = new ArrayList<String>();
		String pageVal1;
		String pageVal2;
		String Expected = "";
		String Actual = "";
		String pageValue1;
		String compName1;
		String pageValue2;
		try {
			for (int j = 0; j < Actualarray.length; j++) {
				ActualList.add(Actualarray[j]);
				ExpectedList.add(Expectedarray[j]);
				ScenarioList.add(Scenarioarray[j]);
			}

			for (int i = 0; i < ActualList.size(); i++) {
				try {
					Expected = valuesMap
							.get(ExpectedList.get(i))
							.substring(
									valuesMap.get(ExpectedList.get(i)).indexOf(
											"^^") + 2).replace(pound, "")
							.replace(euro, "").trim();
					pageVal1 = valuesMap.get(ExpectedList.get(i)).substring(0,
							valuesMap.get(ExpectedList.get(i)).indexOf("^^"));
					logger.info("pageVal:" + valuesMap.get(ExpectedList.get(i)));
					pageValue1 = pageVal1.substring(pageVal1.indexOf("_") + 1,
							pageVal1.lastIndexOf("_"));
					compName1 = pageVal1
							.substring(pageVal1.lastIndexOf("_") + 1);
				} catch (NullPointerException e) {
					Expected = ExpectedList.get(i).replace(pound, "")
							.replace(euro, "").trim();
					pageVal1 = "previous page";
					e.printStackTrace();
					pageValue1 = "previous page";
					compName1 = "value";
				}
				try {
					Actual = valuesMap
							.get(ActualList.get(i))
							.substring(
									valuesMap.get(ActualList.get(i)).indexOf(
											"^^") + 2).replace(pound, "")
							.replace(euro, "").trim();
					pageVal2 = valuesMap.get(ActualList.get(i)).substring(0,
							valuesMap.get(ActualList.get(i)).indexOf("^^"));
					pageValue2 = pageVal2.substring(pageVal2.indexOf("_") + 1,
							pageVal2.lastIndexOf("_"));
				} catch (Exception e) {
					// Actual=ActualList.get(i).replace(pound,
					// "").replace(euro,"");
					pageVal2 = "nextpage";
					pageValue2 = "nextpage";
				}

				String Scenario = ScenarioList.get(i);
				boolean cmpresult = false;

				try {
					logger.info("Expected" + Expected + "Expected");
					logger.info("Actual" + Actual + "Actual");

					if (Expected.equalsIgnoreCase(Actual)) {

						cmpresult = true;
						logger.info(compName1
								+ " Verifying condition is passed from "
								+ pageValue1 + " to " + pageValue2
								+ " page where Expected:" + Expected + " "
								+ "Actual:" + Actual);
						report.log(LogStatus.PASS, compName1
								+ " Verifying condition is passed from "
								+ pageValue1 + " to " + pageValue2
								+ " page where Expected:" + Expected + " "
								+ "Actual:" + Actual);
						logger.info("Comparingsummary parameters" + "-"
								+ "Expected" + Expected + " " + "Actual"
								+ Actual + " " + "Result-" + cmpresult);
					} else {
						cmpresult = false;
						logger.info(compName1
								+ " Verifying condition is failed from "
								+ pageValue1 + " to " + pageValue2
								+ " page where Expected:" + Expected + " "
								+ "Actual:" + Actual);
						report.log(LogStatus.FAIL, compName1
								+ " Verifying condition is failed from "
								+ pageValue1 + " to " + pageValue2
								+ " page where Expected:" + Expected + " "
								+ "Actual:" + Actual);
						report.attachScreenshot(takeScreenShotExtentReports());
						logger.info("Comparing parameters" + "-" + "Expected"
								+ Expected + " " + "ActualPrice" + Actual + " "
								+ "Result-" + cmpresult);
					}
				} catch (Exception e) {
					logger.info(Expected + " is not selected **********");
					logger.error(e);
					logger.error(Actual + "*******" + e.getMessage());

				}

			}

		} catch (Exception e) {
			logger.info("Error while comparing may be number of actual and expected are not equal");
			logger.error(ExpectedList.get(0));
			logger.error("--" + valuesMap.get(ExpectedList.get(0)));
			e.printStackTrace();

		}

	}

	public void EnterpassengerDetailsFO() {
		try {
			List<WebElement> ele = driver.findElements(By
					.xpath(".//*[contains(@id,'title')]"));
			List<WebElement> fname = driver
					.findElements(By
							.xpath(".//*[contains(@id,'first-name') or contains(@id,'firstName')]"));
			List<WebElement> lname = driver
					.findElements(By
							.xpath(".//*[contains(@id,'last-name') or contains(@id,'surName')]"));
			List<WebElement> adultageBoxes = driver
					.findElements(By
							.xpath(".//*[contains(@id,'dobNode')]/input[contains(@id,'dob')]"));
			List<WebElement> childBoxes = driver.findElements(By
					.xpath(".//input[contains(@id,'dob')]"));
			List<String> multVal = new ArrayList<String>();
			List<WebElement> Cages = driver.findElements(By
					.xpath(".//span[@class='size-15']"));

			performActionSelectDropDown_Select("FO_PASSENGERPAGE_COUNTRY",
					"Australia");

			driver.findElement(By.xpath(".//*[@id='address-one']")).sendKeys(
					"123123");
			report.log(LogStatus.INFO, "House Number is Entered as : 123123");
			driver.findElement(By.xpath(".//*[@id='address-two']")).sendKeys(
					"AutoAdderss");
			report.log(LogStatus.INFO, "Address 1 is Entered as : AutoAdderss");
			driver.findElement(By.xpath(".//*[@id='city-town']")).sendKeys(
					"Hyderabad");
			report.log(LogStatus.INFO, "Town/city is Entered as : Hyderabad");
			driver.findElement(By.xpath(".//*[@id='county']")).sendKeys(
					"Australia");
			report.log(LogStatus.INFO, "Country is Entered as : Australia");
			performActionEnterText("FO_Passenger_PostCode", "TW33TL");

			logger.info("waiting for 4 second");
			Thread.sleep(4000);
			driver.findElement(By.xpath(".//*[@id='tel']")).sendKeys(
					"8143252685");
			report.log(LogStatus.INFO,
					"Telephone Number is Entered as : 8143252685");
			driver.findElement(By.xpath(".//*[@id='email']")).sendKeys(
					"Mobile.automation@sonata-software.com");
			report.log(LogStatus.INFO,
					"Email Address is Entered as : Mobile.automation@sonata-software.com");
			driver.findElement(By.xpath(".//*[@id='confirmationEmail']"))
					.sendKeys("Mobile.automation@sonata-software.com");
			report.log(LogStatus.INFO,
					"Confirm Email Address is Entered as : Mobile.automation@sonata-software.com");

			for (int i = 0; i < ele.size(); i++) {
				Select sel = new Select(ele.get(i));
				sel.selectByIndex(1);
				fname.get(i).sendKeys("Automation");
				report.log(LogStatus.INFO,
						"FirstName is Entered as : Automation");
				lname.get(i).sendKeys("Kumar");
				report.log(LogStatus.INFO, "surName is Entered as : Kumar");
			}
			int ChiladAge;
			for (int i = 0; i < Cages.size(); i++) {
				String Agetext1 = Cages.get(i).getText()
						.replaceAll("[^0-9,-]", "");
				if (Agetext1.contains("-")) {
					ChiladAge = Character.getNumericValue(Agetext1.charAt(2));
					ChiladAge = ChiladAge - 1;
				} else {
					ChiladAge = Integer.parseInt(Agetext1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String date = sdf.format(DateUtils.addYears(new Date(),
						-ChiladAge));
				multVal.add(String.valueOf(date));
			}
			for (int i = 0; i < childBoxes.size(); i++) {
				childBoxes.get(i).sendKeys(multVal.get(i));
				report.log(LogStatus.INFO,
						"Childage Date Of Birth Entered as :" + multVal.get(i));
			}

			performActiongetView("FO_PASSENGERSPAGE_IAGREEVIEW");
			driver.findElement(
					By.xpath(".//*[@name='importantInformationChecked']"))
					.click();
			report.log(LogStatus.INFO,
					"Passengers Page I agree Checkbox is selected");
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void CompareWcfPrices(String Actual, String Expected) {

		String pageVal1 = valuesMap.get(Actual).substring(0,
				valuesMap.get(Actual).indexOf("^^"));
		String pageVal2 = valuesMap.get(Expected).substring(0,
				valuesMap.get(Expected).indexOf("^^"));
		double actual = Double.parseDouble(valuesMap.get(Actual)
				.substring(valuesMap.get(Actual).indexOf("^^") + 2)
				.replaceAll("[^.0-9]", ""));
		double expected = Double.parseDouble(valuesMap.get(Expected)
				.substring(valuesMap.get(Expected).indexOf("^^") + 2)
				.replaceAll("[^.0-9]", ""));
		logger.info(actual);
		logger.info(expected);
		report.log(LogStatus.INFO, pageVal1 + " : " + actual);
		report.log(LogStatus.INFO, pageVal2 + " : " + expected);

		if (actual != expected) {
			report.log(LogStatus.PASS,
					"Price Before selecting WorldCareFund : " + actual
							+ " and After selecting WorldCareFund : "
							+ expected + " are different");
		} else {
			report.log(LogStatus.FAIL,
					"Price Before selecting WorldCareFund : " + actual
							+ " and After selecting WorldCareFund : "
							+ expected + " are same");
		}
	}

	public void FosearchPanelTwoWay(String Inputfrom, String InputTo,
			String month, String days) throws InterruptedException {

		try {
			performActionClick("FO_SEARCHPANEL_FROM");
			Thread.sleep(5000);
			List<WebElement> FromList = driver.findElements(By
					.xpath(".//li[@class='cb sub-location ']/span"));
			report.log(
					LogStatus.INFO,
					"No of Airports available from location are :"
							+ FromList.size());
			report.log(LogStatus.INFO, " Given Input Airport Location Name:"
					+ Inputfrom);

			int count = 0;
			for (int i = 0; i < FromList.size(); i++) {
				org.openqa.selenium.interactions.internal.Coordinates coordinate = ((Locatable) FromList
						.get(i)).getCoordinates();
				coordinate.onPage();
				coordinate.inViewPort();
				if (FromList.get(i).getText().contains(Inputfrom)) {
					report.log(
							LogStatus.PASS,
							"Given where From input Airport location name is Found in the list of available airports : "
									+ Inputfrom);
					FromList.get(i).click();
					break;
				} else {
					count++;
				}
			}
			if (count == FromList.size()) {
				report.log(
						LogStatus.ERROR,
						"Given where From input Airport location name is NotFound in the list of available airports : "
								+ Inputfrom);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, "NotFound");
			}
			Thread.sleep(4000);

			performActionClick("FO_SEARCHPANEL_TO");
			Thread.sleep(4000);
			List<WebElement> ToList = driver.findElements(By
					.xpath(".//li[@class='cb sub-location ']/span"));
			report.log(
					LogStatus.INFO,
					"No of Airports available Where To location are :"
							+ ToList.size());
			report.log(LogStatus.INFO, " Given Input Airport Location Name:"
					+ InputTo);

			int coun = 0;
			for (int i = 0; i < ToList.size(); i++) {
				org.openqa.selenium.interactions.internal.Coordinates coordinate = ((Locatable) ToList
						.get(i)).getCoordinates();
				coordinate.onPage();
				coordinate.inViewPort();
				if (ToList.get(i).getText().contains(InputTo)) {
					report.log(
							LogStatus.PASS,
							"Given where To input Airport location name is Found in the list of available airports : "
									+ InputTo);
					ToList.get(i).click();
					break;
				} else {
					coun++;
				}
			}
			if (coun == ToList.size()) {
				report.log(
						LogStatus.ERROR,
						"Given where To input Airport location name is NotFound in the list of available airports : "
								+ InputTo);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, "NotFound");
			}
			Thread.sleep(4000);

			performActionClick("FO_SEARCHPANEL_DEPARTUREDATE");
			performActionSelectDropDown_Select("FO_SEARCHPANEL_MONTHSELECTION",
					month);
			String from = driver
					.findElement(
							By.xpath("(.//div[@id='calendar']/div[2]/div[3]/table/tbody/tr/td[contains(@id,'tui_widget_finderNew_view_ActiveDate')]/i)[1]"))
					.getText();
			driver.findElement(
					By.xpath("(.//div[@id='calendar']/div[2]/div[3]/table/tbody/tr/td[contains(@id,'tui_widget_finderNew_view_ActiveDate')]/i)[1]"))
					.click();
			report.log(LogStatus.INFO,
					"First Available date in the given input month : " + month
							+ " is : " + from);

			int Fromdate = Integer.parseInt(from);
			performActionClick("FO_SEARCHPANEL_RETURNDATE");
			performActionSelectDropDown_Select("FO_SEARCHPANEL_MONTHSELECTION",
					month);

			List<WebElement> ToDate = driver
					.findElements(By
							.xpath(".//div[@id='returnCalendar']/div[2]/div[3]/table/tbody/tr/td[contains(@id,'tui_widget_finderNew_view_ActiveDate')]/i"));
			report.log(LogStatus.INFO,
					"No of Available dates in the given input month : " + month
							+ " : " + ToDate.size());
			for (int i = 0; i < ToDate.size(); i++) {
				int Todate = Integer.parseInt(ToDate.get(i).getText());
				int diff = Todate - Fromdate;

				if (diff > Integer.parseInt(days)) {
					report.log(LogStatus.INFO, "Input duration Given as : "
							+ days);
					report.log(
							LogStatus.INFO,
							"Dates are available for Given Input Duration : "
									+ days + " selected Date is : "
									+ ToDate.get(i));
					ToDate.get(i).click();
					break;
				} else {
					if (Todate != Fromdate) {
						ToDate.get(i).click();
						report.log(LogStatus.INFO, "Input duration Given as : "
								+ days);
						report.log(LogStatus.INFO,
								"As Dates are not available for Given Input Duration : "
										+ days + " ,selecting Random Date.");
						break;
					}
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error in search panel due to " + e);
		}
	}

	public void FosearchPaneloneWay(String Inputfrom, String InputTo,
			String month) throws InterruptedException {
		try {
			performActionSelectBox("FO_SEARCHPANEL_ONEWAY");
			performActionClick("FO_SEARCHPANEL_FROM");
			Thread.sleep(5000);
			List<WebElement> FromList = driver.findElements(By
					.xpath(".//li[@class='cb sub-location ']/span"));
			report.log(
					LogStatus.INFO,
					"No of Airports available from location are :"
							+ FromList.size());
			report.log(LogStatus.INFO, " Given Input Airport Location Name:"
					+ Inputfrom);
			int count = 0;
			for (int i = 0; i < FromList.size(); i++) {
				org.openqa.selenium.interactions.internal.Coordinates coordinate = ((Locatable) FromList
						.get(i)).getCoordinates();
				coordinate.onPage();
				coordinate.inViewPort();
				if (FromList.get(i).getText().contains(Inputfrom)) {
					report.log(
							LogStatus.PASS,
							"Given where From input Airport location name is Found in the list of available airports : "
									+ Inputfrom);
					FromList.get(i).click();
					break;
				} else {
					count++;
				}
			}
			if (count == FromList.size()) {
				report.log(
						LogStatus.ERROR,
						"Given where From input Airport location name is NotFound in the list of available airports : "
								+ Inputfrom);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, "NotFound");
			}
			Thread.sleep(4000);
			performActionClick("FO_SEARCHPANEL_TO");
			Thread.sleep(4000);
			List<WebElement> ToList = driver.findElements(By
					.xpath(".//li[@class='cb sub-location ']/span"));
			report.log(
					LogStatus.INFO,
					"No of Airports available Where To location are :"
							+ ToList.size());
			report.log(LogStatus.INFO, " Given Input Airport Location Name:"
					+ InputTo);
			int coun = 0;
			for (int i = 0; i < ToList.size(); i++) {
				org.openqa.selenium.interactions.internal.Coordinates coordinate = ((Locatable) ToList
						.get(i)).getCoordinates();
				coordinate.onPage();
				coordinate.inViewPort();
				if (ToList.get(i).getText().contains(InputTo)) {
					report.log(
							LogStatus.PASS,
							"Given where To input Airport location name is Found in the list of available airports : "
									+ InputTo);
					ToList.get(i).click();
					break;
				} else {
					coun++;
				}
			}
			if (coun == ToList.size()) {
				report.log(
						LogStatus.ERROR,
						"Given where To input Airport location name is NotFound in the list of available airports : "
								+ Inputfrom);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, "NotFound");
			}
			Thread.sleep(4000);

			performActionClick("FO_SEARCHPANEL_DEPARTUREDATE");
			performActionSelectDropDown_Select("FO_SEARCHPANEL_MONTHSELECTION",
					month);
			String from = driver
					.findElement(
							By.xpath("(.//div[@id='calendar']/div[2]/div[3]/table/tbody/tr/td[contains(@id,'tui_widget_finderNew_view_ActiveDate')]/i)[1]"))
					.getText();
			driver.findElement(
					By.xpath("(.//div[@id='calendar']/div[2]/div[3]/table/tbody/tr/td[contains(@id,'tui_widget_finderNew_view_ActiveDate')]/i)[1]"))
					.click();
			report.log(LogStatus.INFO,
					"First Available date in the given input month : " + month
							+ " is : " + from);
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error in search panel due to " + e);
		}

	}

	public void scrolldownpixel(String pixel) {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollBy(0," + pixel + ")", "");
		logger.info("Scroll down by " + pixel);
	}

	public void scrolldown() {
		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("window.scrollTo(0,document.body.scrollHeight);");
		logger.info("Scroll down by ");
	}

	/**
	 * Method to check for an element until it is displayed Upadted by Anant
	 * 
	 * @date oct4 2016
	 * @author Omar
	 * @param ObjectName
	 * @return void
	 */
	public void IsElementDisplayed(String objectName) {
		objectDetails = getObjectDetails(objectName);

		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.id(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
				report.log(LogStatus.PASS, objectName
						+ " Component is Displayed" + " Successfully");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.name(objectDetails.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
				report.log(LogStatus.PASS, objectName
						+ " Component is Displayed" + " Successfully");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
				report.log(LogStatus.PASS, objectName
						+ " Component is Displayed" + " Successfully");
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				wait.until(ExpectedConditions.visibilityOfElementLocated(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE))));
				report.log(LogStatus.PASS, objectName
						+ " Component is Displayed " + " Successfully");
			}
		} catch (Exception e) {
			logger.error(e);
			report.log(LogStatus.FAIL, objectName
					+ " Component is not Displayed " + " Successfully");
		}
	}

	/**
	 * The method is used to Sort Savings Amount Results returns void
	 */
	public void savingsamount() {
		try {
			int index = 0;
			List<Integer> intprices = new ArrayList<Integer>();
			List<Integer> AllPrices = new ArrayList<Integer>();

			performActionClick("THOMSON_SEARCHRES_SORTBYFILTER");
			Thread.sleep(2000);
			performActionClick("THOMSON_SORTBYFILTER_SAVINGAMOUNT");
			Thread.sleep(15000);
			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			if (holicount == 1) {
				report.log(LogStatus.PASS,
						"Only 1 result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO, "Total Number of Holidays Found:"
						+ holicount);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}

					List<String> prices = performActionMultipleGetText("THOMSON_SEARCHRESULTS_DISCOUNTPERPERSONLIST");
					;
					for (String each : prices) {
						intprices.add(Integer.parseInt(each.replaceAll(
								"[^0-9]", "")));
					}
					report.log(
							LogStatus.INFO,
							"Number of Holidays displayed in Page ("
									+ (index + 1) + ") are : "
									+ intprices.size()
									+ " and their values are : " + intprices);
					AllPrices.addAll(intprices);
					intprices.clear();
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));

				boolean sorted = Ordering.natural().reverse()
						.isOrdered((Iterable<? extends Comparable>) AllPrices);
				if (sorted) {
					report.log(LogStatus.PASS,
							"Results are sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				} else {
					report.log(
							LogStatus.FAIL,
							"Results are not sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				}
				if (AllPrices.size() == holicount) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays count : "
									+ holicount
									+ " is equal to Total number of results displayed : "
									+ AllPrices.size());
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays count : "
									+ holicount
									+ " is not equal to Total number of results displayed : "
									+ AllPrices.size());
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	/**
	 * The method is used to Sort Names A to Z Results returns void
	 */
	public void AtoZSorting() {
		try {
			int index = 0;
			List<String> intprices = new ArrayList<String>();

			List<String> AllPrices = new ArrayList<String>();

			performActionClick("THOMSON_SEARCHRES_SORTBYFILTER");
			Thread.sleep(2000);
			performActionClick("THOMSON_SORTBYFILTER_HOTELNAME");
			Thread.sleep(15000);
			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			if (holicount == 1) {
				report.log(LogStatus.PASS,
						"Only 1 result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO, "Total Number of Holidays Found:"
						+ holicount);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					List<String> prices = performActionMultipleGetText("THOMSON_SEARCHRESULTS_HOTELNAMESLIST");
					;
					intprices.addAll(prices);
					report.log(
							LogStatus.INFO,
							"Number of Holidays displayed in Page ("
									+ (index + 1) + ") are : "
									+ intprices.size()
									+ " and their values are : " + intprices);
					AllPrices.addAll(intprices);
					intprices.clear();
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
				boolean sorted = Ordering.from(String.CASE_INSENSITIVE_ORDER)
						.isOrdered(AllPrices);
				if (sorted) {
					report.log(LogStatus.PASS,
							"Results are sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				} else {
					report.log(
							LogStatus.FAIL,
							"Results are not sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				}
				if (AllPrices.size() == holicount) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays count : "
									+ holicount
									+ " is equal to Total number of results displayed : "
									+ AllPrices.size());
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays count : "
									+ holicount
									+ " is not equal to Total number of results displayed : "
									+ AllPrices.size());
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	/**
	 * The method is used to to filter the All Collections Results It accepts 3
	 * parameters(Collections Types,Collections Text,Collections searchResults)
	 * returns void
	 * 
	 * @throws InterruptedException
	 */
	public void AllCollections(String BudgetButton, String BudgetPrice,
			String perpersonprice) throws InterruptedException {
		try {
			int Count;
			String board, brdName;
			int index = 0;
			List<String> listOfPriceSection;
			List<String> prices = new ArrayList<String>();

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRESULTS_FILTERS");
			Thread.sleep(2000);
			performActionClick("THOMSON_FILTERS_COLLECTIONSARROW");
			Thread.sleep(1000);
			brdName = performActionRandomClickandGetText(BudgetButton,
					BudgetPrice);
			performActionClick("THOMSON_COLLECTIONS_APPLY");
			Thread.sleep(15000);
			performActionClick("THOMSON_FILTERS_CLOSE");
			Thread.sleep(2000);
			if (brdName.contains("ALL COLLECTIONS")) {
				report.log(LogStatus.INFO,
						"Selected All Collections type Filter is : " + brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);
				if (holicount == holicount1) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are not same : "
									+ holicount1);
				}
			} else {
				Count = Integer.parseInt(brdName.replaceAll("[^0-9]+", ""));
				board = brdName.replaceAll("[^A-Za-z&]+", "").trim()
						.toLowerCase();
				report.log(LogStatus.INFO, "Selected BoardBasis Filter is : "
						+ brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					listOfPriceSection = performActionMultipleGetText(perpersonprice);
					prices.addAll(listOfPriceSection);
					report.log(LogStatus.INFO,
							"List of selected Budget Filter size after applying filter in page no : ("
									+ (index + 1) + ") are : "
									+ listOfPriceSection.size());
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
				report.log(
						LogStatus.INFO,
						"List of All selected Budget Filter size : "
								+ prices.size());
				if ((prices.size() == holicount1) && (holicount1 == Count)) {
					report.log(
							LogStatus.PASS,
							"Results count after applying filter : "
									+ prices.size()
									+ " and the total no of results after applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							" Results count after applying filter : "
									+ prices.size()
									+ " and the total no of results after applying filter are not same : "
									+ holicount1);
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Budget filter due to : " + e);
		}
	}

	/**
	 * The method is used to to filter the Brand Rating Results It accepts 4
	 * parameters(Rating Types,Rating Text,Rating searchResults,searchResults
	 * Attribute) returns void
	 * 
	 * @throws InterruptedException
	 */
	public void RatingFilter(String BudgetButton, String BudgetPrice,
			String perpersonprice, String attribute)
			throws InterruptedException {
		try {
			int index = 0;
			String bdgPrc;
			List<String> listOfPriceSection;
			List<String> prices = new ArrayList<String>();

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRESULTS_FILTERS");
			Thread.sleep(2000);
			performActionClick("THOMSON_FILTERS_RATINGARROW");
			Thread.sleep(2000);
			bdgPrc = performActionRandomClickandGetText(BudgetButton,
					BudgetPrice).replaceAll("[^0-9]+", "");
			report.log(LogStatus.INFO, "Selected Brand-Rating  Filter is : "
					+ bdgPrc);
			performActionClick("THOMSON_RATINGS_APPLY");
			Thread.sleep(15000);
			performActionClick("THOMSON_FILTERS_CLOSE");
			Thread.sleep(2000);
			int holicount1 = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found after Applying filter:"
							+ holicount1);
			do {
				if (index != 0) {
					performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
					performActionClick("THOMSON_SEARCHRESULTS_NEXT");
					Thread.sleep(15000);
				}
				listOfPriceSection = performActionMultipleGetTextfromattributes(
						perpersonprice, attribute);

				prices.addAll(listOfPriceSection);
				report.log(
						LogStatus.INFO,
						"List of selected Rating Filter size after applying filter in page no : ("
								+ (index + 1) + ") are : "
								+ listOfPriceSection.size());
				report.log(LogStatus.INFO,
						"List of selected Rating Filter prices after applying filter in page no : ("
								+ (index + 1) + ") are : " + listOfPriceSection);
				index++;
			} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
			report.log(
					LogStatus.INFO,
					"List of All selected Rating Filter size : "
							+ prices.size());
			for (int i = 0; i < prices.size(); i++) {
				String z = prices.get(i).replaceAll("[^0-9]+", "");
				if (Integer.parseInt(z) >= Integer.parseInt(bdgPrc)) {
					report.log(
							LogStatus.PASS,
							"selected Rating Filter Price in searchresults page : "
									+ z
									+ " is less than or equal to Applied filter Price : "
									+ bdgPrc);
				} else {
					report.log(
							LogStatus.PASS,
							"selected Rating Filter Price in searchresults page : "
									+ z
									+ " is not less than or equal to Applied filter Price : "
									+ bdgPrc);
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Rating filter due to : " + e);
		}
	}

	/**
	 * The method is used to to filter the Best For Results It accepts 3
	 * parameters(Best For Types,Best For Text,Best For searchResults) returns
	 * void
	 * 
	 * @throws InterruptedException
	 */

	public void BestFor(String BudgetButton, String BudgetPrice,
			String perpersonprice) throws InterruptedException {

		try {
			int Count;
			String board, brdName;
			int index = 0;
			List<String> listOfPriceSection;
			List<String> prices = new ArrayList<String>();

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRESULTS_FILTERS");
			Thread.sleep(2000);
			performActionClick("THOMSON_FILTERS_BESTFORARROW");
			Thread.sleep(2000);
			brdName = performActionRandomClickandGetText(BudgetButton,
					BudgetPrice);
			performActionClick("THOMSON_BESTFOR_APPLY");
			Thread.sleep(15000);
			performActionClick("THOMSON_FILTERS_CLOSE");
			Thread.sleep(2000);
			if (brdName.contains("ALL CATEGORIES")) {
				report.log(LogStatus.INFO, "Selected Best For Filter is : "
						+ brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);
				if (holicount == holicount1) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are not same : "
									+ holicount1);
				}
			} else {

				Count = Integer.parseInt(brdName.replaceAll("[^0-9]+", ""));
				board = brdName.replaceAll("[^A-Za-z&]+", "").trim()
						.toLowerCase();
				report.log(LogStatus.INFO, "Selected Best For Filter is : "
						+ brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					listOfPriceSection = performActionMultipleGetText(perpersonprice);
					prices.addAll(listOfPriceSection);
					report.log(LogStatus.INFO,
							"List of selected Best For Filter size after applying filter in page no : ("
									+ (index + 1) + ") are : "
									+ listOfPriceSection.size());
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
				report.log(
						LogStatus.INFO,
						"List of All selected Best For Filter size : "
								+ prices.size());
				if ((prices.size() == holicount1) && (holicount1 == Count)) {
					report.log(
							LogStatus.PASS,
							"Results count after applying filter : "
									+ prices.size()
									+ " and the total no of results after applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							" Results count after applying filter : "
									+ prices.size()
									+ " and the total no of results after applying filter are not same : "
									+ holicount1);
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Best For filter due to : " + e);
		}
	}

	/**
	 * The method is used to to filter the Budget It accepts 3 parameters(Budget
	 * Types,Budget Text,Budget searchResults) returns void
	 * 
	 * @throws InterruptedException
	 */

	public void budgetFilter(String BudgetButton, String BudgetPrice,
			String perpersonprice) throws InterruptedException {
		try {
			int index = 0;
			String bdgPrc;
			List<String> listOfPriceSection;
			List<String> prices = new ArrayList<String>();

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRESULTS_FILTERS");
			Thread.sleep(2000);
			performActionClick("THOMSON_FILTERS_BUDGETARROW");
			Thread.sleep(2000);
			bdgPrc = performActionRandomClickandGetText(BudgetButton,
					BudgetPrice).replaceAll("[^0-9]+", "");
			report.log(LogStatus.INFO, "Selected Budget Filter is : " + bdgPrc);
			performActionClick("THOMSON_FILTERS_APPLY");
			Thread.sleep(15000);
			performActionClick("THOMSON_FILTERS_CLOSE");
			Thread.sleep(2000);
			int holicount1 = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found after Applying filter:"
							+ holicount1);
			do {
				if (index != 0) {
					performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
					performActionClick("THOMSON_SEARCHRESULTS_NEXT");
					Thread.sleep(15000);
				}
				listOfPriceSection = performActionMultipleGetText(perpersonprice);
				prices.addAll(listOfPriceSection);
				report.log(
						LogStatus.INFO,
						"List of selected Budget Filter size after applying filter in page no : ("
								+ (index + 1) + ") are : "
								+ listOfPriceSection.size());
				report.log(LogStatus.INFO,
						"List of selected Budget Filter prices after applying filter in page no : ("
								+ (index + 1) + ") are : " + listOfPriceSection);
				index++;
			} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
			report.log(
					LogStatus.INFO,
					"List of All selected Budget Filter size : "
							+ prices.size());
			for (int i = 0; i < prices.size(); i++) {
				String z = prices.get(i).replaceAll("[^0-9]+", "");
				if (Integer.parseInt(z) <= Integer.parseInt(bdgPrc)) {
					report.log(
							LogStatus.PASS,
							"selected Budget Filter Price in searchresults page : "
									+ z
									+ "is less than or equal to Applied filter Price : "
									+ bdgPrc);
				} else {
					report.log(
							LogStatus.PASS,
							"selected Budget Filter Price in searchresults page : "
									+ z
									+ "is not less than or equal to Applied filter Price : "
									+ bdgPrc);
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Budget filter due to : " + e);
		}
	}

	/**
	 * The method is used to to filter the Board It accepts 3 parameters(Board
	 * Types,Board Text,Board searchResults) returns void
	 * 
	 * @throws InterruptedException
	 */

	public void boardFilter(String BudgetButton, String BudgetCount,
			String BoardList) throws InterruptedException {
		try {
			int Count;
			int index = 0;
			String board, brdName;
			List<String> listOfbrdSection = new ArrayList<String>();
			List<String> prices = new ArrayList<String>();

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRES_BBFILTER");
			Thread.sleep(2000);
			brdName = performActionRandomClickandGetText(BudgetButton,
					BudgetCount);
			performActionClick("THOMSON_BBFILTER_APPLYBUTTON");
			Thread.sleep(15000);
			if (brdName.contains("ALL BOARD BASIS")) {
				report.log(LogStatus.INFO, "Selected Boardbasis Filter is : "
						+ brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);
				if (holicount == holicount1) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are not same : "
									+ holicount1);
				}
			} else {
				Count = Integer.parseInt(brdName.replaceAll("[^0-9]+", ""));
				board = brdName.replaceAll("[^A-Za-z&]+", "").trim()
						.toLowerCase();
				report.log(LogStatus.INFO, "Selected BoardBasis Filter is : "
						+ brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);

				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					listOfbrdSection = performActionMultipleGetText(BoardList);
					prices.addAll(listOfbrdSection);
					report.log(LogStatus.INFO,
							"List of selected Boardbasis Filter size after applying filter in page no : ("
									+ (index + 1) + ") are : "
									+ listOfbrdSection.size());
					report.log(LogStatus.INFO,
							"List of selected Boardbasis Filter Names after applying filter in page no : ("
									+ (index + 1) + ") are : "
									+ listOfbrdSection);
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
				report.log(LogStatus.INFO,
						"List of All selected Boardbasis Filter size : "
								+ prices.size());
				if (prices.size() == Count) {
					report.log(
							LogStatus.PASS,
							"selected Boardbasis Filter count after applying filter : "
									+ prices.size()
									+ "and the total no of results after applying filter filter are same : "
									+ Count);
				} else {
					report.log(
							LogStatus.FAIL,
							"selected Boardbasis Filter count after applying filter : "
									+ prices.size()
									+ "and the total no of results after applying filter filter are not same : "
									+ Count);
				}
				for (int i = 0; i < prices.size(); i++) {
					String Z = prices.get(i).toLowerCase()
							.replaceAll("\\s+", "");
					if (Z.equals(board)) {
						report.log(LogStatus.PASS,
								"selected Boardbasis in searchresults page : "
										+ Z + " is equal to Applied filter : "
										+ board);
					} else {
						report.log(LogStatus.FAIL,
								"selected Boardbasis in searchresults page : "
										+ Z
										+ " is not equal to Applied filter : "
										+ board);
					}
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Board filter due to : " + e);
		}
	}

	/**
	 * The method is used to to filter the Date It accepts 3 parameters(Date
	 * Types,Date Text,Date searchResults) returns void
	 * 
	 * @throws InterruptedException
	 *             ,ParseException
	 */
	public void DateFilter(String BudgetButton, String BudgetCount,
			String BoardList) throws InterruptedException, ParseException {
		try {
			int index = 0;
			String brdName;
			List<String> listOfbrdSection = new ArrayList<String>();
			List<String> prices = new ArrayList<String>();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM yyyy");
			SimpleDateFormat sdf1 = new SimpleDateFormat("EEE dd/MM/yyyy");

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRES_DATESFILTER");
			Thread.sleep(15000);
			brdName = performActionRandomClickandGetText(BudgetButton,
					BudgetCount);
			if (brdName.equals("ALL DATES")) {
				report.log(LogStatus.INFO, "Selected Date Filter is : "
						+ brdName);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);
				if (holicount == holicount1) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays Found after Applying filter : "
									+ holicount1
									+ " and Total Number of Holidays Found before Applying filter are not same : "
									+ holicount1);
				}
			} else {
				Date date1 = sdf.parse(brdName);
				String date2 = sdf1.format(date1);
				report.log(LogStatus.INFO, "Selected Date Filter is : " + date2);
				int holicount1 = Integer.parseInt(performActionGetText(
						"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll(
						"[^0-9]", ""));
				report.log(LogStatus.INFO,
						"Total Number of Holidays Found after Applying filter:"
								+ holicount1);

				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					listOfbrdSection = performActionMultipleGetText(BoardList);
					prices.addAll(listOfbrdSection);
					report.log(LogStatus.INFO,
							"List of Dates size after applying filter in page no : ("
									+ (index + 1) + ") are : "
									+ listOfbrdSection.size());
					report.log(LogStatus.INFO,
							"List of Dates after applying filter in page no : ("
									+ (index + 1) + ") are : "
									+ listOfbrdSection);
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
				report.log(LogStatus.INFO,
						"List of All Dates size : " + prices.size());

				if (prices.size() == holicount1) {
					report.log(
							LogStatus.PASS,
							"Dates count after applying filter : "
									+ prices.size()
									+ " and the total no of results after applying filter are same : "
									+ holicount1);
				} else {
					report.log(
							LogStatus.FAIL,
							"Dates count after applying filter : "
									+ prices.size()
									+ " and the total no of results after applying filter are not same : "
									+ holicount1);
				}
				for (int i = 0; i < prices.size(); i++) {
					String Z = prices.get(i);
					String[] s11 = Z.split("-");
					Date date11 = sdf.parse(s11[1].trim());
					String date22 = sdf1.format(date11);
					if (date22.equals(date2)) {
						report.log(LogStatus.PASS,
								"Date in searchresults page : " + date22
										+ " is  equal to Applied filter : "
										+ date2);
					} else {
						report.log(LogStatus.FAIL,
								"Date in searchresults page : " + date22
										+ " is not equal to Applied filter  : "
										+ date2);
					}
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Dates filter due to : " + e);
		}
	}

	/**
	 * The method is used to to filter the Duration It accepts 3
	 * parameters(Duration Types,Duration Text,Duration searchResults) returns
	 * void
	 * 
	 * @throws InterruptedException
	 *             ,ParseException
	 */
	public void DurationFilter(String BudgetButton, String BudgetCount,
			String BoardList) throws InterruptedException, ParseException {
		try {
			int index = 0;
			String brdName;
			List<String> listOfbrdSection = new ArrayList<String>();
			List<String> prices = new ArrayList<String>();

			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found before Applying filter:"
							+ holicount);
			performActionClick("THOMSON_SEARCHRESULTS_FILTERS");
			Thread.sleep(2000);
			performActionClick("THOMSON_FILTERS_DURATIONARROW");
			Thread.sleep(15000);
			brdName = performActionRandomClickandGetText(BudgetButton,
					BudgetCount);
			report.log(LogStatus.INFO, "Selected Duration Filter is : "
					+ brdName);
			performActionClick("THOMSON_FILTERS_CLOSE");
			Thread.sleep(2000);
			int holicount1 = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			report.log(LogStatus.INFO,
					"Total Number of Holidays Found after Applying filter:"
							+ holicount1);
			do {
				if (index != 0) {
					performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
					performActionClick("THOMSON_SEARCHRESULTS_NEXT");
					Thread.sleep(15000);
				}
				listOfbrdSection = performActionMultipleGetText(BoardList);
				prices.addAll(listOfbrdSection);
				report.log(
						LogStatus.INFO,
						"List of Durations size after applying filter in page no : ("
								+ (index + 1) + ") are : "
								+ listOfbrdSection.size());
				report.log(LogStatus.INFO,
						"List of Durations names after applying filter in page no : ("
								+ (index + 1) + ") are : " + listOfbrdSection);
				index++;
			} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
			report.log(LogStatus.INFO,
					"List of All Durations size : " + prices.size());
			if (prices.size() == holicount1) {
				report.log(
						LogStatus.PASS,
						"Duration Results count after applying filter : "
								+ prices.size()
								+ " and the total no of results after applying filter are same : "
								+ holicount1);
			} else {
				report.log(
						LogStatus.FAIL,
						"Duration Results count after applying filter : "
								+ prices.size()
								+ " and the total no of results after applying filter are not same : "
								+ holicount1);
			}
			for (int i = 0; i < prices.size(); i++) {
				String Z = prices.get(i);
				String[] s11 = Z.split("-");
				if (s11[0].trim().equals(brdName)) {
					report.log(LogStatus.PASS,
							"Duration name in searchresults page : " + s11[0]
									+ " is  equal to Applied filter : "
									+ brdName);
				} else {
					report.log(LogStatus.FAIL,
							"Duration name in searchresults page : " + s11[0]
									+ " is not equal to Applied filter  : "
									+ brdName);
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Applying Duration filter due to : " + e);

		}
	}

	// /////////////////////////////////
	// FiltersandSortingMobileEnd//////////////////////

	public List<String> performActionMultipleGetTextfromattributes(
			String objectName, String Attributename) {
		// String objectValue = null;
		objectDetails = getObjectDetails(objectName);
		List<String> multVal = new ArrayList<String>();
		List<WebElement> ele = new ArrayList();
		try {
			if ((objectDetails.get(CONSTANTS_OBJECT_TYPE)
					.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_TEXT))) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElements(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElements(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					ele = driver.findElements(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

					logger.info(objectName + " text is captured");
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElements(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

					logger.info(objectName + " text is captured");
				}
				// ||(!valuetext.isEmpty()) (valuetext!=null)
			}
			for (WebElement ele1 : ele) {
				String valuetext = ele1.getAttribute(Attributename);
				if (valuetext.isEmpty()) {

				} else {
					multVal.add(valuetext);
				}
			}

		} catch (Exception e) {
			logger.info(objectName + " could not get the text **********");
			logger.error(e);
		}
		return multVal;
	}

	// ///////////////// swati code///////////////////////
	/**
	 * Method to enterpassenger details
	 * 
	 * @date nov 2016
	 * @param String
	 * 
	 * @author Swati
	 * @return void
	 */
	public void EnterpassengerDetailsth() throws InterruptedException {
		try {
			List<WebElement> ele = driver.findElements(By
					.xpath(".//*[contains(@id,'title')]"));
			List<WebElement> fname = driver
					.findElements(By
							.xpath(".//*[contains(@id,'first-name') or contains(@id,'firstName')]"));
			List<WebElement> lname = driver
					.findElements(By
							.xpath(".//*[contains(@id,'last-name') or contains(@id,'surName')]"));
			// List<WebElement>
			// adultageBoxes=driver.findElements(By.xpath(".//*[contains(@id,'dobNode')]/input[contains(@id,'dob')]"));
			List<WebElement> adultageBoxes = driver
					.findElements(By
							.xpath("//h2[contains(.,'Adult')]/following-sibling::div[1]//div[contains(@data-dojo-type,'tui.widget.bookflow.views.passengerDetails.ValidationDOB')]//input[1]"));
			List<WebElement> childBoxes = driver
					.findElements(By
							.xpath("//h2[contains(.,'Child') or contains(.,'Infant')]/following-sibling::div[1]//div[4]//input[1]"));
			List<WebElement> insuranceSelect = driver.findElements(By
					.xpath(".//select[contains(@id,'insurance')]"));
			List<String> multVal = new ArrayList<String>();
			List<WebElement> Cages = driver.findElements(By
					.xpath(".//span[@class='size-15']"));

			// performActionSelectDropDown_Select("FO_PASSENGERPAGE_COUNTRY","Australia");

			driver.findElement(By.xpath(".//*[@id='address-one']")).sendKeys(
					"123123");
			report.log(LogStatus.INFO, "House Number is Entered as : 123123");
			driver.findElement(By.xpath(".//*[@id='address-two']")).sendKeys(
					"AutoAdderss");
			report.log(LogStatus.INFO, "Address 1 is Entered as : AutoAdderss");
			driver.findElement(By.xpath(".//*[@id='city-town']")).sendKeys(
					"Hyderabad");
			report.log(LogStatus.INFO, "Town/city is Entered as : Hyderabad");
			driver.findElement(By.xpath(".//*[@id='county']")).sendKeys(
					"Australia");
			report.log(LogStatus.INFO, "Country is Entered as : Australia");

			driver.findElement(By.xpath(".//*[@id='postcode']")).sendKeys(
					"TW33TL");// fdsf
			report.log(LogStatus.INFO, "PostCode is Entered as : TW33TL");
			driver.findElement(By.xpath(".//*[@id='tel']")).sendKeys(
					"8143252685");
			report.log(LogStatus.INFO,
					"Telephone Number is Entered as : 8143252685");
			driver.findElement(By.xpath(".//*[@id='email']")).sendKeys(
					"Mobile.automation@sonata-software.com");
			report.log(LogStatus.INFO,
					"Email Address is Entered as : Mobile.automation@sonata-software.com");
			driver.findElement(By.xpath(".//*[@id='confirmationEmail']"))
					.sendKeys("Mobile.automation@sonata-software.com");
			report.log(LogStatus.INFO,
					"Confirm Email Address is Entered as : Mobile.automation@sonata-software.com");

			if (isPresent("Thomson_PassengerDetail_insuranceSelect")) {
				for (int i = 0; i < insuranceSelect.size(); i++) {
					Thread.sleep(2000);
					Select sel = new Select(insuranceSelect.get(i));
					// sel.selectByVisibleText("Single trip - Age 18-64");
					sel.selectByIndex(2);
					// driver.findElement(By.xpath(".//*[@id='dob0']")).sendKeys("30/11/1985");
					report.log(LogStatus.INFO,
							"Insurance type is selected as Single trip");
				}
			}
			Thread.sleep(1000);
			for (int i = 0; i < ele.size(); i++) {
				Select sel = new Select(ele.get(i));
				sel.selectByIndex(1);
				fname.get(i).sendKeys("Automation");
				report.log(LogStatus.INFO,
						"FirstName is Entered as : Automation");
				lname.get(i).sendKeys("Kumar");
				report.log(LogStatus.INFO, "surName is Entered as : Kumar");
			}
			int ChiladAge;
			for (int i = 0; i < Cages.size(); i++) {
				String Agetext1 = Cages.get(i).getText()
						.replaceAll("[^0-9,-]", "");
				if (Agetext1.contains("-")) {
					ChiladAge = Character.getNumericValue(Agetext1.charAt(2));
					ChiladAge = ChiladAge - 1;
				} else {
					ChiladAge = Integer.parseInt(Agetext1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String date = sdf.format(DateUtils.addYears(new Date(),
						-ChiladAge));
				multVal.add(String.valueOf(date));
			}
			for (int i = 0; i < childBoxes.size(); i++) {
				childBoxes.get(i).sendKeys(multVal.get(i));
				report.log(LogStatus.INFO,
						"Childage Date Of Birth Entered as :" + multVal.get(i));
			}

			if (adultageBoxes.size() > 0) {
				driver.findElement(By.xpath(".//*[@id='dob0']")).sendKeys(
						"30/11/1985");
				for (int i = 0; i < adultageBoxes.size(); i++) {
					adultageBoxes.get(i).sendKeys("30/11/1984");
					report.log(LogStatus.INFO,
							"Adult age Date Of Birth Entered as :"
									+ "30/11/1984");
				}
			}
			takeScreen();
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public void takeScreen() {
		report.attachScreenshot(takeScreenShotExtentReports());
	}

	/**************
	 * Mobile Methods Ends
	 *****************************************/

	/**
	 * The method to select the alternate select and baggage for booklfow It
	 * receives four parameter It returns void
	 */

	public void ThreadSleep() throws InterruptedException {
		Thread.sleep(3000);

	}

	public void alternateSeatSelection(String totalPrice,
			String selctedAlternate, String diffTotalPrice, String selectButton)
			throws InterruptedException {
		String totPrcBeforeSelct, diffPrc, totAfterSelct;
		Double addedPrc;
		boolean flag = false;

		totPrcBeforeSelct = performActionGetText(totalPrice).replaceAll(
				"[^0-9.]", "");

		diffPrc = performActionRandomClickandGetText(selectButton,
				diffTotalPrice);
		report.log(LogStatus.INFO, "The alternate seat differe price "
				+ diffPrc);
		Thread.sleep(500L);

		selctedAlternate = performActionGetText(selctedAlternate).trim()
				.replaceAll("[^0-9.]", "");
		report.log(LogStatus.INFO,
				"The selected seat differe price in flight summay panel "
						+ selctedAlternate);

		logger.info("The difference:" + diffPrc + "\t" + selctedAlternate);

		PerformActionSleep("2000");
		totAfterSelct = performActionGetText(totalPrice).replaceAll("[^0-9.]",
				"");
		if (diffPrc != null) {
			if (diffPrc.contains("+")) {
				diffPrc = diffPrc.replace("+", "");
				report.log(LogStatus.INFO,
						"The alternate seat differe price and total price before select "
								+ diffPrc + "+" + totPrcBeforeSelct);
				addedPrc = Double.parseDouble(diffPrc)
						+ Double.parseDouble(totPrcBeforeSelct);
			} else {
				diffPrc = diffPrc.replace("-", "");
				addedPrc = Double.parseDouble(totPrcBeforeSelct)
						- Double.parseDouble(diffPrc);
				report.log(LogStatus.INFO,
						"The alternate seat differe price and total price before select "
								+ totPrcBeforeSelct + "-" + diffPrc);
			}
			if (addedPrc == Double.parseDouble(totAfterSelct)) {
				flag = true;
				compareStringOnePage(totalPrice, flag, addedPrc.toString(),
						totAfterSelct);
			} else {
				flag = false;
				compareStringOnePage(totalPrice, flag, addedPrc.toString(),
						totAfterSelct);
			}
		}
		compareStringTwoPage(diffTotalPrice, selctedAlternate,
				diffPrc.replace("+", "").replace("-", ""), selctedAlternate);

	}

	public void scrollDown() {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,250)", "");
		} catch (Exception e) {

		}
	}

	public void scrollHorizontal() {
		try {
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(250,0)", "");
			report.log(LogStatus.INFO, "Scrolled horizontally");
		} catch (Exception e) {

		}
	}

	public void capturingbookingreference() {

		waitForElementToBeDisplayed("MC_CONFIRMATION_BOOKINGREFERENCE");
		String bookingRefID = performActionGetText("MC_CONFIRMATION_BOOKINGREFERENCE");
		logger.info(bookingRefID);
		report.log(LogStatus.INFO, "Booking ref for the booking is: "
				+ bookingRefID);
		report.attachScreenshot(takeScreenShotExtentReports());
	}

	public void multipleselection(String count) {
		PerformActionSleep("3000");
		List<WebElement> checks = driver.findElements(By
				.xpath(getObjectDetails(
						"MC_LANDING_MULTIPLEAIRPORTSELCTFLYFROM").get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));
		logger.info("checks:" + checks.size());
		for (int i = 0; i < Integer.parseInt(count); i++) {
			logger.info("inside");
			checks.get(i).click();
		}

	}

	public void multipledestinationselection(String count) {
		PerformActionSleep("3000");
		List<WebElement> checks = driver.findElements(By
				.xpath(getObjectDetails("MC_LANDING_MULTIPLEDESTINATION").get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));
		logger.info("checks:" + checks.size());
		for (int i = 0; i < Integer.parseInt(count); i++) {
			logger.info("inside");
			checks.get(i).click();
		}

	}

	public void performsorting(String sorttype)  {
		if (sorttype.equalsIgnoreCase("HighToLow")) {
			performActionClick("MC_SEARCHRESULT_TOHIGHTOLOWSORT");

		} else if (sorttype.equalsIgnoreCase("LowToHigh")) {
			performActionClick("MC_SEARCHRESULT_LOWTOHIGHSORT");

		}

		else if (sorttype.equalsIgnoreCase("DepartureDate")) {
			performActionClick("MC_SEARCHRESULT_DEPARTUREDATESORT");

		}
	}

	public void deprtureairportfilterby(String count) {
		PerformActionSleep("3000");
		List<WebElement> checks = driver.findElements(By
				.xpath(getObjectDetails(
						"MC_SEARCHRESULT_DEPARTUREAIRPORTFILTERBY").get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));
		logger.info("checks:" + checks.size());
		for (int i = 0; i < Integer.parseInt(count); i++) {
			logger.info("inside");
			checks.get(i).click();
		}
	}

	public void upgradingseat(String seattype) {
		if (seattype.equalsIgnoreCase("SelectYourSeat")) {
			performActionClick("MC_FLIGHTOPTION_SELECTYOURSEAT");

		} else if (seattype.equalsIgnoreCase("SeatWithExtraSpace")) {
			performActionClick("MC_FLIGHTOPTION_SEATEXTRASPACE");

		}

		else if (seattype.equalsIgnoreCase("SeatWithExtraLegroom")) {
			performActionClick("MC_FLIGHTOPTION_SELECTWITHEXTRALEGROOM");

		} else if (seattype.equalsIgnoreCase("PremiumClub")) {
			performActionClick("MC_FLIGHTOPTION_SELECTWITHPREMIUMCLUB");
		}

	}

	public void Screenshot() {
		report.attachScreenshot(takeScreenShotExtentReports());
	}

	public String performActionRandomSelection(String objectName)
			throws InterruptedException {
		objectDetails = getObjectDetails(objectName);
		String option = null;
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				List<WebElement> listBox1 = driver.findElements(By
						.xpath(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Random rd = new Random();
				int x = rd.nextInt(listBox1.size());
				logger.info("selected " + x);
				option = listBox1.get(x).getText().replace(pound, "");
				listBox1.get(x).click();
				logger.info(objectName + " value is selected as " + option);
				report.log(LogStatus.INFO, objectName
						+ " value is selected as " + option);
			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
				List<WebElement> listBox1 = driver.findElements(By
						.cssSelector(objectDetails
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				Random rd = new Random();
				int x = rd.nextInt(listBox1.size());
				logger.info("selected " + x);
				option = listBox1.get(x).getText().replace(pound, "");
				listBox1.get(x).click();
				logger.info(objectName + " value is selected as " + option);
			}
		} catch (Exception e) {
			logger.info(objectName
					+ " could not select random value **********");
			logger.error(e);
		}
		return option;
	}

	public void captureTextValuesandHold(String objectName, String key) {
		objectDetails = getObjectDetails(objectName);
		String tempHold = "";
		try {
			List<String> valuesElement = performActionMultipleGetText(objectName);
			for (int i = 0; i < valuesElement.size(); i++) {
				logger.info("To be spilit " + valuesElement.get(i));
				if (!valuesElement.get(i).contains(" X ")) {
					if (!valuesElement.get(i).equals("")) {
						if (valuesElement.size() == 1
								| valuesElement.size() - 1 == i) {
							tempHold = tempHold + valuesElement.get(i);
						} else {
							tempHold = tempHold + valuesElement.get(i) + ",";
						}
					}
				} else {
					logger.info("To be spilit " + valuesElement.get(i));
					String[] temparr = valuesElement.get(i).trim().split(" X ");
					if (temparr.length > 1) {
						if (temparr[1].length() > 2) {
							String[] temparr2 = temparr[1].split(" ");
							temparr[1] = temparr2[0];
						}
						int count = Integer.parseInt(temparr[1]);
						for (int j = 0; j < count; j++) {
							tempHold = tempHold + temparr[0] + ",";
						}
					}
				}
			}
			tempHold = objectName + "^^" + tempHold;
			valuesMap.put(key, tempHold);
			logger.info("Value stored " + tempHold + " at " + key);
			report.log(LogStatus.INFO, "Value stored " + tempHold + " at "
					+ key);

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to capture and store at " + key);

		}
	}

	/*
	 * public void BrowserSetup1(String browser) throws MalformedURLException {
	 * result = true; try {
	 * 
	 * org.openqa.selenium.Proxy proxy=new org.openqa.selenium.Proxy(); proxy
	 * .setSslProxy("10.40.1.98:8080").setHttpProxy("10.40.1.98:8080");
	 * 
	 * cap1 = new DesiredCapabilities(); //
	 * cap1.setCapability(CapabilityType.PROXY, proxy);
	 * 
	 * if (browser.equalsIgnoreCase("Firefox")) { cap1 = new
	 * DesiredCapabilities(); cap1.setBrowserName("firefox");
	 * cap1.setPlatform(Platform.XP); driver=new RemoteWebDriver(new
	 * URL("http://10.174.16.156:5566/wd/hub"),cap1);
	 * System.setProperty(CONSTANTS_FIREFOX_PROPERTY, getProjectPath() +
	 * CONSTANTS_FIREFOX_DRIVER_PATH); //driver = new FirefoxDriver(cap1);
	 * driver = new FirefoxDriver(); wait = new WebDriverWait(driver, 80);
	 * report.log(LogStatus.INFO, "Firefox driver has started");
	 * logger.info("Firefox driver has started");
	 * 
	 * } else if (browser.equalsIgnoreCase("Chrome")) {
	 * System.setProperty(CONSTANTS_CHROME_PROPERTY, getProjectPath() +
	 * CONSTANTS_CHROME_DRIVER_PATH); driver = new ChromeDriver(); wait = new
	 * WebDriverWait(driver, 80); report.log(LogStatus.INFO,
	 * "Chrome driver has started"); logger.info("Chrome driver has started"); }
	 * else if (browser.equalsIgnoreCase("PhantomJS")) {
	 * 
	 * System.setProperty(CONSTANTS_PHANTOM_PROPERTY, getProjectPath() +
	 * CONSTANTS_PHANTOM_DRIVER_PATH); cap1.setJavascriptEnabled(true);
	 * cap1.setCapability("takesScreenshot", true); cap1.setCapability(
	 * PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
	 * getProjectPath() + CONSTANTS_PHANTOM_DRIVER_PATH); driver = new
	 * PhantomJSDriver(cap1); driver.manage().window().setSize(new
	 * Dimension(1920, 1200));
	 * 
	 * wait = new WebDriverWait(driver, 80); report.log(LogStatus.INFO,
	 * "PhantomJS/Ghost driver has started");
	 * logger.info("PhantomJS/Ghost driver has started"); } else if
	 * (browser.equalsIgnoreCase("IE")) {
	 * System.setProperty(CONSTANTS_IE_PROPERTY, getProjectPath() +
	 * CONSTANTS_IE_DRIVER_PATH); driver = new InternetExplorerDriver(); wait =
	 * new WebDriverWait(driver, 80);
	 * 
	 * report.log(LogStatus.INFO, "Internet Explorer driver has started");
	 * logger.info("Internet Explorer driver has started"); }
	 * 
	 * } finally { try { Assert.assertTrue(result, "Set up Test"); } catch
	 * (Exception e) { logger.error("Try and catch block while assert " + e);
	 * 
	 * } } }
	 */

	public void AddingPrices(String Hotelprice1, String Hotelprice2,
			String ExpectedPrice) {

		int price1 = Integer.parseInt(valuesMap.get(Hotelprice1).substring(
				valuesMap.get(Hotelprice1).indexOf("^^") + 2));
		int price2 = Integer.parseInt(valuesMap.get(Hotelprice2).substring(
				valuesMap.get(Hotelprice2).indexOf("^^") + 2));
		String total = valuesMap.get(ExpectedPrice)
				.substring(valuesMap.get(ExpectedPrice).indexOf("^^") + 2)
				.replaceAll("[^0-9.]", "");
		double price3 = Double.parseDouble(total);
		if ((price2 * 2) - (price3) < 1) {
			report.log(LogStatus.PASS, "Total Price in summary page matches"
					+ " " + "Expected Price" + (price2 * 2) + " "
					+ "Actual Price" + price3);
		} else {
			report.log(LogStatus.FAIL,
					"Total Price in summary page is not matching" + " "
							+ "Expected Price" + (price2 * 2) + " "
							+ "Actual Price" + price3);
		}

	}

	public void PageDisplay1(String ObjectName, String pageDisplay) {
		objectDetails = getObjectDetails(ObjectName);
		pageDisplay = pageDisplay.toUpperCase();
		String pageSrc = driver.getPageSource(), errMsg;
		String page = null;
		try {
			page = ObjectName.substring(ObjectName.indexOf("_") + 1,
					ObjectName.lastIndexOf("_"));
			String element = performActionGetText(ObjectName);
			if (element != null) {
				if (element.toUpperCase().contains(pageDisplay)) {
					report.log(LogStatus.PASS, ObjectName
							+ " Component is Displayed from " + page
							+ " Successfully");
				} else if (pageSrc.contains(pageDisplay)) {
					report.log(LogStatus.PASS, ObjectName
							+ " Component is Displayed from " + page
							+ " Successfully");
				}
			} else if (pageSrc.contains(pageDisplay)) {
				report.log(LogStatus.PASS, ObjectName
						+ " Component is Displayed from " + page
						+ " Successfully");
			} else if (pageSrc.contains("Technical Difficulties")) {
				errMsg = "Technical Difficulties";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page diaplayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");

			} else if (pageSrc.contains("Service temporarily unavailable")) {
				errMsg = "Service temporarily unavailable";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page displayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			} else if (pageSrc.contains("All Gone")) {
				errMsg = "All Gone";
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page as page displayed with " + errMsg);
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			} else {
				report.log(LogStatus.FAIL, ObjectName
						+ " Component is not displayed from " + page
						+ " page hence pagenot loaded properly");
				logger.info("The Given text is not present in the page as page not loaded properly");
				report.attachScreenshot(takeScreenShotExtentReports());
				Assert.assertTrue(false, ObjectName + "Page Not Displayed");
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The array index out of bound execption in pageDisplay method:"
					+ a.getMessage());
		} catch (Exception e) {
			logger.error("The execption occured as given object " + ObjectName
					+ " doesnt have the page value " + e.getMessage());
			report.log(LogStatus.FAIL, ObjectName
					+ " Component is not displayed from " + page
					+ " page hence pagenot loaded properly");
			report.attachScreenshot(takeScreenShotExtentReports());
			Assert.assertTrue(false, ObjectName + "Page Not Displayed");
		}
	}

	/**
	 * Method to select random element
	 * 
	 * @author Madan
	 * 
	 */
	public void performActionRandomClicking(String objectName) {
		objectDetails = getObjectDetails(objectName);
		List<WebElement> wd;
		Random rd = new Random();
		try {

			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				wd = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				rd = new Random();
				int y = rd.nextInt(wd.size());

				wd.get(y).click();

				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {

				wd = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				rd = new Random();
				int y = rd.nextInt(wd.size());

				wd.get(y).click();

				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wd = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				rd = new Random();
				int y = rd.nextInt(wd.size());

				wd.get(y).click();

				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				rd = new Random();
				int y = rd.nextInt(wd.size());

				wd.get(y).click();

				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + "is selcted");

			}

		} catch (Exception e) {
			logger.info(objectName + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);

		}

	}

	/***************************************
	 * MultiCentre Scripts Starting
	 **********************************************/

	public void changeHotel(String index) {
		try {
			if (index.equals("1")) {
				performActionClick("MULTICENTRE_HOTELSELECTHUB_CHANGEFIRSTHOTEL");
			} else {
				performActionClick("MULTICENTRE_HOTELSELECTHUB_CHANGESECONDHOTEL");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void performDMSVerfication(String BookingReferenceNumber,
			String holidayOwner, String webPrice, String webDate,
			String anitelogin, String anitepassword) {
		try {
			driver.get("https://dms-st2/dms/cgi/dms-dash.pl");
			waitForPageLoad(driver);
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void MobileBrowserSetup(String DeviceID, String Browser)
			throws Exception {

		result = true;

		DesiredCapabilities capabilities = DesiredCapabilities.android();
		capabilities.setCapability("browserName", Browser);
		capabilities.setCapability("app", "Chrome");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "Nexus");
		capabilities.setCapability("platformVersion", "5.0.1");
		capabilities.setCapability("appWaitActivity", ".DispatchActivity");
		capabilities.setCapability("appWaitActivity", ".StartActivity");
		capabilities.setCapability("udid", DeviceID);
		capabilities.setCapability("autoLaunch", true);
		driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"),
				capabilities);

		driver.manage().timeouts().implicitlyWait(40L, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40L, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 20);
	}

	public void testSaucelabs() throws MalformedURLException {
		String USERNAME = "Kaushik456";
		String ACCESS_KEY = "823849d8-3701-4da1-b7e1-48de4535ed63";
		String URL = "https://" + USERNAME + ":" + ACCESS_KEY
				+ "@ondemand.saucelabs.com:443/wd/hub";

		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "Samsung Galaxy S4 Emulator");
		capabilities.setCapability("platformVersion", "4.4");
		// capabilities.setCapability("app",
		// "http://saucelabs.com/example_files/ContactManager.apk");
		capabilities.setCapability("browserName", "Browser");
		capabilities.setCapability("deviceOrientation", "portrait");
		capabilities.setCapability("appiumVersion", "1.5.3");

		driver = new AndroidDriver<>(new URL(URL), capabilities);
		wait = new WebDriverWait(driver, 40);
	}

	public void launchApplicationMobile(String url) {
		result = true;
		try {

			// driver.manage().window().maximize();
			driver.get(url);
			waitForPageLoad(driver);
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			report.log(LogStatus.INFO, "Launching URL:" + url);
			logger.info("Application under test is launching");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "URL has not launched");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info("URL has not launched **********");
			logger.error(e);
			result = false;
		} finally {
			try {
				Assert.assertTrue(result, "Launch URL");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	public void selectCruiseMonthWise(String month) throws InterruptedException {
		Thread.sleep(2000);
		WebElement element = driver.findElement(By
				.xpath("//li[contains(@dataid,'" + month + "')]/span"));
		((JavascriptExecutor) driver).executeScript(
				"arguments[0].scrollIntoView(true);", element);
		driver.findElement(
				By.xpath("//li[contains(@dataid,'" + month + "')]/span"))
				.click();
		Thread.sleep(1000);
	}

	public void selectCruiseMonth(String objectName)
			throws InterruptedException {
		Thread.sleep(2000);

		objectDetails = getObjectDetails(objectName);
		List<WebElement> wd1;
		Random rd = new Random();
		String d;
		try {

			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				int y = rd.nextInt(wd1.size());
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				Thread.sleep(500);
				d = wd1.get(y).getText();
				wd1.get(y).click();

				report.log(LogStatus.INFO, objectName + " is clicked! as " + d);
				logger.info(objectName + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));

				rd = new Random();
				int y = rd.nextInt(wd1.size());
				((JavascriptExecutor) driver).executeScript(
						"arguments[0].scrollIntoView(true);", wd1.get(y));
				d = wd1.get(y).getText();
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName + " is clicked! ");
				logger.info(objectName + " " + "is selcted as" + "<strong>" + d
						+ "</strong>");
				report.log(LogStatus.INFO, objectName + " " + "is selcted as"
						+ "<strong>" + d + "</strong>");
			}

		} catch (Exception e) {
			logger.info(objectName + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);

		}

	}

	public void launchApplication1(String url) {
		result = true;
		try {

			// driver.manage().window().maximize();
			driver.get(url);
			waitForPageLoad(driver);
			driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
			report.log(LogStatus.INFO, "Launching URL:" + url);
			logger.info("Application under test is launching");
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "URL has not launched");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.info("URL has not launched **********");
			logger.error(e);
			result = false;
		} finally {
			try {
				Assert.assertTrue(result, "Launch URL");
			} catch (Exception e) {
				logger.error("Try and catch block while assert " + e);
			}
		}
	}

	/**
	 * The method is used to add two price values in the a page and compare with
	 * the total price value in the same page It accepts paidToday, stillToPay,
	 * totalPrice as parameters returns void Added on Dec 2016
	 * 
	 * @author Samson
	 */
	public void addTwoPriceValuesAndCompareToTotal(String paidToday,
			String stillToPay, String totalPrice)   {
		String value1 = performActionGetText(paidToday);
		String value2 = performActionGetText(stillToPay);
		String sumValue = performActionGetText(totalPrice);

		try {

			value1 = value1.replace(pound, "").replace(euro, "")
					.replaceAll("[^0-9\\.]*", "").trim();
			value2 = value2.replace(pound, "").replace(euro, "")
					.replace("[^0-9\\.]*", "").trim();
			sumValue = sumValue.replace(pound, "").replace(euro, "")
					.replaceAll("[^0-9\\.]*", "").trim();
			double valueA = Double.parseDouble(value1);
			double valueB = Double.parseDouble(value2);
			double valueSum = Double.parseDouble(sumValue);
			double valueAdd = valueA + valueB;

			if (valueSum == valueAdd) {
				report.log(LogStatus.PASS, "The sum of Paid Today " + valueA
						+ " and Still To Pay " + valueB
						+ " is equal to the total price " + valueSum
						+ " of the holiday");
				logger.info("The sum of Paid Today " + valueA
						+ " and Still To Pay " + valueB
						+ " is equal to the total price " + valueSum
						+ " of the holiday");
			} else {
				report.log(LogStatus.FAIL, "The sum of Paid Today " + valueA
						+ " and Still To Pay " + valueB
						+ " is equal to the total price " + valueSum
						+ " of the holiday");
				logger.info("The sum of Paid Today " + valueA
						+ " and Still To Pay " + valueB
						+ " is NOT equal to the total price " + valueSum
						+ " of the holiday");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			logger.info("Component is not displayed");
			report.log(LogStatus.FAIL,
					"One or more of the price components could not be identified in the page");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
		}
	}

	public void deselectWCFGenwithpriceval(String objectName,
			String beforePrice, String afterPrice, String wcfprice) {
		try {
			String befPrice = performActionGetText(beforePrice).replaceAll(
					"[^\\.0123456789]", "");
			String wcfPrice = performActionGetText(wcfprice);
			logger.info("Total price " + befPrice + "wcf " + wcfPrice);
			// performActiongetView("Cruise_Extras_WCFSection");
			performActionClick(objectName);
			Thread.sleep(3000);
			String aftPrice = performActionGetText(afterPrice).replaceAll(
					"[^\\.0123456789]", "");
			logger.info("after price " + aftPrice);
			double expected = Double.parseDouble(befPrice)
					- Double.parseDouble(wcfPrice);
			if (Double.parseDouble(aftPrice) == Double.parseDouble(befPrice)
					- Double.parseDouble(wcfPrice)) {
				report.log(LogStatus.PASS, "Expected Price: " + expected
						+ " Actual Price:" + aftPrice);
			} else {
				report.log(LogStatus.FAIL, "Expected Price: " + expected
						+ " Actual Price:" + aftPrice);
				// report.log(LogStatus.FAIL,pageVal3+" : ( "+TotalPriceAter+")
				// is not equal to ( "+TotalpriceBefore+" + "+AmountAdded+"
				// )("+pageVal1+"+"+pageVal2+")");
			}
		} catch (Exception e) {
			logger.error(e);

		}

	}

	/**
	 * The method is used to verify shell name from shell results page shell
	 * details page It receives one parameter
	 * 
	 * @author-Chaitra
	 * @param shellNameObj
	 */

	public void shellNameVerification_MC(String shellNameObj) {

		String compName1, pageValue1;
		pageValue1 = shellNameObj.substring(shellNameObj.indexOf("_") + 1,
				shellNameObj.lastIndexOf("_"));
		compName1 = shellNameObj.substring(shellNameObj.lastIndexOf("_") + 1);
		try {
			Boolean cmpresult;
			String actualValue = performActionGetText(shellNameObj);
			String expectedValue = packageNameMC;

			if (actualValue.toLowerCase().contains(packageNameMC.toLowerCase())) {
				cmpresult = true;
				logger.info(compName1 + " Verifying condition is passed from "
						+ pageValue1 + " page where Expected:" + expectedValue
						+ " " + "Actual:" + actualValue);
				report.log(LogStatus.PASS, compName1
						+ " Verifying condition is passed from " + pageValue1
						+ " page where Expected:" + expectedValue + " "
						+ "Actual:" + actualValue);
				logger.info("Comparing summary parameters" + "-" + "Expected "
						+ expectedValue + " " + "Actual " + actualValue + " "
						+ "Result-" + cmpresult);
			} else {
				cmpresult = false;
				logger.info(compName1 + " Verifying condition is failed from "
						+ pageValue1 + " page where Expected:" + expectedValue
						+ " " + "Actual:" + actualValue);
				report.log(LogStatus.FAIL, compName1
						+ " Verifying condition is failed from " + pageValue1
						+ " page where Expected:" + expectedValue + " "
						+ "Actual:" + actualValue);
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.info("Comparing parameters" + "-" + "Expected"
						+ expectedValue + " " + "ActualPrice" + actualValue
						+ " " + "Result-" + cmpresult);
			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The ArrayIndexOutOfBoundsException in "
					+ "shellNameVerification method " + a);
			report.log(LogStatus.ERROR,
					"The ArrayIndexOutOfBoundsException in "
							+ "getToolTipText method " + a);
		} catch (Exception e) {
			e.printStackTrace();
			report.log(LogStatus.ERROR, "The Exception in"
					+ "getToolTipText method " + e);
		}

	}

	/**
	 * The method is used to identify only hotel stay multi centre It receives 3
	 * parameters
	 * 
	 * @author-Chaitra
	 * @param shellTitleObj
	 *            , viewDetailsObj, nextButtonObject
	 */

	public void hotelStayShellIdentification_MC(String shellTitleObj,
			String viewDetailsObj, String nextButtonObj) {
		try {

			List<String> allShellTitles = performActionMultipleGetText(shellTitleObj);
			Integer hotelCount = 0;
			Integer cnt = 1;

			for (int i = 0; i < allShellTitles.size(); i++) {
				if (hotelCount == 0) {
					logger.info(allShellTitles.get(i).toString());
					if (allShellTitles.get(i).toString().contains("Tour")) {

						logger.info("tours");
						Thread.sleep(400);
						PerformActionSleep("500");
						scrollWindow();
					} else {
						packageNameMC = allShellTitles.get(i).toString();
						logger.info("hotel");
						performActionClickBasedonIndex(viewDetailsObj,
								cnt.toString());
						hotelCount += 1;
						logger.info("hotelCount" + hotelCount);
						Thread.sleep(400);
						PerformActionSleep("500");
						scrollWindow();
					}
				}
				cnt++;
			}
			if (hotelCount == 0) {
				performActionComponentPresent(nextButtonObj);
				if (performActionIsEnabled(nextButtonObj) == true) {
					performActionClick(nextButtonObj);
					Thread.sleep(400);
					PerformActionSleep("500");
					hotelStayShellIdentification_MC(shellTitleObj,
							viewDetailsObj, nextButtonObj);
				} else {
					logger.info("Currently there are only hotel stay multi centres in search results page from "
							+ "hotelStayShellIdentification method ");
				}

			}

		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The ArrayIndexOutOfBoundsException in "
					+ "hotelStayShellIdentification method " + a);
			report.log(LogStatus.ERROR,
					"The ArrayIndexOutOfBoundsException in "
							+ "hotelStayShellIdentification method " + a);
		} catch (Exception e) {
			e.printStackTrace();
			report.log(LogStatus.ERROR, "The Exception in"
					+ "hotelStayShellIdentification method " + e);
		}
	}

	/**
	 * The method is used to identify tour and hotel stay multi centre It
	 * receives 3 parameters It returns of void
	 * 
	 * @author-Chaitra
	 * @param shellTitleObj
	 *            , viewDetailsObj, nextButtonObject
	 */

	public void tourAndHotelStayShellIdentification_MC(String shellTitleObj,
			String viewDetailsObj, String nextButtonObj) {
		try {

			List<String> allShellTitles = performActionMultipleGetText(shellTitleObj);
			logger.info(allShellTitles.size());
			Integer tourCount = 0;
			Integer cnt = 1;

			for (int i = 0; i < allShellTitles.size(); i++) {
				if (tourCount == 0) {
					logger.info(allShellTitles.get(i).toString());
					if (allShellTitles.get(i).toString().contains("Tour")) {
						packageNameMC = allShellTitles.get(i).toString();
						logger.info("tours");
						performActionClickBasedonIndex(viewDetailsObj,
								cnt.toString());
						tourCount += 1;
						logger.info("tourCount" + tourCount);
						Thread.sleep(400);
						PerformActionSleep("500");
						scrollWindow();
					} else {
						logger.info("hotel");
						Thread.sleep(400);
						PerformActionSleep("500");
						scrollWindow();

					}
				}
				cnt++;
			}
			if (tourCount == 0) {
				performActionComponentPresent(nextButtonObj);
				if (performActionIsEnabled(nextButtonObj) == true) {
					performActionClick(nextButtonObj);
					Thread.sleep(400);
					PerformActionSleep("500");
					tourAndHotelStayShellIdentification_MC(shellTitleObj,
							viewDetailsObj, nextButtonObj);
				} else {
					logger.info("Currently there are no tour and hotel stay multi centres in search results page from "
							+ "tourAndHotelStayShellIdentification method ");
				}

			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The ArrayIndexOutOfBoundsException in "
					+ "tourAndHotelStayShellIdentification method " + a);
			report.log(LogStatus.ERROR,
					"The ArrayIndexOutOfBoundsException in "
							+ "tourAndHotelStayShellIdentification method " + a);
		} catch (Exception e) {
			e.printStackTrace();
			report.log(LogStatus.ERROR, "The Exception in"
					+ "tourAndHotelStayShellIdentification method " + e);
		}
	}

	/**
	 * The method is used to sort the price value from low to high
	 * 
	 * @author-Chaitra It receives 5 parameters
	 * @param resultCount
	 * @param srtFltArrwClick
	 * @param fltSelection
	 * @param perPrcVal
	 * @param nextButtonObj
	 * @throws InterruptedException
	 */
	public void sortingFilterLowToHigh_MC(String resultCount,
			String srtFltArrwClick, String fltSelection, String perPrcVal,
			String nextButtonObj) throws InterruptedException {
		List<String> prcAftrSrt, ppBeforeSrt;
		List<Integer> ppBfsrt;
		ppBfsrt = new ArrayList<Integer>();
		String srtdppValPg = "", srtdppVlfmcd = "";
		boolean flag;

		String res = performActionGetText(resultCount);
		if (res.matches("\\d+")) {
			ppBeforeSrt = performActionMultipleGetText(perPrcVal);
			for (String pp : ppBeforeSrt) {
				if (pp != null && pp.length() > 0) {
					ppBfsrt.add(Integer.valueOf(pp.replaceAll("[^0-9\\.]+", "")));
				}
			}
			logger.info("The pp value in search page before sorting :"
					+ ppBfsrt.size());
			Collections.sort(ppBfsrt);
			performActionClick(srtFltArrwClick);
			PerformActionSleep("1000");
			performActionClick(fltSelection);
			PerformActionSleep("1500");
			prcAftrSrt = performActionMultipleGetText(perPrcVal);
			logger.info("The pp value in search page after sorting :"
					+ prcAftrSrt.size());

			for (int k = 0; k < prcAftrSrt.size(); k++) {
				if (prcAftrSrt.get(k) != null)
					srtdppValPg = srtdppValPg
							+ prcAftrSrt.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
			}
			for (int k = 0; k < ppBfsrt.size(); k++) {
				srtdppVlfmcd = srtdppVlfmcd + ppBfsrt.get(k) + "|";
			}
			logger.info("the value:" + srtdppValPg + "\t" + srtdppVlfmcd);
			if (srtdppValPg.equals(srtdppVlfmcd)) {
				flag = true;
				compareStringOnePage(fltSelection, flag, srtdppVlfmcd,
						srtdppValPg);
			} else {
				flag = false;
				compareStringOnePage(fltSelection, flag, srtdppVlfmcd,
						srtdppValPg);
			}
		}
	}

	/**
	 * The method is used to sort the price value from high to low
	 * 
	 * @author-Chaitra It receives 5 parameters
	 * @param resultCount
	 * @param srtFltArrwClick
	 * @param fltSelection
	 * @param perPrcVal
	 * @param nextButtonObj
	 * @throws InterruptedException
	 */
	public void sortingFilterHighToLow_MC(String resultCount,
			String srtFltArrwClick, String fltSelection, String perPrcVal,
			String nextButtonObj) throws InterruptedException {
		List<String> prcAftrSrt, ppBeforeSrt;
		List<Integer> ppBfsrt;
		ppBfsrt = new ArrayList<Integer>();
		String srtdppValPg = "", srtdppVlfmcd = "";
		boolean flag;
		String res = performActionGetText(resultCount);
		if (res.matches("\\d+")) {
			logger.error("the index val:" + res);
			ppBeforeSrt = performActionMultipleGetText(perPrcVal);
			for (String pp : ppBeforeSrt) {
				if (pp != null && pp.length() > 0) {
					ppBfsrt.add(Integer.valueOf(pp.replaceAll("[^0-9\\.]+", "")));
				}
			}
			logger.info("The pp value in search page before sort: "
					+ ppBeforeSrt.size());
			Collections.sort(ppBfsrt);
			Collections.reverse(ppBfsrt);
			performActionClick(srtFltArrwClick);
			PerformActionSleep("1000");
			performActionClick(fltSelection);
			PerformActionSleep("1500");
			prcAftrSrt = performActionMultipleGetText(perPrcVal);
			logger.info("The pp value in search page before sort: "
					+ prcAftrSrt.size());

			for (int k = 0; k < prcAftrSrt.size(); k++) {
				if (prcAftrSrt.get(k) != null)
					srtdppValPg = srtdppValPg
							+ prcAftrSrt.get(k).replaceAll("[^0-9\\.]+", "")
							+ "|";
			}
			for (int k = 0; k < ppBfsrt.size(); k++) {
				srtdppVlfmcd = srtdppVlfmcd + ppBfsrt.get(k) + "|";
			}
			if (srtdppValPg.equals(srtdppVlfmcd)) {
				flag = true;
				compareStringOnePage(perPrcVal, flag, srtdppVlfmcd, srtdppValPg);
			} else {
				flag = false;
				compareStringOnePage(perPrcVal, flag, srtdppVlfmcd, srtdppValPg);
			}

		}
	}

	/**
	 * The method is used to sort the dates based on date sort option
	 * 
	 * @author-Chaitra It receives 5 parameters
	 * @param searchRsltCt
	 * @param srtFlterClk
	 * @param srtOptSelection
	 * @param datVal
	 * @param nextButtonObj
	 * @throws InterruptedException
	 * @throws ParseException
	 */
	public void dateSort_MC(String searchRsltCt, String srtFlterClk,
			String srtOptSelection, String datVal, String nextButtonObj)
			throws InterruptedException, ParseException {
		List<String> datesBfSrt, datesAftSort;
		String srtDtValPg = "", srtdDtVlfmcd = "";
		List<Date> datValue = new ArrayList<Date>();
		SimpleDateFormat fmt = new SimpleDateFormat("EEEddMMMyyyy");
		Date d;
		boolean flag;

		String res = performActionGetText(searchRsltCt);
		if (res.matches("\\d+")) {
			datesBfSrt = performActionMultipleGetText(datVal);
			logger.info("The date value in search page before selecting the sorting: "
					+ datesBfSrt.size());
			for (String dat : datesBfSrt) {
				if (dat.length() > 0 && dat != null) {
					try {
						dat = dat.substring(dat.indexOf("-"), dat.length())
								.trim().replace(" ", "").replace("-", "");
						d = fmt.parse(dat);
						datValue.add(d);
					} catch (Exception e) {
						logger.error("The execption occuren in date sort method as unable to parse the stirng to date:"
								+ e.getMessage());
					}
				}
			}

			Collections.sort(datValue);
			performActionClick(srtFlterClk);
			performActionClick(srtOptSelection);
			Thread.sleep(1500L);

			datesAftSort = performActionMultipleGetText(datVal);
			logger.info("The date value in search page after selecting the sorting: "
					+ datesAftSort.size());
			for (int k = 0; k < datesAftSort.size(); k++) {
				if (datesAftSort.get(k) != null) {
					srtDtValPg = srtDtValPg
							+ datesAftSort
									.get(k)
									.substring(
											datesAftSort.get(k).indexOf("-") + 1)
									.replace(".", "").toLowerCase().trim()
									.replace(" ", "") + "|";
				}
			}
			fmt.applyLocalizedPattern("EEEddMMMyyyy");
			for (int k = 0; k < datValue.size(); k++) {
				try {

					String val = fmt.format(datValue.get(k));
					srtdDtVlfmcd = srtdDtVlfmcd + val.toLowerCase() + "|";
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			logger.info("the value:" + srtDtValPg + "\t" + srtdDtVlfmcd);
			if (srtDtValPg.equals(srtdDtVlfmcd)) {
				flag = true;
				compareStringOnePage(datVal, flag, srtdDtVlfmcd, srtDtValPg);
			} else {
				flag = false;
				compareStringOnePage(datVal, flag, srtdDtVlfmcd, srtDtValPg);
			}
		}
	}

	/**
	 * The method is used to filter based on airport
	 * 
	 * @author-Chaitra It receives 7 parameters
	 * @param searchResultCount
	 * @param deptArptFilter
	 * @param arptButton
	 * @param arptName
	 * @param arptTextCt
	 * @param fltApplyButton
	 * @param nextButtonObj
	 * @throws InterruptedException
	 */

	public void airportFilter_MC(String searchResultCount,
			String deptArptFilter, String arptButton, String arptName,
			String arptTextCt, String fltApplyButton, String nextButtonObj)
			throws InterruptedException {
		String typFeatur, accmCt;
		List<String> listOfAccomSection;
		PerformActionSleep("1500");
		performActionClick(deptArptFilter);
		PerformActionSleep("3000");
		typFeatur = performActionRandomClickandGetText(arptButton, arptName);
		typFeatur = typFeatur.toLowerCase();
		Thread.sleep(5000L);
		performActionMoveToElement(fltApplyButton);
		Thread.sleep(5000L);
		performActionClick(fltApplyButton);

		String res = performActionGetText(searchResultCount);
		if (res.matches("\\d+")) {

			try {
				if (typFeatur != null) {
					logger.info("Thee selected filter from search departure airport filter panel: "
							+ typFeatur);
					accmCt = typFeatur.replaceAll("[^0-9]+", "").trim();
					listOfAccomSection = performActionMultipleGetText(arptTextCt);

					for (int k = 0; k < listOfAccomSection.size(); k++) {
						if (listOfAccomSection.get(k).contains(typFeatur)) {

							compareStringTwoPage(arptName, arptTextCt,
									typFeatur, listOfAccomSection.get(k)
											.toLowerCase());
						} else {
							compareStringTwoPage(arptName, arptTextCt,
									typFeatur, listOfAccomSection.get(k)
											.toLowerCase());
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * The method is used to verify the enter child age entered It receives 1
	 * parameters It returns of void
	 * 
	 * @author-Chaitra
	 * @param chilAgeObj
	 */

	public void enteredChildAgeValuesCheck_MC(String chilAgeObj) {
		String compName1, pageValue1;
		pageValue1 = chilAgeObj.substring(chilAgeObj.indexOf("_") + 1,
				chilAgeObj.lastIndexOf("_"));
		compName1 = chilAgeObj.substring(chilAgeObj.lastIndexOf("_") + 1);
		try {
			Boolean cmpresult;
			String actualValue = performActionGetText(chilAgeObj);
			String expectedValue = childAgeMC;

			String[] expectedAge = expectedValue.split(",");
			String[] actualAge = actualValue.split(",");

			for (int k = 0; k < expectedAge.length; k++) {

				if (actualAge[k].contains(expectedAge[k])) {
					cmpresult = true;
					logger.info(compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " page where Expected:"
							+ expectedAge[k] + " " + "Actual:" + actualAge[k]);
					report.log(LogStatus.PASS, compName1
							+ " Verifying condition is passed from "
							+ pageValue1 + " page where Expected:"
							+ expectedAge[k] + " " + "Actual:" + actualAge[k]);
					logger.info("Comparing summary parameters" + "-"
							+ "Expected " + expectedAge[k] + " " + "Actual "
							+ actualAge[k] + " " + "Result-" + cmpresult);
				} else {
					cmpresult = false;
					logger.info(compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " page where Expected:"
							+ expectedAge[k] + " " + "Actual:" + actualAge[k]);
					report.log(LogStatus.FAIL, compName1
							+ " Verifying condition is failed from "
							+ pageValue1 + " page where Expected:"
							+ expectedAge[k] + " " + "Actual:" + actualAge[k]);
					report.attachScreenshot(takeScreenShotExtentReports());
					logger.info("Comparing parameters" + "-" + "Expected"
							+ expectedAge[k] + " " + "ActualPrice"
							+ actualAge[k] + " " + "Result-" + cmpresult);
				}

			}
		} catch (ArrayIndexOutOfBoundsException a) {
			logger.error("The ArrayIndexOutOfBoundsException in "
					+ "enteredChildAgeVerification method " + a);
			report.log(LogStatus.ERROR,
					"The ArrayIndexOutOfBoundsException in "
							+ "enteredChildAgeVerification method " + a);

		} catch (Exception e) {
			e.printStackTrace();
			report.log(LogStatus.ERROR, "The Exception in"
					+ "enteredChildAgeVerification method " + e);
		}
	}

	public void SplitingText(String TexttobeSplitted) {
		TexttobeSplitted = valuesMap.get(TexttobeSplitted).substring(
				valuesMap.get(TexttobeSplitted).indexOf("^^") + 2);
		String[] tt = TexttobeSplitted.split(" ");
		for (int i = 0; i < tt.length; i++) {
			logger.info(tt[i]);
			if (tt[i].contains("/")) {
				logger.info(tt[i]);
			}
		}
	}

	public void performMultiCentreSearchWithMCcode(String searchType,
			String date, String startdays, String stay, String departloc,
			String arrivecountry, String adultcount, String childcount,
			String infantcount, String childages) throws InterruptedException {
		int flag = 0;
		if (searchType.contains("Package")) {
			performActionClick("ATCORE_MULTICENTRESEARCH_PACKAGETYPE");
			ThreadSleep();
			performActionEnterText("ATCORE_DEPART_FROM_CODE", departloc);
		} else {
			performActionClick("ATCORE_MULTICENTRESEARCH_MULTICENTRETYPE");
		}
		logger.info(date);
		performActionSelectAllAndEnterText(
				"ATCORE_INCLUSIVE_SEARCH_START_DATE", date);
		Select startselect = new Select(driver.findElement(By.id("STDT_MTCH")));
		startselect.selectByValue(startdays);
		Select stayselect = new Select(driver.findElement(By.id("STAY_MTCH")));
		stayselect.selectByValue(stay);
		Select arrivelocselect = new Select(
				driver.findElement(By.id("CTY1_ID")));
		arrivelocselect.selectByVisibleText(arrivecountry);

		performActionSelectAllAndEnterText(
				"ATCORE_NEWINCLUSIVEBOOKINGPAGE_ADULTCOUNT", adultcount);
		performActionSelectAllAndEnterText(
				"ATCORE_NEWINCLUSIVEBOOKINGPAGE_CHILDCOUNT", childcount);
		performActionSelectAllAndEnterText("ATCORE_BOOKINGINITIATION_INFANTS",
				infantcount);
		if (Integer.parseInt(adultcount) > 2) {
			driver.findElement(By.id("RM1_AD")).clear();
			driver.findElement(By.id("RM1_AD")).sendKeys(adultcount);
		}
		List<WebElement> childagetextboxes = new ArrayList<WebElement>();
		logger.info("Children:" + childcount);

		childagetextboxes = driver
				.findElements(By
						.xpath(".//*[@id='frmSubmit']/div[@id='searchFormUpper']/div[@id='paxQuantites']/fieldset[@class='paxQuantites std_Row std_container']/ul[3]/li[3]/input"));
		String[] childrenages = childages.split(",");
		for (int i = 0; i < Integer.parseInt(childcount); i++) {
			childagetextboxes.get(i).sendKeys(childrenages[i]);
		}
		Thread.sleep(2000);
		performActionClick("ATCORE_SELLINGACCOM_SEARCH");

	}

	// ************Multi-Centre*************//

	public void checkForPagination() {
		try {
			String holidaycnt = performActionGetText("MULTICENTRE_SEARCHRESULTS_RESULTCOUNT");
			logger.info("Captured holiday count text:" + holidaycnt);
			String[] cnt = holidaycnt.split(" ");
			holidaycnt = cnt[0];
			int holidaycount = Integer.parseInt(holidaycnt.trim());
			logger.info("Final Holiday Count:" + holidaycnt);
			int countindex = holidaycount / 10;
			int countrem = holidaycount % 10;
			List<WebElement> paginationcnt = driver.findElements(By
					.xpath(getObjectDetails(
							"MULTICENTRE_SEARCHRESULTS_PAGINATIONLINKS").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE)));
			logger.info("Captured Pagination links count:"
					+ paginationcnt.size());
			if (holidaycount > 10) {
				logger.info("Into >10 count method!!");
				if (holidaycount > 70) {
					report.log(LogStatus.INFO, "Serach Results count:"
							+ holidaycount);
					if (driver.findElement(By.xpath(getObjectDetails(
							"MULTICENTRE_SEARCHRESULTS_DOTRESULTS").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE))) != null) {
						logger.info("Dot Links are shown in Pagination since results are >70");
						report.log(LogStatus.PASS,
								"Dot Links are shown in Pagination since results are >70");
						List<WebElement> toclickpagelinks = driver
								.findElements(By
										.xpath(getObjectDetails(
												"MULTICENTRE_SEARCHRESULTS_PAGINATIONLINKS")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						logger.info("Links count to click:"
								+ toclickpagelinks.size());
						int indexfornext = 0;
						for (int i = 0; i < toclickpagelinks.size(); i++) {
							JavascriptExecutor jse = (JavascriptExecutor) driver;
							jse.executeScript("window.scrollBy(0,-250)", "");
							toclickpagelinks.get(i).click();
							ThreadSleep();
							List<WebElement> perpageresults = driver
									.findElements(By
											.xpath(getObjectDetails(
													"MULTICENTRE_SEARCHRESULTS_PERPAGERESULTSBUTTONS")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
							logger.info("Results found on page "
									+ (toclickpagelinks.get(i).getText())
									+ " : " + perpageresults.size());
							if (perpageresults.size() == 10) {
								logger.info("Results are equal to 10 found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								report.log(
										LogStatus.PASS,
										"Results are equal to 10 found on page "
												+ (toclickpagelinks.get(i)
														.getText()) + " : "
												+ perpageresults.size());
							} else {
								logger.info("Results are not equal to 10 found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								report.log(
										LogStatus.FAIL,
										"Results are not equal to 10 found on page "
												+ (toclickpagelinks.get(i)
														.getText()) + " : "
												+ perpageresults.size());
								report.attachScreenshot(takeScreenShotExtentReports());
							}
							if (i == toclickpagelinks.size() - 1) {
								indexfornext = i + 1;
							}

						}
						List<WebElement> dotlinks = driver.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_DOTRESULTS")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						logger.info("Dot links count:" + dotlinks.size());
						JavascriptExecutor jse = (JavascriptExecutor) driver;
						jse.executeScript("window.scrollBy(0,-250)", "");
						dotlinks.get(dotlinks.size() - 1).click();
						ThreadSleep();
						toclickpagelinks = driver
								.findElements(By
										.xpath(getObjectDetails(
												"MULTICENTRE_SEARCHRESULTS_PAGINATIONLINKS")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						logger.info("Links count to click after clcikcing dots:"
								+ toclickpagelinks.size());
						for (int i = 0; i < toclickpagelinks.size(); i++) {
							jse.executeScript("window.scrollBy(0,-250)", "");
							if (Integer.parseInt(toclickpagelinks.get(i)
									.getText().trim()) > indexfornext) {
								logger.info("Inside!!");
								toclickpagelinks.get(i).click();
								ThreadSleep();
								List<WebElement> perpageresults = driver
										.findElements(By
												.xpath(getObjectDetails(
														"MULTICENTRE_SEARCHRESULTS_PERPAGERESULTSBUTTONS")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								logger.info("Results found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								if (i != toclickpagelinks.size() - 1) {
									if (perpageresults.size() == 10) {
										logger.info("Results are equal to 10 found on page "
												+ (toclickpagelinks.get(i)
														.getText())
												+ " : "
												+ perpageresults.size());
										report.log(
												LogStatus.PASS,
												"Results are equal to 10 found on page "
														+ (toclickpagelinks
																.get(i)
																.getText())
														+ " : "
														+ perpageresults.size());
									} else {
										logger.info("Results are not equal to 10 found on page "
												+ (toclickpagelinks.get(i)
														.getText())
												+ " : "
												+ perpageresults.size());
										report.log(
												LogStatus.FAIL,
												"Results are not equal to 10 found on page "
														+ (toclickpagelinks
																.get(i)
																.getText())
														+ " : "
														+ perpageresults.size());
										report.attachScreenshot(takeScreenShotExtentReports());
									}
								} else {
									if (perpageresults.size() == countrem) {
										logger.info("Results are equal to remaining results found on page "
												+ (toclickpagelinks.get(i)
														.getText())
												+ " : "
												+ perpageresults.size());
										report.log(
												LogStatus.PASS,
												"Results are equal to remaining results found on page "
														+ (toclickpagelinks
																.get(i)
																.getText())
														+ " : "
														+ perpageresults.size());
									} else {
										logger.info("Results are not equal to remaining results found on page "
												+ (toclickpagelinks.get(i)
														.getText())
												+ " : "
												+ perpageresults.size());
										report.log(
												LogStatus.FAIL,
												"Results are not equal to remaining results found on page "
														+ (toclickpagelinks
																.get(i)
																.getText())
														+ " : "
														+ perpageresults.size());
										report.attachScreenshot(takeScreenShotExtentReports());
									}
								}
							} else {
								toclickpagelinks.get(i).click();
								ThreadSleep();
							}
						}
					} else {
						logger.info("Dot Links are not shown in Pagination though results are >70");
						report.log(LogStatus.FAIL,
								"Dot Links are not shown in Pagination though results are >70");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				} else {
					report.log(LogStatus.INFO, "Search Results count:"
							+ holidaycount);
					List<WebElement> toclickpagelinks = driver
							.findElements(By
									.xpath(getObjectDetails(
											"MULTICENTRE_SEARCHRESULTS_PAGINATIONLINKS")
											.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Links count to click:"
							+ toclickpagelinks.size());
					int indexfornext = 0;
					for (int i = 0; i < toclickpagelinks.size(); i++) {
						JavascriptExecutor jse = (JavascriptExecutor) driver;
						jse.executeScript("window.scrollBy(0,-250)", "");
						toclickpagelinks.get(i).click();
						ThreadSleep();
						List<WebElement> perpageresults = driver
								.findElements(By
										.xpath(getObjectDetails(
												"MULTICENTRE_SEARCHRESULTS_PERPAGERESULTSBUTTONS")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						logger.info("Results found on page "
								+ (toclickpagelinks.get(i).getText()) + " : "
								+ perpageresults.size());
						if (i != toclickpagelinks.size() - 1) {
							if (perpageresults.size() == 10) {
								logger.info("Results are equal to 10 found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								report.log(
										LogStatus.PASS,
										"Results are equal to 10 found on page "
												+ (toclickpagelinks.get(i)
														.getText()) + " : "
												+ perpageresults.size());
							} else {
								logger.info("Results are not equal to 10 found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								report.log(
										LogStatus.FAIL,
										"Results are not equal to 10 found on page "
												+ (toclickpagelinks.get(i)
														.getText()) + " : "
												+ perpageresults.size());
								report.attachScreenshot(takeScreenShotExtentReports());
							}
						} else {
							if (perpageresults.size() == countrem) {
								logger.info("Results are equal to remaining results found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								report.log(
										LogStatus.PASS,
										"Results are equal to remaining results found on page "
												+ (toclickpagelinks.get(i)
														.getText()) + " : "
												+ perpageresults.size());
							} else {
								logger.info("Results are not equal to remaining results found on page "
										+ (toclickpagelinks.get(i).getText())
										+ " : " + perpageresults.size());
								report.log(
										LogStatus.FAIL,
										"Results are not equal to remaining results found on page "
												+ (toclickpagelinks.get(i)
														.getText()) + " : "
												+ perpageresults.size());
								report.attachScreenshot(takeScreenShotExtentReports());
							}
						}
					}
				}
			} else {
				report.log(
						LogStatus.INFO,
						"Results count is less than or equal to 10 , so there should be only one page in Pagination.");

				if (!IsElementPresent("MULTICENTRE_SEARCHRESULTS_PAGINATIONLINKS")) {
					logger.info("Since results are less than or equal to 10 there are no pagination links present.Pagination count:"
							+ paginationcnt.size());
					report.log(
							LogStatus.PASS,
							"Since results are less than or equal to 10 there are no pagination links present.Pagination count:"
									+ paginationcnt.size());

					List<WebElement> perpageresults = driver
							.findElements(By
									.xpath(getObjectDetails(
											"MULTICENTRE_SEARCHRESULTS_PERPAGERESULTSBUTTONS")
											.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Captured per page results count:"
							+ perpageresults.size());
					if (perpageresults.size() == holidaycount
							| perpageresults.size() == 10) {
						logger.info("Per Page results are shown as per the given holiday count or equal to 10 in Results Page.Count captured per page:"
								+ perpageresults.size());
						report.log(
								LogStatus.PASS,
								"Per Page results are shown as per the given holiday count or equal to 10 in Results Page.Count captured per page:"
										+ perpageresults.size());
					} else {
						logger.info("Per Page results are not shown as per the given holiday count or not equal to 10 in Results Page.Count captured per page:"
								+ perpageresults.size());
						report.log(
								LogStatus.FAIL,
								"Per Page results are not shown as per the given holiday count or not equal to 10 in Results Page.Count captured per page:"
										+ perpageresults.size());
						report.attachScreenshot(takeScreenShotExtentReports());
					}

				} else {
					logger.info("More than one pagination links are present though results are less than or equal to 10.Pagination count:"
							+ paginationcnt.size());
					report.log(
							LogStatus.FAIL,
							"More than one pagination links are present though results are less than or equal to 10.Pagination count:"
									+ paginationcnt.size());
					report.attachScreenshot(takeScreenShotExtentReports());
				}

			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectCountry(String type) {
		try {
			logger.info("Type is :" + type);
			int flagtype = 0, flagtype2 = 0, index = 0;
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				jse.executeScript("window.scrollBy(0,-250)", "");

				List<WebElement> names = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_COUNTRYCENTRESNAMES")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> buttons = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_PERPAGERESULTSBUTTONS")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Names count , Select buttons count:"
						+ names.size() + " and " + buttons.size());
				if (!type.equalsIgnoreCase("Tour")) {
					logger.info("We need to select 2 Hotels Stay!");
					report.log(LogStatus.INFO,
							"We need to select 2 Hotels Stay!");
					for (int i = 0; i < names.size(); i++) {
						logger.info("Name:" + names.get(i).getText());
						if (!names.get(i).getText().contains("Tour")) {
							flagtype = 1;
							logger.info("Found Hotel Stay!!!");
							report.log(LogStatus.INFO, "Found Hotel Stay!!!");
							buttons.get(i).click();
							ThreadSleep();
							break;
						}
					}
				} else {
					logger.info("We need to select Tour Stay!");
					report.log(LogStatus.INFO, "We need to select Tour Stay!");
					for (int i = 0; i < names.size(); i++) {
						if (names.get(i).getText().contains("Tour")) {
							flagtype = 1;
							logger.info("Found Tour Stay!!!");
							report.log(LogStatus.INFO, "Found Tour Stay!!!");
							buttons.get(i).click();
							ThreadSleep();
							break;
						}
					}
				}
				if (flagtype == 1) {
					logger.info("Given type is found in Search Results Page.Hence moving to Hotel Select Hub Page");
					flagtype2 = 1;
					break;
				} else {
					index++;
				}
			} while (driver.findElement(By.xpath(getObjectDetails(
					"MULTICENTRE_SEARCHRESULTS_NEXTBUTTON").get(
					CONSTANTS_OBJECT_LOCATOR_VALUE))) != null);
			if (flagtype2 == 1) {
				report.log(
						LogStatus.INFO,
						"Given type is found in Search Results Page.Hence moving to Hotel Select Hub Page");
			} else {
				logger.info("Given type is not found in Search Results Page.Hence not moving to Hotel Select Hub Page");
				report.log(
						LogStatus.ERROR,
						"Given type is not found in Search Results Page.Hence not moving to Hotel Select Hub Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForItineraryNotSelected() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				logger.info("This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				report.log(
						LogStatus.PASS,
						"This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				logger.info("First Centre shows the View Itinerary link in Itinerary&Hotels section as its already selected");
				report.log(
						LogStatus.PASS,
						"First Centre shows the View Itinerary link in Itinerary&Hotels section as its already selected");
				if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWN")) {
					logger.info("Second Centre Hotel details are shown in What's Inlcuded section though the second centre hotel is not selected");
					report.log(
							LogStatus.FAIL,
							"Second Centre Hotel details are shown in What's Inlcuded section though the second centre hotel is not selected");
					report.attachScreenshot(takeScreenShotExtentReports());
				} else {
					logger.info("Second Centre Hotel details are not shown in What's Inlcuded section since the second centre hotel is not selected");
					report.log(
							LogStatus.PASS,
							"Second Centre Hotel details are not shown in What's Inlcuded section since the second centre hotel is not selected");
				}
				if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
					logger.info("Second Centre Hotel details are not shown in Itinerary&Hotels section as choose button is visible since the second centre hotel is not selected ");
					report.log(
							LogStatus.PASS,
							"Second Centre Hotel details are not shown in Itinerary&Hotels section as choose button is visible since the second centre hotel is not selected");
					report.attachScreenshot(takeScreenShotExtentReports());
				} else {
					logger.info("Second Centre Hotel details are shown in Itinerary&Hotels section as choose button is not visible though the second centre hotel is not selected");
					report.log(
							LogStatus.FAIL,
							"Second Centre Hotel details are shown in Itinerary&Hotels section as choose button is not visible though the second centre hotel is not selected");
				}
			} else {
				logger.info("This is not a Tour Stay Page as either first centre is not selected or second centre is selected");
				report.log(
						LogStatus.FAIL,
						"This is not a Tour Stay Page as either first centre is not selected or second centre is selected");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForItinerarySelected() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				logger.info("This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				report.log(
						LogStatus.PASS,
						"This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				logger.info("First Centre shows the View Itinerary link in Itinerary&Hotels section as its already selected");
				report.log(
						LogStatus.PASS,
						"First Centre shows the View Itinerary link in Itinerary&Hotels section as its already selected");

				if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWN")) {
					logger.info("Second Centre Hotel details are not shown in What's Included Section since the second centre hotel is not selected");
					report.log(
							LogStatus.PASS,
							"Second Centre Hotel details are not shown in What's Included Section since the second centre hotel is not selected");
					if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
						logger.info("Second Centre Hotel details are not shown in Itinerary&Hotels section since the second centre hotel is not selected");
						report.log(
								LogStatus.PASS,
								"Second Centre Hotel details are not shown in Itinerary&Hotels section since the second centre hotel is not selected");
					} else {
						logger.info("Second Centre Hotel details are shown in Itinerary&Hotels section though the second centre hotel is not selected");
						report.log(
								LogStatus.FAIL,
								"Second Centre Hotel details are shown in Itinerary&Hotels section though the second centre hotel is not selected");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					String minprice = performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE").trim();
					logger.info("Minimum price captured in HOtel Select Hub Page:"
							+ minprice);
					performActionClick("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON");
					ThreadSleep();
					if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
						logger.info("Results present in Hotel Results Page");
						report.log(LogStatus.INFO,
								"Results present in Hotel Results Page");
						String price = performActionGetText(
								"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE")
								.trim();
						logger.info("Minimum price captured in Hotel Results Page:"
								+ price);
						if (price.equals(minprice)) {
							logger.info("Initial Price shown in HOtel Select Hub Page is the minimum price");
							report.log(LogStatus.PASS,
									"Initial Price shown in HOtel Select Hub Page is the minimum price");
							String hotelname = performActionGetText(
									"MULTICENTRE_HOTELRESULTS_FIRSTRESULTTEXT")
									.trim();
							logger.info("Hotel Name:" + hotelname);
							performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
							ThreadSleep();
							if (IsElementPresent("MULTICENTRE_HOTELDEATILS_HEADING")) {
								logger.info("Hotle Details Page page is shown after slecting the Second Center HOtel from Results Page");
								report.log(
										LogStatus.PASS,
										"Hotle Details Page page is shown after slecting the Second Center HOtel from Results Page");
								if (IsElementPresent("MULTICENTRE_HOTELDEATILS_BACKBUTTON")
										& IsElementPresent("MULTICENTRE_HOTELDEATILS_CONFIRMBUTTON")) {
									logger.info("Confirm and BAck buttons are present in HOtel Details Page");
									report.log(LogStatus.PASS,
											"Confirm and BAck buttons are present in HOtel Details Page");
									logger.info("HOtel Details Page is shown after clicking on one of the View Details Link in Hotel Results Page");
									report.log(
											LogStatus.PASS,
											"HOtel Details Page is shown after clicking on one of the View Details Link in Hotel Results Page");
									performActionClick("MULTICENTRE_HOTELDEATILS_CONFIRMBUTTON");
									ThreadSleep();
									if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWN")) {
										logger.info("Second Centre Hotel details are shown in What's Included Section since the second centre hotel is selected");
										report.log(
												LogStatus.PASS,
												"Second Centre Hotel details are shown in What's Included Section since the second centre hotel is selected");
										String name = performActionGetText(
												"MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWNNAME")
												.trim();
										logger.info("Second hotel name captured in Hotel Hub Page after slecetion:"
												+ name);
										report.log(LogStatus.INFO,
												"Second hotel name captured in Hotel Hub Page after slecetion:"
														+ name);
										if (name.trim().equalsIgnoreCase(
												hotelname.trim())) {
											logger.info("Second Hotel Name matched in What'sIncluded Section after selecting it");
											report.log(LogStatus.PASS,
													"Second Hotel Name matched in What'sIncluded Section after selecting it");
										} else {
											logger.info("Second Hotel Name not matched in What'sIncluded Section after selecting it");
											report.log(LogStatus.FAIL,
													"Second Hotel Name not matched in What'sIncluded Section after selecting it");
										}
										name = performActionGetText(
												"MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWNNAME")
												.trim();
										logger.info("Second hotel name captured in Hotel Hub Page in Itinerary&Hotels Section after slecetion:"
												+ name);
										report.log(
												LogStatus.INFO,
												"Second hotel name captured in Hotel Hub Page Itinerary&Hotels Section after slecetion:"
														+ name);
										if (name.trim().equalsIgnoreCase(
												hotelname.trim())) {
											logger.info("Second Hotel Name matched in Itinerary&Hotels Section after selecting it");
											report.log(LogStatus.PASS,
													"Second Hotel Name matched in Itinerary&Hotels Section after selecting it");
										} else {
											logger.info("Second Hotel Name not matched in What's Included Section after selecting it");
											report.log(LogStatus.FAIL,
													"Second Hotel Name not matched in What's Included Section after selecting it");
										}
										if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTREVIEWITINERARYLINK")) {
											logger.info("Second Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting it");
											report.log(
													LogStatus.PASS,
													"Second Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting it");
										} else {
											logger.info("Second Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting it");
											report.log(
													LogStatus.PASS,
													"Second Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting it");
										}
										if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")) {
											logger.info("First Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting Second Centre Hotel");
											report.log(
													LogStatus.PASS,
													"First Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting Second Centre Hotel");
										} else {
											logger.info("First Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting Second Centre Hotel");
											report.log(
													LogStatus.PASS,
													"First Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting Second Centre Hotel");
										}
									} else {
										logger.info("Second Centre Hotel details are not shown in What'sIncluded Section though the second centre hotel is selected");
										report.log(
												LogStatus.FAIL,
												"Second Centre Hotel details are not shown in What'sIncluded Section though the second centre hotel is selected");
										report.attachScreenshot(takeScreenShotExtentReports());
									}
								} else {
									logger.info("Either Confirm or Back buttons are not present in HOtel Details Page or Hotel Details Page is not opened");
									report.log(
											LogStatus.ERROR,
											"Either Confirm or Back buttons are not present in HOtel Details Page or Hotel Details Page is not opened");
									logger.info("HOtel Details Page is not shown after clicking on one of the View Details Link in Hotel Results Page");
									report.log(
											LogStatus.FAIL,
											"HOtel Details Page is not shown after clicking on one of the View Details Link in Hotel Results Page");
									report.attachScreenshot(takeScreenShotExtentReports());
								}
							} else {
								logger.info("Hotle Details Page page is not shown after slecting the Second Center HOtel from Results Page");
								report.log(
										LogStatus.PASS,
										"Hotle Details Page page is not shown after slecting the Second Center HOtel from Results Page");
								PageDisplay("MULTICENTRE_HOTELDEATILS_HEADING",
										"Text");
							}
						} else {
							logger.info("Initial Price shown in HOtel Select Hub Page is not the minimum price");
							report.log(LogStatus.FAIL,
									"Initial Price shown in HOtel Select Hub Page is not the minimum price");
							report.attachScreenshot(takeScreenShotExtentReports());
						}

					} else {
						logger.info("Results present in Hotel Results Page");
						report.log(LogStatus.FAIL,
								"Results present in Hotel Results Page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				} else {
					logger.info("Second Centre Hotel details are shown in What's Included Section though the second centre hotel is not selected");
					report.log(
							LogStatus.FAIL,
							"Second Centre Hotel details are shown in What's Included Section though the second centre hotel is not selected");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				logger.info("This is not a Tour STay Page as either first centre is not selected or second centre is selected");
				report.log(
						LogStatus.FAIL,
						"This is not a Tour STay Page as either first centre is not selected or second centre is selected");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForItinerary_Hotel() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON")) {
				logger.info("This is a Hotel Stay Page as both the centres can be selected");
				report.log(LogStatus.PASS,
						"This is a Hotel Stay Page as both the centres can be selected");
				logger.info("Choose Hotel button is visible in both centres block under Itinerary&Hotels Section before selecting both hotels");
				report.log(
						LogStatus.PASS,
						"Choose Hotel button is visible in both centres block under Itinerary&Hotels Section before selecting both hotels");
				if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTPACKGBRKDWN")
						& !IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDPACKGBRKDWN")) {
					logger.info("First and Second Centres Hotel details are not shown in What'sIncluded Section since none of the centre hotel is selected");
					report.log(
							LogStatus.PASS,
							"First and Second Centres Hotel details are not shown in What'sIncluded Section since none of the centre hotel is selected");
					if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
						logger.info("Second Centre Hotel details are not shown in Itinerary&Hotels section since the second centre hotel is not selected");
						report.log(
								LogStatus.PASS,
								"Second Centre Hotel details are not shown in Itinerary&Hotels section since the second centre hotel is not selected");
					} else {
						logger.info("Second Centre Hotel details are shown in Itinerary&Hotels section though the second centre hotel is not selected");
						report.log(
								LogStatus.FAIL,
								"Second Centre Hotel details are shown in Itinerary&Hotels section though the second centre hotel is not selected");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					String minprice = performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE").trim();
					logger.info("Minimum price captured in Hotel Select Hub Page before selcting first centre:"
							+ minprice);

					performActionClick("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON");
					ThreadSleep();
					String hotelname1 = "";
					if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
						logger.info("Results present in Hotel Results Page for selecting hotel of centre 1");
						report.log(LogStatus.INFO,
								"Results present in Hotel Results Page for selecting hotel of centre 1");
						String price = performActionGetText(
								"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE")
								.trim();
						logger.info("Minimum price captured in Hotel Results Page for centre 1:"
								+ price);
						if (price.equals(minprice)) {
							logger.info("Initial Price shown in Hotel Select Hub Page before selecting both hotels is the minimum price");
							report.log(
									LogStatus.PASS,
									"Initial Price shown in Hotel Select Hub Page before selecting both hotels is the minimum price");
							hotelname1 = performActionGetText(
									"MULTICENTRE_HOTELRESULTS_FIRSTRESULTTEXT")
									.trim();
							logger.info("Centre 1 Hotel Name:" + hotelname1);
							performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
							ThreadSleep();
							if (IsElementPresent("MULTICENTRE_HOTELDEATILS_HEADING")) {
								logger.info("Hotle Details Page page is shown after slecting the FIRST Center Hotel from Results Page");
								report.log(
										LogStatus.PASS,
										"Hotle Details Page page is shown after slecting the FIRST Center Hotel from Results Page");
								if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")
										& IsElementPresent("MULTICENTRE_HOTELDETAILS_BACKBUT")) {
									logger.info("Confirm and Back buttons are present in Hotel Details Page for Centre 1 hotel");
									report.log(LogStatus.PASS,
											"Confirm and Back buttons are present in Hotel Details Page for Centre 1 hotel");
									logger.info("HOtel Details Page is shown after clicking on one of the View Details Link in Hotel Results Page");
									report.log(
											LogStatus.PASS,
											"HOtel Details Page is shown after clicking on one of the View Details Link in Hotel Results Page");
									performActionClick("MULTICENTRE_HOTELDETAILS_CONFIRMBUT");
									ThreadSleep();
									if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTPACKGBRKDWN")) {
										logger.info("First Centre Hotel details are shown in What's Included Section since the first centre hotel is selected");
										report.log(
												LogStatus.PASS,
												"First Centre Hotel details are shown in What's Included Section since the first centre hotel is selected");
										String name = performActionGetText(
												"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREDETAILSPACKGBRKDWNNAME")
												.trim();
										logger.info("First hotel name captured in Hotel Hub Page after selection in Itinerary&Hotels Section:"
												+ name);
										report.log(
												LogStatus.INFO,
												"First hotel name captured in Hotel Hub Page after selection in Itinerary&Hotels Section:"
														+ name);
										if (name.trim().equalsIgnoreCase(
												hotelname1.trim())) {
											logger.info("First Hotel Name matched in Itinerary&Hotels Section after selecting it");
											report.log(LogStatus.PASS,
													"First Hotel Name matched in Itinerary&Hotels Section after selecting it");
										} else {
											logger.info("Second Hotel Name not matched in Package breakdown after selecting it");
											report.log(LogStatus.FAIL,
													"Second Hotel Name not matched in Package breakdown after selecting it");
										}
										if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
											logger.info("Second Centre Choose Hotel button in Itinerary&Hotels Section is shown in Itinerary&Hotels section since the first centre hotel selected and second centre hotel is not selected");
											report.log(
													LogStatus.PASS,
													"Second Centre Choose Hotel button in Itinerary&Hotels Section is shown in Itinerary&Hotels section since the first centre hotel selected and second centre hotel is not selected");
										} else {
											logger.info("Second Centre Choose Hotel is not shown in Itinerary&Hotels section though the first centre hotel selected and second centre hotel is not selected");
											report.log(
													LogStatus.FAIL,
													"Second Centre Choose Hotel is not shown in Itinerary&Hotels section though the first centre hotel selected and second centre hotel is not selected");
											report.attachScreenshot(takeScreenShotExtentReports());
										}
										String name1 = performActionGetText(
												"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN")
												.trim();
										logger.info("First hotel name captured in Hotel Hub Page after selcetion in What's Included Section"
												+ name1);
										report.log(
												LogStatus.INFO,
												"First hotel name captured in Hotel Hub Page after selcetion in What's Included Section:"
														+ name1);
										if (name.trim().equalsIgnoreCase(
												hotelname1.trim())) {
											logger.info("First Hotel Name matched in What'sIncluded Section after selecting it");
											report.log(LogStatus.PASS,
													"First Hotel Name matched in What'sIncluded Section after selecting it");
										} else {
											logger.info("First Hotel Name not matched in What's Included Section after selecting it");
											report.log(LogStatus.FAIL,
													"First Hotel Name not matched in What's Included Section after selecting it");
											report.attachScreenshot(takeScreenShotExtentReports());
										}
										if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDPACKGBRKDWN")) {
											logger.info("Second Centre Hotel is not shown in What's Included Section when only First Centre Hotel is selected");
											report.log(
													LogStatus.PASS,
													"Second Centre Hotel is not shown in What's Included Section when only First Centre Hotel is selected");
										} else {
											logger.info("Second Centre Hotel is shown in What's Included Section when only First Centre Hotel is selected");
											report.log(
													LogStatus.FAIL,
													"Second Centre Hotel is shown in What's Included Section when only First Centre Hotel is selected");
											report.attachScreenshot(takeScreenShotExtentReports());
										}
									} else {
										logger.info("First Centre Hotel details are not shown in What's Included Section though the first centre hotel is selected");
										report.log(
												LogStatus.FAIL,
												"First Centre Hotel details are not shown in What's Included Section though the first centre hotel is selected");
										report.attachScreenshot(takeScreenShotExtentReports());
									}
								} else {
									logger.info("Either Confirm or Back buttons are not present in Hotel Details Page for Centre 1 hotel");
									report.log(
											LogStatus.ERROR,
											"Either Confirm or Back buttons are not present in Hotel Details Page for Centre 1 hotel");
									logger.info("HOtel Details Page is not shown after clicking on one of the View Details Link in Hotel Results Page");
									report.log(
											LogStatus.FAIL,
											"HOtel Details Page is not shown after clicking on one of the View Details Link in Hotel Results Page");
									report.attachScreenshot(takeScreenShotExtentReports());
								}
							} else {
								logger.info("Hotle Details Page page is not shown after slecting the Second Center HOtel from Results Page");
								report.log(
										LogStatus.PASS,
										"Hotle Details Page page is not shown after slecting the Second Center HOtel from Results Page");
								PageDisplay("MULTICENTRE_HOTELDEATILS_HEADING",
										"Text");
							}
						} else {
							logger.info("Initial Price shown in Hotel Select Hub Page before selecting both hotels is not the minimum price");
							report.log(
									LogStatus.FAIL,
									"Initial Price shown in Hotel Select Hub Page before selecting both hotels is not the minimum price");
							report.attachScreenshot(takeScreenShotExtentReports());
						}

					} else {
						logger.info("Results present in Hotel Results Page");
						report.log(LogStatus.FAIL,
								"Results present in Hotel Results Page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}

					performActionClick("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON");
					ThreadSleep();
					if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
						logger.info("Results present in Hotel Results Page for selecting hotel of centre 2");
						report.log(LogStatus.INFO,
								"Results present in Hotel Results Page for selecting hotel of centre 2");
						String price = performActionGetText(
								"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE")
								.trim();
						logger.info("Minimum price captured in Hotel Results Page:"
								+ price);
						if (price.equals(minprice)) {
							logger.info("Initial Price shown in Hotel Select Hub Page is the minimum price shown for Centre 2 after selecting Centre 1");
							report.log(
									LogStatus.PASS,
									"Initial Price shown in Hotel Select Hub Page is the minimum price shown for Centre 2 after selecting Centre 1");
							String hotelname = performActionGetText(
									"MULTICENTRE_HOTELRESULTS_FIRSTRESULTTEXT")
									.trim();
							logger.info("Hotel Name for Centre 2:" + hotelname);
							performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
							ThreadSleep();
							if (IsElementPresent("MULTICENTRE_HOTELDEATILS_HEADING")) {
								logger.info("Hotle Details Page page is shown after slecting the Second Center Hotel from Results Page");
								report.log(
										LogStatus.PASS,
										"Hotle Details Page page is shown after slecting the Second Center Hotel from Results Page");
								if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")
										& IsElementPresent("MULTICENTRE_HOTELDETAILS_BACKBUT")) {
									logger.info("Confirm and Back buttons are present in Hotel Details Page for Centre 2");
									report.log(LogStatus.PASS,
											"Confirm and Back buttons are present in Hotel Details Page for Centre 2");
									logger.info("HOtel Details Page is shown after clicking on one of the View Details Link in Hotel Results Page");
									report.log(
											LogStatus.PASS,
											"HOtel Details Page is shown after clicking on one of the View Details Link in Hotel Results Page");
									performActionClick("MULTICENTRE_HOTELDETAILS_CONFIRMBUT");
									ThreadSleep();
									if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREDETAILSPACKGBRKDWNNAME")
											& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTPACKGBRKDWN")) {
										logger.info("First Centre Hotel details are shown in What's Included Section after selcting both the centre hotels");
										report.log(
												LogStatus.PASS,
												"First Centre Hotel details are shown in What's Included Section after selcting both the centre hotels");
									} else {
										logger.info("First Centre Hotel details are npot shown in What's Included Section after selecting both the centre hotels");
										report.log(
												LogStatus.FAIL,
												"First Centre Hotel details are not shown in What's Included Section after selecting both the centre hotels");
									}
									if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDPACKGBRKDWN")) {
										logger.info("Second Centre Hotel details are shown in What's Included Section since the second centre hotel is selected");
										report.log(
												LogStatus.PASS,
												"Second Centre Hotel details are shown in What's Included Section since the second centre hotel is selected");
										String name = performActionGetText(
												"MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWNNAME")
												.trim();
										logger.info("Second hotel name captured in Hotel Hub Page in Itinerary&Hotels Section after selection:"
												+ name);
										report.log(
												LogStatus.INFO,
												"Second hotel name captured in Hotel Hub Page Itinerary&Hotels Section after selection:"
														+ name);
										if (name.trim().equalsIgnoreCase(
												hotelname.trim())) {
											logger.info("Second Hotel Name matched in Itinerary&Hotels Section after selecting it");
											report.log(LogStatus.PASS,
													"Second Hotel Name matched in Itinerary&Hotels Section after selecting it");
										} else {
											logger.info("Second Hotel Name not matched in What's Included Section after selecting it");
											report.log(LogStatus.FAIL,
													"Second Hotel Name not matched in What's Included Section after selecting it");
										}
										if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTREVIEWITINERARYLINK")) {
											logger.info("Second Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
											report.log(
													LogStatus.PASS,
													"Second Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
										} else {
											logger.info("Second Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
											report.log(
													LogStatus.PASS,
													"Second Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
										}
										if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")) {
											logger.info("First Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
											report.log(
													LogStatus.PASS,
													"First Centre Hotel shows the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
										} else {
											logger.info("First Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
											report.log(
													LogStatus.PASS,
													"First Centre Hotel does not show the View Itinerary Link in Itinerary&Hotels Section after selecting both hotels");
										}
										name = performActionGetText(
												"MULTICENTRE_HOTELSELECTHUB_SECONDCENTRENAMEPACKGBRKDWN")
												.trim();
										logger.info("Second hotel name captured in Hotel Hub Page in What's Included Section after selection:"
												+ name);
										report.log(
												LogStatus.INFO,
												"Second hotel name captured in Hotel Hub Page in What's Included Section after selection:"
														+ name);
										if (name.trim().equalsIgnoreCase(
												hotelname.trim())) {
											logger.info("Second Hotel Name matched in What's Included Section after selecting it");
											report.log(LogStatus.PASS,
													"Second Hotel Name matched in What's Included Section after selecting it");
										} else {
											logger.info("Second Hotel Name not matched in What's Included Section after selecting it");
											report.log(LogStatus.FAIL,
													"Second Hotel Name not matched in What's Included Section after selecting it");
										}
									} else {
										logger.info("Second Centre Hotel details are not shown in What's Included Section though the second centre hotel is selected");
										report.log(
												LogStatus.FAIL,
												"Second Centre Hotel details are not shown in What's Included Section though the second centre hotel is selected");
										report.attachScreenshot(takeScreenShotExtentReports());
									}
								} else {
									logger.info("Either Confirm or Back buttons are not present in Hotel Details Page for Centre 2");
									report.log(
											LogStatus.ERROR,
											"Either Confirm or Back buttons are not present in Hotel Details Page for Centre 2");
									logger.info("HOtel DPage is not shown after clicking on one of the View Details Link in Hotel Results Page");
									report.log(
											LogStatus.FAIL,
											"HOtel Details Page is not shown after clicking on one of the View Details Link in Hotel Results Page");
									report.attachScreenshot(takeScreenShotExtentReports());
								}
							} else {
								logger.info("Hotle Details Page page is not shown after slecting the Second Center Hotel from Results Page");
								report.log(
										LogStatus.PASS,
										"Hotle Details Page page is not shown after slecting the Second Center Hotel from Results Page");
								PageDisplay("MULTICENTRE_HOTELDEATILS_HEADING",
										"Text");
							}
						} else {
							logger.info("Initial Price shown in Hotel Select Hub Page is not the minimum price shown for Centre 2 after selecting Centre 1");
							report.log(
									LogStatus.FAIL,
									"Initial Price shown in Hotel Select Hub Page is not the minimum price shown for Centre 2 after selecting Centre 1");
							report.attachScreenshot(takeScreenShotExtentReports());
						}

					} else {
						logger.info("Results present in Hotel Results Page for Centre 2");
						report.log(LogStatus.FAIL,
								"Results present in Hotel Results Page for Centre 2");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				} else {
					logger.info("First and Second Centres Hotel details are shown in What'sIncluded Section though none of the centre hotel is selected");
					report.log(
							LogStatus.FAIL,
							"First and Second Centres Hotel details are shown in What'sIncluded Section though none of the centre hotel is selected");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				logger.info("This is not a Hotel Stay Page as either or both the centres can't be selected");
				report.log(LogStatus.FAIL,
						"This is not a Hotel Stay Page as either or both the centres can't be selected");
				logger.info("Choose Hotel button not visible in both centres block under Itinerary&Hotels Section before selecting both hotels");
				report.log(
						LogStatus.PASS,
						"Choose Hotel button not visible in both centres block under Itinerary&Hotels Section before selecting both hotels");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForPackageBreakDownNotSelected() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				logger.info("This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				report.log(
						LogStatus.PASS,
						"This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				String perpersonprice = performActionGetText(
						"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE").trim();
				logger.info("Per Price price shown in Package Breakdown before selecting Second hotel:"
						+ perpersonprice);
				/*
				 * String totalprice=performActionGetText(
				 * "MULTICENTRE_HOTELSELECTHUB_TOTALPRICE").trim(); logger.
				 * info(
				 * "Total Price shown in Package Breakdown before selecting Second hotel:"
				 * +totalprice); String
				 * pax=performActionGetText("MULTICENTRE_HOTELSELECTHUB_PAX").
				 * trim(); logger.info("Pax:"+pax);
				 * report.log(LogStatus.INFO,"Per Pax Price, Pax :"
				 * +perpersonprice+" and "+pax); String
				 * calculatedprice=String.valueOf(Integer.parseInt(
				 * perpersonprice)*Integer.parseInt(pax));
				 * logger.info("Calculated total price:"+calculatedprice);
				 * report.log(LogStatus.INFO,"Calculated Total Price :"
				 * +calculatedprice);
				 */
				if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_3USP")
						& performActionMultipleGetText(
								"MULTICENTRE_HOTELSELECTHUB_3USP").size() == 3) {
					logger.info("3-USP is shown for Centre 1 and is correct");
					report.log(LogStatus.PASS,
							"3-USP is shown for Centre 1 and is correct");

				} else {
					logger.info("Either 3-USP is not shown for Centre 1 or count is not 3. COunt captured:"
							+ performActionMultipleGetText(
									"MULTICENTRE_HOTELSELECTHUB_3USP").size());
					report.log(
							LogStatus.FAIL,
							"Either 3-USP is not shown for Centre 1 or count is not 3. COunt captured:"
									+ performActionMultipleGetText(
											"MULTICENTRE_HOTELSELECTHUB_3USP")
											.size());
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				logger.info("This is not a Tour Stay Page as either first centre is not selected or second centre is selected");
				report.log(
						LogStatus.FAIL,
						"This is not a Tour Stay Page as either first centre is not selected or second centre is selected");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForPackageBreakDownSelected() {

		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				logger.info("This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");
				report.log(
						LogStatus.PASS,
						"This is a Tour STay Page as first centre is already selected and Hotel is yet to be selected");

				String perpersonprice = performActionGetText(
						"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE").trim();
				logger.info("Per Price price shown in Package Breakdown before selecting Second hotel:"
						+ perpersonprice);
				/*
				 * String totalprice=performActionGetText(
				 * "MULTICENTRE_HOTELSELECTHUB_TOTALPRICE").trim(); logger.
				 * info(
				 * "Total Price shown in Package Breakdown before selecting Second hotel:"
				 * +totalprice); String
				 * pax=performActionGetText("MULTICENTRE_HOTELSELECTHUB_PAX").
				 * trim(); logger.info("Pax:"+pax);
				 * report.log(LogStatus.INFO,"Per Pax Price, Total Price, Pax :"
				 * +perpersonprice+" and "+totalprice+" and "+pax); String
				 * calculatedprice=String.valueOf(Integer.parseInt(
				 * perpersonprice)*Integer.parseInt(pax));
				 * logger.info("Calculated total price:"+calculatedprice);
				 * report.log(LogStatus.INFO,"Calculated Total Price :"
				 * +calculatedprice);
				 */
				if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_3USP")
						& performActionMultipleGetText(
								"MULTICENTRE_HOTELSELECTHUB_3USP").size() == 3) {
					logger.info("3-USP is shown for Centre 1 and is correct");
					report.log(LogStatus.PASS,
							"3-USP is shown for Centre 1 and is correct");

				} else {
					logger.info("Either 3-USP is not shown for Centre 1 or count is not 3. COunt captured:"
							+ performActionMultipleGetText(
									"MULTICENTRE_HOTELSELECTHUB_3USP").size());
					report.log(
							LogStatus.FAIL,
							"Either 3-USP is not shown for Centre 1 or count is not 3. COunt captured:"
									+ performActionMultipleGetText(
											"MULTICENTRE_HOTELSELECTHUB_3USP")
											.size());
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				performActionClick("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON");
				ThreadSleep();
				if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
					logger.info("Results present in Hotel Results Page");
					report.log(LogStatus.INFO,
							"Results present in Hotel Results Page");
					String price = performActionGetText(
							"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE").trim();
					logger.info("Price captured in Hotel Results Page of Second Hotel being selected:"
							+ price);
					report.log(LogStatus.INFO,
							"Price captured in Hotel Results Page of Second Hotel being selected:"
									+ price);
					String room = performActionGetText(
							"MULTICENTRE_HOTELRESULTS_TOURSTAYROOM").trim();
					String board = performActionGetText(
							"MULTICENTRE_HOTELRESULTS_TOURSTAYBOARD").trim();
					performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
					ThreadSleep();

					if (IsElementPresent("MULTICENTRE_HOTELDEATILS_HEADING")) {
						logger.info("Hotle Details Page page is shown after slecting the Second Center HOtel from Results Page");
						report.log(
								LogStatus.PASS,
								"Hotle Details Page page is shown after slecting the Second Center HOtel from Results Page");
						performActionClick("MULTICENTRE_HOTELDEATILS_CONFIRMBUTTON");
						ThreadSleep();
						String selectedprice = performActionGetText(
								"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE")
								.trim();
						logger.info("Price captured in Hotel Select Hub Page after selecting Second Hotel:"
								+ selectedprice);
						report.log(LogStatus.INFO,
								"Price captured in Hotel Select Hub Page after selecting Second Hotel:"
										+ selectedprice);

						if (price.equals(selectedprice)) {
							logger.info("Price selected for Second Hotel has matched");
							report.log(LogStatus.PASS,
									"Price selected for Second Hotel has matched");
							perpersonprice = performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE")
									.trim();
							logger.info("Per Price price shown in Package Breakdown after selecting Second hotel:"
									+ perpersonprice);
							/*
							 * totalprice=performActionGetText(
							 * "MULTICENTRE_HOTELSELECTHUB_TOTALPRICE").trim();
							 * logger. info(
							 * "Total Price shown in Package Breakdown after selecting Second hotel:"
							 * +totalprice); pax=performActionGetText(
							 * "MULTICENTRE_HOTELSELECTHUB_PAX").trim();
							 * logger.info("Pax:"+pax); report.log(LogStatus.
							 * INFO,"Per Pax Price, Total Price, Pax :"
							 * +perpersonprice+" and "+totalprice+" and "+pax);
							 * calculatedprice=String.valueOf(Integer.parseInt(
							 * perpersonprice)*Integer.parseInt(pax));
							 * logger.info("Calculated total price:"
							 * +calculatedprice); report.log(LogStatus.
							 * INFO,"Calculated Total Price :"+calculatedprice);
							 */
							if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_CHANGESECONDHOTEL")) {
								logger.info("Change HOtel link is present for Centre 2 HOtel");
								report.log(LogStatus.PASS,
										"Change HOtel link is present for Centre 2 HOtel");
							} else {
								logger.info("Change HOtel link is not present for Centre 2 HOtel");
								report.log(LogStatus.FAIL,
										"Change HOtel link is not present for Centre 2 HOtel");
								report.attachScreenshot(takeScreenShotExtentReports());
							}

							if (IsElementPresent("MULTICENTRE_HOTELRESULTS_TOURSTAYROOM")
									& performActionGetText(
											"MULTICENTRE_HOTELRESULTS_TOURSTAYROOM")
											.trim().equalsIgnoreCase(room)) {
								logger.info("Room selected for centre 2 is present and  has matched in What's Include section");
								report.log(
										LogStatus.PASS,
										"Room selected for centre 2 is present and has matched in What's Include section");

							} else {
								logger.info("Either Room selected for centre 2 is not present or has not matched in What's Include section");
								report.log(
										LogStatus.FAIL,
										"Either Room selected for centre 2 is not present or has not matched in What's Include section");
								report.attachScreenshot(takeScreenShotExtentReports());
							}
							if (IsElementPresent("MULTICENTRE_HOTELRESULTS_TOURSTAYBOARD")
									& performActionGetText(
											"MULTICENTRE_HOTELRESULTS_TOURSTAYBOARD")
											.trim().equalsIgnoreCase(board)) {
								logger.info("Board selected for centre 2 is present and  has matched in What's Include section");
								report.log(
										LogStatus.PASS,
										"Board selected for centre 2 is present and has matched in What's Include section");

							} else {
								logger.info("Either Board selected for centre 2 is not present or has not matched in What's Include section");
								report.log(
										LogStatus.FAIL,
										"Either Board selected for centre 2 is not present or has not matched in What's Include section");
								report.attachScreenshot(takeScreenShotExtentReports());
							}
							if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_TOURSTAYROOM")
									& performActionGetText(
											"MULTICENTRE_HOTELSELECTHUB_TOURSTAYROOM")
											.trim().equalsIgnoreCase(room)) {
								logger.info("Room selected for centre 2 is present and  has matched in What's Included section");
								report.log(
										LogStatus.PASS,
										"Room selected for centre 2 is present and has matched in What's Included section");

							} else {
								logger.info("Either Room selected for centre 2 is not present or has not matched in What's Included section");
								report.log(
										LogStatus.FAIL,
										"Either Room selected for centre 2 is not present or has not matched in What's Included section");
								report.attachScreenshot(takeScreenShotExtentReports());
							}
							if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_TOURSTAYBOARD")
									& performActionGetText(
											"MULTICENTRE_HOTELSELECTHUB_TOURSTAYBOARD")
											.trim().equalsIgnoreCase(board)) {
								logger.info("Board selected for centre 2 is present and  has matched in Itinerary&Hotels section");
								report.log(
										LogStatus.PASS,
										"Board selected for centre 2 is present and has matched in Itinerary&Hotels section");

							} else {
								logger.info("Either Board selected for centre 2 is not present or has not matched in Itinerary&Hotels section");
								report.log(
										LogStatus.FAIL,
										"Either Board selected for centre 2 is not present or has not matched in Itinerary&Hotels section");
								report.attachScreenshot(takeScreenShotExtentReports());
							}
						} else {
							logger.info("Price selected for Second Hotel has not matched");
							report.log(LogStatus.FAIL,
									"Price selected for Second Hotel has not matched");
							report.attachScreenshot(takeScreenShotExtentReports());
						}
					} else {
						logger.info("Hotle Details Page page is not shown after slecting the Second Center HOtel from Results Page");
						report.log(
								LogStatus.PASS,
								"Hotle Details Page page is not shown after slecting the Second Center HOtel from Results Page");
						PageDisplay("MULTICENTRE_HOTELDEATILS_HEADING", "Text");
					}

				} else {
					logger.info("Results present in Hotel Results Page");
					report.log(LogStatus.FAIL,
							"Results present in Hotel Results Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				logger.info("This is not a Tour Stay Page as either first centre is not selected or second centre is selected");
				report.log(
						LogStatus.FAIL,
						"This is not a Tour Stay Page as either first centre is not selected or second centre is selected");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}

	}

	public void checkForHotelResults() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON")) {
				logger.info("Both hotels are not selected");
				report.log(LogStatus.PASS, "Both hotels are not selected");
				performActionClick("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON");
				ThreadSleep();
				if (IsElementPresent("MULTICENTRE_HOTELDETAILS_BACKOVERVIEW")) {
					logger.info("Back to Overview Link is visible in Hotel Results Page");
					report.log(LogStatus.PASS,
							"Back to Overview Link is visible in Hotel Results Page");
				} else {
					logger.info("Back to Overview Link is not visible in Hotel Results Page");
					report.log(LogStatus.FAIL,
							"Back to Overview Link is not visible in Hotel Results Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				int index1 = 0;
				List<String> prices = new ArrayList<String>();
				String initialprice = performActionGetText(
						"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE").trim();
				report.log(LogStatus.INFO,
						"Initial Price captured of First Hotel in Hotel Results Page:"
								+ initialprice);
				do {
					if (index1 != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> hotelslist = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_HOTELRESULTS_HOTELSLIST").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Hotels list in page " + index1 + " : "
							+ hotelslist.size());
					for (int i = 0; i < hotelslist.size(); i++) {
						if (hotelslist.get(i).findElement(
								By.xpath(getObjectDetails(
										"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Image is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Image is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Image is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Image is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELGALLERY")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Gallery Button is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Gallery Button is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Gallery Button is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Gallery Button is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist.get(i).findElement(
								By.xpath(getObjectDetails(
										"MULTICENTRE_HOTELRESULTS_HOTELNAME")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Name is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Name is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Name is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Name is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELTRIPRATING")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Trip Rating is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Trip Rating is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Trip Rating is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Trip Rating is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELPRICEBREAKDOWN")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Package Breakdown is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Package Breakdown is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Package Breakdown is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Package Breakdown is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELPRICEPERPERSON")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Price Per Person is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Price Per Person is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Price Per Person is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Price Per Person is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELTOTALPRICE")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Total Price is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Total Price is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Total Price is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Total Price is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELWHATSTHISLINK")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel What is this Price link is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel What is this Price link is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel What is this Price link is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel What is this Price link is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELPAYONLY")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Pay Only link is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Pay Only link is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Pay Only link is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Pay Only link is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						if (hotelslist
								.get(i)
								.findElement(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELSELECTBUTTON")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
								& hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
										.isDisplayed()) {
							logger.info("Hotel Select Button is displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.PASS,
									"Hotel Select Button is displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						} else {
							logger.info("Hotel Select Button is not displayed for Hotel "
									+ (i + 1) + " on Page " + (index1 + 1));
							report.log(LogStatus.FAIL,
									"Hotel Select Button is not displayed for Hotel "
											+ (i + 1) + " on Page "
											+ (index1 + 1));
						}
						List<WebElement> usplist = hotelslist
								.get(i)
								.findElements(
										By.xpath(getObjectDetails(
												"MULTICENTRE_HOTELRESULTS_HOTELUSP")
												.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						if (usplist.size() == 3) {
							logger.info("Hotel 3-USP are displayed for Hotel "
									+ (i + 1)
									+ " on Page "
									+ (index1 + 1 + " . Found USP count : " + usplist
											.size()));
							report.log(
									LogStatus.PASS,
									"Hotel 3-USP are displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1
													+ " . Found USP count : " + usplist
														.size()));
						} else {
							logger.info("Hotel 3-USP are not displayed for Hotel "
									+ (i + 1)
									+ " on Page "
									+ (index1 + 1 + " . Found USP count : " + usplist
											.size()));
							report.log(
									LogStatus.FAIL,
									"Hotel 3-USP are not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1
													+ " . Found USP count : " + usplist
														.size()));
						}

					}
					prices.addAll(performActionMultipleGetText("MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES"));
					logger.info("List of prices added to list in this loop:"
							+ performActionMultipleGetText(
									"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES")
									.size());
					logger.info("Prices list:" + prices.size());
					index1++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
				List<Integer> integervalueprices = new ArrayList<Integer>();
				for (String each : prices) {
					integervalueprices.add(Integer.parseInt(each));
				}
				String pric = valuesMap.get("price")
						.substring(valuesMap.get("price").indexOf("^^") + 2)
						.trim();
				logger.info("Price:" + pric);
				/*
				 * pric=pric.substring(1,pric.length()).trim();
				 * logger.info("Prce:"+pric);
				 */
				Collections.sort(integervalueprices);
				if (String.valueOf(integervalueprices.get(0)).equals(
						initialprice)
						& pric.equals(initialprice)) {
					logger.info("Initial Price shown in Hotel Results Page is the minimum price");
					report.log(LogStatus.PASS,
							"Initial Price shown in Hotel Results Page is the minimum price");
				} else {
					logger.info("Initial Price shown in Hotel Results Page is not the minimum price.Values captured before and presnet page:"
							+ initialprice + " and " + pric);
					report.log(
							LogStatus.FAIL,
							"Initial Price shown in Hotel Results Page is not the minimum price.Values captured before and presnet page:"
									+ initialprice + " and " + pric);
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,150)", "");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void check1stHotelChangeResults() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON")) {
				logger.info("First hotel is selected and Second Hotel is not selected");
				report.log(LogStatus.INFO,
						"First hotel is selected and Second Hotel is not selected");
				performActionClick("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELDETAILS_CONFIRMBUT");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELSELECTHUB_CHANGEFIRSTHOTEL");
				ThreadSleep();
				if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
					logger.info("Hotel Results Page is opened after clicking on Change Hotel link in Centre 1 block");
					report.log(
							LogStatus.PASS,
							"Hotel Results Page is opened after clicking on Change Hotel link in Centre 1 block");
					if (IsElementPresent("MULTICENTRE_HOTELRESULTS_BACKBUTTON")) {
						logger.info("Back to Overview Link is visible in Hotel Results Page");
						report.log(LogStatus.PASS,
								"Back to Overview Link is visible in Hotel Results Page");
					} else {
						logger.info("Back to Overview Link is not visible in Hotel Results Page");
						report.log(LogStatus.FAIL,
								"Back to Overview Link is not visible in Hotel Results Page");
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					int index1 = 0;
					List<String> prices = new ArrayList<String>();
					String initialprice = performActionGetText(
							"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE").trim();
					report.log(LogStatus.INFO,
							"Initial Price captured of First Hotel in Hotel Results Page:"
									+ initialprice);
					do {
						if (index1 != 0) {
							performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
							ThreadSleep();
						}
						List<WebElement> hotelslist = driver.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_HOTELRESULTS_HOTELSLIST")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
						logger.info("Hotels list in page " + index1 + " : "
								+ hotelslist.size());
						for (int i = 0; i < hotelslist.size(); i++) {
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Image is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Image is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Image is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Image is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELGALLERY")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Gallery Button is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Gallery Button is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Gallery Button is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Gallery Button is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELNAME")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Name is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Name is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Name is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Name is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELTRIPRATING")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Trip Rating is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Trip Rating is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Trip Rating is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Trip Rating is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELPRICEBREAKDOWN")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Package Breakdown is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Package Breakdown is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Package Breakdown is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Package Breakdown is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELPRICEPERPERSON")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Price Per Person is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Price Per Person is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Price Per Person is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Price Per Person is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELTOTALPRICE")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Total Price is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Total Price is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Total Price is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Total Price is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELWHATSTHISLINK")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel What is this Price link is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel What is this Price link is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel What is this Price link is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel What is this Price link is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELPAYONLY")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Pay Only link is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Pay Only link is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Pay Only link is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Pay Only link is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							if (hotelslist
									.get(i)
									.findElement(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELSELECTBUTTON")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
									& hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
											.isDisplayed()) {
								logger.info("Hotel Select Button is displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.PASS,
										"Hotel Select Button is displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							} else {
								logger.info("Hotel Select Button is not displayed for Hotel "
										+ (i + 1) + " on Page " + (index1 + 1));
								report.log(LogStatus.FAIL,
										"Hotel Select Button is not displayed for Hotel "
												+ (i + 1) + " on Page "
												+ (index1 + 1));
							}
							List<WebElement> usplist = hotelslist
									.get(i)
									.findElements(
											By.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELUSP")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
							if (usplist.size() == 3) {
								logger.info("Hotel 3-USP are displayed for Hotel "
										+ (i + 1)
										+ " on Page "
										+ (index1 + 1 + " . Found USP count : " + usplist
												.size()));
								report.log(
										LogStatus.PASS,
										"Hotel 3-USP are displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1
														+ 1
														+ " . Found USP count : " + usplist
															.size()));
							} else {
								logger.info("Hotel 3-USP are not displayed for Hotel "
										+ (i + 1)
										+ " on Page "
										+ (index1 + 1 + " . Found USP count : " + usplist
												.size()));
								report.log(
										LogStatus.FAIL,
										"Hotel 3-USP are not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1
														+ 1
														+ " . Found USP count : " + usplist
															.size()));
							}

						}
						prices.addAll(performActionMultipleGetText("MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES"));
						logger.info("List of prices added to list in this loop:"
								+ performActionMultipleGetText(
										"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES")
										.size());
						logger.info("Prices list:" + prices.size());
						index1++;
					} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
					List<Integer> integervalueprices = new ArrayList<Integer>();
					for (String each : prices) {
						integervalueprices.add(Integer.parseInt(each));
					}
					Collections.sort(integervalueprices);
					if (String.valueOf(integervalueprices.get(0)).equals(
							initialprice)) {
						logger.info("Initial Price shown in Hotel Results Page is the minimum price");
						report.log(LogStatus.PASS,
								"Initial Price shown in Hotel Results Page is the minimum price");
					} else {
						logger.info("Initial Price shown in Hotel Results Page is not the minimum price");
						report.log(LogStatus.FAIL,
								"Initial Price shown in Hotel Results Page is not the minimum price");
					}
				} else {
					logger.info("Hotel Results Page is not opened after clicking on Change Hotel link in Centre 1 block");
					report.log(
							LogStatus.FAIL,
							"Hotel Results Page is not opened after clicking on Change Hotel link in Centre 1 block");
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void check2ndHotelResults() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				logger.info("First hotel is not selected and Second Hotel is not selected");
				report.log(LogStatus.INFO,
						"First hotel is not selected and Second Hotel is not selected");
				performActionClick("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELDETAILS_CONFIRMBUT");
				ThreadSleep();
				if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")
						& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")) {
					logger.info("First Hotel is selected and Second Hotel is not selected");
					report.log(LogStatus.INFO,
							"First Hotel is selected and Second Hotel is not selected");
					performActionClick("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON");
					ThreadSleep();
					if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
						logger.info("Hotel Results Page is opened after clicking on Choose Hotel button in Centre 2 block");
						report.log(
								LogStatus.PASS,
								"Hotel Results Page is opened after clicking on Choose Hotel button in Centre 2 block");
						if (IsElementPresent("MULTICENTRE_HOTELRESULTS_BACKBUTTON")) {
							logger.info("Back to Overview Link is visible in Hotel Results Page");
							report.log(LogStatus.PASS,
									"Back to Overview Link is visible in Hotel Results Page");
						} else {
							logger.info("Back to Overview Link is not visible in Hotel Results Page");
							report.log(LogStatus.FAIL,
									"Back to Overview Link is not visible in Hotel Results Page");
							report.attachScreenshot(takeScreenShotExtentReports());
						}
						int index1 = 0;
						List<String> prices = new ArrayList<String>();
						String initialprice = performActionGetText(
								"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE")
								.trim();
						report.log(LogStatus.INFO,
								"Initial Price captured of Second Hotel in Hotel Results Page:"
										+ initialprice);
						do {
							if (index1 != 0) {
								performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
								ThreadSleep();
							}
							List<WebElement> hotelslist = driver
									.findElements(By
											.xpath(getObjectDetails(
													"MULTICENTRE_HOTELRESULTS_HOTELSLIST")
													.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
							logger.info("Hotels list in page " + index1 + " : "
									+ hotelslist.size());
							for (int i = 0; i < hotelslist.size(); i++) {
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Image is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Image is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Image is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Image is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELGALLERY")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Gallery Button is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Gallery Button is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Gallery Button is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Gallery Button is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELNAME")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Name is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Name is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Name is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Name is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELTRIPRATING")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Trip Rating is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Trip Rating is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Trip Rating is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Trip Rating is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELPRICEBREAKDOWN")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Package Breakdown is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Package Breakdown is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Package Breakdown is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Package Breakdown is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELPRICEPERPERSON")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Price Per Person is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Price Per Person is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Price Per Person is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Price Per Person is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELTOTALPRICE")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Total Price is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Total Price is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Total Price is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Total Price is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELWHATSTHISLINK")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel What is this Price link is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel What is this Price link is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel What is this Price link is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel What is this Price link is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELPAYONLY")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Pay Only link is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Pay Only link is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Pay Only link is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Pay Only link is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								if (hotelslist
										.get(i)
										.findElement(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELSELECTBUTTON")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
										& hotelslist
												.get(i)
												.findElement(
														By.xpath(getObjectDetails(
																"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
												.isDisplayed()) {
									logger.info("Hotel Select Button is displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.PASS,
											"Hotel Select Button is displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								} else {
									logger.info("Hotel Select Button is not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1));
									report.log(LogStatus.FAIL,
											"Hotel Select Button is not displayed for Hotel "
													+ (i + 1) + " on Page "
													+ (index1 + 1));
								}
								List<WebElement> usplist = hotelslist
										.get(i)
										.findElements(
												By.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELUSP")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								if (usplist.size() == 3) {
									logger.info("Hotel 3-USP are displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1
													+ " . Found USP count : " + usplist
														.size()));
									report.log(
											LogStatus.PASS,
											"Hotel 3-USP are displayed for Hotel "
													+ (i + 1)
													+ " on Page "
													+ (index1
															+ 1
															+ " . Found USP count : " + usplist
																.size()));
								} else {
									logger.info("Hotel 3-USP are not displayed for Hotel "
											+ (i + 1)
											+ " on Page "
											+ (index1 + 1
													+ " . Found USP count : " + usplist
														.size()));
									report.log(
											LogStatus.FAIL,
											"Hotel 3-USP are not displayed for Hotel "
													+ (i + 1)
													+ " on Page "
													+ (index1
															+ 1
															+ " . Found USP count : " + usplist
																.size()));
								}

							}
							prices.addAll(performActionMultipleGetText("MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES"));
							logger.info("List of prices added to list in this loop:"
									+ performActionMultipleGetText(
											"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES")
											.size());
							logger.info("Prices list:" + prices.size());
							index1++;
						} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
						List<Integer> integervalueprices = new ArrayList<Integer>();
						for (String each : prices) {
							integervalueprices.add(Integer.parseInt(each));
						}
						Collections.sort(integervalueprices);
						if (String.valueOf(integervalueprices.get(0)).equals(
								initialprice)) {
							logger.info("Initial Price shown in Hotel Results Page is the minimum price");
							report.log(LogStatus.PASS,
									"Initial Price shown in Hotel Results Page is the minimum price");
						} else {
							logger.info("Initial Price shown in Hotel Results Page is not the minimum price");
							report.log(LogStatus.FAIL,
									"Initial Price shown in Hotel Results Page is not the minimum price");
						}
					} else {
						logger.info("Hotel Results Page is not opened after clicking on Choose Hotel button in Centre 2 block");
						report.log(
								LogStatus.FAIL,
								"Hotel Results Page is not opened after clicking on Choose Hotel button in Centre 2 block");
					}
				} else {
					logger.info("Either First Hotel is not selected or Second Hotel is selected");
					report.log(LogStatus.ERROR,
							"Either First Hotel is not selected or Second Hotel is selected");
				}
			} else {
				logger.info("Both Hotels are not selected");
				report.log(LogStatus.ERROR, "Both Hotels are not selected");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}

	}

	public void check2ndHotelChangeResults() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON")
					& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				logger.info("First hotel is not selected and Second Hotel is not selected");
				report.log(LogStatus.INFO,
						"First hotel is not selected and Second Hotel is not selected");
				performActionClick("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRECHOOSEHOTELBUTTON");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
				ThreadSleep();
				performActionClick("MULTICENTRE_HOTELDETAILS_CONFIRMBUT");
				ThreadSleep();
				if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")
						& IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREVIEWITINERARYLINK")) {
					logger.info("First Hotel is selected and Second Hotel is not selected");
					report.log(LogStatus.INFO,
							"First Hotel is selected and Second Hotel is not selected");
					performActionClick("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON");
					ThreadSleep();
					if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
						logger.info("Hotel Results Page is opened after clicking on Choose Hotel button in Centre 2 block");
						report.log(
								LogStatus.PASS,
								"Hotel Results Page is opened after clicking on Choose Hotel button in Centre 2 block");
						performActionClick("MULTICENTRE_HOTELRESULTS_FIRSTRESULT");
						ThreadSleep();
						performActionClick("MULTICENTRE_HOTELDETAILS_CONFIRMBUT");
						ThreadSleep();
						performActionClick("MULTICENTRE_HOTELSELECTHUB_CHANGESECONDHOTEL");
						ThreadSleep();
						if (IsElementPresent("MULTICENTRE_HOTELRESULTS_FIRSTRESULT")) {
							logger.info("Hotel Results Page is opened after clicking on Change Hotel link in Centre 2 block");
							report.log(
									LogStatus.PASS,
									"Hotel Results Page is opened after clicking on Change Hotel link in Centre 2 block");
							if (IsElementPresent("MULTICENTRE_HOTELRESULTS_BACKBUTTON")) {
								logger.info("Back to Overview Link is visible in Hotel Results Page");
								report.log(LogStatus.PASS,
										"Back to Overview Link is visible in Hotel Results Page");
							} else {
								logger.info("Back to Overview Link is not visible in Hotel Results Page");
								report.log(LogStatus.FAIL,
										"Back to Overview Link is not visible in Hotel Results Page");
								report.attachScreenshot(takeScreenShotExtentReports());
							}
							int index1 = 0;
							List<String> prices = new ArrayList<String>();
							String initialprice = performActionGetText(
									"MULTICENTRE_HOTELRESULTS_FIRSTRESULTPRICE")
									.trim();
							report.log(LogStatus.INFO,
									"Initial Price captured of Second Hotel in Hotel Results Page:"
											+ initialprice);
							do {
								if (index1 != 0) {
									performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
									ThreadSleep();
								}
								List<WebElement> hotelslist = driver
										.findElements(By
												.xpath(getObjectDetails(
														"MULTICENTRE_HOTELRESULTS_HOTELSLIST")
														.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
								logger.info("Hotels list in page " + index1
										+ " : " + hotelslist.size());
								for (int i = 0; i < hotelslist.size(); i++) {
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Image is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Image is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Image is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Image is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELGALLERY")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Gallery Button is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Gallery Button is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Gallery Button is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Gallery Button is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELNAME")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Name is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Name is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Name is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Name is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELTRIPRATING")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Trip Rating is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Trip Rating is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Trip Rating is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Trip Rating is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELPRICEBREAKDOWN")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Package Breakdown is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Package Breakdown is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Package Breakdown is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Package Breakdown is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELPRICEPERPERSON")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Price Per Person is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Price Per Person is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Price Per Person is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Price Per Person is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELWHATSTHISLINK")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel What is this Price link is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel What is this Price link is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel What is this Price link is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel What is this Price link is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									/*
									 * if(hotelslist.get(i).findElement(By.xpath
									 * (getObjectDetails(
									 * "MULTICENTRE_HOTELRESULTS_HOTELPAYONLY").
									 * get(CONSTANTS_OBJECT_LOCATOR_VALUE)))!=
									 * null&hotelslist.get(i).findElement(By.
									 * xpath(getObjectDetails(
									 * "MULTICENTRE_HOTELRESULTS_HOTELIMAGE").
									 * get(CONSTANTS_OBJECT_LOCATOR_VALUE))).
									 * isDisplayed()){ logger. info(
									 * "Hotel Pay Only link is displayed for Hotel "
									 * +(i+1)+" on Page "+(index1+1));
									 * report.log(LogStatus. PASS,
									 * "Hotel Pay Only link is displayed for Hotel "
									 * +(i+1)+" on Page "+(index1+1)); }else{
									 * logger. info(
									 * "Hotel Pay Only link is not displayed for Hotel "
									 * +(i+1)+" on Page "+(index1+1));
									 * report.log(LogStatus. FAIL,
									 * "Hotel Pay Only link is not displayed for Hotel "
									 * +(i+1)+" on Page "+(index1+1)); }
									 */
									if (hotelslist
											.get(i)
											.findElement(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELSELECTBUTTON")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null
											& hotelslist
													.get(i)
													.findElement(
															By.xpath(getObjectDetails(
																	"MULTICENTRE_HOTELRESULTS_HOTELIMAGE")
																	.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
													.isDisplayed()) {
										logger.info("Hotel Select Button is displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.PASS,
												"Hotel Select Button is displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									} else {
										logger.info("Hotel Select Button is not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1 + 1));
										report.log(LogStatus.FAIL,
												"Hotel Select Button is not displayed for Hotel "
														+ (i + 1) + " on Page "
														+ (index1 + 1));
									}
									List<WebElement> usplist = hotelslist
											.get(i)
											.findElements(
													By.xpath(getObjectDetails(
															"MULTICENTRE_HOTELRESULTS_HOTELUSP")
															.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
									if (usplist.size() > 0) {
										logger.info("Hotel 3-USP are displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1
														+ 1
														+ " . Found USP count : " + usplist
															.size()));
										report.log(
												LogStatus.PASS,
												"Hotel 3-USP are displayed for Hotel "
														+ (i + 1)
														+ " on Page "
														+ (index1
																+ 1
																+ " . Found USP count : " + usplist
																	.size()));
									} else {
										logger.info("Hotel 3-USP are not displayed for Hotel "
												+ (i + 1)
												+ " on Page "
												+ (index1
														+ 1
														+ " . Found USP count : " + usplist
															.size()));
										report.log(
												LogStatus.FAIL,
												"Hotel 3-USP are not displayed for Hotel "
														+ (i + 1)
														+ " on Page "
														+ (index1
																+ 1
																+ " . Found USP count : " + usplist
																	.size()));
									}

								}
								prices.addAll(performActionMultipleGetText("MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES"));
								logger.info("List of prices added to list in this loop:"
										+ performActionMultipleGetText(
												"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES")
												.size());
								logger.info("Prices list:" + prices.size());
								index1++;
							} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
							List<Integer> integervalueprices = new ArrayList<Integer>();
							for (String each : prices) {
								integervalueprices.add(Integer.parseInt(each));
							}
							Collections.sort(integervalueprices);
							if (String.valueOf(integervalueprices.get(0))
									.equals(initialprice)) {
								logger.info("Initial Price shown in Hotel Results Page is the minimum price");
								report.log(LogStatus.PASS,
										"Initial Price shown in Hotel Results Page is the minimum price");
							} else {
								logger.info("Initial Price shown in Hotel Results Page is not the minimum price");
								report.log(LogStatus.FAIL,
										"Initial Price shown in Hotel Results Page is not the minimum price");
							}
						} else {
							logger.info("Hotel Results Page is not opened after clicking on Change Hotel link in Centre 2 block");
							report.log(
									LogStatus.FAIL,
									"Hotel Results Page is not opened after clicking on Change Hotel link in Centre 2 block");
						}
					} else {
						logger.info("Hotel Results Page is not opened after clicking on Choose Hotel button in Centre 2 block");
						report.log(
								LogStatus.FAIL,
								"Hotel Results Page is not opened after clicking on Choose Hotel button in Centre 2 block");
					}
				} else {
					logger.info("Either First Hotel is not selected or Second Hotel is selected");
					report.log(LogStatus.ERROR,
							"Either First Hotel is not selected or Second Hotel is selected");
				}
			} else {
				logger.info("Both Hotels are not selected");
				report.log(LogStatus.ERROR, "Both Hotels are not selected");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}

	}

	public void checkForDefaultSort(String text) {
		try {
			performActionClick("MULTICENTRE_HOTELSELECTHUB_SORTOPTION");
			ThreadSleep();
			if (performActionGetAttributeValue(
					"MULTICENTRE_SEARCHRESULTS_DEFAULTSORT", "analytics-text")
					.trim().equalsIgnoreCase(text)) {
				logger.info("In Initial Search Results Page the Default selected Sort option is '"
						+ text
						+ "' and Captured Option in Sort block is '"
						+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
						+ "'");
				report.log(
						LogStatus.PASS,
						"In Initial Search Results Page the Default selected Sort option is '"
								+ text
								+ "' and Captured Option in Sort block is '"
								+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
								+ "'");
			} else {
				logger.info("In Initial Search Results Page the Default selected Sort option is not'"
						+ text
						+ "' and Captured Option in Sort block is '"
						+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
						+ "'");
				report.log(
						LogStatus.FAIL,
						"In Initial Search Results Page the Default selected Sort option is not'"
								+ text
								+ "' and Captured Option in Sort block is '"
								+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
								+ "'");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForDefaultSort_HotelPage(String text) {
		try {
			if (performActionGetText("MULTICENTRE_HOTELRESULTS_DEFAULTSORT")
					.trim().equalsIgnoreCase(text)) {
				logger.info("In Initial Search Results Page the Default selected Sort option is '"
						+ text
						+ "' and Captured Option in Sort block is '"
						+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
						+ "'");
				report.log(
						LogStatus.PASS,
						"In Initial Search Results Page the Default selected Sort option is '"
								+ text
								+ "' and Captured Option in Sort block is '"
								+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
								+ "'");
			} else {
				logger.info("In Initial Search Results Page the Default selected Sort option is not'"
						+ text
						+ "' and Captured Option in Sort block is '"
						+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
						+ "'");
				report.log(
						LogStatus.FAIL,
						"In Initial Search Results Page the Default selected Sort option is not'"
								+ text
								+ "' and Captured Option in Sort block is '"
								+ performActionGetText("MULTICENTRE_SEARCHRESULTS_DEFAULTSORT")
								+ "'");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForSortedResults(String sortselect) {
		try {
			String selectedsort = valuesMap.get(sortselect)
					.substring(valuesMap.get(sortselect).indexOf("^^") + 2)
					.trim();
			logger.info("Selected sort option:" + selectedsort);
			report.log(LogStatus.INFO, "Selected sort option:" + selectedsort);
			switch (selectedsort) {
			case "PRICE - LOW TO HIGH":
				sortLowHighPrice();
				break;
			case "PRICE - HIGH TO LOW":
				sortHighLowPrice();
				break;
			case "DEPARTURE DATE":
				sortDepDate();
				break;
			default:
				logger.info("NA");
				break;
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void sortLowHighPrice() {
		try {
			int holicount = Integer
					.parseInt(performActionGetText(
							"MULTICENTRE_SEARCHRESULTS_RESULTCOUNT").trim()
							.split(" ")[0]);
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				int index = 0;
				List<Integer> intprices = new ArrayList<Integer>();

				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> prices = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_ALLPRICES").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Prices count:" + prices);

					for (WebElement each : prices) {
						intprices.add(Integer.parseInt(each.getText().trim()
								.substring(1, each.getText().length())));
					}
					boolean sorted = Ordering.natural().isOrdered(
							(Iterable<? extends Comparable>) intprices);
					logger.info("Sorted value:" + sorted);
					if (sorted) {
						logger.info("Results are sorted in page " + (index + 1));
						report.log(LogStatus.PASS,
								"Results are sorted in page " + (index + 1));
					} else {
						logger.info("Results are not sorted in page "
								+ (index + 1));
						report.log(LogStatus.FAIL,
								"Results are not sorted in page " + (index + 1));
					}
					logger.info(intprices.size());
					int finalprice = intprices.get(intprices.size() - 1);
					logger.info("Final Price being added to list to get tested with next page values:"
							+ finalprice);
					intprices.clear();
					intprices.add(finalprice);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void sortHighLowPrice() {
		try {
			int holicount = Integer
					.parseInt(performActionGetText(
							"MULTICENTRE_SEARCHRESULTS_RESULTCOUNT").trim()
							.split(" ")[0]);
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				int index = 0;
				List<Integer> intprices = new ArrayList<Integer>();

				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> prices = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_ALLPRICES").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Prices count:" + prices.size());

					/*
					 * for(int k=prices.size()-1;k>=0;k--){
					 * intprices.add(Integer.parseInt(prices.get(k).getText().
					 * trim().substring(1,prices.get(k).getText().length()))); }
					 */
					for (WebElement each : prices) {
						intprices.add(Integer.parseInt(each.getText().trim()
								.substring(1, each.getText().length())));
					}
					boolean sorted = Ordering
							.natural()
							.reverse()
							.isOrdered(
									(Iterable<? extends Comparable>) intprices);
					logger.info("Sorted value:" + sorted);
					if (sorted) {
						logger.info("Results are sorted in page " + index);
						report.log(LogStatus.PASS,
								"Results are sorted in page " + index);
					} else {
						logger.info("Results are not soreted in page " + index);
						report.log(LogStatus.FAIL,
								"Results are not soreted in page " + index);
					}
					int finalprice = intprices.get(intprices.size() - 1);
					logger.info("Final Price being added to list to get tested with next page values:"
							+ finalprice);
					intprices.clear();
					intprices.add(finalprice);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void sortDepDate() {
		try {
			int holicount = Integer
					.parseInt(performActionGetText(
							"MULTICENTRE_SEARCHRESULTS_RESULTCOUNT").trim()
							.split(" ")[0]);
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				int index = 0;
				List<Date> datestexts = new ArrayList<>();

				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> dates = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_ALLDATES").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Dates count:" + dates.size());
					SimpleDateFormat sdf = new SimpleDateFormat(
							"EEE dd MMM yyyy");

					for (WebElement each : dates) {
						datestexts.add(sdf.parse(each.getText().split("-")[1]
								.trim().substring(
										0,
										each.getText().split("-")[1].trim()
												.length() - 1)));
					}
					logger.info("Size:" + datestexts.size());
					for (int d = 1; d < datestexts.size(); d++) {
						if (datestexts.get(d - 1).before(datestexts.get(d))
								| datestexts.get(d - 1).equals(
										datestexts.get(d))) {
							logger.info("Result "
									+ (d)
									+ " is lesser than Result "
									+ (d + 1)
									+ " with respect to the Departure Date in Page "
									+ index);
							report.log(
									LogStatus.PASS,
									"Result "
											+ (d)
											+ " is lesser than Result "
											+ (d + 1)
											+ " with respect to the Departure Date in Page "
											+ index);
						} else {
							logger.info("Result "
									+ (d)
									+ " is not lesser than Result "
									+ (d + 1)
									+ " with respect to the Departure Date in Page "
									+ index);
							report.log(
									LogStatus.FAIL,
									"Result "
											+ (d)
											+ " is not lesser than Result "
											+ (d + 1)
											+ " with respect to the Departure Date in Page "
											+ index);
						}
					}

					Date finaldate = sdf.parse(dates.get(dates.size() - 1)
							.getText().split("-")[1].trim().substring(
							0,
							dates.get(dates.size() - 1).getText().split("-")[1]
									.trim().length() - 1));
					logger.info("Final Date being added to list to get tested with next page values:"
							+ finaldate);
					datestexts.clear();
					datestexts.add(finaldate);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectRequiredAirports(String list) {
		try {
			String[] airports = list.split(",");
			int flag = 0;
			logger.info("Airports to be selected count:" + airports.length);
			List<WebElement> airportsselect = driver.findElements(By
					.xpath(getObjectDetails(
							"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTCHECKS")
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
			List<String> airportnames = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_AIPORTS");
			logger.info("Counts:" + airportnames.size() + " and "
					+ airportsselect.size());
			for (int i = 0; i < airports.length; i++) {
				for (int j = 0; j < airportnames.size(); j++) {
					if (airports[i].trim().equalsIgnoreCase(
							airportnames.get(j).trim())) {
						flag = 1;
						logger.info("Airport Matched");
						airportsselect.get(j).click();
					}
				}
			}
			if (flag == 1) {
				logger.info("Given airports are present in the Airports filter");
				report.log(LogStatus.INFO,
						"Given airports are present in the Airports filter");
				List<WebElement> airportsselected = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTED")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Count:" + airportsselected.size());
				String[] airpselected = new String[airportsselected.size()];
				for (int i = 0; i < airportsselected.size(); i++) {
					// airpselected[i]=airportsselected.get(i).getAttribute("data-select-value");
					airpselected[i] = airportsselected.get(i).getText().trim();
				}
				performActionClick("MULTICENTRE_SEARCHRESULTS_APPLY");
				ThreadSleep();
				int index = 0;
				List<String> airporttexts = new ArrayList<>();
				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					ThreadSleep();
					List<WebElement> departures = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_AIPORTSSHOWN")
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Departure Airports count:" + departures.size());

					for (WebElement each : departures) {
						// airporttexts.add(each.getText().trim().split("-")[0].trim());
						airporttexts.add(each.getText().trim().split("\n")[0]
								.trim());
					}
					for (int d = 0; d < airporttexts.size(); d++) {
						int flag1 = 0;
						for (int e = 0; e < airpselected.length; e++) {
							if (airporttexts
									.get(d)
									.toLowerCase()
									.trim()
									.contains(
											airpselected[e].toLowerCase()
													.trim())) {
								flag1 = 1;
								break;
							}
						}
						if (flag1 == 1) {
							logger.info("Departure Airport has matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.PASS,
									"Departure Airport has matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						} else {
							logger.info("Departure Airport has not matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.FAIL,
									"Departure Airport has not matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						}
					}
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			} else {
				logger.info("Given airports are not present in the Airports filter");
				report.log(LogStatus.ERROR,
						"Given airports are not present in the Airports filter");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForSortedResults_Hotel() {
		try {
			int holicount = Integer.parseInt(performActionGetText(
					"MULTICENTRE_HOTELRESULTS_COUNT").trim().split(" ")[2]
					.trim());
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Hotel Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Hotel Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				int index = 0;
				List<Integer> intprices = new ArrayList<Integer>();

				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> prices = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_HOTELRESULTS_PRCIESALL").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Prices count:" + prices.size());

					for (WebElement each : prices) {
						intprices.add(Integer.parseInt(each.getText().trim()));
					}
					for (Integer each : intprices) {
						logger.info("1:" + each);
					}
					boolean sorted = Ordering.natural().isOrdered(
							(Iterable<? extends Comparable>) intprices);
					logger.info("Sorted value:" + sorted);
					if (sorted) {
						logger.info("Results are sorted in page " + index);
						report.log(LogStatus.PASS,
								"Results are sorted in page " + index);
					} else {
						logger.info("Results are not sorted in page " + index);
						report.log(LogStatus.FAIL,
								"Results are not sorted in page " + index);
					}
					logger.info(intprices.size());
					int finalprice = intprices.get(intprices.size() - 1);
					logger.info("Final Price being added to list to get tested with next page values:"
							+ finalprice);
					intprices.clear();
					intprices.add(finalprice);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForSortedResults_WithFilter(String sortselect, String list) {
		try {
			String selectedsort = valuesMap.get(sortselect)
					.substring(valuesMap.get(sortselect).indexOf("^^") + 2)
					.trim();
			logger.info("Selected sort option:" + selectedsort);
			report.log(LogStatus.INFO, "Selected sort option:" + selectedsort);
			switch (selectedsort) {
			case "PRICE - LOW TO HIGH":
				sortLowHighPrice_Filter(list);
				break;
			case "PRICE - HIGH TO LOW":
				sortHighLowPrice_Filter(list);
				break;
			case "DEPARTURE DATE":
				sortDepDate_Filter(list);
				break;
			default:
				logger.info("NA");
				break;
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void sortLowHighPrice_Filter(String list) {
		try {
			int holicount = Integer
					.parseInt(performActionGetText(
							"MULTICENTRE_SEARCHRESULTS_RESULTCOUNT").trim()
							.split(" ")[0]);
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				String[] airports = list.split(",");
				int flag = 0;
				logger.info("Airports to be selected count:" + airports.length);
				List<WebElement> airportsselect = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTCHECKS")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<String> airportnames = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_AIPORTS");
				logger.info("Counts:" + airportnames.size() + " and "
						+ airportsselect.size());
				for (int i = 0; i < airports.length; i++) {
					for (int j = 0; j < airportnames.size(); j++) {
						if (airports[i].trim().equalsIgnoreCase(
								airportnames.get(j).trim())) {
							flag = 1;
							logger.info("Airport Matched");
							airportsselect.get(j).click();
						}
					}
				}
				List<WebElement> airportsselected = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTED")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Count:" + airportsselected.size());
				String[] airpselected = new String[airportsselected.size()];
				for (int i = 0; i < airportsselected.size(); i++) {
					// airpselected[i]=airportsselected.get(i).getAttribute("data-select-name");
					airpselected[i] = airportsselected.get(i).getText().trim();
				}
				performActionClick("MULTICENTRE_SEARCHRESULTS_APPLY");
				ThreadSleep();
				List<String> airporttexts = new ArrayList<>();
				int index = 0;
				List<Integer> intprices = new ArrayList<Integer>();

				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> departures = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_AIPORTSSHOWN")
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Departure Airports count:" + departures.size());

					for (WebElement each : departures) {
						// airporttexts.add(each.getText().trim().split("-")[0].trim());
						airporttexts.add(each.getText().trim().split("\n")[0]
								.trim());
					}
					for (int d = 0; d < airporttexts.size(); d++) {
						int flag1 = 0;
						for (int e = 0; e < airpselected.length; e++) {
							if (airporttexts
									.get(d)
									.toLowerCase()
									.trim()
									.contains(
											airpselected[e].toLowerCase()
													.trim())) {
								flag1 = 1;
								break;
							}
						}
						if (flag1 == 1) {
							logger.info("Departure Airport has matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.PASS,
									"Departure Airport has matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						} else {
							logger.info("Departure Airport has not matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.FAIL,
									"Departure Airport has not matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						}
					}

					List<WebElement> prices = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_ALLPRICES").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Prices count:" + prices);

					for (WebElement each : prices) {
						intprices.add(Integer.parseInt(each.getText().trim()
								.substring(1, each.getText().length())));
					}
					boolean sorted = Ordering.natural().isOrdered(
							(Iterable<? extends Comparable>) intprices);
					logger.info("Sorted value:" + sorted);
					if (sorted) {
						logger.info("Results are sorted in page " + (index + 1));
						report.log(LogStatus.PASS,
								"Results are sorted in page " + (index + 1));
					} else {
						logger.info("Results are not sorted in page "
								+ (index + 1));
						report.log(LogStatus.FAIL,
								"Results are not sorted in page " + (index + 1));
					}
					int finalprice = intprices.get(intprices.size() - 1);
					logger.info("Final Price being added to list to get tested with next page values:"
							+ finalprice);
					intprices.clear();
					intprices.add(finalprice);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void sortHighLowPrice_Filter(String list) {
		try {
			int holicount = Integer
					.parseInt(performActionGetText(
							"MULTICENTRE_SEARCHRESULTS_RESULTCOUNT").trim()
							.split(" ")[0]);
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				String[] airports = list.split(",");
				int flag = 0;
				logger.info("Airports to be selected count:" + airports.length);
				List<WebElement> airportsselect = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTCHECKS")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<String> airportnames = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_AIPORTS");
				logger.info("Counts:" + airportnames.size() + " and "
						+ airportsselect.size());
				for (int i = 0; i < airports.length; i++) {
					for (int j = 0; j < airportnames.size(); j++) {
						if (airports[i].trim().equalsIgnoreCase(
								airportnames.get(j).trim())) {
							flag = 1;
							logger.info("Airport Matched");
							airportsselect.get(j).click();
						}
					}
				}
				List<WebElement> airportsselected = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTED")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Count:" + airportsselected.size());
				String[] airpselected = new String[airportsselected.size()];
				for (int i = 0; i < airportsselected.size(); i++) {
					// airpselected[i]=airportsselected.get(i).getAttribute("data-select-name");
					airpselected[i] = airportsselected.get(i).getText().trim();
				}
				performActionClick("MULTICENTRE_SEARCHRESULTS_APPLY");
				ThreadSleep();
				List<String> airporttexts = new ArrayList<>();
				int index = 0;
				List<Integer> intprices = new ArrayList<Integer>();

				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> departures = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_AIPORTSSHOWN")
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Departure Airports count:" + departures.size());

					for (WebElement each : departures) {
						// airporttexts.add(each.getText().trim().split("-")[0].trim());
						airporttexts.add(each.getText().trim().split("\n")[0]
								.trim());
					}
					for (int d = 0; d < airporttexts.size(); d++) {
						int flag1 = 0;
						for (int e = 0; e < airpselected.length; e++) {
							if (airporttexts
									.get(d)
									.toLowerCase()
									.trim()
									.contains(
											airpselected[e].toLowerCase()
													.trim())) {
								flag1 = 1;
								break;
							}
						}
						if (flag1 == 1) {
							logger.info("Departure Airport has matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.PASS,
									"Departure Airport has matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						} else {
							logger.info("Departure Airport has not matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.FAIL,
									"Departure Airport has not matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						}
					}

					List<WebElement> prices = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_ALLPRICES").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Prices count:" + prices);

					for (int k = prices.size() - 1; k >= 0; k--) {
						intprices.add(Integer
								.parseInt(prices
										.get(k)
										.getText()
										.trim()
										.substring(
												1,
												prices.get(k).getText()
														.length())));
					}
					boolean sorted = Ordering
							.natural()
							.reverse()
							.isOrdered(
									(Iterable<? extends Comparable>) intprices);
					logger.info("Sorted value:" + sorted);
					if (sorted) {
						logger.info("Results are soreted in page "
								+ (index + 1));
						report.log(LogStatus.PASS,
								"Results are soreted in page " + (index + 1));
					} else {
						logger.info("Results are not sorted in page "
								+ (index + 1));
						report.log(LogStatus.FAIL,
								"Results are not sorted in page " + (index + 1));
					}
					int finalprice = intprices.get(intprices.size() - 1);
					logger.info("Final Price being added to list to get tested with next page values:"
							+ finalprice);
					intprices.clear();
					intprices.add(finalprice);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void sortDepDate_Filter(String list) {
		try {
			int holicount = Integer
					.parseInt(performActionGetText(
							"MULTICENTRE_SEARCHRESULTS_RESULTCOUNT").trim()
							.split(" ")[0]);
			logger.info("Holiday Count:" + holicount);
			if (holicount == 1) {
				logger.info("There is only one result found in Results Page.Hence it is sorted");
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				String[] airports = list.split(",");
				int flag = 0;
				logger.info("Airports to be selected count:" + airports.length);
				List<WebElement> airportsselect = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTCHECKS")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<String> airportnames = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_AIPORTS");
				logger.info("Counts:" + airportnames.size() + " and "
						+ airportsselect.size());
				for (int i = 0; i < airports.length; i++) {
					for (int j = 0; j < airportnames.size(); j++) {
						if (airports[i].trim().equalsIgnoreCase(
								airportnames.get(j).trim())) {
							flag = 1;
							logger.info("Airport Matched");
							airportsselect.get(j).click();
						}
					}
				}
				List<WebElement> airportsselected = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_AIPORTSSELECTED")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Count:" + airportsselected.size());
				String[] airpselected = new String[airportsselected.size()];
				for (int i = 0; i < airportsselected.size(); i++) {
					// airpselected[i]=airportsselected.get(i).getAttribute("data-select-name");
					airpselected[i] = airportsselected.get(i).getText().trim();
				}
				performActionClick("MULTICENTRE_SEARCHRESULTS_APPLY");
				ThreadSleep();
				List<String> airporttexts = new ArrayList<>();
				int index = 0;
				List<Date> datestexts = new ArrayList<>();
				do {
					if (index != 0) {
						performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
						ThreadSleep();
					}
					List<WebElement> departures = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_AIPORTSSHOWN")
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Departure Airports count:" + departures.size());

					for (WebElement each : departures) {
						// airporttexts.add(each.getText().trim().split("-")[0].trim());
						airporttexts.add(each.getText().trim().split("\n")[0]
								.trim());
					}
					for (int d = 0; d < airporttexts.size(); d++) {
						int flag1 = 0;
						for (int e = 0; e < airpselected.length; e++) {
							if (airporttexts
									.get(d)
									.toLowerCase()
									.trim()
									.contains(
											airpselected[e].toLowerCase()
													.trim())) {
								flag1 = 1;
								break;
							}
						}
						if (flag1 == 1) {
							logger.info("Departure Airport has matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.PASS,
									"Departure Airport has matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						} else {
							logger.info("Departure Airport has not matched for Hotel "
									+ (d + 1) + " in Page " + (index + 1));
							report.log(LogStatus.FAIL,
									"Departure Airport has not matched for Hotel "
											+ (d + 1) + " in Page "
											+ (index + 1));
						}
					}

					List<WebElement> dates = driver.findElements(By
							.xpath(getObjectDetails(
									"MULTICENTRE_SEARCHRESULTS_ALLDATES").get(
									CONSTANTS_OBJECT_LOCATOR_VALUE)));
					logger.info("Dates count:" + dates);
					SimpleDateFormat sdf = new SimpleDateFormat(
							"EEE dd MMM yyyy");

					for (WebElement each : dates) {
						datestexts.add(sdf.parse(each.getText().split("-")[1]
								.trim().substring(
										0,
										each.getText().split("-")[1].trim()
												.length() - 1)));
					}
					for (int d = 1; d < datestexts.size(); d++) {
						if (datestexts.get(d - 1).before(datestexts.get(d))) {
							logger.info("Result "
									+ (d - 1)
									+ " is lesser than Result "
									+ d
									+ " with respect to the Departure Date in Page "
									+ (index + 1));
							report.log(
									LogStatus.PASS,
									"Result "
											+ (d - 1)
											+ " is lesser than Result "
											+ d
											+ " with respect to the Departure Date in Page "
											+ (index + 1));
						} else {
							logger.info("Result "
									+ (d - 1)
									+ " is not lesser than Result "
									+ d
									+ " with respect to the Departure Date in Page "
									+ (index + 1));
							report.log(
									LogStatus.FAIL,
									"Result "
											+ (d - 1)
											+ " is not lesser than Result "
											+ d
											+ " with respect to the Departure Date in Page "
											+ (index + 1));
						}
					}

					Date finaldate = sdf.parse(dates.get(dates.size() - 1)
							.getText().split("-")[1].trim().substring(
							0,
							dates.get(dates.size() - 1).getText().split("-")[1]
									.trim().length() - 1));
					logger.info("Final Date being added to list to get tested with next page values:"
							+ finaldate);
					datestexts.clear();
					datestexts.add(finaldate);
					index++;
				} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkforFlightAndRoomButtons(String type, String name1,
			String name2) {
		try {
			String firstname = valuesMap.get(name1)
					.substring(valuesMap.get(name1).indexOf("^^") + 2).trim();
			String secondname = valuesMap.get(name2)
					.substring(valuesMap.get(name2).indexOf("^^") + 2).trim();
			logger.info("Names:" + firstname + " and " + secondname);

			if (type.equalsIgnoreCase("Hotel")) {
				if (driver.findElements(
						By.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONS").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).size() == 2) {
					logger.info("Room and Board section is present for both Hotels in Hotel Summary Page");
					report.log(LogStatus.PASS,
							"Room and Board section is present for both Hotels in Hotel Summary Page");
				} else {
					logger.info("Room and Board section is not present for both Hotels in Hotel Summary Page");
					report.log(LogStatus.FAIL,
							"Room and Board section is not present for both Hotels in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (driver
						.findElements(
								By.xpath(getObjectDetails(
										"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONSINCLUDEDSECTION")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
						.size() == 2) {
					logger.info("Included/Selected section under Room and Board section is present for both Hotels in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Included/Selected section under Room and Board section is present for both Hotels in Hotel Summary Page");
				} else {
					logger.info("Included/Selected section under Room and Board section is not present for both Hotels in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Included/Selected section under Room and Board section is not present for both Hotels in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (driver.findElements(
						By.xpath(getObjectDetails(
								"MULTICENTRE_SUMMARYHUB_ROOMIMAGE").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).size() == 2) {
					logger.info("Image in Room and Board section is present in Hotel Summary Page");
					report.log(LogStatus.PASS,
							"Image in Room and Board section is present is present in Hotel Summary Page");
				} else {
					logger.info("Image in Room and Board section is present is not present in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Image in Room and Board section is present is not present in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME1")
						.trim().equalsIgnoreCase(firstname)) {
					logger.info("Hotel Name for Centre 1 in Room and Board section is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Hotel Name for Centre 1 in Room and Board section is correct in Hotel Summary Page");
				} else {
					logger.info("Hotel Name for Centre 1 in Room and Board section is not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Hotel Name for Centre 1 in Room and Board section is not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME2")
						.trim().equalsIgnoreCase(secondname)) {
					logger.info("Hotel Name for Centre 2 in Room and Board section is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Hotel Name for Centre 2 in Room and Board section is correct in Hotel Summary Page");
				} else {
					logger.info("Hotel Name for Centre 2 in Room and Board section is not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Hotel Name for Centre 2 in Room and Board section is not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				String croom1 = valuesMap
						.get("firstroom")
						.substring(valuesMap.get("firstroom").indexOf("^^") + 2)
						.trim();
				if (croom1.length() > 3) {
					croom1 = croom1.replaceAll("[^\\p{L}\\p{Z}]", "");
					croom1 = croom1.substring(3, croom1.length()).trim();
				}
				logger.info("Room1:" + croom1);
				String cboard1 = valuesMap
						.get("firstboard")
						.substring(
								valuesMap.get("firstboard").indexOf("^^") + 2)
						.trim();
				String croom2 = valuesMap
						.get("secondroom")
						.substring(
								valuesMap.get("secondroom").indexOf("^^") + 2)
						.trim();
				if (croom2.length() > 3) {
					croom2 = croom2.replaceAll("[^\\p{L}\\p{Z}]", "");
					croom2 = croom2.substring(3, croom2.length()).trim();
				}
				logger.info("Room2:" + croom2);
				String cboard2 = valuesMap
						.get("secondboard")
						.substring(
								valuesMap.get("secondboard").indexOf("^^") + 2)
						.trim();
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOM1").trim()
						.split("?")[0].trim().equalsIgnoreCase(
						croom1.trim().split("?")[1].trim())) {
					logger.info("Room for Centre 1 in Room and Board section is shown and is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Room for Centre 1 in Room and Board section is shown and is correct in Hotel Summary Page");
				} else {
					logger.info("Room for Centre 1 in Room and Board section is not shown or not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Room for Centre 1 in Room and Board section is not shown or not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOM2").trim()
						.split(multiply)[0].trim().equalsIgnoreCase(
						croom2.trim().split(multiply)[1].trim())) {
					logger.info("Room for Centre 2 in Room and Board section is shown and is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Room for Centre 2 in Room and Board section is shown and is correct in Hotel Summary Page");
				} else {
					logger.info("Room for Centre 2 in Room and Board section is not shown or not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Room for Centre 2 in Room and Board section is not shown or not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMBOARD1")
						.trim().equalsIgnoreCase(cboard1.trim())) {
					logger.info("Board Basis for Centre 1 in Room and Board section is shown in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Board Basis for Centre 1 in Room and Board section is shown in Hotel Summary Page");
				} else {
					logger.info("Board Basis for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Board Basis for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMBOARD2")
						.trim().equalsIgnoreCase(cboard2.trim())) {
					logger.info("Board Basis for Centre 2 in Room and Board section is shown in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Board Basis for Centre 2 in Room and Board section is shown in Hotel Summary Page");
				} else {
					logger.info("Board Basis for Centre 2 in Room and Board section is not shown in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Board Basis for Centre 2 in Room and Board section is not shown in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				logger.info(driver.findElements(
						By.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONS").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).size());
				if (driver.findElements(
						By.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONS").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).size() == 2) {
					logger.info("Room and Board section is present for one Hotel in Hotel Summary Page");
					report.log(LogStatus.PASS,
							"Room and Board section is present for one Hotel in Hotel Summary Page");
				} else {
					logger.info("Room and Board section is not present for one Hotel in Hotel Summary Page");
					report.log(LogStatus.FAIL,
							"Room and Board section is not present for one Hotel in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (driver
						.findElements(
								By.xpath(getObjectDetails(
										"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONSINCLUDEDSECTION")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
						.size() == 2) {
					logger.info("Included/Selected section under Room and Board section is present for one Hotel in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Included/Selected section under Room and Board section is present for one Hotel in Hotel Summary Page");
				} else {
					logger.info("Included/Selected section under Room and Board section is not present for one Hotel in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Included/Selected section under Room and Board section is not present for one Hotel in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (driver.findElements(
						By.xpath(getObjectDetails(
								"MULTICENTRE_SUMMARYHUB_ROOMIMAGE").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).size() == 1) {
					logger.info("Image in Room and Board section is present in Hotel Summary Page");
					report.log(LogStatus.PASS,
							"Image in Room and Board section is present is present in Hotel Summary Page");
				} else {
					logger.info("Image in Room and Board section is present is not present in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Image in Room and Board section is present is not present in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				String croom2 = valuesMap
						.get("secondroom")
						.substring(
								valuesMap.get("secondroom").indexOf("^^") + 2)
						.trim();
				logger.info(croom2);
				if (croom2.length() > 3) {
					croom2 = croom2.replaceAll("[^\\p{L}\\p{Z}]", "");
					logger.info(croom2);
					croom2 = croom2.substring(3, croom2.length()).trim();
				}
				logger.info("Room2:" + croom2);
				String cboard2 = valuesMap
						.get("secondboard")
						.substring(
								valuesMap.get("secondboard").indexOf("^^") + 2)
						.trim();
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME1")
						.trim().equalsIgnoreCase(firstname)) {
					logger.info("Hotel Name for Centre 1 in Room and Board section is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Hotel Name for Centre 1 in Room and Board section is correct in Hotel Summary Page");
				} else {
					logger.info("Hotel Name for Centre 1 in Room and Board section is not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Hotel Name for Centre 1 in Room and Board section is not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME2")
						.trim().equalsIgnoreCase(secondname)) {
					logger.info("Hotel Name for Centre 2 in Room and Board section is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Hotel Name for Centre 2 in Room and Board section is correct in Hotel Summary Page");
				} else {
					logger.info("Hotel Name for Centre 2 in Room and Board section is not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Hotel Name for Centre 2 in Room and Board section is not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}

				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOM1")
						.contains("Room")) {
					logger.info("Room for Centre 1 in Room and Board section is shown in Hotel Summary Page");
					report.log(LogStatus.PASS,
							"Room for Centre 1 in Room and Board section is shown in Hotel Summary Page");
				} else {
					logger.info("Room for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Room for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				String r2 = performActionGetText("MULTICENTRE_SUMMARYHUB_ROOM2")
						.trim();
				logger.info(r2);
				if (r2.length() > 3) {
					r2 = r2.replaceAll("[^\\p{L}\\p{Z}]", "");
					logger.info(r2);
					// r2=r2.substring(0,r2.length()-3).trim();
				}
				logger.info("Room2:" + r2);
				if (r2.trim().equalsIgnoreCase(croom2.trim())) {
					logger.info("Room for Centre 2 in Room and Board section is shown and is correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Room for Centre 2 in Room and Board section is shown and is correct in Hotel Summary Page");
				} else {
					logger.info("Room for Centre 2 in Room and Board section is not shown or not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Room for Centre 2 in Room and Board section is not shown or not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMBOARD1")
						.length() > 1) {
					logger.info("Board Basis for Centre 1 in Room and Board section is shown in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Board Basis for Centre 1 in Room and Board section is shown in Hotel Summary Page");
				} else {
					logger.info("Board Basis for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Board Basis for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMBOARD2")
						.trim().equalsIgnoreCase(cboard2.trim())) {
					logger.info("Board Basis for Centre 2 in Room and Board section is shown and correct in Hotel Summary Page");
					report.log(
							LogStatus.PASS,
							"Board Basis for Centre 2 in Room and Board section is shown and correct in Hotel Summary Page");
				} else {
					logger.info("Board Basis for Centre 2 in Room and Board section is either not shown or not correct in Hotel Summary Page");
					report.log(
							LogStatus.FAIL,
							"Board Basis for Centre 2 in Room and Board section is either not shown or not correct in Hotel Summary Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
			if (IsElementPresent("MULTICENTRE_HOTELSUMMARY_FLIGHTOPTIONBUTTON")) {
				logger.info("Flight Options Button is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Flight Options Button is present in Hotel Summary Page");
			} else {
				logger.info("Flight Options Button is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Flight Options Button is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			if (IsElementPresent("MULTICENTRE_SUMMARYHUB_FLIGHTOPTIONSIMAGE")) {
				logger.info("Flight Options Image is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Flight Options Image is present in Hotel Summary Page");
			} else {
				logger.info("Flight Options Image is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Flight Options Image is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_SUMMARYHUB_FLIGHTOPTIONSLINK")) {
				logger.info("Flight Options Link is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Flight Options Link is present in Hotel Summary Page");
			} else {
				logger.info("Flight Options Link is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Flight Options Link is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			performActionClick("MULTICENTRE_SUMMARYHUB_FLIGHTOPTIONSLINK");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_HEADING")) {
				logger.info("Flight Options Page is opened by clicking on the link given in Flight Options section");
				report.log(
						LogStatus.PASS,
						"Flight Options Page is opened by clicking on the link given in Flight Options section");
			} else {
				logger.info("Flight Options Page is not opened by clicking on the link given in Flight Options section");
				report.log(
						LogStatus.FAIL,
						"Flight Options Page isn not opened by clicking on the link given in Flight Options section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			performActionClick("MULTICENTRE_FLIGHTOPTIONS_BACKSUMMARY");
			ThreadSleep();
			performActionClick("MULTICENTRE_HOTELSUMMARY_FLIGHTOPTIONBUTTON");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_SELECTSEAT")) {
				logger.info("Select Seat section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Select Seat section is available in Flight Options Page");
			} else {
				logger.info("Select Seat section is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Select Seat section is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_SEATWITHSPACE")) {
				logger.info("Seat with Extra space section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Seat with Extra space section is present in Hotel Summary Page");
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_SEATWITHLEGROOM")) {
				logger.info("Seat with Extra legroom section is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Seat with Extra legroom is present in Hotel Summary Page");
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_BAGGAGE")) {
				logger.info("Baggage section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Baggage section is available in Flight Options Page");
			} else {
				logger.info("Baggage section is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Baggage section is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_FLIGHTMEAL")) {
				logger.info("FLight Meal section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"FLight Meal section is available in Flight Options Page");
			} else {
				logger.info("FLight Meal section is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"FLight Meal section is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_BACKSUMMARY")) {
				logger.info("Back To Summary button is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Back To Summary button is available in Flight Options Page");
			} else {
				logger.info("Back To Summary button is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Back To Summary button is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_CONTINUE")) {
				logger.info("Continue button is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Continue button is available in Flight Options Page");
			} else {
				logger.info("Continue button is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Continue button is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			performActionClick("MULTICENTRE_FLIGHTOPTIONS_BACKSUMMARY");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_HEADING")) {
				logger.info("Flight Options Page is opened after clicking back button in Flight Options Page");
				report.log(
						LogStatus.PASS,
						"Flight Options Page is opened after clicking back button in Flight Options Page");
			} else {
				logger.info("Flight Options Page is not opened after clicking back button in Flight Options Page");
				report.log(
						LogStatus.FAIL,
						"Flight Options Page is not opened after clicking back button in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			performActionClick("MULTICENTRE_HOTELSUMMARY_FLIGHTOPTIONBUTTON");
			ThreadSleep();
			performActionClick("MULTICENTRE_FLIGHTOPTIONS_CONTINUE");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_PASSENGERDETAILS_HEADING")) {
				logger.info("Passenger Details Page is opened after clicking Continue button in Flight Options Page");
				report.log(
						LogStatus.PASS,
						"Passenger Details Page is opened after clicking Continue button in Flight Options Page");
			} else {
				logger.info("Passenger Details Page is not opened after clicking Continue button in Flight Options Page");
				report.log(
						LogStatus.FAIL,
						"Passenger Details Page is not opened after clicking Continue button in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForPackageBrk() {
		try {
			String centrename1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_CENTREBLOCKNAME1").trim();
			String centrename2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_CENTREBLOCKNAME2").trim();
			String name1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_PACKAGENAME1").trim();
			String name2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_PACKAGENAME2").trim();
			logger.info("NAmes:" + centrename1 + " and " + centrename2
					+ " and " + name1 + " and " + name2);
			if (centrename1.equalsIgnoreCase(name1)
					& centrename2.equalsIgnoreCase(name2)) {
				logger.info("Locations names are shown  before selecting both hotels");
				report.log(LogStatus.PASS,
						"Locations names are shown  before selecting both hotels");
			} else {
				logger.info("Locations names are not shown  before selecting both hotels");
				report.log(LogStatus.FAIL,
						"Locations names are not shown  before selecting both hotels");
			}
			if (performActionGetText("MULTICENTRE_HOTELSELECTHUB_FROMTEXT")
					.trim().equalsIgnoreCase("From")) {
				logger.info("From text is shown  before selecting both hotels");
				report.log(LogStatus.PASS,
						"From text is shown  before selecting both hotels");
			} else {
				logger.info("From text is not shown  before selecting both hotels");
				report.log(LogStatus.FAIL,
						"From text is not shown  before selecting both hotels");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}

	}

	public void checkForPackageBrk_Tour(String hname, String lname) {
		try {
			String centrename1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_CENTREBLOCKNAME1").trim();
			String centrename2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_CENTREBLOCKNAME2").trim();
			String name1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_HOTELNAME1").trim();
			String name2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME").trim();
			logger.info("NAmes:" + centrename1 + " and " + centrename2
					+ " and " + name1 + " and " + name2);
			valuesMap.put(hname, "MC_HOTELSELECTHUB_HNAME^^" + centrename1);
			valuesMap.put(lname, "MC_HOTELSELECTHUB_LNAME^^" + centrename2);
			if (centrename1.toLowerCase().contains(
					name1.split("-")[0].trim().toLowerCase())
					& centrename2.equalsIgnoreCase(name2)) {
				logger.info("Hotel name is displayed for centre 1 and Location name for centre 2 before selectinhg centre 2 hotel for Tour Stay");
				report.log(
						LogStatus.PASS,
						"Hotel name is displayed for centre 1 and Location name for centre 2 before selectinhg centre 2 hotel for Tour Stay");
			} else {
				logger.info("Either Hotel name is not displayed for centre 1 or Location name for centre 2 before selectinhg centre 2 hotel for Tour Stay");
				report.log(
						LogStatus.FAIL,
						"Either Hotel name is not displayed for centre 1 or Location name for centre 2 before selectinhg centre 2 hotel for Tour Stay");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			logger.info(performActionGetText("MULTICENTRE_HOTELSELECTHUB_FROMTEXT"));
			if (performActionGetText("MULTICENTRE_HOTELSELECTHUB_FROMTEXT")
					.trim().equalsIgnoreCase("From")) {
				logger.info("From text is shown  before selecting both hotels");
				report.log(LogStatus.PASS,
						"From text is shown  before selecting both hotels");
			} else {
				logger.info("From text is not shown  before selecting both hotels");
				report.log(LogStatus.FAIL,
						"From text is not shown  before selecting both hotels");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}

	}

	public void checkForNamesHotelResults(String hname, String lname) {
		try {
			String hotelname = valuesMap.get(hname)
					.substring(valuesMap.get(hname).indexOf("^^") + 2).trim();
			logger.info("Hotel Name:" + hotelname);
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELNAME1").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				for (int d = 0; d < hotels.size(); d++) {
					if (hotelname.toLowerCase().contains(
							hotels.get(d).getText().split("-")[0].trim()
									.toLowerCase())) {
						logger.info("Already selected Centre 1 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Already selected Centre 1 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Already selected Centre 1 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Already selected Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					}
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.equalsIgnoreCase(hotelsnew.get(d).getText().trim())) {
						logger.info("Respective Centre 2 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Centre 2 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Centre 2 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Centre 2 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForValidations(String pric, String name1, String room1,
			String board1, String name2, String room2, String board2) {
		try {
			logger.info("1:" + valuesMap.get(pric));
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			logger.info("2:" + valuesMap.get(name1));
			String hname1 = valuesMap.get(name1)
					.substring(valuesMap.get(name1).indexOf("^^") + 2).trim();
			logger.info("3:" + valuesMap.get(room1));
			String rname1 = valuesMap.get(room1)
					.substring(valuesMap.get(room1).indexOf("^^") + 2).trim();
			logger.info("4:" + valuesMap.get(board1));
			String bname1 = valuesMap.get(board1)
					.substring(valuesMap.get(board1).indexOf("^^") + 2).trim();
			logger.info("5:" + valuesMap.get(name2));
			String hname2 = valuesMap.get(name2)
					.substring(valuesMap.get(name2).indexOf("^^") + 2).trim();
			logger.info("6:" + valuesMap.get(room2));
			String rname2 = valuesMap.get(room2)
					.substring(valuesMap.get(room2).indexOf("^^") + 2).trim();
			logger.info("7:" + valuesMap.get(board2));
			String bname2 = valuesMap.get(board2)
					.substring(valuesMap.get(board2).indexOf("^^") + 2).trim();
			logger.info("Values:" + price + " and " + hname1 + " and " + rname1
					+ " and " + bname1 + " and " + hname2 + " and " + rname2
					+ " and " + bname2);
			logger.info(performActionGetText("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN"));
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN")
							.trim().equalsIgnoreCase(hname1.trim())) {
				logger.info("Centre 1 Hotel Name block is present and name has matched in What's Included Section");
				report.log(
						LogStatus.PASS,
						"Centre 1 Hotel Name block is present and name has matched in What's Included Section");
			} else {
				logger.info("Either Centre 1 Hotel Name block is not present or name has not matched in What's Included Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 1 Hotel Name block is not present or name has not matched in What's Included Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTIONHUB_FIRSTCENTREROOM")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTIONHUB_FIRSTCENTREROOM")
							.trim().equalsIgnoreCase(rname1.trim())) {
				logger.info("Centre 1 Room Name block is present and name has matched in What's Included Section");
				report.log(
						LogStatus.PASS,
						"Centre 1 Room Name block is present and name has matched in What's Included Section");
			} else {
				logger.info("Either Centre 1 Room Name block is not present or name has not matched in What's Included Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 1 Room Name block is not present or name has not matched in What's Included Section");
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTIONHUB_FIRSTCENTREBOARD")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTIONHUB_FIRSTCENTREBOARD")
							.trim().equalsIgnoreCase(bname1.trim())) {
				logger.info("Centre 1 Board Name block is present and name has matched in What's Included Section");
				report.log(
						LogStatus.PASS,
						"Centre 1 Board Name block is present and name has matched in What's Included Section");
			} else {
				logger.info("Either Centre 1 Board Name block is not present or name has not matched in What's Included Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 1 Board Name block is not present or name has not matched in What's Included Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRENAMEPACKGBRKDWN")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_SECONDCENTRENAMEPACKGBRKDWN")
							.trim().equalsIgnoreCase(hname2.trim())) {
				logger.info("Centre 2 Hotel Name block is present and name has matched in What's Included Section");
				report.log(
						LogStatus.PASS,
						"Centre 2 Hotel Name block is present and name has matched in What's Included Section");
			} else {
				logger.info("Either Centre 2 Hotel Name block is not present or name has not matched in What's Included Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 2 Hotel Name block is not present or name has not matched in What's Included Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTIONHUB_SECONDCENTREROOM")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTIONHUB_SECONDCENTREROOM")
							.trim().equalsIgnoreCase(rname2.trim())) {
				logger.info("Centre 2 Room Name block is present and name has matched in What's Included Section");
				report.log(
						LogStatus.PASS,
						"Centre 2 Room Name block is present and name has matched in What's Included Section");
			} else {
				logger.info("Either Centre 2 Room Name block is not present or name has not matched in What's Included Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 2 Room Name block is not present or name has not matched in What's Included Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTIONHUB_SECONDCENTREBOARD")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTIONHUB_SECONDCENTREBOARD")
							.trim().equalsIgnoreCase(bname2.trim())) {
				logger.info("Centre 2 Board Name block is present and name has matched in What's Included Section");
				report.log(
						LogStatus.PASS,
						"Centre 2 Board Name block is present and name has matched in What's Included Section");
			} else {
				logger.info("Either Centre 2 Board Name block is not present or name has not matched in What's Included Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 2 Board Name block is not present or name has not matched in What's Included Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREDETAILSPACKGBRKDWNNAME")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTREDETAILSPACKGBRKDWNNAME")
							.trim().equalsIgnoreCase(hname1.trim())) {
				logger.info("Centre 1 Hotel Name block is present and name has matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.PASS,
						"Centre 1 Hotel Name block is present and name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Either Centre 1 Hotel Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 1 Hotel Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_SELECTHUB_ITINERARYFIRSTROOM")
					& performActionGetText(
							"MULTICENTRE_SELECTHUB_ITINERARYFIRSTROOM").trim()
							.equalsIgnoreCase(rname1.trim())) {
				logger.info("Centre 1 Room Name block is present and name has matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.PASS,
						"Centre 1 Room Name block is present and name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Either Centre 1 Room Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 1 Room Name block is not present or name has not matched in Itinerary&Hotels Section");
			}
			if (IsElementPresent("MULTICENTRE_SELECTHUB_ITINERARYFIRSTBOARD")
					& performActionGetText(
							"MULTICENTRE_SELECTHUB_ITINERARYFIRSTBOARD").trim()
							.equalsIgnoreCase(bname1.trim())) {
				logger.info("Centre 1 Board Name block is present and name has matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.PASS,
						"Centre 1 Board Name block is present and name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Either Centre 1 Board Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 1 Board Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWNNAME")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_SECONDCENTREDETAILSPACKGBRKDWNNAME")
							.trim().equalsIgnoreCase(hname2.trim())) {
				logger.info("Centre 2 Hotel Name block is present and name has matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.PASS,
						"Centre 2 Hotel Name block is present and name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Either Centre 2 Hotel Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 2 Hotel Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_SELECTHUB_ITINERARYSECONDROOM")
					& performActionGetText(
							"MULTICENTRE_SELECTHUB_ITINERARYSECONDROOM").trim()
							.equalsIgnoreCase(rname2.trim())) {
				logger.info("Centre 2 Room Name block is present and name has matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.PASS,
						"Centre 2 Room Name block is present and name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Either Centre 2 Room Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 2 Room Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_SELECTHUB_ITINERARYSECONDBOARD")
					& performActionGetText(
							"MULTICENTRE_SELECTHUB_ITINERARYSECONDBOARD")
							.trim().equalsIgnoreCase(bname2.trim())) {
				logger.info("Centre 2 Board Name block is present and name has matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.PASS,
						"Centre 2 Board Name block is present and name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Either Centre 2 Board Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.log(
						LogStatus.FAIL,
						"Either Centre 2 Board Name block is not present or name has not matched in Itinerary&Hotels Section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE").trim()
							.equalsIgnoreCase(price.trim())) {
				logger.info("Price is present and price has matched after selecting both the hotels");
				report.log(LogStatus.PASS,
						"Price is present and price has matched after selecting both the hotels");
			} else {
				logger.info("Either Price is not present or price has not matched after selecting both the hotels");
				report.log(
						LogStatus.FAIL,
						"Either Price is not present or price has not matched after selecting both the hotels");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (driver.findElement(By.xpath(getObjectDetails(
					"MULTICENTRE_HOTELSELECTHUB_FROMTEXTHIDE").get(
					CONSTANTS_OBJECT_LOCATOR_VALUE))) != null) {
				logger.info("From text is not shown after selecting both the hotels");
				report.log(LogStatus.PASS,
						"From text is not shown after selecting both the hotels");
			} else {
				logger.info("From text is shown after selecting both the hotels");
				report.log(LogStatus.FAIL,
						"From text is shown after selecting both the hotels");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForNamesHotelResults_1(String hname) {
		try {
			String locname = valuesMap.get(hname)
					.substring(valuesMap.get(hname).indexOf("^^") + 2).trim();
			logger.info("Hotel Name:" + locname);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> prices = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("prices  count:" + prices.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					if (hotelsnames.get(d).getText().trim()
							.equalsIgnoreCase(hotels.get(d).getText().trim())) {
						logger.info("Respective Centre 1 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Centre 1 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Centre 1 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (locname.toLowerCase().contains(
							hotelsnew.get(d).getText().split("-")[0].trim()
									.toLowerCase())) {
						logger.info("Location Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Location Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Location Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Location Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (prices.get(d).getText().trim().length() > 0) {
						logger.info("Price is shown for Hotel " + (d + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.PASS, "Price is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Price is not shown for Hotel " + (d + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Price is not shown for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
					} else {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void chekForHotelDetailsValidation(String hotelnam, String locnam,
			String pric, String type) {
		try {
			String locname = valuesMap.get(locnam)
					.substring(valuesMap.get(locnam).indexOf("^^") + 2).trim();
			logger.info("locnam Name:" + locname);
			String hotname = valuesMap.get(hotelnam)
					.substring(valuesMap.get(hotelnam).indexOf("^^") + 2)
					.trim();
			logger.info("hotname Name:" + hotname);
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			String room = valuesMap.get("firstroom")
					.substring(valuesMap.get("firstroom").indexOf("^^") + 2)
					.trim();
			if (room.length() > 3) {
				room = room.replaceAll("[^\\p{L}\\p{Z}]", "");
				room = room.substring(3, room.length()).trim();
			}
			logger.info("Room:" + room);
			String board = valuesMap.get("firstboard")
					.substring(valuesMap.get("firstboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board:" + board);
			logger.info("price:" + price);
			if (type.equalsIgnoreCase("Hotel")) {
				if (hotname
						.trim()
						.toLowerCase()
						.equalsIgnoreCase(
								performActionGetText(
										"MULTICENTRE_HOTELDETAILS_HOTELNAME")
										.trim().toLowerCase())) {
					logger.info("Hotel Name selected for Centre 1 has matched on top of the page");
					report.log(LogStatus.PASS,
							"Hotel Name selected for Centre 1 has matched on top of the page");
				} else {
					logger.info("Hotel Name selected for Centre 1 has not matched on top of the page");
					report.log(LogStatus.FAIL,
							"Hotel Name selected for Centre 1 has not matched on top of the page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				if (hotname
						.trim()
						.toLowerCase()
						.contains(
								performActionGetText(
										"MULTICENTRE_HOTELDETAILS_HOTELNAME")
										.trim().split("-")[0].trim()
										.toLowerCase())) {
					logger.info("Hotel Name selected for Centre 1 has matched on top of the page");
					report.log(LogStatus.PASS,
							"Hotel Name selected for Centre 1 has matched on top of the page");
				} else {
					logger.info("Hotel Name selected for Centre 1 has not matched on top of the page");
					report.log(LogStatus.FAIL,
							"Hotel Name selected for Centre 1 has not matched on top of the page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
			if (price.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE")
							.trim())) {
				logger.info("Price of Hotel selected for Centre 1 has matched");
				report.log(LogStatus.PASS,
						"Price of Hotel selected for Centre 1 has matched");
			} else {
				logger.info("Price of Hotel selected for Centre 1 has not matched in Details Page");
				report.log(LogStatus.FAIL,
						"Price of Hotel selected for Centre 1 has not  matched in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT").trim()
					.equalsIgnoreCase("From")) {
				logger.info("From text is presnet beside price in Details Page");
				report.log(LogStatus.PASS,
						"From text is presnet beside price in Details Page");
			} else {
				logger.info("From text is not presnet beside price in Details Page");
				report.log(LogStatus.FAIL,
						"From text is not presnet beside price in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (type.equalsIgnoreCase("Hotel")) {
				if (hotname
						.trim()
						.toLowerCase()
						.equalsIgnoreCase(
								performActionGetText(
										"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
										.trim().toLowerCase())) {
					logger.info("Hotel Name selected for Centre 1 is shown under Whats's Included section");
					report.log(LogStatus.PASS,
							"Hotel Name selected for Centre 1 is shown under Whats's Included section");
				} else {
					logger.info("Hotel Name selected for Centre 1 is not shown under Whats's Included section");
					report.log(LogStatus.FAIL,
							"Hotel Name selected for Centre 1 is not shown under Whats's Included section");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				if (hotname
						.trim()
						.toLowerCase()
						.contains(
								performActionGetText(
										"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
										.trim().split("-")[0].trim()
										.toLowerCase())) {
					logger.info("Hotel Name selected for Centre 1 is shown under Whats's Included section");
					report.log(LogStatus.PASS,
							"Hotel Name selected for Centre 1 is shown under Whats's Included section");
				} else {
					logger.info("Hotel Name selected for Centre 1 is not shown under Whats's Included section");
					report.log(LogStatus.FAIL,
							"Hotel Name selected for Centre 1 is not shown under Whats's Included section");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
			String r1 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_ROOMTYPE").trim();
			if (r1.length() > 3) {
				r1 = r1.replaceAll("[^\\p{L}\\p{Z}]", "");
				r1 = r1.substring(3, r1.length()).trim();
			}
			logger.info("r1:" + r1);
			String r2 = performActionGetText("MULTICENTRE_HOTELRESULTS_BOARD")
					.trim();
			logger.info("r2:" + r2);
			if (r1.toLowerCase().equalsIgnoreCase(room.trim())
					& (r1.length() > 3 & r2.length() > 3)) {
				logger.info("Room of the Hotel selected for Centre 1 is shown/mathed in Details Page");
				report.log(LogStatus.PASS,
						"Room of the Hotel selected for Centre 1 is shown/mathed in Details Page");
			} else {
				logger.info("Room of the Hotel selected for Centre 1 is not shown/mathed in Details Page");
				report.log(LogStatus.FAIL,
						"Room of the Hotel selected for Centre 1 is not shown/mathed in Details Page");
			}
			if (r2.toLowerCase().equalsIgnoreCase(board.trim())) {
				logger.info("Board Basis of the Hotel selected for Centre 1 is shown/mathed in Details Page");
				report.log(
						LogStatus.PASS,
						"Board Basis of the Hotel selected for Centre 1 is shown/mathed in Details Page");
			} else {
				logger.info("Board Basis of the Hotel selected for Centre 1 is not bshown/mathed in Details Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis of the Hotel selected for Centre 1 is not shown/mathed in Details Page");
			}
			if (locname
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Location Name for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Location Name for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Location Name for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Location Name for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (type.equalsIgnoreCase("Hotel")) {
				if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")) {
					logger.info("Confirm Button is present in Details Page");
					report.log(LogStatus.PASS,
							"Confirm Button is present in Details Page");
				} else {
					logger.info("Confirm Button is not present in Details Page");
					report.log(LogStatus.FAIL,
							"Confirm Button is not present in Details Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				if (!IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")) {
					logger.info("Confirm Button is not present in Details Page");
					report.log(LogStatus.PASS,
							"Confirm Button is not present in Details Page");
				} else {
					logger.info("Confirm Button is present in Details Page");
					report.log(LogStatus.FAIL,
							"Confirm Button is present in Details Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
				if (IsElementPresent("MULTICENTRE_HOTELDETAILS_BACKSUMMARY")) {
					logger.info("Back To Summary Button is present in Details Page");
					report.log(LogStatus.PASS,
							"Back To Summary Button is present in Details Page");
				} else {
					logger.info("Back To Summary Button is not present in Details Page");
					report.log(LogStatus.FAIL,
							"Back To Summary Button is not present in Details Page");
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void chekForHotelDetailsValidation_Tour(String hotelnam,
			String hotenam2, String pric, String type) {
		try {
			String hotelname2 = valuesMap.get(hotenam2)
					.substring(valuesMap.get(hotenam2).indexOf("^^") + 2)
					.trim();
			logger.info("hotenam2 Name:" + hotelname2);
			String hotname = valuesMap.get(hotelnam)
					.substring(valuesMap.get(hotelnam).indexOf("^^") + 2)
					.trim();
			logger.info("hotname Name:" + hotname);
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			logger.info("price:" + price);
			if (hotelname2
					.trim()
					.toLowerCase()
					.contains(
							performActionGetText(
									"MULTICENTRE_HOTELDETAILS_HOTELNAME")
									.trim().split("-")[0].trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 has matched on top of the page");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 2 has matched on top of the page");
			} else {
				logger.info("Hotel Name selected for Centre 2 has not matched on top of the page");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 2 has not matched on top of the page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (price.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE")
							.trim())) {
				logger.info("Price of Hotel selected for Centre 1 has matched");
				report.log(LogStatus.PASS,
						"Price of Hotel selected for Centre 1 has matched");
			} else {
				logger.info("Price of Hotel selected for Centre 1 has not matched in Details Page");
				report.log(LogStatus.FAIL,
						"Price of Hotel selected for Centre 1 has not  matched in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (driver.findElement(By.xpath(getObjectDetails(
					"MULTICENTRE_HOTELSELECTHUB_FROMTEXTHIDE").get(
					CONSTANTS_OBJECT_LOCATOR_VALUE))) != null) {
				logger.info("From text is not present beside price in Details Page");
				report.log(LogStatus.PASS,
						"From text is not present beside price in Details Page");
			} else {
				logger.info("From text is present beside price in Details Page");
				report.log(LogStatus.FAIL,
						"From text is present beside price in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotname
					.trim()
					.toLowerCase()
					.contains(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_HOTELNAME1")
									.trim().split("-")[0].trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 1 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Hotel Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotelname2
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Hotel Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")) {
				logger.info("Confirm Button is present in Details Page");
				report.log(LogStatus.PASS,
						"Confirm Button is present in Details Page");
			} else {
				logger.info("Confirm Button is not present in Details Page");
				report.log(LogStatus.FAIL,
						"Confirm Button is not present in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Tour(String hotelname1, String hotelname2,
			String pric, String type) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			String name1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_HOTELNAME1").trim();
			String name2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN")
					.trim();
			String priceshown = performActionGetText(
					"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE").trim();
			logger.info("NAmes:" + centrename1 + " and " + centrename2
					+ " and " + name1 + " and " + name2 + " and " + price
					+ " and " + priceshown);
			if (centrename1.trim().toLowerCase()
					.contains(name1.split("-")[0].trim().toLowerCase())
					& centrename1.toLowerCase().contains("tour")
					& centrename2.equalsIgnoreCase(name2)) {
				logger.info("Tour Name is shown for Centre 1 and Hotel Name is shown for Centre 2");
				report.log(LogStatus.PASS,
						"Tour Name is shown for Centre 1 and Hotel Name is shown for Centre 2");
			} else {
				logger.info("Either Tour Name is not shown for Centre 1 or Hotel Name is not shown for Centre 2");
				report.log(
						LogStatus.FAIL,
						"Either Tour Name is not shown for Centre 1 or Hotel Name is not shown for Centre 2");
			}
			if (driver.findElement(By.xpath(getObjectDetails(
					"MULTICENTRE_HOTELSELECTHUB_FROMTEXTHIDE").get(
					CONSTANTS_OBJECT_LOCATOR_VALUE))) != null) {
				logger.info("From text is not shown after selecting second centre hotel");
				report.log(LogStatus.PASS,
						"From text is not shown after selecting second centre hotel");
			} else {
				logger.info("From text is shown after selecting second centre hotel");
				report.log(LogStatus.FAIL,
						"From text is shown after selecting second centre hotel");
			}
			if (price.equalsIgnoreCase(priceshown)) {
				logger.info("Price has matched");
				report.log(LogStatus.PASS, "Price has matched");
			} else {
				logger.info("Price has not matched");
				report.log(LogStatus.FAIL, "Price has not matched");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Tour(String hotelname1, String hotelname2) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			logger.info("Names:" + centrename1 + " and " + centrename2);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELNAME1").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					if (centrename1
							.trim()
							.toLowerCase()
							.contains(
									hotels.get(d).getText().trim().split("-")[0]
											.trim().toLowerCase())) {
						logger.info("Respective Centre 1 Tour Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Centre 1 Tour Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Centre 1 Tour Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Centre 1 Tour Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Hotel Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Hotel Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Hotel Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Hotel Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& !fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Hotel(String hotelname1, String hotelname2,
			String pric) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			String name1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME").trim();
			String name2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME").trim();
			String priceshown = performActionGetText(
					"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE").trim();
			String room = valuesMap.get("firstroom")
					.substring(valuesMap.get("firstroom").indexOf("^^") + 2)
					.trim();
			if (room.length() > 3) {
				room = room.replaceAll("[^\\p{L}\\p{Z}]", "");
				room = room.substring(3, room.length()).trim();
			}
			logger.info("Room:" + room);
			String board = valuesMap.get("firstboard")
					.substring(valuesMap.get("firstboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board:" + board);
			logger.info("NAmes:" + centrename1 + " and " + centrename2
					+ " and " + name1 + " and " + name2 + " and " + price
					+ " and " + priceshown);
			if (centrename1.toLowerCase().equalsIgnoreCase(
					name1.toLowerCase().trim())
					& centrename2.toLowerCase().equalsIgnoreCase(
							name2.toLowerCase())) {
				logger.info("Hotel Name is shown for Centre 1 and Location Name is shown for Centre 2");
				report.log(LogStatus.PASS,
						"Hotel Name is shown for Centre 1 and Location Name is shown for Centre 2");
			} else {
				logger.info("Either Hotel Name is not shown for Centre 1 or Location Name is not shown for Centre 2");
				report.log(
						LogStatus.FAIL,
						"Either Hotel Name is not shown for Centre 1 or Location Name is not shown for Centre 2");
			}
			String r1 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_ROOMTYPE").trim();
			if (r1.length() > 3) {
				r1 = r1.replaceAll("[^\\p{L}\\p{Z}]", "");
				r1 = r1.substring(3, r1.length()).trim();
			}
			logger.info("r1:" + r1);
			String r2 = performActionGetText("MULTICENTRE_HOTELRESULTS_BOARD")
					.trim();
			/*
			 * r2=r2.replaceAll("[^\\p{L}\\p{Z}]",""); logger.info("r2:"+r2);
			 */
			if (r1.toLowerCase().equalsIgnoreCase(room.trim())) {
				logger.info("Room of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.PASS,
						"Room of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under What's Included");
			} else {
				logger.info("Room of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.FAIL,
						"Room of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under What's Included");
			}
			if (r2.toLowerCase().equalsIgnoreCase(board.trim())) {
				logger.info("Board Basis of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.PASS,
						"Board Basis of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under What's Included");
			} else {
				logger.info("Board Basis of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.FAIL,
						"Board Basis of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under What's Included");
			}
			logger.info(name1);
			logger.info(performActionGetText("MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN"));
			if (name1
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_FIRSTCENTRENAMEPACKGBRKDWN")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 1 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.PASS,
						"Hotel Name selected for Centre 1 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
			} else {
				logger.info("Hotel Name selected for Centre 1 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.FAIL,
						"Hotel Name selected for Centre 1 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
			}

			r1 = performActionGetText("MULTICENTRE_HOTELRESULTS_ROOMTYPE")
					.trim();
			if (r1.length() > 3) {
				r1 = r1.replaceAll("[^\\p{L}\\p{Z}]", "");
				r1 = r1.substring(3, r1.length()).trim();
			}
			logger.info("r1:" + r1);
			r2 = performActionGetText("MULTICENTRE_HOTELRESULTS_BOARD").trim();
			logger.info("r2:" + r2);
			if (r1.toLowerCase().equalsIgnoreCase(room.trim())) {
				logger.info("Room of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.PASS,
						"Room of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
			} else {
				logger.info("Room of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.FAIL,
						"Room of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
			}
			if (r2.toLowerCase().equalsIgnoreCase(board.trim())) {
				logger.info("Board Basis of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.PASS,
						"Board Basis of the Hotel selected for Centre 1 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
			} else {
				logger.info("Board Basis of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.FAIL,
						"Board Basis of the Hotel selected for Centre 1 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FROMTEXT")) {
				logger.info("From text is shown after selecting first centre hotel");
				report.log(LogStatus.PASS,
						"From text is shown after selecting first centre hotel");
			} else {
				logger.info("From text is not shown after selecting first centre hotel");
				report.log(LogStatus.FAIL,
						"From text is not shown after selecting first centre hotel");
			}
			if (price.equalsIgnoreCase(priceshown)) {
				logger.info("Price has matched");
				report.log(LogStatus.PASS, "Price has matched");
			} else {
				logger.info("Price has not matched");
				report.log(LogStatus.FAIL, "Price has not matched");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Hotel(String hotelname1, String hotelname2) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			logger.info("Names:" + centrename1 + " and " + centrename2);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.contains(
									hotels.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Respective Centre 1 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Centre 1 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Centre 1 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (centrename2
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Location Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"HoLocationtel Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Location Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Location Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
					} else {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Hotel1(String hotelname1) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			logger.info("Names:" + centrename1);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> rooms1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTREROOM")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("rooms1  count:" + rooms1.size());
				List<WebElement> boards1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTREBOARD")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("boards1  count:" + boards1.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> rooms2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTREROOM")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("rooms2  count:" + rooms2.size());
				List<WebElement> boards2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTREBOARD")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("boards2  count:" + boards2.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				String room1 = valuesMap
						.get("firstroom")
						.substring(valuesMap.get("firstroom").indexOf("^^") + 2)
						.trim();
				logger.info("Room1:" + room1);
				String board1 = valuesMap
						.get("firstboard")
						.substring(
								valuesMap.get("firstboard").indexOf("^^") + 2)
						.trim();
				logger.info("Board1:" + board1);
				for (int d = 0; d < hotels.size(); d++) {
					if (centrename1
							.trim()
							.toLowerCase()
							.contains(
									hotels.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Already selected Centre 1 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Already selected Centre 1 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Already selected Centre 1 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Already selected Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (rooms1.get(d).getText().trim()
							.equalsIgnoreCase(room1.trim())
							& room1.length() > 3 & room1.length() > 3) {
						logger.info("Room for Centre 1 already selected is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room for Centre 1 already selected is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room for Centre 1 already selected is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room for Centre 1 already selected is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (boards1.get(d).getText().trim()
							.equalsIgnoreCase(board1.trim())) {
						logger.info("Board basis for Centre 1 already selected is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board basis for Centre 1 already selected is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board basis for Centre 1 already selected is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board basis for Centre 1 already selected is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Respective Hotel Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Hotel Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Hotel Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Hotel Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (rooms2.get(d).getText().length() > 4) {
						logger.info("Room for Centre 2 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room for Centre 2 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room for Centre 2 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room for Centre 2 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (boards2.get(d).getText().split(" ").length > 1) {
						logger.info("Board basis for Centre 2 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board basis for Centre 2 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board basis for Centre 2 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board basis for Centre 2 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& !fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void chekForHotelDetailsValid_Hotel(String hotelnam,
			String hotenam2, String pric) {
		try {
			String hotelname2 = valuesMap.get(hotenam2)
					.substring(valuesMap.get(hotenam2).indexOf("^^") + 2)
					.trim();
			logger.info("hotenam2 Name:" + hotelname2);
			String hotname = valuesMap.get(hotelnam)
					.substring(valuesMap.get(hotelnam).indexOf("^^") + 2)
					.trim();
			logger.info("hotname Name:" + hotname);
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			logger.info("price:" + price);
			String room1 = valuesMap.get("firstroom")
					.substring(valuesMap.get("firstroom").indexOf("^^") + 2)
					.trim();
			logger.info("Room1 Name:" + room1);
			String board1 = valuesMap.get("firstboard")
					.substring(valuesMap.get("firstboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board1 Name:" + board1);
			String room2 = valuesMap.get("secondroom")
					.substring(valuesMap.get("secondroom").indexOf("^^") + 2)
					.trim();
			logger.info("Room2 Name:" + room2);
			String board2 = valuesMap.get("secondboard")
					.substring(valuesMap.get("secondboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board2 Name:" + board2);
			if (hotelname2
					.trim()
					.toLowerCase()
					.contains(
							performActionGetText(
									"MULTICENTRE_HOTELDETAILS_HOTELNAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 has matched on top of the page");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 2 has matched on top of the page");
			} else {
				logger.info("Hotel Name selected for Centre 2 has not matched on top of the page");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 2 has not matched on top of the page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (price.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE")
							.trim())) {
				logger.info("Price of Hotel selected for Centre 2 has matched");
				report.log(LogStatus.PASS,
						"Price of Hotel selected for Centre 2 has matched");
			} else {
				logger.info("Price of Hotel selected for Centre 2 has not matched in Details Page");
				report.log(LogStatus.FAIL,
						"Price of Hotel selected for Centre 2 has not  matched in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (!performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
					.equalsIgnoreCase("From")) {
				logger.info("From text is not present beside price in Details Page");
				report.log(LogStatus.PASS,
						"From text is not present beside price in Details Page");
			} else {
				logger.info("From text is present beside price in Details Page");
				report.log(LogStatus.FAIL,
						"From text is present beside price in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotname
					.trim()
					.toLowerCase()
					.contains(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 1 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Hotel Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_HOTELRESULTS_ROOMTYPE")
					.trim().equalsIgnoreCase(room1.trim())
					& room1.length() > 3
					& performActionGetText("MULTICENTRE_HOTELRESULTS_ROOMTYPE")
							.trim().length() > 3) {
				logger.info("Room Name selected for Centre 1 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Room Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Room Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Room Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_HOTELRESULTS_BOARD").trim()
					.equalsIgnoreCase(board1.trim())) {
				logger.info("Board Basis Name selected for Centre 1 is shown under Whats's Included section");
				report.log(
						LogStatus.PASS,
						"Board Basis Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Board Basis Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(
						LogStatus.FAIL,
						"Board Basis Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotelname2
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Hotel Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (room2
					.trim()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
									.trim())
					& room2.length() > 3
					& performActionGetText(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
							.trim().length() > 3) {
				logger.info("Room Name selected for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Room Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Room Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Room Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			logger.info(board2);
			logger.info(performActionGetText("MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST"));
			if (board2.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST")
							.trim())) {
				logger.info("Board Basis Name selected for Centre 2 is shown under Whats's Included section");
				report.log(
						LogStatus.PASS,
						"Board Basis Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Board Basis Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(
						LogStatus.FAIL,
						"Board Basis Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")) {
				logger.info("Confirm Button is present in Details Page");
				report.log(LogStatus.PASS,
						"Confirm Button is present in Details Page");
			} else {
				logger.info("Confirm Button is not present in Details Page");
				report.log(LogStatus.FAIL,
						"Confirm Button is not present in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForNamesHotelResults_2(String hname) {
		try {
			String locname = valuesMap.get(hname)
					.substring(valuesMap.get(hname).indexOf("^^") + 2).trim();
			logger.info("Hotel Name:" + locname);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					logger.info(locname.trim().toLowerCase());
					logger.info(hotels.get(d).getText().trim().toLowerCase());
					if (locname
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotels.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Location Name for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Location Name for Centre 1 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Location Name for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Location Name for Centre 1 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.contains(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Respective Hotel Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Hotel Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Hotel Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Hotel Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
					} else {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void chekForHotelDetailsValid_2(String locnam, String hotelnam,
			String pric) {
		try {
			String locname = valuesMap.get(locnam)
					.substring(valuesMap.get(locnam).indexOf("^^") + 2).trim();
			logger.info("locnam Name:" + locname);
			String hotname = valuesMap.get(hotelnam)
					.substring(valuesMap.get(hotelnam).indexOf("^^") + 2)
					.trim();
			logger.info("hotname Name:" + hotname);
			String room = valuesMap.get("secondroom")
					.substring(valuesMap.get("secondroom").indexOf("^^") + 2)
					.trim();
			if (room.length() > 3) {
				room = room.replaceAll("[^\\p{L}\\p{Z}]", "");
				room = room.substring(3, room.length()).trim();
			}
			logger.info("Room:" + room);
			String board = valuesMap.get("secondboard")
					.substring(valuesMap.get("secondboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board:" + board);
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			logger.info(performActionGetText(
					"MULTICENTRE_HOTELDETAILS_HOTELNAME").trim().toLowerCase());
			if (hotname
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELDETAILS_HOTELNAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 has matched on top of the page");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 2 has matched on top of the page");
			} else {
				logger.info("Hotel Name selected for Centre 2 has not matched on top of the page");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 2 has not matched on top of the page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (price.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE")
							.trim())) {
				logger.info("Price of Hotel selected for Centre 2 has matched");
				report.log(LogStatus.PASS,
						"Price of Hotel selected for Centre 2 has matched");
			} else {
				logger.info("Price of Hotel selected for Centre 2 has not matched in Details Page");
				report.log(LogStatus.FAIL,
						"Price of Hotel selected for Centre 2 has not  matched in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT").trim()
					.equalsIgnoreCase("From")) {
				logger.info("From text is present beside price in Details Page");
				report.log(LogStatus.PASS,
						"From text is present beside price in Details Page");
			} else {
				logger.info("From text is not present beside price in Details Page");
				report.log(LogStatus.FAIL,
						"From text is not present beside price in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			logger.info(locname.trim().toLowerCase());
			logger.info(performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME").trim()
					.toLowerCase());
			if (locname
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Location Name selected for Centre 1 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Location Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Location Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(
						LogStatus.FAIL,
						"Location Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotname
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Selected Hotel Name for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Selected Hotel Name for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Selected Hotel Name for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Selected Hotel Name for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			String r1 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
					.trim();
			if (r1.length() > 3) {
				r1 = r1.replaceAll("[^\\p{L}\\p{Z}]", "");
				r1 = r1.substring(3, r1.length()).trim();
			}
			logger.info("r1:" + r1);
			String r2 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST").trim();
			logger.info("r2:" + r2);
			if (r1.toLowerCase().equalsIgnoreCase(room.trim())
					& r1.length() > 3 & room.length() > 3) {
				logger.info("Room of the Hotel selected for Centre 2 is shown/mathed in Details Page");
				report.log(LogStatus.PASS,
						"Room of the Hotel selected for Centre 2 is shown/mathed in Details Page");
			} else {
				logger.info("Room of the Hotel selected for Centre 2 is not shown/mathed in Details Page");
				report.log(LogStatus.FAIL,
						"Room of the Hotel selected for Centre 2 is not shown/mathed in Details Page");
			}
			if (r2.toLowerCase().equalsIgnoreCase(board.trim())) {
				logger.info("Board Basis of the Hotel selected for Centre 2 is shown/mathed in Details Page");
				report.log(
						LogStatus.PASS,
						"Board Basis of the Hotel selected for Centre 2 is shown/mathed in Details Page");
			} else {
				logger.info("Board Basis of the Hotel selected for Centre 2 is not shown/mathed in Details Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis of the Hotel selected for Centre 2 is not shown/mathed in Details Page");
			}
			if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")) {
				logger.info("Confirm Button is present in Details Page");
				report.log(LogStatus.PASS,
						"Confirm Button is present in Details Page");
			} else {
				logger.info("Confirm Button is not present in Details Page");
				report.log(LogStatus.FAIL,
						"Confirm Button is not present in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Hotel1(String hotelname1, String hotelname2,
			String pric) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			String name1 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME").trim();
			String name2 = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME").trim();
			String priceshown = performActionGetText(
					"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE").trim();
			String room = valuesMap.get("secondroom")
					.substring(valuesMap.get("secondroom").indexOf("^^") + 2)
					.trim();
			if (room.length() > 3) {
				room = room.replaceAll("[^\\p{L}\\p{Z}]", "");
				room = room.substring(3, room.length()).trim();
			}
			logger.info("Room:" + room);
			String board = valuesMap.get("secondboard")
					.substring(valuesMap.get("secondboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board:" + board);
			logger.info("NAmes:" + centrename1 + " and " + centrename2
					+ " and " + name1 + " and " + name2 + " and " + price
					+ " and " + priceshown);
			if (centrename1.toLowerCase().equalsIgnoreCase(
					name1.toLowerCase().trim())
					& centrename2.toLowerCase().equalsIgnoreCase(
							name2.toLowerCase())) {
				logger.info("Location Name is shown for Centre 1 and Hotel Name is shown for Centre 2");
				report.log(LogStatus.PASS,
						"Location Name is shown for Centre 1 and Hotel Name is shown for Centre 2");
			} else {
				logger.info("Either Location Name is not shown for Centre 1 or Hotel Name is not shown for Centre 2");
				report.log(
						LogStatus.FAIL,
						"Either Location Name is not shown for Centre 1 or Hotel Name is not shown for Centre 2");
			}
			String r1 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
					.trim();
			if (r1.length() > 3) {
				r1 = r1.replaceAll("[^\\p{L}\\p{Z}]", "");
				r1 = r1.substring(3, r1.length()).trim();
			}
			logger.info("r1:" + r1);
			String r2 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST").trim();
			logger.info("r2:" + r2);
			logger.info(r1.toLowerCase());
			if (r1.toLowerCase().equalsIgnoreCase(room.trim())
					& r1.length() > 3 & room.length() > 3) {
				logger.info("Room of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.PASS,
						"Room of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under What's Included");
			} else {
				logger.info("Room of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.FAIL,
						"Room of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under What's Included");
			}
			if (r2.toLowerCase().equalsIgnoreCase(board.trim())) {
				logger.info("Board Basis of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.PASS,
						"Board Basis of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under What's Included");
			} else {
				logger.info("Board Basis of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under What's Included");
				report.log(
						LogStatus.FAIL,
						"Board Basis of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under What's Included");
			}

			if (name2
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_SECONDCENTRENAMEPACKGBRKDWN")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.PASS,
						"Hotel Name selected for Centre 2 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
			} else {
				logger.info("Hotel Name selected for Centre 2 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.FAIL,
						"Hotel Name selected for Centre 2 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
			}
			r1 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
					.trim();
			if (r1.length() > 3) {
				r1 = r1.replaceAll("[^\\p{L}\\p{Z}]", "");
				r1 = r1.substring(3, r1.length()).trim();
			}
			logger.info("r1:" + r1);
			r2 = performActionGetText(
					"MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST").trim();
			logger.info("r2:" + r2);
			if (r1.toLowerCase().equalsIgnoreCase(room.trim())
					& r1.length() > 3 & room.length() > 3) {
				logger.info("Room of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.PASS,
						"Room of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
			} else {
				logger.info("Room of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.FAIL,
						"Room of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
			}
			if (r2.toLowerCase().equalsIgnoreCase(board.trim())) {
				logger.info("Board Basis of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.PASS,
						"Board Basis of the Hotel selected for Centre 2 is shown/mathed in Selection Hub Page under Itinerary&Hotels");
			} else {
				logger.info("Board Basis of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
				report.log(
						LogStatus.FAIL,
						"Board Basis of the Hotel selected for Centre 2 is not shown/mathed in Selection Hub Page under Itinerary&Hotels");
			}
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FROMTEXT")) {
				logger.info("From text is shown after selecting second centre hotel");
				report.log(LogStatus.PASS,
						"From text is shown after selecting second centre hotel");
			} else {
				logger.info("From text is not shown after selecting second centre hotel");
				report.log(LogStatus.FAIL,
						"From text is not shown after selecting second centre hotel");
			}
			if (price.equalsIgnoreCase(priceshown)) {
				logger.info("Price has matched");
				report.log(LogStatus.PASS, "Price has matched");
			} else {
				logger.info("Price has not matched");
				report.log(LogStatus.FAIL, "Price has not matched");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Hotel1(String hotelname1, String hotelname2) {
		try {
			String centrename1 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			logger.info("Names:" + centrename1 + " and " + centrename2);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					if (centrename1
							.trim()
							.toLowerCase()
							.contains(
									hotels.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Location Name for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Location Name for Centre 1  is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Location Name for Centre 1  is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Location Name for Centre 1 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Respective Centre 2 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Centre 2 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Centre 2 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Centre 2 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
					} else {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForButton() {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELDETAILS_BACKOVERVIEW")) {
				logger.info("Back to Overview Button is present in Details Page");
				report.log(LogStatus.PASS,
						"Back to Overview Button is present in Details Page");
			} else {
				logger.info("Back to Overview Button is not present in Details Page");
				report.log(LogStatus.FAIL,
						"Back to Overview Button is not present in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForHub_Hotel2(String hotelname2) {
		try {
			String centrename2 = valuesMap.get(hotelname2)
					.substring(valuesMap.get(hotelname2).indexOf("^^") + 2)
					.trim();
			logger.info("Names:" + centrename2);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> rooms1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTREROOM")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("rooms1  count:" + rooms1.size());
				List<WebElement> boards1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTREBOARD")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("boards1  count:" + boards1.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> rooms2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTREROOM")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("rooms2  count:" + rooms2.size());
				List<WebElement> boards2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTREBOARD")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("boards2  count:" + boards2.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				String room2 = valuesMap
						.get("secondroom")
						.substring(
								valuesMap.get("secondroom").indexOf("^^") + 2)
						.trim();
				logger.info("Room2:" + room2);
				String board2 = valuesMap
						.get("secondboard")
						.substring(
								valuesMap.get("secondboard").indexOf("^^") + 2)
						.trim();
				logger.info("Board2:" + board2);
				for (int d = 0; d < hotels.size(); d++) {
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.contains(
									hotels.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Respective Hotel Name is shown for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Hotel Name is shown for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Hotel Name for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Hotel Name for Centre 1 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (rooms1.get(d).getText().length() > 4) {
						logger.info("Room for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room for Centre 1 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room for Centre 1 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (boards1.get(d).getText().split(" ").length > 1) {
						logger.info("Board basis for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board basis for Centre 1 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board basis for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board basis for Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (centrename2
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Already Selected Hotel Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Already Selected Hotel Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Already Selected Hotel Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Already Selected Hotel Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (rooms2.get(d).getText().trim()
							.equalsIgnoreCase(room2.trim())
							& rooms2.get(d).getText().trim().length() > 3
							& room2.trim().length() > 3) {
						logger.info("Room for Centre 2 already selected is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room for Centre 2 already selected is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room for Centre 2 already selected is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room for Centre 2 already selected is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (boards2.get(d).getText().trim()
							.equalsIgnoreCase(board2.trim())) {
						logger.info("Board basis for Centre 2 already selected is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board basis for Centre 2 already selected is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board basis for Centre 2 already selected is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board basis for Centre 2 already selected is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& !fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void chekForHotelDetailsValid_Hotel1(String hotelnam,
			String hotenam2, String pric) {
		try {
			String hotelname2 = valuesMap.get(hotenam2)
					.substring(valuesMap.get(hotenam2).indexOf("^^") + 2)
					.trim();
			logger.info("hotenam2 Name:" + hotelname2);
			String hotname = valuesMap.get(hotelnam)
					.substring(valuesMap.get(hotelnam).indexOf("^^") + 2)
					.trim();
			logger.info("hotname Name:" + hotname);
			String price = valuesMap.get(pric)
					.substring(valuesMap.get(pric).indexOf("^^") + 2).trim();
			logger.info("price:" + price);
			String room1 = valuesMap.get("firstroom")
					.substring(valuesMap.get("firstroom").indexOf("^^") + 2)
					.trim();
			logger.info("Room1 Name:" + room1);
			String board1 = valuesMap.get("firstboard")
					.substring(valuesMap.get("firstboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board1 Name:" + board1);
			String room2 = valuesMap.get("secondroom")
					.substring(valuesMap.get("secondroom").indexOf("^^") + 2)
					.trim();
			logger.info("Room2 Name:" + room2);
			String board2 = valuesMap.get("secondboard")
					.substring(valuesMap.get("secondboard").indexOf("^^") + 2)
					.trim();
			logger.info("Board2 Name:" + board2);
			if (hotname
					.trim()
					.toLowerCase()
					.contains(
							performActionGetText(
									"MULTICENTRE_HOTELDETAILS_HOTELNAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 1 has matched on top of the page");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 1 has matched on top of the page");
			} else {
				logger.info("Hotel Name selected for Centre 1 has not matched on top of the page");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 1 has not matched on top of the page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			logger.info(price);
			logger.info(performActionGetText(
					"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE").trim());
			if (price.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELDETAILS_SELECTEDHOTELPRICE")
							.trim())) {
				logger.info("Price of Hotel selected both hotels has matched");
				report.log(LogStatus.PASS,
						"Price of Hotel selected both hotels has matched");
			} else {
				logger.info("Price of Hotel selected for both hotels has not matched in Details Page");
				report.log(LogStatus.FAIL,
						"Price of Hotel selected for both hotels has not matched in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (driver.findElement(By.xpath(getObjectDetails(
					"MULTICENTRE_HOTELSELECTHUB_FROMTEXTHIDE").get(
					CONSTANTS_OBJECT_LOCATOR_VALUE))) != null) {
				logger.info("From text is not present beside price in Details Page");
				report.log(LogStatus.PASS,
						"From text is not present beside price in Details Page");
			} else {
				logger.info("From text is present beside price in Details Page");
				report.log(LogStatus.FAIL,
						"From text is present beside price in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotname
					.trim()
					.toLowerCase()
					.contains(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 1 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Hotel Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_HOTELRESULTS_ROOMTYPE")
					.trim().equalsIgnoreCase(room1.trim())
					& performActionGetText("MULTICENTRE_HOTELRESULTS_ROOMTYPE")
							.trim().length() > 3 & room1.trim().length() > 3) {
				logger.info("Room Name selected for Centre 1 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Room Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Room Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Room Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_HOTELRESULTS_BOARD").trim()
					.equalsIgnoreCase(board1.trim())) {
				logger.info("Board Basis Name selected for Centre 1 is shown under Whats's Included section");
				report.log(
						LogStatus.PASS,
						"Board Basis Name selected for Centre 1 is shown under Whats's Included section");
			} else {
				logger.info("Board Basis Name selected for Centre 1 is not shown under Whats's Included section");
				report.log(
						LogStatus.FAIL,
						"Board Basis Name selected for Centre 1 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (hotelname2
					.trim()
					.toLowerCase()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
									.trim().toLowerCase())) {
				logger.info("Hotel Name selected for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Hotel Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Hotel Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Hotel Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (room2
					.trim()
					.equalsIgnoreCase(
							performActionGetText(
									"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
									.trim())
					& room2.trim().length() > 3
					& performActionGetText(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST")
							.trim().length() > 3) {
				logger.info("Room Name selected for Centre 2 is shown under Whats's Included section");
				report.log(LogStatus.PASS,
						"Room Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Room Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(LogStatus.FAIL,
						"Room Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (board2.trim().equalsIgnoreCase(
					performActionGetText(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST")
							.trim())) {
				logger.info("Board Basis Name selected for Centre 2 is shown under Whats's Included section");
				report.log(
						LogStatus.PASS,
						"Board Basis Name selected for Centre 2 is shown under Whats's Included section");
			} else {
				logger.info("Board Basis Name selected for Centre 2 is not shown under Whats's Included section");
				report.log(
						LogStatus.FAIL,
						"Board Basis Name selected for Centre 2 is not shown under Whats's Included section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_HOTELDETAILS_CONFIRMBUT")) {
				logger.info("Confirm Button is present in Details Page");
				report.log(LogStatus.PASS,
						"Confirm Button is present in Details Page");
			} else {
				logger.info("Confirm Button is not present in Details Page");
				report.log(LogStatus.FAIL,
						"Confirm Button is not present in Details Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectRequiredSortOption(String option) {
		try {
			List<WebElement> sorts = driver.findElements(By
					.xpath(getObjectDetails(
							"MULTICENTRE_HOTELSELECTHUB_SORTOPTIONLIST").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE)));
			logger.info("Count:" + sorts.size());
			for (WebElement each : sorts) {
				logger.info("Inside sort");
				logger.info(each.getAttribute("analytics-text") + " and "
						+ option);
				if (each.getAttribute("analytics-text").trim().toLowerCase()
						.contains(option.trim().toLowerCase())) {
					logger.info("Found sort option");
					each.click();
					break;
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkResults(String selectFromAir, String selectDate) {
		try {
			int index = 0;
			String[] selectedairports = (valuesMap.get(selectFromAir)
					.substring(valuesMap.get(selectFromAir).indexOf("^^") + 2)
					.trim()).split(",");
			for (int i = 0; i < selectedairports.length; i++) {
				logger.info("1:" + selectedairports[i]);
				report.log(LogStatus.INFO, "Selected Dep Airport:"
						+ selectedairports[i]);
			}
			String selecteddate = valuesMap.get(selectDate)
					.substring(valuesMap.get(selectDate).indexOf("^^") + 2)
					.trim();
			logger.info("2:" + selecteddate);
			// logger.info(selecteddate.split("'")[1]);
			// selecteddate=selecteddate.split("'")[1].split("'")[0].trim();
			logger.info("2:" + selecteddate);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Date selecteddt = sdf.parse(selecteddate);
			Date beforedt = new Date();
			Date afterdt = new Date();
			/*
			 * //performActionClick("MC_LANDING_CALENDER"); ThreadSleep();
			 */
			boolean flag = false;
			logger.info(performActionGetText("MULTICENTRE_SEARCH_FLEXIBLEDATECHECKBOX"));
			if (performActionGetText("MULTICENTRE_SEARCH_FLEXIBLEDATECHECKBOX")
					.contains("flexible")) {
				logger.info("Flexible date enabled");
				flag = true;
				Calendar cal = Calendar.getInstance();
				cal.setTime(selecteddt);
				cal.add(Calendar.DATE, 3);
				afterdt = sdf.parse(sdf.format(cal.getTime()));
				cal.setTime(selecteddt);
				cal.add(Calendar.DATE, -3);
				beforedt = sdf.parse(sdf.format(cal.getTime()));
				logger.info("Dates:" + beforedt + " and " + afterdt);
			}
			do {
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,-250)", "");
				if (index != 0) {
					logger.info("1111");
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				List<String> depaiports = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_RESULTSDEPAIRPORTS");
				List<String> depdates = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_RESULTSDEPDATE");
				List<String> flightdetails = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_FLIGHTDETAILSLINK");
				List<String> prices = performActionMultipleGetText("MULTICENTRE_SEARCHRESULTS_ALLPRICES");
				logger.info("Counts:" + depaiports.size() + " and "
						+ depdates.size() + " and " + flightdetails.size()
						+ " and " + prices.size());

				for (int i = 0; i < depaiports.size(); i++) {
					int flag1 = 0;
					for (int j = 0; j < selectedairports.length; j++) {
						if (depaiports.get(i).split("\n")[0]
								.trim()
								.toLowerCase()
								.contains(
										selectedairports[j].trim()
												.toLowerCase())) {
							flag1 = 1;
							break;
						}
					}
					if (flag1 == 1) {
						logger.info("Departure Airport of Hotel "
								+ (i + 1)
								+ " has matched acording to selected dep aiports list in Page "
								+ (index + 1));
						report.log(
								LogStatus.PASS,
								"Departure Airport of Hotel "
										+ (i + 1)
										+ " has matched acording to selected dep aiports list in Page "
										+ (index + 1));
					} else {
						logger.info("Departure Airport of Hotel "
								+ (i + 1)
								+ " has not matched acording to selected dep aiports list in Page "
								+ (index + 1) + " . This airport is: "
								+ depaiports.get(i).split("\n")[0]);
						report.log(
								LogStatus.FAIL,
								"Departure Airport of Hotel "
										+ (i + 1)
										+ " has not matched acording to selected dep aiports list in Page "
										+ (index + 1) + " . This airport is: "
										+ depaiports.get(i).split("\n")[0]);
					}
					if (depaiports.get(i).split("\n")[0].contains("Direct")) {
						logger.info("Departure Airport of Hotel " + (i + 1)
								+ " is showing \"Direct\" keyword in Page "
								+ (index + 1));
						report.log(
								LogStatus.PASS,
								"Departure Airport of Hotel "
										+ (i + 1)
										+ " is showing \"Direct\" keyword in Page "
										+ (index + 1));
					} else {
						logger.info("Departure Airport of Hotel " + (i + 1)
								+ " is not showing \"Direct\" keyword in Page "
								+ (index + 1) + " . Text captured is: "
								+ depaiports.get(i).split("\n")[0]);
						report.log(
								LogStatus.FAIL,
								"Departure Airport of Hotel "
										+ (i + 1)
										+ " is not showing \"Direct\" keyword in Page "
										+ (index + 1) + " . Text captured is: "
										+ depaiports.get(i).split("\n")[0]);
					}
					SimpleDateFormat sdfnew = new SimpleDateFormat(
							"EEE dd MMM yyyy");
					String formatteddate = depdates.get(i).split("-")[1].trim();
					formatteddate = formatteddate.substring(0,
							formatteddate.length() - 1).trim();
					Calendar cal = Calendar.getInstance();
					cal.setTime(sdfnew.parse(formatteddate));
					Date formatdate = cal.getTime();
					logger.info("Final date string:" + formatdate);
					logger.info(beforedt.before(formatdate));
					logger.info(beforedt.equals(formatdate));
					logger.info(afterdt.after(formatdate));
					logger.info(afterdt.equals(formatdate));
					if (flag == true) {
						if ((beforedt.before(formatdate) | beforedt
								.equals(formatdate))
								& (afterdt.after(formatdate) | afterdt
										.equals(formatdate))) {
							logger.info("Displayed Date for Hotel "
									+ (i + 1)
									+ " has matched according to selected dep date in Page "
									+ (index + 1));
							report.log(
									LogStatus.PASS,
									"Displayed Date for Hotel "
											+ (i + 1)
											+ " has matched according to selected dep date in Page "
											+ (index + 1));
						} else {
							logger.info("Displayed Date for Hotel "
									+ (i + 1)
									+ " has not matched according to selected dep date in Page "
									+ (index + 1) + " . Date captured is: "
									+ formatdate);
							report.log(
									LogStatus.FAIL,
									"Displayed Date for Hotel "
											+ (i + 1)
											+ " has not matched according to selected dep date in Page "
											+ (index + 1)
											+ " . Date captured is: "
											+ formatdate);
						}
					} else {
						if (sdfnew.parse(formatteddate).equals(selecteddt)) {
							logger.info("Displayed Date for Hotel "
									+ (i + 1)
									+ " has matched according to selected dep date in Page "
									+ (index + 1));
							report.log(
									LogStatus.PASS,
									"Displayed Date for Hotel "
											+ (i + 1)
											+ " has matched according to selected dep date in Page "
											+ (index + 1));
						} else {
							logger.info("Displayed Date for Hotel "
									+ (i + 1)
									+ " has not matched according to selected dep date in Page "
									+ (index + 1) + " . Date captured is: "
									+ formatteddate);
							report.log(
									LogStatus.FAIL,
									"Displayed Date for Hotel "
											+ (i + 1)
											+ " has not matched according to selected dep date in Page "
											+ (index + 1)
											+ " . Date captured is: "
											+ formatteddate);
						}
					}
					if (flightdetails.get(i).trim()
							.equalsIgnoreCase("Flight details")) {
						logger.info("Displayed Flight Details Link for Hotel "
								+ (i + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Displayed Flight Details Link for Hotel "
										+ (i + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Not Displayed Flight Details Link for Hotel "
								+ (i + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Not Displayed Flight Details Link for Hotel "
										+ (i + 1) + " in Page " + (index + 1));
					}
					if (prices.get(i).trim().length() > 1) {
						logger.info("Displayed Price for Hotel " + (i + 1)
								+ " in Page " + (index + 1)
								+ " . Price displayed:" + prices.get(i));
						report.log(LogStatus.PASS, "Displayed Price for Hotel "
								+ (i + 1) + " in Page " + (index + 1)
								+ " . Price displayed:" + prices.get(i));
					} else {
						logger.info("Not Displayed Price for Hotel " + (i + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Not Displayed Price for Hotel " + (i + 1)
										+ " in Page " + (index + 1));
					}
				}
				index++;
				// jse.executeScript("window.scrollBy(0,750)", "");
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectCountries(String element, String count) {
		try {
			ThreadSleep();
			List<WebElement> checks = driver.findElements(By
					.xpath(getObjectDetails(element).get(
							CONSTANTS_OBJECT_LOCATOR_VALUE)));
			logger.info("checks:" + checks.size());
			for (int i = 0; i < Integer.parseInt(count); i++) {
				logger.info("inside");
				checks.get(i).click();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getCause());
		}
	}

	public void selectDate(String dat, String key) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
			Date parse = sdf.parse(dat);
			Calendar c = Calendar.getInstance();
			c.setTime(parse);
			logger.info((c.get(Calendar.MONTH) + 1) + " and "
					+ c.get(Calendar.DATE) + " and " + c.get(Calendar.YEAR));
			int date = c.get(Calendar.DATE);
			int month = c.get(Calendar.MONTH) + 1;
			int year = c.get(Calendar.YEAR);
			String mon = new String();
			switch (month) {
			case 1:
				mon = "January";
				break;
			case 2:
				mon = "February";
				break;
			case 3:
				mon = "March";
				break;
			case 4:
				mon = "April";
				break;
			case 5:
				mon = "May";
				break;
			case 6:
				mon = "June";
				break;
			case 7:
				mon = "July";
				break;
			case 8:
				mon = "August";
				break;
			case 9:
				mon = "September";
				break;
			case 10:
				mon = "October";
				break;
			case 11:
				mon = "November";
				break;
			case 12:
				mon = "December";
				break;
			default:
				logger.info("No month match Found");
			}
			logger.info("Month:" + mon);
			boolean flag = true, flag2 = false;
			;
			do {
				String text = performActionGetText(
						"MULTICENTRE_SEARCH_DISPLAYEDMONTHYEAR").trim();
				logger.info("Text:" + text);
				if (text.toLowerCase().contains(mon.toLowerCase())
						& text.contains(String.valueOf(year))) {
					flag = false;
				} else {
					if (IsElementPresent("MULTICENTRE_SEARCH_DATENAVIGATOR")) {
						performActionClick("MULTICENTRE_SEARCH_DATENAVIGATOR");
						ThreadSleep();
					} else {
						flag2 = true;
					}
				}
			} while (flag & flag2 == false);
			ThreadSleep();
			boolean flag1 = false;
			if (flag == false) {
				logger.info("Given month&year is found");
				List<WebElement> dates = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCH_AVAILABLEDATES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Dates count:" + dates.size());
				for (WebElement each : dates) {
					if (each.getText().trim()
							.equalsIgnoreCase(String.valueOf(date))) {
						logger.info("Given date is found");
						flag1 = true;
						each.click();
						break;
					}
				}
				if (flag1) {
					logger.info("Given date is found");
					valuesMap.put(key, "MULTICENTRE_SEARCHPANEL_DATE^^" + dat);
					logger.info(valuesMap.get(key));
				} else {
					logger.info("Given date is not found");
					report.log(LogStatus.ERROR, "Given date is not found");
					Assert.assertTrue(false, "Given date is not found");
				}
			} else {
				logger.info("Given month&year is not found");
				report.log(LogStatus.ERROR, "Given month&year is not found");
				Assert.assertTrue(false, "Given month&year is not found");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectAges(String agesString) {
		try {
			int child = Integer.parseInt(performActionGetText(
					"MULTICENTRE_SEARCH_SELECTEDCHILDNUMBER").trim());
			logger.info("Number:" + child);
			if (child != 0) {
				logger.info("String:" + agesString);
				String[] ageArray = agesString.split(",");
				logger.info(ageArray[0] + " and " + ageArray[1]);
				List<WebElement> ages = driver.findElements(By
						.xpath(getObjectDetails("MULTICENTRE_SEARCH_CHILDAGES")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Ages count:" + ages.size());
				int i = 0;
				for (WebElement each : ages) {
					each.sendKeys(ageArray[i]);
					i++;
				}
				performActionClick("MULTICENTRE_SEARCH_AGESDONEBUTTON");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void returnInitialPage() {
		try {
			while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_PREVBUTTON")) {
				performActionClick("MULTICENTRE_SEARCHRESULTS_PREVBUTTON");
				ThreadSleep();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForPackBrkValues(String fdur, String sdur, String loc,
			String loc1, String loc2) {
		try {
			String dur1 = valuesMap.get(fdur)
					.substring(valuesMap.get(fdur).indexOf("^^") + 2).trim();
			String dur2 = valuesMap.get(sdur)
					.substring(valuesMap.get(sdur).indexOf("^^") + 2).trim();
			String location = valuesMap.get(loc)
					.substring(valuesMap.get(loc).indexOf("^^") + 2).trim();
			logger.info("Durations:" + dur1 + " and " + dur2 + " and "
					+ location);
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_MINIMUMPRICE")) {
				logger.info("Per Person is shown in Package breakdown");
				report.log(LogStatus.PASS,
						"Per Person is shown in Package breakdown");
			} else {
				logger.info("Per Person is not shown in Package breakdown");
				report.log(LogStatus.FAIL,
						"Per Person is not shown in Package breakdown");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			String heading = performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_HOTELHEADING").trim();
			logger.info(heading);
			String[] centrenames = heading.split("&");
			logger.info(centrenames[0]);
			logger.info(centrenames[1]);
			String txt = "";
			if (centrenames[0].trim().contains("(")) {
				logger.info("Has (");
				String[] a = centrenames[0].trim().split("\\(");
				logger.info("1");
				valuesMap.put(loc1,
						"MC_SELECTHUB_CENTRELOCNAME1^^" + a[0].trim());
				logger.info("2");
				txt = a[0].trim();
				logger.info("3");
			} else {
				valuesMap.put(loc1, "MC_SELECTHUB_CENTRELOCNAME1^^"
						+ centrenames[0].trim());
				txt = centrenames[0].trim();
			}
			valuesMap.put(loc2, "MC_SELECTHUB_CENTRELOCNAME2^^"
					+ centrenames[1].trim());
			if (driver.findElements(
					By.xpath(getObjectDetails(
							"MULTICENTRE_HOTELSELECTHUB_CHOOSEBUTTONS").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE))).size() == 2) {
				logger.info("Both Centre Choose Hotel Buttons are visible");
				report.log(LogStatus.PASS,
						"Both Centre Choose Hotel Buttons are visible");
			} else {
				logger.info("Both Centre Choose Hotel Buttons are not visible");
				report.log(LogStatus.FAIL,
						"Both Centre Choose Hotel Buttons are not visible");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,250)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (txt.equalsIgnoreCase(performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME").trim())
					& centrenames[1]
							.trim()
							.equalsIgnoreCase(
									performActionGetText(
											"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
											.trim())) {
				logger.info("Centre Names for both Centres has matched in Package breakdown");
				report.log(LogStatus.PASS,
						"Centre Names for both Centres has matched in Package breakdown");
			} else {
				logger.info("Centre Names for both Centres has not matched in Package breakdown");
				report.log(LogStatus.FAIL,
						"Centre Names for both Centres has not matched in Package breakdown");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,250)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SEARCHRESULTS_DURATION1")
					.contains(dur1.trim().split("-")[0].trim())) {
				logger.info("Centre 1 duration has matched in Package breakdown");
				report.log(LogStatus.PASS,
						"Centre 1 duration has matched in Package breakdown");
			} else {
				logger.info("Centre 1 duration has not matched in Package breakdown");
				report.log(LogStatus.FAIL,
						"Centre 1 duration has not matched in Package breakdown");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SEARCHRESULTS_DURATION2")
					.contains(dur2.trim().split("-")[0].trim())) {
				logger.info("Centre 2 duration has matched in Package breakdown");
				report.log(LogStatus.PASS,
						"Centre 2 duration has matched in Package breakdown");
			} else {
				logger.info("Centre 2 duration has not matched in Package breakdown");
				report.log(LogStatus.FAIL,
						"Centre 2 duration has not matched in Package breakdown");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SEARCHRESULTS_FLIGHT1")
					.split("-")[0].trim().toLowerCase()
					.contains(location.trim().toLowerCase())) {
				logger.info("Outbound Flight Location Deatils have matched");
				report.log(LogStatus.PASS,
						"Outbound Flight Location Deatils have matched");
			} else {
				logger.info("Outbound Flight Location Deatils have not matched");
				report.log(LogStatus.FAIL,
						"Outbound Flight Location Deatils have not matched");
			}

			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_ITINERARYHEADING")
					& performActionGetText(
							"MULTICENTRE_HOTELSELECTHUB_ITINERARYHEADING")
							.trim().equalsIgnoreCase("ITINERARY & HOTELS")) {
				logger.info("Itinerary&Hotels Section is displayed");
				report.log(LogStatus.PASS,
						"Itinerary&Hotels Section is displayed");
			} else {
				logger.info("Itinerary&Hotels Section is not displayed");
				report.log(LogStatus.FAIL,
						"Itinerary&Hotels Section is not displayed");
			}
			if (performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_CENTREBLOCKNAME1")
					.equalsIgnoreCase(txt)) {
				logger.info("Centre 1 location name has matched in Itinerary&Hotels Section");
				report.log(LogStatus.PASS,
						"Centre 1 location name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Centre 1 location name has not matched in Itinerary&Hotels Section");
				report.log(LogStatus.FAIL,
						"Centre 1 location name has not matched in Itinerary&Hotels Section");
			}
			if (performActionGetText(
					"MULTICENTRE_HOTELSELECTHUB_CENTREBLOCKNAME2")
					.equalsIgnoreCase(centrenames[1].trim())) {
				logger.info("Centre 2 location name has matched in Itinerary&Hotels Section");
				report.log(LogStatus.PASS,
						"Centre 2 location name has matched in Itinerary&Hotels Section");
			} else {
				logger.info("Centre 2 location name has not matched in Itinerary&Hotels Section");
				report.log(LogStatus.FAIL,
						"Centre 2 location name has not matched in Itinerary&Hotels Section");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectCountry_New(String type, String type2, String first,
			String second, String loc) {
		try {
			logger.info("Type is :" + type);
			int flagtype = 0, flagtype2 = 0, index = 0;
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				jse.executeScript("window.scrollBy(0,-250)", "");

				List<WebElement> names = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_COUNTRYCENTRESNAMES")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> buttons = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_PERPAGERESULTSBUTTONS")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> centre1 = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_FIRSTCENTREDURATION")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> centre2 = driver
						.findElements(By
								.xpath(getObjectDetails(
										"MULTICENTRE_SEARCHRESULTS_SECONDCENTREDURATION")
										.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> locs = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_LOCATIONS").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> dur1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_DURATIONS1").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> dur2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_SEARCHRESULTS_DURATIONS2").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Names count , Select buttons count:"
						+ names.size() + " and " + buttons.size() + " and "
						+ centre1.size() + " and " + centre2.size() + " and "
						+ locs.size() + " and " + dur1.size() + " and "
						+ dur2.size());
				if (!type.equalsIgnoreCase("Tour")) {
					logger.info("We need to select 2 Hotels Stay!");
					report.log(LogStatus.INFO,
							"We need to select 2 Hotels Stay!");
					for (int i = 0; i < names.size(); i++) {
						logger.info("Name:" + names.get(i).getText());
						String durat1 = dur1.get(i).getText().trim().split("-")[0]
								.trim();
						String durat2 = dur2.get(i).getText().trim().split("-")[0]
								.trim();
						if (type2.equalsIgnoreCase("less")) {
							if (!names.get(i).getText().contains("Tour")
									& (Integer.parseInt(durat1) < Integer
											.parseInt(durat2))) {
								flagtype = 1;
								valuesMap.put(first,
										"MC_SEARCHRESULTS_FIRSTCENTDUR^^"
												+ centre1.get(i).getText()
														.trim());
								valuesMap.put(second,
										"MC_SEARCHRESULTS_SECONDCENTDUR^^"
												+ centre2.get(i).getText()
														.trim());
								valuesMap.put(
										loc,
										"MC_SEARCHRESULTS_FLIGHT^^"
												+ locs.get(i).getText().trim()
														.split(". Direct")[0]);
								logger.info("Found Hotel Stay!!!");
								report.log(LogStatus.INFO,
										"Found Hotel Stay!!!");
								buttons.get(i).click();
								ThreadSleep();
								break;
							}
						}
						if (type2.equalsIgnoreCase("more")) {
							if (!names.get(i).getText().contains("Tour")
									& (Integer.parseInt(durat1) > Integer
											.parseInt(durat2))) {
								flagtype = 1;
								valuesMap.put(first,
										"MC_SEARCHRESULTS_FIRSTCENTDUR^^"
												+ centre1.get(i).getText()
														.trim());
								valuesMap.put(second,
										"MC_SEARCHRESULTS_SECONDCENTDUR^^"
												+ centre2.get(i).getText()
														.trim());
								valuesMap.put(
										loc,
										"MC_SEARCHRESULTS_FLIGHT^^"
												+ locs.get(i).getText().trim()
														.split(". Direct")[0]);
								logger.info("Found Hotel Stay!!!");
								report.log(LogStatus.INFO,
										"Found Hotel Stay!!!");
								buttons.get(i).click();
								ThreadSleep();
								break;
							}
						}
						if (type2.equalsIgnoreCase("equal")) {
							if (!names.get(i).getText().contains("Tour")
									& (durat1.equals(durat2))) {
								flagtype = 1;
								valuesMap.put(first,
										"MC_SEARCHRESULTS_FIRSTCENTDUR^^"
												+ centre1.get(i).getText()
														.trim());
								valuesMap.put(second,
										"MC_SEARCHRESULTS_SECONDCENTDUR^^"
												+ centre2.get(i).getText()
														.trim());
								valuesMap.put(
										loc,
										"MC_SEARCHRESULTS_FLIGHT^^"
												+ locs.get(i).getText().trim()
														.split(". Direct")[0]);
								logger.info("Found Hotel Stay!!!");
								report.log(LogStatus.INFO,
										"Found Hotel Stay!!!");
								buttons.get(i).click();
								ThreadSleep();
								break;
							}
						}

					}
				} else {
					logger.info("We need to select Tour Stay!");
					report.log(LogStatus.INFO, "We need to select Tour Stay!");
					for (int i = 0; i < names.size(); i++) {
						if (names.get(i).getText().contains("Tour")) {
							flagtype = 1;
							logger.info("Found Tour Stay!!!");
							report.log(LogStatus.INFO, "Found Tour Stay!!!");
							buttons.get(i).click();
							ThreadSleep();
							break;
						}
					}
				}
				if (flagtype == 1) {
					logger.info("Given type is found in Search Results Page.Hence moving to Hotel Select Hub Page");
					flagtype2 = 1;
					break;
				} else {
					index++;
				}
			} while (driver.findElement(By.xpath(getObjectDetails(
					"MULTICENTRE_SEARCHRESULTS_NEXTBUTTON").get(
					CONSTANTS_OBJECT_LOCATOR_VALUE))) != null);
			if (flagtype2 == 1) {
				report.log(
						LogStatus.INFO,
						"Given type is found in Search Results Page.Hence moving to Hotel Select Hub Page");
			} else {
				logger.info("Given type is not found in Search Results Page.Hence not moving to Hotel Select Hub Page");
				report.log(
						LogStatus.ERROR,
						"Given type is not found in Search Results Page.Hence not moving to Hotel Select Hub Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void selectChooseHotel(String index) {
		try {
			List<WebElement> buttons = driver.findElements(By
					.xpath(getObjectDetails(
							"MULTICENTRE_HOTELSELECTHUB_CHOOSEBUTTONS").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE)));
			logger.info("Buttons count:" + buttons.size());
			if (index.equals("1")) {
				buttons.get(0).click();
			} else {
				buttons.get(1).click();
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForPkgBrdwn_HotelResults(String dur1, String dur2,
			String loc1, String loc2) {
		try {
			String location1 = valuesMap.get(loc1)
					.substring(valuesMap.get(loc1).indexOf("^^") + 2).trim();
			String location2 = valuesMap.get(loc2)
					.substring(valuesMap.get(loc2).indexOf("^^") + 2).trim();
			logger.info("Values:" + location1 + " and " + location2);
			String heading = performActionGetText(
					"MULTICENTRE_HOTELSEARCHRESULTS_PLEASECHOOSEHEADING")
					.trim();
			logger.info("Text:" + heading);
			heading = heading.split("Please choose your ")[1].split("hotel")[0]
					.trim();
			if (heading.equalsIgnoreCase(location1)) {
				checkForNamesHotelResults_New1(loc2, dur1, dur2);
			} else {
				checkForNamesHotelResults_New2(loc1, dur1, dur2);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForNamesHotelResults_New1(String hname, String dur1,
			String dur2) {
		try {
			String locname = valuesMap.get(hname)
					.substring(valuesMap.get(hname).indexOf("^^") + 2).trim();
			logger.info("Hotel Name:" + locname);
			String durat1 = valuesMap.get(dur1)
					.substring(valuesMap.get(dur1).indexOf("^^") + 2).trim();
			logger.info("durat1:" + durat1);
			String durat2 = valuesMap.get(dur2)
					.substring(valuesMap.get(dur2).indexOf("^^") + 2).trim();
			logger.info("durat2:" + durat2);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> rooms1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTREROOM")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("rooms1  count:" + rooms1.size());
				List<WebElement> boards1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTREBOARD")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("boards1  count:" + boards1.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> prices = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("prices  count:" + prices.size());
				List<WebElement> duration1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSEARCHRESULTS_DUR1").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("duration1 count:" + duration1.size());
				List<WebElement> duration2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSEARCHRESULTS_DUR2").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("duration2 count:" + duration2.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					if (hotelsnames.get(d).getText().trim()
							.equalsIgnoreCase(hotels.get(d).getText().trim())) {
						logger.info("Respective Centre 1 Hotel Name is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Centre 1 Hotel Name is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Centre 1 Hotel Name is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (rooms1.get(d).getText().length() > 4) {
						logger.info("Room for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room for Centre 1 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room for Centre 1 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (boards1.get(d).getText().split(" ").length > 1) {
						logger.info("Board basis for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board basis for Centre 1 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board basis for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board basis for Centre 1 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (locname.toLowerCase().contains(
							hotelsnew.get(d).getText().split("-")[0].trim()
									.toLowerCase())) {
						logger.info("Location Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Location Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Location Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Location Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}

					if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SCNDCENTREROOM")) {
						logger.info("Room is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SCNDCENTREBOARD")) {
						logger.info("Board is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (durat1
							.contains(duration1.get(d).getText().split("-")[0]
									.trim().split(" ")[0])) {
						logger.info("Duration is shown correct for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Duration is shown correct for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Duration is not shown correct for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Duration is not shown correct for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (durat2
							.contains(duration2.get(d).getText().split("-")[0]
									.trim().split(" ")[0])) {
						logger.info("Duration is shown correct for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Duration is shown correct for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Duration is not shown correct for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Duration is not shown correct for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (prices.get(d).getText().trim().length() > 0) {
						logger.info("Price is shown for Hotel " + (d + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.PASS, "Price is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Price is not shown for Hotel " + (d + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Price is not shown for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
					} else {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForNamesHotelResults_New2(String hname, String dur1,
			String dur2) {
		try {
			String locname = valuesMap.get(hname)
					.substring(valuesMap.get(hname).indexOf("^^") + 2).trim();
			logger.info("Hotel Name:" + locname);
			String durat1 = valuesMap.get(dur1)
					.substring(valuesMap.get(dur1).indexOf("^^") + 2).trim();
			logger.info("durat1:" + durat1);
			String durat2 = valuesMap.get(dur2)
					.substring(valuesMap.get(dur2).indexOf("^^") + 2).trim();
			logger.info("durat2:" + durat2);
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				int index = 0;
				if (index != 0) {
					performActionClick("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON");
					ThreadSleep();
				}
				ThreadSleep();
				ThreadSleep();
				jse.executeScript("window.scrollBy(0,-250)", "");
				List<WebElement> hotels = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_FRSTCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels1  count:" + hotels.size());
				List<WebElement> rooms1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTREROOM")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("rooms1  count:" + rooms1.size());
				List<WebElement> boards1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTREBOARD")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("boards1  count:" + boards1.size());
				List<WebElement> hotelsnew = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_SCNDCENTRENAME")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels new  count:" + hotelsnew.size());
				List<WebElement> hotelsnames = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_HOTELLISTNAMES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("hotels names  count:" + hotelsnames.size());
				List<WebElement> prices = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELRESULTS_ALLHOTELPRICES").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("prices  count:" + prices.size());
				List<WebElement> duration1 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSEARCHRESULTS_DUR1").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("duration1 count:" + duration1.size());
				List<WebElement> duration2 = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSEARCHRESULTS_DUR2").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("duration2 count:" + duration2.size());
				List<WebElement> fromtexts = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_HOTELSELECTHUB_HOTELSFROMTEXT")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("fromtexts count:" + fromtexts.size());
				for (int d = 0; d < hotels.size(); d++) {
					logger.info(locname.trim().toLowerCase());
					logger.info(hotels.get(d).getText().trim().toLowerCase());
					if (locname
							.trim()
							.toLowerCase()
							.equalsIgnoreCase(
									hotels.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Location Name for Centre 1 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Location Name for Centre 1 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Location Name for Centre 1 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Location Name for Centre 1 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FRSTCENTREROOM")) {
						logger.info("Room is not shown for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room is not shown for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room is shown for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room is shown for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (!IsElementPresent("MULTICENTRE_HOTELSELECTHUB_FRSTCENTREBOARD")) {
						logger.info("Board is not shown for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board is not shown for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board is shown for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board is shown for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (hotelsnames
							.get(d)
							.getText()
							.trim()
							.toLowerCase()
							.contains(
									hotelsnew.get(d).getText().trim()
											.toLowerCase())) {
						logger.info("Respective Hotel Name is shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Respective Hotel Name is shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Respective Hotel Name is not shown for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Respective Hotel Name is not shown for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (rooms1.get(d).getText().length() > 4) {
						logger.info("Room for Centre 2 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Room for Centre 2 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Room for Centre 2 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Room for Centre 2 is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (boards1.get(d).getText().split(" ").length > 1) {
						logger.info("Board basis for Centre 2 is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Board basis for Centre 2 is shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Board basis for Centre 2 is not shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Board basis for Centre 2 Hotel Name is not shown for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (durat1
							.contains(duration1.get(d).getText().split("-")[0]
									.trim().split(" ")[0])) {
						logger.info("Duration is shown correct for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Duration is shown correct for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Duration is not shown correct for Centre 1 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Duration is not shown correct for Centre 1 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (durat2
							.contains(duration2.get(d).getText().split("-")[0]
									.trim().split(" ")[0])) {
						logger.info("Duration is shown correct for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"Duration is shown correct for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Duration is not shown correct for Centre 2 for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Duration is not shown correct for Centre 2 for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (prices.get(d).getText().trim().length() > 0) {
						logger.info("Price is shown for Hotel " + (d + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.PASS, "Price is shown for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
					} else {
						logger.info("Price is not shown for Hotel " + (d + 1)
								+ " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"Price is not shown for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
					if (fromtexts.get(d).isDisplayed()
							& fromtexts.get(d).getText().trim()
									.equalsIgnoreCase("From")) {
						logger.info("From Text is displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.PASS,
								"From Text is displayed for Hotel " + (d + 1)
										+ " in Page " + (index + 1));
					} else {
						logger.info("From Text is not displayed for Hotel "
								+ (d + 1) + " in Page " + (index + 1));
						report.log(LogStatus.FAIL,
								"From Text is not displayed for Hotel "
										+ (d + 1) + " in Page " + (index + 1));
						report.attachScreenshot(takeScreenShotExtentReports());
					}
				}
				index++;
			} while (IsElementPresent("MULTICENTRE_SEARCHRESULTS_NEXTBUTTON"));
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForDetailsPage(String loc1, String loc2, String hotelname,
			String price) {
		try {
			String location = performActionGetText(
					"MULTICENTRE_HOTELDETAILS_HEADING").trim().split("in")[1]
					.trim();
			logger.info("Location in Details Page:" + location);
			String location1 = valuesMap.get(loc1)
					.substring(valuesMap.get(loc1).indexOf("^^") + 2).trim();
			logger.info("location1:" + location1);
			String location2 = valuesMap.get(loc2)
					.substring(valuesMap.get(loc2).indexOf("^^") + 2).trim();
			logger.info("location2:" + location2);
			String hot = valuesMap.get(hotelname)
					.substring(valuesMap.get(hotelname).indexOf("^^") + 2)
					.trim();
			logger.info("hot:" + hot);
			if (location.equalsIgnoreCase(location1)) {
				chekForHotelDetailsValidation(hotelname, loc2, price, "Hotel");
			} else {
				chekForHotelDetailsValid_2(loc1, hotelname, price);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkSelectHubAfterSelectionFirst(String loc1, String loc2,
			String hotelname, String price) {
		try {
			if (IsElementPresent("MULTICENTRE_HOTELSELECTHUB_SECONDCENTRECHOOSEHOTELBUTTON")) {
				checkForHub_Hotel(hotelname, loc2, price);
			} else {
				checkForHub_Hotel1(loc1, hotelname, price);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForPkgBrdwn_HotelResultsForSecondSelect(String loc1,
			String loc2, String hotelname) {
		try {
			String location1 = valuesMap.get(loc1)
					.substring(valuesMap.get(loc1).indexOf("^^") + 2).trim();
			String location2 = valuesMap.get(loc2)
					.substring(valuesMap.get(loc2).indexOf("^^") + 2).trim();
			logger.info("Values:" + location1 + " and " + location2);
			String heading = performActionGetText(
					"MULTICENTRE_HOTELSEARCHRESULTS_PLEASECHOOSEHEADING")
					.trim();
			logger.info("Text:" + heading);
			heading = heading.split("Please choose your ")[1].split(" ")[0]
					.trim();
			if (heading.equalsIgnoreCase(location1)) {
				checkForHub_Hotel2(hotelname);
			} else {
				checkForHub_Hotel1(hotelname);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForDetailsPage_ForSecondSelect(String loc1, String loc2,
			String hotelname1, String hotelname2, String price) {
		try {
			String location = performActionGetText(
					"MULTICENTRE_HOTELDETAILS_HEADING").trim().split(" ")[1]
					.trim();
			logger.info("Location in Details Page:" + location);
			String location1 = valuesMap.get(loc1)
					.substring(valuesMap.get(loc1).indexOf("^^") + 2).trim();
			logger.info("location1:" + location1);
			String location2 = valuesMap.get(loc2)
					.substring(valuesMap.get(loc2).indexOf("^^") + 2).trim();
			logger.info("location2:" + location2);
			if (location.equalsIgnoreCase(location1)) {
				chekForHotelDetailsValid_Hotel1(hotelname1, hotelname2, price);
			} else {
				chekForHotelDetailsValid_Hotel(hotelname1, hotelname2, price);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForValues(String firsthotel, String firstroom,
			String firstboard, String secondhotel, String secondroom,
			String secondboard) {
		try {
			String firstname = valuesMap.get(firsthotel)
					.substring(valuesMap.get(firsthotel).indexOf("^^") + 2)
					.trim();
			String secondname = valuesMap.get(secondhotel)
					.substring(valuesMap.get(secondhotel).indexOf("^^") + 2)
					.trim();
			String firstrm = valuesMap.get(firstroom)
					.substring(valuesMap.get(firstroom).indexOf("^^") + 2)
					.trim();
			if (firstrm.length() > 3) {
				firstrm = firstrm.replaceAll("[^\\p{L}\\p{Z}]", "");
				firstrm = firstrm.substring(3, firstrm.length()).trim();
			}
			String secondrm = valuesMap.get(secondroom)
					.substring(valuesMap.get(secondroom).indexOf("^^") + 2)
					.trim();
			logger.info("2:" + secondrm);
			if (secondrm.length() > 3) {
				secondrm = secondrm.replaceAll("[^\\p{L}\\p{Z}]", "");
				secondrm = secondrm.substring(3, secondrm.length()).trim();
			}
			String firstbrd = valuesMap.get(firstboard)
					.substring(valuesMap.get(firstboard).indexOf("^^") + 2)
					.trim();
			String secondbrd = valuesMap.get(secondboard)
					.substring(valuesMap.get(secondboard).indexOf("^^") + 2)
					.trim();
			logger.info("Names:" + firstname + " and " + secondname + " and "
					+ firstrm + " and " + secondrm + " and " + firstbrd
					+ " and " + secondbrd);
			logger.info(performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME1")
					.trim().toLowerCase());
			logger.info(firstname.trim().toLowerCase());
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,300)", "");
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME1").trim()
					.toLowerCase()
					.equalsIgnoreCase(firstname.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 1 in Room and Board section is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Hotel Name for Centre 1 in Room and Board section is correct in Hotel Summary Page");
			} else {
				logger.info("Hotel Name for Centre 1 in Room and Board section is not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 in Room and Board section is not correct in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMNAME2").trim()
					.toLowerCase()
					.equalsIgnoreCase(secondname.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 2 in Room and Board section is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Hotel Name for Centre 2 in Room and Board section is correct in Hotel Summary Page");
			} else {
				logger.info("Hotel Name for Centre 2 in Room and Board section is not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 in Room and Board section is not correct in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			String r1 = performActionGetText("MULTICENTRE_SUMMARYHUB_ROOM1")
					.replaceAll("[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r1);
			// r1=r1.substring(0,r1.length()-4);
			logger.info("r1:" + r1);
			String r2 = performActionGetText("MULTICENTRE_SUMMARYHUB_ROOM2")
					.replaceAll("[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r2);
			// r2=r2.substring(0,r2.length()-4);
			logger.info("r2:" + r2);
			if (r1.trim().toLowerCase()
					.equalsIgnoreCase(firstrm.trim().toLowerCase())
					& r1.length() > 3 & firstrm.length() > 3) {
				logger.info("Room for Centre 1 in Room and Board section is shown and is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 1 in Room and Board section is shown and is correct in Hotel Summary Page");
			} else {
				logger.info("Room for Centre 1 in Room and Board section is not shown or not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 1 in Room and Board section is not shown or not correct in Hotel Summary Page");
				jse.executeScript("window.scrollBy(0,150)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (r2.trim().toLowerCase()
					.equalsIgnoreCase(secondrm.trim().toLowerCase())
					& r2.length() > 3 & secondrm.length() > 3) {
				logger.info("Room for Centre 2 in Room and Board section is shown and is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 2 in Room and Board section is shown and is correct in Hotel Summary Page");
			} else {
				logger.info("Room for Centre 2 in Room and Board section is not shown or not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 2 in Room and Board section is not shown or not correct in Hotel Summary Page");
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMBOARD1")
					.trim().equalsIgnoreCase(firstbrd.trim())) {
				logger.info("Board Basis for Centre 1 in Room and Board section is shown in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Board Basis for Centre 1 in Room and Board section is shown in Hotel Summary Page");
			} else {
				logger.info("Board Basis for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 1 in Room and Board section is not shown in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_ROOMBOARD2")
					.trim().equalsIgnoreCase(secondbrd.trim())) {
				logger.info("Board Basis for Centre 2 in Room and Board section is shown in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Board Basis for Centre 2 in Room and Board section is shown in Hotel Summary Page");
			} else {
				logger.info("Board Basis for Centre 2 in Room and Board section is not shown in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 2 in Room and Board section is not shown in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			if (performActionGetText("MULTICENTRE_SUMMARYHUB_FIRSTNAME").trim()
					.toLowerCase()
					.equalsIgnoreCase(firstname.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 1 in Room and Board section under Package Breakdown is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Hotel Name for Centre 1 in Room and Board section under Package Breakdown is correct in Hotel Summary Page");
			} else {
				logger.info("Hotel Name for Centre 1 in Room and Board section under Package Breakdown is not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 in Room and Board section under Package Breakdown is not correct in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_SECONDNAME")
					.trim().toLowerCase()
					.equalsIgnoreCase(secondname.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 2 in Room and Board section under Package Breakdown is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Hotel Name for Centre 2 in Room and Board section under Package Breakdown is correct in Hotel Summary Page");
			} else {
				logger.info("Hotel Name for Centre 2 in Room and Board section under Package Breakdown is not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 in Room and Board section under Package Breakdown is not correct in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			String r3 = performActionGetText("MULTICENTRE_SUMMARYHUB_FIRSTROOM")
					.replaceAll("[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r3);
			// r3=r3.substring(0,r1.length()-4);
			logger.info("r3:" + r3);
			String r4 = performActionGetText(
					"MULTICENTRE_SUMMARYHUB_SECONDROOM").replaceAll(
					"[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r4);
			// r4=r4.substring(0,r4.length()-4);
			logger.info("r4:" + r4);
			if (r3.trim().toLowerCase()
					.equalsIgnoreCase(firstrm.trim().toLowerCase())
					& r3.length() > 3 & firstrm.length() > 3) {
				logger.info("Room for Centre 1 in Room and Board section under Package Breakdown is shown and is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 1 in Room and Board section under Package Breakdown is shown and is correct in Hotel Summary Page");
			} else {
				logger.info("Room for Centre 1 in Room and Board section under Package Breakdown is not shown or not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 1 in Room and Board section under Package Breakdown is not shown or not correct in Hotel Summary Page");
				/*
				 * JavascriptExecutor jse = (JavascriptExecutor) driver;
				 * jse.executeScript("window.scrollBy(0,-250)", "");
				 */
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (r4.trim().toLowerCase()
					.equalsIgnoreCase(secondrm.trim().toLowerCase())
					& r4.length() > 3 & secondrm.length() > 3) {
				logger.info("Room for Centre 2 in Room and Board section under Package Breakdown is shown and is correct in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 2 in Room and Board section under Package Breakdown is shown and is correct in Hotel Summary Page");
			} else {
				logger.info("Room for Centre 2 in Room and Board section under Package Breakdown is not shown or not correct in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 2 in Room and Board section under Package Breakdown is not shown or not correct in Hotel Summary Page");
				/*
				 * JavascriptExecutor jse = (JavascriptExecutor) driver;
				 * jse.executeScript("window.scrollBy(0,-250)", "");
				 */
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_FIRSTBOARD")
					.trim().toLowerCase()
					.contains(firstbrd.trim().toLowerCase())) {
				logger.info("Board Basis for Centre 1 in Room and Board section under Package Breakdown is shown in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Board Basis for Centre 1 in Room and Board section under Package Breakdown is shown in Hotel Summary Page");
			} else {
				logger.info("Board Basis for Centre 1 in Room and Board section under Package Breakdown is not shown in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 1 in Room and Board section under Package Breakdown is not shown in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_SUMMARYHUB_SECONDBOARD")
					.trim().toLowerCase()
					.contains(secondbrd.trim().toLowerCase())) {
				logger.info("Board Basis for Centre 2 in Room and Board section under Package Breakdown is shown in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Board Basis for Centre 2 in Room and Board section under Package Breakdown is shown in Hotel Summary Page");
			} else {
				logger.info("Board Basis for Centre 2 in Room and Board section under Package Breakdown is not shown in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 2 in Room and Board section under Package Breakdown is not shown in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			// if(type.equalsIgnoreCase("Hotel")){
			if (driver.findElements(
					By.xpath(getObjectDetails(
							"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONS").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE))).size() == 2) {
				logger.info("Room and Board section is present for both Hotels in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Room and Board section is present for both Hotels in Hotel Summary Page");
			} else {
				logger.info("Room and Board section is not present for both Hotels in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Room and Board section is not present for both Hotels in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (driver
					.findElements(
							By.xpath(getObjectDetails(
									"MULTICENTRE_HOTELSUMMARY_ROOMOPTIONSINCLUDEDSECTION")
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
					.size() == 2) {
				logger.info("Included/Selected section under Room and Board section is present for both Hotels in Hotel Summary Page");
				report.log(
						LogStatus.PASS,
						"Included/Selected section under Room and Board section is present for both Hotels in Hotel Summary Page");
			} else {
				logger.info("Included/Selected section under Room and Board section is not present for both Hotels in Hotel Summary Page");
				report.log(
						LogStatus.FAIL,
						"Included/Selected section under Room and Board section is not present for both Hotels in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_SUMMARYHUB_ROOMIMAGE")) {
				logger.info("Image in Room and Board section is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Image in Room and Board section is present is present in Hotel Summary Page");
			} else {
				logger.info("Image in Room and Board section is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Image in Room and Board section is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			if (IsElementPresent("MULTICENTRE_HOTELSUMMARY_FLIGHTOPTIONBUTTON")) {
				logger.info("Flight Options Button is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Flight Options Button is present in Hotel Summary Page");
			} else {
				logger.info("Flight Options Button is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Flight Options Button is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			if (IsElementPresent("MULTICENTRE_SUMMARYHUB_FLIGHTOPTIONSIMAGE")) {
				logger.info("Flight Options Image is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Flight Options Image is present in Hotel Summary Page");
			} else {
				logger.info("Flight Options Image is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Flight Options Image is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_SUMMARYHUB_FLIGHTOPTIONSLINK")) {
				logger.info("Flight Options Link is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Flight Options Link is present in Hotel Summary Page");
			} else {
				logger.info("Flight Options Link is not present in Hotel Summary Page");
				report.log(LogStatus.FAIL,
						"Flight Options Link is not present in Hotel Summary Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			performActionClick("MULTICENTRE_SUMMARYHUB_FLIGHTOPTIONSLINK");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_HEADING")) {
				logger.info("Flight Options Page is opened by clicking on the link given in Flight Options section");
				report.log(
						LogStatus.PASS,
						"Flight Options Page is opened by clicking on the link given in Flight Options section");
			} else {
				logger.info("Flight Options Page is not opened by clicking on the link given in Flight Options section");
				report.log(
						LogStatus.FAIL,
						"Flight Options Page isn not opened by clicking on the link given in Flight Options section");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			performActionClick("MULTICENTRE_FLIGHTOPTIONS_BACKSUMMARY");
			ThreadSleep();
			performActionClick("MULTICENTRE_HOTELSUMMARY_FLIGHTOPTIONBUTTON");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_SELECTSEAT")) {
				logger.info("Select Seat section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Select Seat section is available in Flight Options Page");
			} else {
				logger.info("Select Seat section is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Select Seat section is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_SEATWITHSPACE")) {
				logger.info("Seat with Extra space section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Seat with Extra space section is present in Hotel Summary Page");
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_SEATWITHLEGROOM")) {
				logger.info("Seat with Extra legroom section is present in Hotel Summary Page");
				report.log(LogStatus.PASS,
						"Seat with Extra legroom is present in Hotel Summary Page");
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_BAGGAGE")) {
				logger.info("Baggage section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Baggage section is available in Flight Options Page");
			} else {
				logger.info("Baggage section is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Baggage section is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_FLIGHTMEAL")) {
				logger.info("FLight Meal section is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"FLight Meal section is available in Flight Options Page");
			} else {
				logger.info("FLight Meal section is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"FLight Meal section is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_BACKSUMMARY")) {
				logger.info("Back To Summary button is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Back To Summary button is available in Flight Options Page");
			} else {
				logger.info("Back To Summary button is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Back To Summary button is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_CONTINUE")) {
				logger.info("Continue button is available in Flight Options Page");
				report.log(LogStatus.PASS,
						"Continue button is available in Flight Options Page");
			} else {
				logger.info("Continue button is not available in Flight Options Page");
				report.log(LogStatus.FAIL,
						"Continue button is not available in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			performActionClick("MULTICENTRE_FLIGHTOPTIONS_BACKSUMMARY");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_FLIGHTOPTIONS_HEADING")) {
				logger.info("Flight Options Page is opened after clicking back button in Flight Options Page");
				report.log(
						LogStatus.PASS,
						"Flight Options Page is opened after clicking back button in Flight Options Page");
			} else {
				logger.info("Flight Options Page is not opened after clicking back button in Flight Options Page");
				report.log(
						LogStatus.FAIL,
						"Flight Options Page is not opened after clicking back button in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			performActionClick("MULTICENTRE_HOTELSUMMARY_FLIGHTOPTIONBUTTON");
			ThreadSleep();
			performActionClick("MULTICENTRE_FLIGHTOPTIONS_CONTINUE");
			ThreadSleep();
			if (IsElementPresent("MULTICENTRE_PASSENGERDETAILS_HEADING")) {
				logger.info("Passenger Details Page is opened after clicking Continue button in Flight Options Page");
				report.log(
						LogStatus.PASS,
						"Passenger Details Page is opened after clicking Continue button in Flight Options Page");
			} else {
				logger.info("Passenger Details Page is not opened after clicking Continue button in Flight Options Page");
				report.log(
						LogStatus.FAIL,
						"Passenger Details Page is not opened after clicking Continue button in Flight Options Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void changeHotel(String condition, String index) {
		try {
			if (condition.equalsIgnoreCase("yes")) {
				if (index.equals("1")) {
					performActionClick("MULTICENTRE_HOTELSELECTHUB_CHANGEFIRSTHOTEL");
				} else {
					performActionClick("MULTICENTRE_HOTELSELECTHUB_CHANGESECONDHOTEL");
				}
			} else {

			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void takechildages(String count, String key) {
		try {
			String cnt = valuesMap.get(count)
					.substring(valuesMap.get(count).indexOf("^^") + 2).trim();
			logger.info("Child count:" + cnt);
			if (!cnt.equals("0")) {
				logger.info("Children exist");
				captureTextValuesandHold("MULTICENTRE_LANDING_CHILDAGES", key);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void performActionEnterPassengerDeatils_MC(String adultcnt,
			String childcnt, String ages) {
		try {
			int adcnt = Integer.parseInt(valuesMap.get(adultcnt)
					.substring(valuesMap.get(adultcnt).indexOf("^^") + 2)
					.trim());
			int chcnt = Integer.parseInt(valuesMap.get(childcnt)
					.substring(valuesMap.get(childcnt).indexOf("^^") + 2)
					.trim());
			String[] age = new String[] {};
			List<Integer> agesint = new ArrayList<>();
			if (chcnt != 0) {
				logger.info("Chilren present!");
				age = valuesMap.get(ages)
						.substring(valuesMap.get(ages).indexOf("^^") + 2)
						.trim().split(",");
			}
			for (int i = 0; i < age.length; i++) {
				agesint.add(Integer.parseInt(age[i].trim()));
			}
			List<WebElement> titles = driver.findElements(By
					.xpath(getObjectDetails("MULTICENTRE_PASSENGER_TITLE").get(
							CONSTANTS_OBJECT_LOCATOR_VALUE)));
			List<WebElement> firstnames = driver.findElements(By
					.xpath(getObjectDetails("MULTICENTRE_PASSENGER_FIRSTNAME")
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
			List<WebElement> lastnames = driver.findElements(By
					.xpath(getObjectDetails("MULTICENTRE_PASSENGER_LASTNAME")
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
			logger.info("Counts:" + titles.size() + " and " + firstnames.size()
					+ " and " + lastnames.size());
			for (int i = 0; i < adcnt; i++) {
				Select tit = new Select(titles.get(i));
				tit.selectByVisibleText("Mr");
				firstnames.get(i).sendKeys("ABC");
				lastnames.get(i).sendKeys("DEF");
				if (i == 0) {
					logger.info("Into this");
					JavascriptExecutor jse = (JavascriptExecutor) driver;
					jse.executeScript("window.scrollBy(0,100)", "");
					performActionEnterText_SLF1(
							"MULTICENTRE_PASSENGER_ADDRESS", "London Address");
					performActionEnterText_SLF1(
							"MULTICENTRE_PASSENGER_TELEPHONE", "9876543210");
					performActionEnterText_SLF1("MULTICENTRE_PASSENGER_EMAIL",
							"abc@gmail.com");
					performActionEnterText_SLF1(
							"MULTICENTRE_PASSENGER_CONFIRMEMAIL",
							"abc@gmail.com");
					performActionEnterText_SLF1(
							"MULTICENTRE_PASSENGER_ADDRESS1", "Address New");
					performActionEnterText_SLF1("MULTICENTRE_PASSENGER_CITY",
							"London");
					performActionEnterText_SLF1(
							"MULTICENTRE_PASSENGER_COUNTRY", "UK");
					performActionEnterText_SLF1(
							"MULTICENTRE_PASSENGER_POSTALCODE", "LU44TY");

				}
			}
			if (chcnt > 0) {
				List<WebElement> dobs = driver.findElements(By
						.xpath(getObjectDetails(
								"MULTICENTRE_PASSENGER_CHILDDOB").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("DOB count:" + dobs.size());
				int j = 0;
				for (int i = adcnt; i < (adcnt + chcnt); i++) {
					Select tit = new Select(titles.get(i));
					tit.selectByVisibleText("Master");
					firstnames.get(i).sendKeys("UVW");
					lastnames.get(i).sendKeys("XYZ");
					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
					Date dt = new Date();
					Calendar cal = Calendar.getInstance();
					cal.setTime(dt);
					if (agesint.get(j) == 0) {
						cal.add(Calendar.MONTH, -5);
					} else {
						cal.add(Calendar.YEAR, -agesint.get(j));
					}
					dt = sdf.parse(sdf.format(cal.getTime()));
					logger.info(dt);
					logger.info(sdf.format(dt));
					dobs.get(j).sendKeys(sdf.format(dt));
					j++;
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void performActionEnterText_SLF1(String objectName, String testData) {
		objectDetails = getObjectDetails(objectName);
		result = true;
		try {
			// testData=fetchTestDataFromMap(testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					logger.info("INto xpath");
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	public void performAccertifyVerfication(String BookingReferenceNumber,
			String holidayOwner, String webPrice, String webDate,
			String anitelogin, String anitepassword) {
		try {
			driver.get("https://app01.accertify.net/unsecured/index.jsf");
			waitForPageLoad(driver);

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void performDMSVerfication() {
		try {
			driver.get("https://dms-st2/dms/cgi/dms-dash.pl");
			waitForPageLoad(driver);
			settingFunction();
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkDetailsInPayment(String total, String hotel1,
			String hotel2, String room1, String room2, String board1,
			String board2) {
		try {
			String price = valuesMap.get(total)
					.substring(valuesMap.get(total).indexOf("^^") + 2).trim();
			String hotelname1 = valuesMap.get(hotel1)
					.substring(valuesMap.get(hotel1).indexOf("^^") + 2).trim();
			String hotelname2 = valuesMap.get(hotel2)
					.substring(valuesMap.get(hotel2).indexOf("^^") + 2).trim();
			String hotelroom1 = valuesMap.get(room1)
					.substring(valuesMap.get(room1).indexOf("^^") + 2).trim();
			hotelroom1 = hotelroom1.replaceAll("[^\\p{L}\\p{Z}]", "");
			hotelroom1 = hotelroom1.substring(3, hotelroom1.length()).trim();
			String hotelroom2 = valuesMap.get(room2)
					.substring(valuesMap.get(room2).indexOf("^^") + 2).trim();
			hotelroom2 = hotelroom2.replaceAll("[^\\p{L}\\p{Z}]", "");
			hotelroom2 = hotelroom2.substring(3, hotelroom2.length()).trim();
			String hotelboard1 = valuesMap.get(board1)
					.substring(valuesMap.get(board1).indexOf("^^") + 2).trim();
			String hotelboard2 = valuesMap.get(board2)
					.substring(valuesMap.get(board2).indexOf("^^") + 2).trim();
			logger.info("Values in payment:" + price + " and " + hotelname1
					+ " and " + hotelname2 + " and " + hotelroom1 + " and "
					+ hotelroom2 + " and " + hotelboard1 + " and "
					+ hotelboard2);

			if (performActionGetText("MULTICENTRE_PAYMENT_HOTELDETAILSNAME1")
					.trim().split("\n")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname1.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 1 in Hotel Details section is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 1 in Hotel Details section is correct in Payment Page");
			} else {
				logger.info("Hotel Name for Centre 1 in Hotel Details section is not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 in Hotel Details section is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_HOTELDETAILSNAME2")
					.trim().split("\n")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname2.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 2 in Hotel Details section is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 2 in Hotel Details section is correct in Payment Page");
			} else {
				logger.info("Hotel Name for Centre 2 in Hotel Details section is not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 in Hotel Details section is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			String r1 = performActionGetText(
					"MULTICENTRE_PAYMENT_HOTELDETAILSROOM1").replaceAll(
					"[^\\p{L}\\p{Z}]", "").trim();
			// logger.info(r1);
			r1 = r1.substring(0, r1.length() - 1);
			logger.info("r1:" + r1);
			String r2 = performActionGetText(
					"MULTICENTRE_PAYMENT_HOTELDETAILSROOM2").replaceAll(
					"[^\\p{L}\\p{Z}]", "").trim();
			// logger.info(r2);
			r2 = r2.substring(0, r2.length() - 1);
			logger.info("r2:" + r2);
			if (r1.trim().toLowerCase()
					.equalsIgnoreCase(hotelroom1.trim().toLowerCase())) {
				logger.info("Room for Centre 1 in Hotel Details section is shown and is correct in Payment Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 1 in Hotel Details section is shown and is correct in Payment Page");
			} else {
				logger.info("Room for Centre 1 in Hotel Details section is not shown or not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 1 in Hotel Details is not shown or not correct in Payment Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,250)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (r2.trim().toLowerCase()
					.equalsIgnoreCase(hotelroom2.trim().toLowerCase())) {
				logger.info("Room for Centre 2 in Hotel Details section is shown and is correct in Payment Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 2 in Hotel Details section is shown and is correct in Payment Page");
			} else {
				logger.info("Room for Centre 2 in Hotel Details section is not shown or not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 2 in Hotel Details section is not shown or not correct in Payment Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_HOTELDETAILSBOARD1")
					.trim().equalsIgnoreCase(hotelboard1.trim())) {
				logger.info("Board Basis for Centre 1 in Hotel Details section is shown in Payment Page");
				report.log(LogStatus.PASS,
						"Board Basis for Centre 1 in Hotel Details section is shown in Payment Page");
			} else {
				logger.info("Board Basis for Centre 1 in Hotel Details section is not shown in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 1 in Hotel Details section is not shown in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_HOTELDETAILSBOARD2")
					.trim().equalsIgnoreCase(hotelboard2.trim())) {
				logger.info("Board Basis for Centre 2 in Hotel Details section is shown in Payment Page");
				report.log(LogStatus.PASS,
						"Board Basis for Centre 2 in Hotel Details section is shown in Payment Page");
			} else {
				logger.info("Board Basis for Centre 2 in Hotel Details section is not shown in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 2 in Hotel Details section is not shown in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			if (performActionGetText("MULTICENTRE_PAYMENT_PKGBRKDWNFIRSTNAME")
					.trim().split("\n")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname1.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 1 in Room & Board section is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 1 in Room & Board section is correct in Payment Page");
			} else {
				logger.info("Hotel Name for Centre 1 in Room & Board section is not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 in Room & Board section is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_PKGBRKDWNSECONDNAME")
					.trim().split("\n")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname2.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 2 in Room & Board section is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 2 in Room & Board section is correct in Payment Page");
			} else {
				logger.info("Hotel Name for Centre 2 in Room & Board section is not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 in Room & Board section is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			r1 = performActionGetText("MULTICENTRE_PAYMENT_PKGBRKDWNFIRSTROOM")
					.replaceAll("[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r1);
			r1 = r1.substring(0, r1.length() - 2);
			logger.info("r1:" + r1);
			r2 = performActionGetText("MULTICENTRE_PAYMENT_PKGBRKDWNSECONDROOM")
					.replaceAll("[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r2);
			r2 = r2.substring(0, r2.length() - 2);
			logger.info("r2:" + r2);
			if (r1.trim().toLowerCase()
					.equalsIgnoreCase(hotelroom1.trim().toLowerCase())) {
				logger.info("Room for Centre 1 in Room & Board section is shown and is correct in Payment Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 1 in Room & Board section is shown and is correct in Payment Page");
			} else {
				logger.info("Room for Centre 1 in Room & Board section is not shown or not correct in Payment Page");
				report.log(LogStatus.FAIL,
						"Room for Centre 1 in Room & Board is not shown or not correct in Payment Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,250)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (r2.trim().toLowerCase()
					.equalsIgnoreCase(hotelroom2.trim().toLowerCase())) {
				logger.info("Room for Centre 2 in Room & Board section is shown and is correct in Payment Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 2 in Room & Board section is shown and is correct in Payment Page");
			} else {
				logger.info("Room for Centre 2 in Room & Board section is not shown or not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 2 in Room & Board section is not shown or not correct in Payment Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_PKGBRKDWNFIRSTBOARD")
					.trim().equalsIgnoreCase(hotelboard1.trim())) {
				logger.info("Board Basis for Centre 1 in Room & Board section is shown in Payment Page");
				report.log(LogStatus.PASS,
						"Board Basis for Centre 1 in Room & Board section is shown in Payment Page");
			} else {
				logger.info("Board Basis for Centre 1 in Room & Board section is not shown in Payment Page");
				report.log(LogStatus.FAIL,
						"Board Basis for Centre 1 in Room & Board section is not shown in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_PKGBRKDWNSECONDBOARD")
					.trim().equalsIgnoreCase(hotelboard2.trim())) {
				logger.info("Board Basis for Centre 2 in Room & Board section is shown in Payment Page");
				report.log(LogStatus.PASS,
						"Board Basis for Centre 2 in Room & Board section is shown in Payment Page");
			} else {
				logger.info("Board Basis for Centre 2 in Room & Board section is not shown in Payment Page");
				report.log(LogStatus.FAIL,
						"Board Basis for Centre 2 in Room & Board section is not shown in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_TOPHOTELDETAILS1")
					.split("-")[1].split(",")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname1.trim())) {
				logger.info("Hotel Name for Centre 1 under Package Breakdown is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 1 under Package Breakdown is correct in Payment Page");
			} else {
				logger.info("Hotel Name for Centre 1 under Package Breakdown is not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 under Package Breakdown is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_TOPHOTELDETAILS2")
					.split("-")[1].split(",")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname2.trim())) {
				logger.info("Hotel Name for Centre 2 under Package Breakdown is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 2 under Package Breakdown is correct in Payment Page");
			} else {
				logger.info("Hotel Name for Centre 2 under Package Breakdown is not correct in Payment Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 under Package Breakdown is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

			if (price
					.contains(performActionGetText("MULTICENTRE_PAYMENT_TOTALPRICE"))) {
				logger.info("Toatl Price on top is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Toatl Price on top is correct in Payment Page");
			} else {
				logger.info("Toatl Price on top is not correct in Payment Page");
				report.log(LogStatus.FAIL,
						"Toatl Price on top is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_PAYMENT_TOTALPRICEBOTTOM")
					.equalsIgnoreCase(price)) {
				logger.info("Toatl Price on bottom is correct in Payment Page");
				report.log(LogStatus.PASS,
						"Toatl Price on bottom is correct in Payment Page");
			} else {
				logger.info("Toatl Price on bottom is not correct in Payment Page");
				report.log(LogStatus.FAIL,
						"Toatl Price on bottom is not correct in Payment Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void makePayment(String type) {
		try {
			logger.info("Type:" + type);
			boolean flag = false;
			if (type.equalsIgnoreCase("low")
					& IsElementPresent("MULTICENTRE_PAYMENT_LOWDEPOSIT")) {
				logger.info("Inside Low");
				flag = true;
				WebElement wb = driver.findElement(By.xpath(getObjectDetails(
						"MULTICENTRE_PAYMENT_LOWDEPOSIT").get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wb.findElement(
						By.xpath(getObjectDetails(
								"MULTICENTRE_PAYMENT_SELECTION").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).click();
			} else if (type.equalsIgnoreCase("standard")
					& IsElementPresent("MULTICENTRE_PAYMENT_STANDARDDEPOSIT")) {
				logger.info("Inside Standard");
				flag = true;
				WebElement wb = driver.findElement(By.xpath(getObjectDetails(
						"MULTICENTRE_PAYMENT_STANDARDDEPOSIT").get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wb.findElement(
						By.xpath(getObjectDetails(
								"MULTICENTRE_PAYMENT_SELECTION").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).click();
			} else if (type.equalsIgnoreCase("full")
					& IsElementPresent("MULTICENTRE_PAYMENT_FULLDEPOSIT")) {
				logger.info("Inside Full");
				flag = true;
				WebElement wb = driver.findElement(By.xpath(getObjectDetails(
						"MULTICENTRE_PAYMENT_FULLDEPOSIT").get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Fiull");
				wb.findElement(
						By.xpath(getObjectDetails(
								"MULTICENTRE_PAYMENT_SELECTION").get(
								CONSTANTS_OBJECT_LOCATOR_VALUE))).click();
			}
			if (flag == false) {
				logger.info("Given payment option is not present in Payment Page");
				report.log(LogStatus.ERROR,
						"Given payment option is not present in Payment Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
				PageDisplay("MULTICENTRE_CONFIRMATION_HEADING", "");
				Assert.assertTrue(false,
						"Given payment option is not present in Payment Page");
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForAuthentication() {
		try {
			if (IsElementPresent("MULTICENTRE_PAYMENT_AUTHENTICATED")) {
				performActionClick("MULTICENTRE_PAYMENT_AUTHENTICATED");
				ThreadSleep();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkValuesInConfirmation(String total, String hotel1,
			String hotel2, String room1, String room2, String board1,
			String board2) {
		try {
			String price = valuesMap.get(total)
					.substring(valuesMap.get(total).indexOf("^^") + 2).trim();
			String hotelname1 = valuesMap.get(hotel1)
					.substring(valuesMap.get(hotel1).indexOf("^^") + 2).trim();
			String hotelname2 = valuesMap.get(hotel2)
					.substring(valuesMap.get(hotel2).indexOf("^^") + 2).trim();
			String hotelroom1 = valuesMap.get(room1)
					.substring(valuesMap.get(room1).indexOf("^^") + 2).trim();
			hotelroom1 = hotelroom1.replaceAll("[^\\p{L}\\p{Z}]", "");
			hotelroom1 = hotelroom1.substring(3, hotelroom1.length()).trim();
			String hotelroom2 = valuesMap.get(room2)
					.substring(valuesMap.get(room2).indexOf("^^") + 2).trim();
			hotelroom2 = hotelroom2.replaceAll("[^\\p{L}\\p{Z}]", "");
			hotelroom2 = hotelroom2.substring(3, hotelroom2.length()).trim();
			String hotelboard1 = valuesMap.get(board1)
					.substring(valuesMap.get(board1).indexOf("^^") + 2).trim();
			String hotelboard2 = valuesMap.get(board2)
					.substring(valuesMap.get(board2).indexOf("^^") + 2).trim();
			logger.info("Values in confirmation:" + price + " and "
					+ hotelname1 + " and " + hotelname2 + " and " + hotelroom1
					+ " and " + hotelroom2 + " and " + hotelboard1 + " and "
					+ hotelboard2);

			if (performActionGetText("MULTICENTRE_CONFIRMATION_TOPFIRSTHOTEL")
					.trim().split(",")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname1.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 1 in the top section is correct in Confirmation Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 1 in the top section is correct in Confirmation Page");
			} else {
				logger.info("Hotel Name for Centre 1 in the top section is not correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 in the top section is not correct in Confirmation Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_CONFIRMATION_TOPSECONDHOTEL")
					.trim().split(",")[0].trim().toLowerCase()
					.equalsIgnoreCase(hotelname2.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 2 in the top section is correct in Confirmation Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 2 in the top section is correct in Confirmation Page");
			} else {
				logger.info("Hotel Name for Centre 2 in the top section is not correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 in the top section is not correct in Confirmation Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_CONFIRMATION_FIRSTHOTELNAME")
					.trim().toLowerCase()
					.equalsIgnoreCase(hotelname1.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 1 in the bottom section is correct in Confirmation Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 1 in the bottom section is correct in Confirmation Page");
			} else {
				logger.info("Hotel Name for Centre 1 in the bottom section is not correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 in the bottom section is not correct in Confirmation Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_CONFIRMATION_SECONDHOTELNAME")
					.trim().toLowerCase()
					.equalsIgnoreCase(hotelname2.trim().toLowerCase())) {
				logger.info("Hotel Name for Centre 2 in the bottom section is correct in Confirmation Page");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 2 in the bottom section is correct in Confirmation Page");
			} else {
				logger.info("Hotel Name for Centre 2 in the bottom section is not correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 in the bottom section is not correct in Confirmation Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			String r1 = performActionGetText(
					"MULTICENTRE_CONFIRMATION_FIRSTHOTELROOM").replaceAll(
					"[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r1);
			// r1=r1.substring(0,r1.length()-1);
			logger.info("r1:" + r1);
			String r2 = performActionGetText(
					"MULTICENTRE_CONFIRMATION_SECONDHOTELROOM").replaceAll(
					"[^\\p{L}\\p{Z}]", "").trim();
			logger.info(r2);
			// r2=r2.substring(0,r2.length()-1);
			logger.info("r2:" + r2);
			if (r1.trim().toLowerCase()
					.equalsIgnoreCase(hotelroom1.trim().toLowerCase())) {
				logger.info("Room for Centre 1 in the bottom section is shown and is correct in Confirmation Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 1 in the bottom section is shown and is correct in Confirmation Page");
			} else {
				logger.info("Room for Centre 1 in the bottom section is not shown or not correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 1 in the bottom is not shown or not correct in Confirmation Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,250)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (r2.trim().toLowerCase()
					.equalsIgnoreCase(hotelroom2.trim().toLowerCase())) {
				logger.info("Room for Centre 2 in the bottom section is shown and is correct in Confirmation Page");
				report.log(
						LogStatus.PASS,
						"Room for Centre 2 in the bottom section is shown and is correct in Confirmation Page");
			} else {
				logger.info("Room for Centre 2 in the bottom section is not shown or not correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Room for Centre 2 in the bottom section is not shown or not correct in Confirmation Page");
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,100)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_CONFIRMATION_FIRSTHOTELBOARD")
					.trim().equalsIgnoreCase(hotelboard1.trim())) {
				logger.info("Board Basis for Centre 1 in the bottom section is shown in Confirmation Page");
				report.log(LogStatus.PASS,
						"Board Basis for Centre 1 in the bottom section is shown in Confirmation Page");
			} else {
				logger.info("Board Basis for Centre 1 in the bottom section is not shown in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 1 in the bottom section is not shown in Confirmation Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText(
					"MULTICENTRE_CONFIRMATION_SECONDHOTELBOARD").trim()
					.equalsIgnoreCase(hotelboard2.trim())) {
				logger.info("Board Basis for Centre 2 in Hotel Details section is shown in Confirmation Page");
				report.log(
						LogStatus.PASS,
						"Board Basis for Centre 2 in Hotel Details section is shown in Confirmation Page");
			} else {
				logger.info("Board Basis for Centre 2 in Hotel Details section is not shown in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Board Basis for Centre 2 in Hotel Details section is not shown in Confirmation Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (performActionGetText("MULTICENTRE_CONFIRMATION_TOTALPRICE")
					.trim().equals(price)) {
				logger.info("Total Price is shown correct in Confirmation Page");
				report.log(LogStatus.PASS,
						"Total Price is shown correct in Confirmation Page");
			} else {
				logger.info("Total Price is not shown correct in Confirmation Page");
				report.log(
						LogStatus.FAIL,
						"Total Price is not shown correct in Confirmation Page.Values compared:"
								+ price
								+ " and "
								+ performActionGetText("MULTICENTRE_CONFIRMATION_TOTALPRICE"));
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void checkForMoreResults() {
		try {
			if (performActionMultipleGetText(
					"MC_HOTELRESULTSPAGE_VIEWHOTELDETAILSBUTTON").size() > 1) {
				logger.info("More than 1 results are there to change the hotel");
				report.log(LogStatus.INFO,
						"More than 1 results are there to change the hotel");
			} else {
				logger.info("More than 1 results are not present to change the hotel");
				report.log(LogStatus.ERROR,
						"More than 1 results are not present to change the hotel");
				performActionClick("MULTICENTRE_HOTELDETAILS_BACKOVERVIEW");
				ThreadSleep();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void takeHotelDeatils(String index, String loc1, String loc2,
			String firstroom, String firstboard, String secondroom,
			String secondboard) {
		try {
			String location1 = valuesMap.get(loc1)
					.substring(valuesMap.get(loc1).indexOf("^^") + 2).trim();
			String location2 = valuesMap.get(loc2)
					.substring(valuesMap.get(loc2).indexOf("^^") + 2).trim();
			logger.info("Values:" + location1 + " and " + location2);
			String heading = performActionGetText(
					"MULTICENTRE_HOTELSEARCHRESULTS_PLEASECHOOSEHEADING")
					.trim();
			logger.info("Text:" + heading);
			heading = heading.split("Please choose your ")[1].split("hotel")[0]
					.trim();
			if (index.equals("1")) {
				if (heading.equalsIgnoreCase(location1)) {
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_ROOMTYPE", firstroom);
					captureTextValuesandHold("MULTICENTRE_HOTELRESULTS_BOARD",
							firstboard);
				} else {
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPEFIRST",
							secondroom);
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREDBOARDFIRST",
							secondboard);
				}
			} else {
				if (heading.equalsIgnoreCase(location1)) {
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_SECONDROOMTYPE",
							firstroom);
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_SECONDBOARD", firstboard);
				} else {
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREROOMTYPESECOND",
							secondroom);
					captureTextValuesandHold(
							"MULTICENTRE_HOTELRESULTS_SECONDCENTREBOARDSECOND",
							secondboard);
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void AddingPrices_Final(String Hotelprice1, String ExpectedPrice,
			String ad, String ch, String ages) {
		try {
			Double price = Double.parseDouble(valuesMap.get(Hotelprice1)
					.substring(valuesMap.get(Hotelprice1).indexOf("^^") + 2));
			logger.info(price);
			report.log(LogStatus.INFO, "insdie adding");
			// int
			// price2=Integer.parseInt(valuesMap.get(Hotelprice2).substring(valuesMap.get(Hotelprice2).indexOf("^^")
			// + 2));
			Double total = Double.parseDouble(valuesMap.get(ExpectedPrice)
					.substring(valuesMap.get(ExpectedPrice).indexOf("^^") + 2)
					.replaceAll("[^0-9.]", ""));
			logger.info(total);
			int adult = Integer.parseInt(valuesMap.get(ad).substring(
					valuesMap.get(ad).indexOf("^^") + 2));
			logger.info(adult);
			int child = Integer.parseInt(valuesMap.get(ch).substring(
					valuesMap.get(ch).indexOf("^^") + 2));
			logger.info(child);
			if (child > 0) {
				String[] age = valuesMap.get(ages)
						.substring(valuesMap.get(ages).indexOf("^^") + 2)
						.split(",");
				logger.info(age.length);
				for (int i = 0; i < age.length; i++) {
					if (Integer.parseInt(age[i].trim()) > 1) {
						adult = adult + 1;
					}
				}
			}
			logger.info("Final adults:" + adult);
			logger.info((total) - (price * adult));
			if ((total) - (price * adult) <= 3) {
				report.log(LogStatus.PASS,
						"Total Price in booking summary page matches" + " "
								+ "Expected Price" + (price * adult) + " "
								+ "Actual Price" + total);
			} else {
				report.log(LogStatus.FAIL,
						"Total Price in booking summary page is not matching"
								+ " " + "Expected Price" + (price * adult)
								+ " " + "Actual Price" + total);
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}

	}

	public void settingFunction() {
		try {
			if (IsElementPresent("ANITE_SECURITY_ADVANCEDBUTTON")) {
				performActionClick("ANITE_SECURITY_ADVANCEDBUTTON");
				ThreadSleep();
				performActionClick("ANITE_SECURITY_EXCEPTION");
				ThreadSleep();
				alertClick();
			}
		} catch (Exception e) {

		}
	}

	public void aniteVerification(String bookingref, String location,
			String outflight, String inflight, String price1, String paid,
			String remaining, String adultcnt, String childcnt, String ages,
			String hotelname, String hotelname1, String firstroom,
			String secondroom, String firstboard, String secondboard) {
		try {
			report.log(LogStatus.INFO, "INTO ANITE VERIFICATION!!!");
			String bookref = valuesMap.get(bookingref)
					.substring(valuesMap.get(bookingref).indexOf("^^") + 2)
					.trim();
			String loc = valuesMap.get(location)
					.substring(valuesMap.get(location).indexOf("^^") + 2)
					.trim();
			logger.info("Values in anite:" + bookref + " and " + loc);
			String[] locs = loc.split("&");
			String outf = valuesMap.get(outflight)
					.substring(valuesMap.get(outflight).indexOf("^^") + 2)
					.trim();
			logger.info("1");
			String inf = valuesMap.get(inflight)
					.substring(valuesMap.get(inflight).indexOf("^^") + 2)
					.trim();
			logger.info("2");
			Double price = Double.parseDouble(valuesMap.get(price1)
					.substring(valuesMap.get(price1).indexOf("^^") + 2).trim());
			String amtpaid = "";
			Double amountpaid = 0.0;
			if (valuesMap.get(paid) != null) {
				logger.info("3");
				amtpaid = valuesMap.get(paid)
						.substring(valuesMap.get(paid).indexOf("^^") + 2)
						.trim();
				amtpaid = amtpaid.substring(1, amtpaid.length());
				amountpaid = Double.parseDouble(amtpaid.trim());
			}
			String remain = "";
			Double rem = 0.0;
			logger.info("4:" + valuesMap.get(remaining).split("^^").length);
			if (valuesMap.get(remaining).split("^^").length > 1) {
				logger.info("5");
				remain = valuesMap.get(remaining)
						.substring(valuesMap.get(remaining).indexOf("^^") + 2)
						.trim();
				remain = remain.substring(1, remain.length());
				rem = Double.parseDouble(remain.trim());
			}
			int adults = Integer.parseInt(valuesMap.get(adultcnt)
					.substring(valuesMap.get(adultcnt).indexOf("^^") + 2)
					.trim());
			int children = Integer.parseInt(valuesMap.get(childcnt)
					.substring(valuesMap.get(childcnt).indexOf("^^") + 2)
					.trim());
			String childages = valuesMap.get(ages)
					.substring(valuesMap.get(ages).indexOf("^^") + 2).trim();
			String[] cages = new String[children];
			if (children > 0) {
				cages = childages.trim().split(",");
			}
			String hotel1 = valuesMap.get(hotelname)
					.substring(valuesMap.get(hotelname).indexOf("^^") + 2)
					.trim();
			String hotel2 = valuesMap.get(hotelname1)
					.substring(valuesMap.get(hotelname1).indexOf("^^") + 2)
					.trim();
			String room1 = valuesMap.get(firstroom)
					.substring(valuesMap.get(firstroom).indexOf("^^") + 2)
					.trim();
			room1 = room1.replaceAll("[^\\p{L}\\p{Z}]", "");
			room1 = room1.substring(3, room1.length()).trim();
			logger.info("room1:" + room1);
			String room2 = valuesMap.get(secondroom)
					.substring(valuesMap.get(secondroom).indexOf("^^") + 2)
					.trim();
			room2 = room2.replaceAll("[^\\p{L}\\p{Z}]", "");
			room2 = room2.substring(3, room2.length()).trim();
			logger.info("room2:" + room2);
			String board1 = valuesMap.get(firstboard)
					.substring(valuesMap.get(firstboard).indexOf("^^") + 2)
					.trim();
			String board2 = valuesMap.get(secondboard)
					.substring(valuesMap.get(secondboard).indexOf("^^") + 2)
					.trim();
			logger.info("Values in anite:" + bookingref + " and " + outf
					+ " and " + inf + " and " + price + " and " + amountpaid
					+ " and " + rem + " and " + adults + " and " + children
					+ " and " + childages + " and " + hotel1 + " and " + hotel2
					+ " and " + room1 + " and " + room2 + " and " + board1
					+ " and " + board2);

			performActionMoveToElementAndClick("ATCORE_HOME_BOOKING",
					"ATCORE_BOOKING_RECALL");
			ThreadSleep();
			performActionEnterText_SLF1("ATCORE_RECALL_TEXTBOX", bookref);
			performActionClick("ATCORE_RECALL_SEARCHBUTTON");
			ThreadSleep();
			ThreadSleep();
			logger.info(performActionGetText("ATCORE_BOOKING_HEADIING"));
			// if(performActionGetText("ATCORE_BOOKING_HEADIING").contains("Booking
			// Overview")){
			logger.info("Booking Overview Page is opened");
			report.log(LogStatus.INFO, "Booking Overview Page is opened");
			Double anitetotal = Double.parseDouble(performActionGetText(
					"ATCORE_BOOKING_TOTALPRICE").trim());
			logger.info("Anite total:" + anitetotal);
			Double anitepaid = Double.parseDouble(performActionGetText(
					"ATCORE_BOOKING_AMOUNTPAID").trim());
			logger.info("Anite paid:" + anitepaid);
			Double aniterem = Double.parseDouble(performActionGetText(
					"ATCORE_BOOKING_REMAININGAMOUNT").trim());
			logger.info("Anite rem:" + aniterem);
			if (anitetotal - (price * (adults + children)) <= 3) {
				logger.info("Total Amount is matching");
				report.log(LogStatus.PASS, "Total amount is matching");
			} else {
				logger.info("Total amount is not matching.Values compared:"
						+ anitetotal + " and " + (price * (adults + children)));
				report.log(LogStatus.FAIL,
						"Total amount is not matching.Values compared:"
								+ anitetotal + " and "
								+ (price * (adults + children)));
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,200)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (valuesMap.get(paid) != null) {
				if (anitepaid.compareTo(amountpaid) == 0) {
					logger.info("Amount Paid is matching");
					report.log(LogStatus.PASS, "Amount Paid is matching");
				} else {
					logger.info("Amount Paid is not matching.Values compared:"
							+ anitepaid + " and " + amountpaid);
					report.log(LogStatus.FAIL,
							"Amount Paid is not matching.Values compared:"
									+ anitepaid + " and " + amountpaid);
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			}
			if (aniterem - rem <= 3) {
				logger.info("Remaining Amount is matching");
				report.log(LogStatus.PASS, "Remaining amount is matching");
			} else {
				logger.info("Remaining amount is not matching.Values compared:"
						+ aniterem + " and " + rem);
				report.log(LogStatus.FAIL,
						"Remaining amount is not matching.Values compared:"
								+ aniterem + " and " + rem);
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			int paxcnt = Integer.parseInt(performActionGetText(
					"ATCORE_BOOKING_PAXCOUNT").trim());
			logger.info("Anite paxcnt:" + paxcnt);
			if (paxcnt == adults + children) {
				logger.info("Number of pax has matched");
				report.log(LogStatus.PASS, "Number of pax has matched");
			} else {
				logger.info("Number of pax has not matched.Values compared:"
						+ paxcnt + " and " + (adults + children));
				report.log(LogStatus.FAIL,
						"Number of pax has not matched.Values compared:"
								+ paxcnt + " and " + (adults + children));
				JavascriptExecutor jse = (JavascriptExecutor) driver;
				jse.executeScript("window.scrollBy(0,200)", "");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (children > 0) {
				List<WebElement> aniteages = driver.findElements(By
						.xpath(getObjectDetails("ATCORE_BOOKING_CHILDAGES")
								.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				logger.info("Anite age count:" + aniteages.size());
				int flag = 0;
				for (int i = 0; i < aniteages.size(); i++) {
					for (int j = 0; j < cages.length; j++) {
						logger.info(aniteages.get(i).getText() + "and"
								+ cages[j]);
						if (aniteages.get(i).getText().trim().equals(cages[j])) {
							flag = flag + 1;
							break;
						}
					}
				}
				if (flag == aniteages.size()) {
					logger.info("Ages of children have matched");
					report.log(LogStatus.PASS, "Ages of children have matched");
				} else {
					logger.info("Ages of children have not matched");
					report.log(LogStatus.FAIL,
							"Ages of children have not matched");
				}
			}
			String airport1 = performActionGetText("ATCORE_BOOKING_AIRPORT1")
					.trim();
			airport1 = airport1.split("Airport")[0].trim();
			logger.info("Airport1:" + airport1);
			String airport2 = performActionGetText("ATCORE_BOOKING_AIRPORT2")
					.trim();
			airport2 = airport2.split("Airport")[0].trim();
			logger.info("Airport2:" + airport2);
			SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyy");
			SimpleDateFormat sdf1 = new SimpleDateFormat("dd MMM yyyy");
			String depdate = performActionGetText("ATCORE_BOOKING_AIRPORTDATE1")
					.trim();
			logger.info("Depdate:" + depdate);
			depdate = sdf1.format(sdf.parse(depdate));
			logger.info("Depdate1:" + depdate);
			String arrdate = performActionGetText(
					"ATCORE_BOOKING_AIRPORT2DATE2").trim();
			logger.info("Arrdate:" + arrdate);
			arrdate = sdf1.format(sdf.parse(arrdate));
			logger.info("Arrdate1:" + arrdate);
			if (outf.contains(airport1)) {
				logger.info("Departure Aiport has matched");
				report.log(LogStatus.PASS, "Departure Aiport has matched");
			} else {
				logger.info("Departure Aiport has not matched.Values compared:"
						+ outf + " and " + airport1);
				report.log(LogStatus.FAIL,
						"Departure Aiport has not matched.Values compared:"
								+ outf + " and " + airport1);
			}
			if (inf.contains(airport2)) {
				logger.info("Arrival Aiport has matched");
				report.log(LogStatus.PASS, "Arrival Aiport has matched");
			} else {
				logger.info("Arrival Aiport has not matched.Values compared:"
						+ inf + " and " + airport2);
				report.log(LogStatus.FAIL,
						"Arrival Aiport has not matched.Values compared:" + inf
								+ " and " + airport2);
			}
			if (outf.contains(depdate)) {
				logger.info("Departure Date has matched");
				report.log(LogStatus.PASS, "Departure Date has matched");
			} else {
				logger.info("Departure Date has not matched.Values compared:"
						+ outf + " and " + depdate);
				report.log(LogStatus.FAIL,
						"Departure Date has not matched.Values compared:"
								+ outf + " and " + depdate);
			}
			if (inf.contains(arrdate)) {
				logger.info("Arrival Date has matched");
				report.log(LogStatus.PASS, "Arrival Date has matched");
			} else {
				logger.info("Arrival Date has not matched.Values compared:"
						+ outf + " and " + arrdate);
				report.log(LogStatus.FAIL,
						"Arrival Date has not matched.Values compared:" + outf
								+ " and " + arrdate);
			}

			if (performActionGetText("ATCORE_BOOKING_HOTELNAME1").contains(
					hotel1)) {
				logger.info("Hotel Name for Centre 1 has matched");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 1 has matched");
			} else {
				logger.info("Hotel Name for Centre 1 has not matched.Values compared:"
						+ performActionGetText("ATCORE_BOOKING_HOTELNAME1")
						+ " and " + hotel1);
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 1 has not matched.Values compared:"
								+ performActionGetText("ATCORE_BOOKING_HOTELNAME1")
								+ " and " + hotel1);
			}
			if (performActionGetText("ATCORE_BOOKING_ROOMBOARD1").contains(
					room1)) {
				logger.info("Room Name for Centre 1 has matched");
				report.log(LogStatus.PASS, "Room Name for Centre 1 has matched");
			} else {
				logger.info("Room Name for Centre 1 has not matched.Values compared:"
						+ performActionGetText("ATCORE_BOOKING_ROOMBOARD1")
						+ " and " + room1);
				report.log(
						LogStatus.FAIL,
						"Room Name for Centre 1 has not matched.Values compared:"
								+ performActionGetText("ATCORE_BOOKING_ROOMBOARD1")
								+ " and " + room1);
			}
			if (performActionGetText("ATCORE_BOOKING_ROOMBOARD1").contains(
					board1)) {
				logger.info("Board Name for Centre 1 has matched");
				report.log(LogStatus.PASS,
						"Board Name for Centre 1 has matched");
			} else {
				logger.info("Board Name for Centre 1 has not matched.Values compared:"
						+ performActionGetText("ATCORE_BOOKING_ROOMBOARD1")
						+ " and " + board1);
				report.log(
						LogStatus.FAIL,
						"Board Name for Centre 1 has not matched.Values compared:"
								+ performActionGetText("ATCORE_BOOKING_ROOMBOARD1")
								+ " and " + board1);
			}
			if (performActionGetText("ATCORE_BOOKING_HOTELNAME2").contains(
					hotel2)) {
				logger.info("Hotel Name for Centre 2 has matched");
				report.log(LogStatus.PASS,
						"Hotel Name for Centre 2 has matched");
			} else {
				logger.info("Hotel Name for Centre 2 has not matched.Values compared:"
						+ performActionGetText("ATCORE_BOOKING_HOTELNAME2")
						+ " and " + hotel2);
				report.log(
						LogStatus.FAIL,
						"Hotel Name for Centre 2 has not matched.Values compared:"
								+ performActionGetText("ATCORE_BOOKING_HOTELNAME2")
								+ " and " + hotel2);
			}
			if (performActionGetText("ATCORE_BOOKING_ROOMBOARD2").contains(
					room2)) {
				logger.info("Room Name for Centre 2 has matched");
				report.log(LogStatus.PASS, "Room Name for Centre 2 has matched");
			} else {
				logger.info("Room Name for Centre 2 has not matched.Values compared:"
						+ performActionGetText("ATCORE_BOOKING_ROOMBOARD2")
						+ " and " + room2);
				report.log(
						LogStatus.FAIL,
						"Room Name for Centre 2 has not matched.Values compared:"
								+ performActionGetText("ATCORE_BOOKING_ROOMBOARD2")
								+ " and " + room2);
			}
			if (performActionGetText("ATCORE_BOOKING_ROOMBOARD2").contains(
					board2)) {
				logger.info("Board Name for Centre 2 has matched");
				report.log(LogStatus.PASS,
						"Board Name for Centre 2 has matched");
			} else {
				logger.info("Board Name for Centre 2 has not matched.Values compared:"
						+ performActionGetText("ATCORE_BOOKING_ROOMBOARD2")
						+ " and " + board2);
				report.log(
						LogStatus.FAIL,
						"Board Name for Centre 2 has not matched.Values compared:"
								+ performActionGetText("ATCORE_BOOKING_ROOMBOARD2")
								+ " and " + board2);
			}
			/*
			 * }else{ logger.info("Booking Overview Page is not opened");
			 * report.log(LogStatus.ERROR,"Booking Overview Page is not opened"
			 * ); report.attachScreenshot(takeScreenShotExtentReports()); }
			 */
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void aniteVerification1(String bookingref, String location) {
		try {
			report.log(LogStatus.INFO, "INTO ANITE VERIFICATION!!!");
			String bookref = valuesMap.get(bookingref)
					.substring(valuesMap.get(bookingref).indexOf("^^") + 2)
					.trim();
			String loc = valuesMap.get(location)
					.substring(valuesMap.get(location).indexOf("^^") + 2)
					.trim();
			logger.info("Values in anite:" + bookingref + " and " + loc);
			String[] locs = loc.split("&");
			performActionMoveToElementAndClick("ATCORE_HOME_BOOKING",
					"ATCORE_BOOKING_RECALL");
			ThreadSleep();
			performActionEnterText_SLF1("ATCORE_RECALL_TEXTBOX", bookref);
			performActionClick("ATCORE_RECALL_SEARCHBUTTON");
			ThreadSleep();
			// if(performActionGetText("ATCORE_BOOKING_HEADIING").contains("Booking
			// Overview")){
			logger.info("Booking Overview Page is opened");
			report.log(LogStatus.INFO, "Booking Overview Page is opened");
			String loc1 = performActionGetText("ATCORE_BOOKING_LOCATION1")
					.trim();
			String loc2 = performActionGetText("ATCORE_BOOKING_LOCATION2")
					.trim();
			if (loc1.equalsIgnoreCase(locs[0].trim())) {
				logger.info("Centre 1 Name has matched");
				report.log(LogStatus.PASS, "Centre 1 Name has matched");
			} else {
				logger.info("Centre 1 Name has not matched");
				report.log(LogStatus.PASS, "Centre 1 Name has not matched");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			if (loc2.equalsIgnoreCase(locs[1].trim())) {
				logger.info("Centre 2 Name has matched");
				report.log(LogStatus.PASS, "Centre 2 Name has matched");
			} else {
				logger.info("Centre 2 Name has not matched");
				report.log(LogStatus.PASS, "Centre 2 Name has not matched");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
			/*
			 * }else{ logger.info("Booking Overview Page is not opened");
			 * report.log(LogStatus.ERROR,"Booking Overview Page is not opened"
			 * ); report.attachScreenshot(takeScreenShotExtentReports()); }
			 */
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void cdmVerification() throws InterruptedException, Exception {
		report.log(LogStatus.INFO, "INTO CDM VERIFICATION!!!");
		Thread.sleep(3000);
		try {
			// Connection URL Syntax:
			// "jdbc:mysql://ipaddress:portnumber/db_name"
			String dbUrl = "jdbc:Oracle:thin:CDM ST2@//10.145.128.184:1521/CDMST2";
			String username = "bhavana"; // Database Username

			String password = "bhavana321"; // Database Password
			String customerid = "";
			Class.forName("oracle.jdbc.driver.OracleDriver"); // Load mysql jdbc
																// driver
			Connection con = DriverManager.getConnection(dbUrl, username,
					password); // Create
								// Connection
								// to
								// DB

			Statement stmt = con.createStatement(); // Create Statement Object

			// //////////////// // First query
			// running///////////////////////////
			try {
				String query = "select * from cdm_hub.customer_bookings where x_anite_booking_ref='"
						+ bookingRefID + "'";
				ResultSet rs = stmt.executeQuery(query);
				// report.log(LogStatus.INFO, "Booking reference: " +2440821);
				if (rs.next()) {
					report.log(LogStatus.PASS,
							"Records are found in customer booking table query");
					report.log(LogStatus.INFO,
							"Booking reference: " + rs.getString(1));
					report.log(LogStatus.INFO,
							"Departure date: " + rs.getString(2));
					report.log(LogStatus.INFO,
							"Booking Status: " + rs.getString(3));
					report.log(LogStatus.INFO,
							"Holiday Brand: " + rs.getString(8));
				} else
					report.log(LogStatus.FAIL,
							"No Records found in customer booking table query");
			} catch (Exception ex) {
				logger.info("No Records found in customerghfbooking table query");
			}
			// ////////////////// Second query
			// running///////////////////////////
			try {
				String query1 = "Select * from cdm_hub.ROLE_CUSTOMER_BOOKING_REL where X_BOOKING_REF_I1560166415='"
						+ bookingRefID + "'";
				ResultSet rs1 = stmt.executeQuery(query1);
				if (rs1.next()) {
					report.log(LogStatus.PASS,
							" Records are found in customer_booking_rel table query");
					report.log(LogStatus.INFO,
							"Customer id: " + rs1.getString(2));
					customerid = rs1.getString(2);
				} else
					report.log(LogStatus.FAIL,
							"No Records found in customer_booking_rel table query");
			} catch (Exception ex) {
				logger.info("No Records found in customer_booking_rel table query");
			}
			// ////////////////// THIRD query running///////////////////////////
			try {
				String query2 = "select * from cdm_hub.customer where X_CUSTOMER_ID='"
						+ customerid + "'";
				ResultSet rs2 = stmt.executeQuery(query2);
				if (rs2.next()) {
					report.log(LogStatus.PASS,
							" Records are found in customer table query");
					report.log(LogStatus.INFO, "Title:  " + rs2.getString(2));
					report.log(LogStatus.INFO,
							"First Name:  " + rs2.getString(3));
					report.log(LogStatus.INFO,
							"Last Name:  " + rs2.getString(6));
					report.log(LogStatus.INFO, "Gender:  " + rs2.getString(7));
				} else
					report.log(LogStatus.FAIL,
							"No Records found in customer_booking_rel table query");
			} catch (Exception ex) {
				logger.info("No Records found in customer_booking_rel table query");
			}
			// ////////////////// Fourth query
			// running///////////////////////////
			try {
				String query3 = "select * from cdm_hub.contactpoint where x_contactpoint_id in (select x_contactpoint__1535319457 from cdm_hub.customer_contact_point where x_customer_id_x_customer_id ='"
						+ customerid + "')";
				ResultSet rs3 = stmt.executeQuery(query3);
				if (rs3.next()) {
					report.log(LogStatus.PASS,
							" Records are found in contact point table query");
					report.log(LogStatus.INFO,
							"Contact Point type:" + rs3.getString(3) + "\t"
									+ "Contact Point value:" + rs3.getString(5));
				} else
					report.log(LogStatus.FAIL,
							"No Records found in contact point table query");
				while (rs3.next())
					report.log(LogStatus.INFO,
							"Contact Point type:" + rs3.getString(3) + "\t"
									+ "Contact Point value:" + rs3.getString(5));
			} catch (Exception ex) {
				logger.info("No Records found in contact point table query");
			}
			con.close(); // closing DB Connection
		} catch (SQLException sqlEx) {
			report.log(LogStatus.INFO, "SQL Exception:" + sqlEx.getStackTrace());
		}
	}

	public void dmsSearchForBooking(String bRefnum, String out, String in,
			String h1, String r1, String b1, String h2, String r2, String b2,
			String holidayprice, String pay, String bal)  {
		report.log(LogStatus.INFO, "INTO DMS VERIFICATION!!!");
		String BookingRef = valuesMap.get(bRefnum)
				.substring(valuesMap.get(bRefnum).indexOf("^^") + 2).trim();
		String totalprice = valuesMap.get(holidayprice)
				.substring(valuesMap.get(holidayprice).indexOf("^^") + 2)
				.trim();
		String airport1 = valuesMap.get(out)
				.substring(valuesMap.get(out).indexOf("^^") + 2).trim();
		String airport2 = valuesMap.get(in)
				.substring(valuesMap.get(in).indexOf("^^") + 2).trim();
		String hot1 = valuesMap.get(h1)
				.substring(valuesMap.get(h1).indexOf("^^") + 2).trim();
		String rm1 = valuesMap.get(r1)
				.substring(valuesMap.get(r1).indexOf("^^") + 2).trim();
		String brd1 = valuesMap.get(b1)
				.substring(valuesMap.get(b1).indexOf("^^") + 2).trim();
		String hot2 = valuesMap.get(h2)
				.substring(valuesMap.get(h2).indexOf("^^") + 2).trim();
		String rm2 = valuesMap.get(r2)
				.substring(valuesMap.get(r2).indexOf("^^") + 2).trim();
		String brd2 = valuesMap.get(b2)
				.substring(valuesMap.get(b2).indexOf("^^") + 2).trim();
		String amtpaid = valuesMap.get(pay)
				.substring(valuesMap.get(pay).indexOf("^^") + 2).trim();
		String amtbal = valuesMap.get(bal)
				.substring(valuesMap.get(bal).indexOf("^^") + 2).trim();
		logger.info("Value in DMS:" + BookingRef + " and " + totalprice
				+ " and " + airport1 + " and " + airport2 + " and " + hot1
				+ " and " + rm1 + " and " + brd1 + " and " + hot2 + " and "
				+ rm2 + " and " + brd2 + " and " + amtpaid + " and " + amtpaid);
		int iteration;
		int flag = 0;
		String mainWindow = driver.getWindowHandle();
		// webDriver.get("https://dms-uat1/dms/cgi/dms-dash.pl");
		driver.get("https://dms-st1/dms/cgi/dms-dash.pl");
		do {
			flag = flag + 1;
			try {
				Thread.sleep(5000l);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			performActionClick("DMS_SEARCH_BUTTON");
			performActionClick("DMS_BOOKREF_RADIO");
			driver.findElement(
					By.xpath(getObjectDetails("DMS_BOOKREF_TEXTBOX").get(
							"Object_Locator_Value"))).clear();
			performActionEnterText("DMS_BOOKREF_TEXTBOX", bRefnum);
			performActionClick("DMS_CONTINUE_SEARCH");
			iteration = flag;
			if (iteration == 10) {

				break;
			}
			waitForElementToBeDisplayed("//*[@id='expand']/p[2]|//*[@id='expand']/table/tbody/tr[2]/td[2]/u");

		} while (driver.findElement(By.xpath(getObjectDetails("DMS_GUID").get(
				"Object_Locator_Value"))) == null);

		if (iteration != 10) {

			logger.info("waiting for the booking details in DMS..");
			List<WebElement> refList = driver.findElements(By
					.xpath("//*[@id='expand']/table/tbody/tr[*]/td[11]"));
			for (WebElement refItem : refList) {
				if (refItem.getText().equalsIgnoreCase(bRefnum)) {
					performActionClick("DMS_GUID");
					// Store the current window handle
					// String winHandleBefore = webDriver.getWindowHandle();

					// Perform the click operation that opens new window
					waitForElementToBeDisplayed(getObjectDetails("DMS_VIEW_DOC")
							.get("Object_Locator_Value"));
					performActionClick("DMS_VIEW_DOC");

					try {
						Thread.sleep(10000l);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Switch to new window opened
					for (String winHandle : driver.getWindowHandles()) {
						driver.switchTo().window(winHandle);
					}
					if (driver.findElement(By.cssSelector("html>body>p")) != null) {
						driver.navigate().refresh();
					}
					// Perform the actions on new window
					waitForElementToBeDisplayed(getObjectDetails(
							"DMS_PDF_REFNUMBER").get("Object_Locator_Value"));
					BookingRef = driver.findElement(
							By.xpath(getObjectDetails("DMS_PDF_REFNUMBER").get(
									"Object_Locator_Value"))).getText();
					if (bRefnum.trim().equalsIgnoreCase(BookingRef.trim())) {
						logger.info("Booking reference got verified. "
								+ BookingRef);
						logger.info("BOOKING REFERENCE GOT VERIFIED. "
								+ BookingRef);
					}

					// goto 2nd page in pdf
					driver.findElement(By.xpath(".//*[@id='next']")).click();
					String depairport = performActionGetText(
							"DMS_PDF_DEPAIRPORT").trim();
					depairport = depairport.split("-")[0].split(",")[0].trim();
					String arrairport = performActionGetText(
							"DMS_PDF_ARRAIRPORT").trim();
					arrairport = arrairport.split("-")[0].split(",")[0].trim();
					logger.info("Aiprorts:" + depairport + " and " + arrairport);
					if (airport1.trim().contains(depairport.trim())) {
						logger.info("DEPATURE AIRPORT GOT VERIFIED SUCCESSFULLY. "
								+ depairport);
						report.log(LogStatus.PASS, "Departure airport matched");
					} else {
						logger.info("Departure airport not matched. "
								+ depairport + " and " + airport1);
						report.log(LogStatus.FAIL,
								"Departure airport not matched. " + depairport
										+ " and " + airport1);
					}
					if (airport2.trim().contains(arrairport.trim())) {
						logger.info("Arrival AIRPORT GOT VERIFIED SUCCESSFULLY. "
								+ depairport);
						report.log(LogStatus.PASS, "Arrival airport matched");
					} else {
						logger.info("Arrival airport not matched. "
								+ arrairport + " and " + airport2);
						report.log(LogStatus.FAIL,
								"Arrival airport not matched. " + arrairport
										+ " and " + airport2);
					}
					// page 3
					driver.findElement(By.xpath(".//*[@id='next']")).click();
					waitForElementToBeDisplayed(getObjectDetails(
							"DMS_PDF_HOTEL1").get("Object_Locator_Value"));
					String hotel1 = performActionGetText("DMS_PDF_HOTEL1")
							.trim();
					String room1 = performActionGetText("DMS_PDF_ROOM").trim();
					String board1 = performActionGetText("DMS_PDF_BOARD1")
							.trim();
					String hotel2 = performActionGetText("DMS_PDF_HOTEL2")
							.trim();
					logger.info("Values in page 3:" + hotel1 + " and " + room1
							+ " and " + board1 + " and " + hotel2);
					if (hotel1.trim().equalsIgnoreCase(hot1.trim())) {
						logger.info("Hotel Name for hotel 1 matched.");
						report.log(LogStatus.PASS,
								"Hotel Name for hotel 1 matched.");
					} else {
						logger.info("Hotel Name for hotel 1 not matched. "
								+ hotel1 + " and " + hot1);
						report.log(LogStatus.FAIL,
								"Hotel Name for hotel 1 not matched. " + hotel1
										+ " and " + hot1);
					}
					if (room1.trim().equalsIgnoreCase(rm1.trim())) {
						logger.info("Room for hotel 1 matched. ");
						report.log(LogStatus.PASS, "Room for hotel 1 matched.");
					} else {
						logger.info("Room for hotel 1 not matched. " + room1
								+ " and " + rm1);
						report.log(LogStatus.FAIL,
								"Room for hotel 1 not matched. " + room1
										+ " and " + rm1);
					}
					if (board1.trim().equalsIgnoreCase(brd1.trim())) {
						logger.info("Board Basis for hotel 1 matched. ");
						report.log(LogStatus.PASS,
								"Board Basis for hotel 1 matched");
					} else {
						logger.info("Board Basis for hotel 1 not matched. "
								+ board1 + " and " + brd1);
						report.log(LogStatus.FAIL,
								"Board Basis for hotel 1 matched. " + board1
										+ " and " + brd1);
					}
					// page 4
					driver.findElement(By.xpath(".//*[@id='next']")).click();
					waitForElementToBeDisplayed(getObjectDetails(
							"DMS_PDF_TOTAL_PRICE").get("Object_Locator_Value"));
					String board2 = performActionGetText("DMS_PDF_BOARD2")
							.trim();
					String room2 = performActionGetText("DMS_PDF_ROOM").trim();
					String finalprice = performActionGetText(
							"DMS_PDF_TOTAL_PRICE").trim();
					String paid = performActionGetText("DMS_PDF_PAID").trim();
					String balance = performActionGetText("DMS_PDF_BALANCE")
							.trim();
					logger.info("value sin 4:" + board2 + " and " + room2
							+ " and " + finalprice + " and " + paid + " and"
							+ balance);
					if (hotel2.trim().equalsIgnoreCase(hot2.trim())) {
						logger.info("Hotel Name for hotel 2 matched.");
						report.log(LogStatus.PASS,
								"Hotel Name for hotel 2 matched.");
					} else {
						logger.info("Hotel Name for hotel 2 not matched. "
								+ hotel2 + " and " + hot2);
						report.log(LogStatus.FAIL,
								"Hotel Name for hotel 2 not matched. " + hotel2
										+ " and " + hot2);
					}
					if (room2.trim().equalsIgnoreCase(rm2.trim())) {
						logger.info("Room for hotel 2 matched. ");
						report.log(LogStatus.PASS, "Room for hotel 2 matched.");
					} else {
						logger.info("Room for hotel 2 not matched. " + room2
								+ " and " + rm2);
						report.log(LogStatus.FAIL,
								"Room for hotel 2 not matched. " + room2
										+ " and " + rm2);
					}
					if (board2.trim().equalsIgnoreCase(brd2.trim())) {
						logger.info("Board Basis for hotel 2 matched. ");
						report.log(LogStatus.PASS,
								"Board Basis for hotel 2 matched");
					} else {
						logger.info("Board Basis for hotel 2 not matched. "
								+ board2 + " and " + brd2);
						report.log(LogStatus.FAIL,
								"Board Basis for hotel 2 matched. " + board2
										+ " and " + brd2);
					}
					if (finalprice.equalsIgnoreCase(holidayprice.trim())) {
						report.log(LogStatus.PASS, "Totalprice got verified : "
								+ finalprice);
						logger.info("TOTALPRICE GOT VERIFIED. " + finalprice);
					} else {
						report.log(LogStatus.FAIL, "Totalprice not macthed : "
								+ finalprice + " and " + totalprice);
						logger.info("TOTALPRICE GOT VERIFIED. " + finalprice
								+ " and " + totalprice);
					}
					if (paid.equalsIgnoreCase(amtpaid.trim())) {
						report.log(LogStatus.PASS,
								"Paid Amount got verified : " + paid);
						logger.info("Paid Amount GOT VERIFIED. " + paid);
					} else {
						report.log(LogStatus.FAIL, "Paid Amount not macthed : "
								+ paid + " and " + amtpaid);
						logger.info("Paid Amount GOT VERIFIED. " + paid
								+ " and " + amtpaid);
					}
					if (balance.equalsIgnoreCase(amtbal.trim())) {
						report.log(LogStatus.PASS,
								"Balance Amount got verified : " + balance);
						logger.info("Balance Amount GOT VERIFIED. " + balance);
					} else {
						report.log(LogStatus.FAIL,
								"Balance Amount not macthed : " + balance
										+ " and " + amtbal);
						logger.info("Balance Amount GOT VERIFIED. " + balance
								+ " and " + amtbal);
					}
					driver.close();
					break;

				} else {
					logger.info(refItem.getText()
							+ "is not matching to record in DMS ");
				}
			}

			driver.switchTo().window(mainWindow);

		} else {
			logger.info("Entry not recorderd in DMS even after 10 itterations");
		}

	}

	public void datahub1() {
		try {
			report.log(LogStatus.INFO, "INTO DATA HUB VERIFICATION!!!");
			WebElement hiddenDiv = driver.findElement(By.id("FETCH_TEST_XML"));
			String n = hiddenDiv.getText(); // does not work (returns "" as
											// expected)
			String script = "return arguments[0].innerHTML";
			n = (String) ((JavascriptExecutor) driver).executeScript(script,
					hiddenDiv);
			logger.info("Text:" + n);
			String book = "2444080";
			n = n.replace("&lt;", "<");
			Thread.sleep(1000);
			n = n.replace("&gt;", ">");
			Thread.sleep(1000);

			n = n.replace("</com:Res_Id>", book + "</com:Res_Id>");
			String myval = "";
			myval = n.substring(n.indexOf("<com:Res_Id>"),
					n.indexOf("</com:Res_Id>"));

			logger.info(myval);

			// ThreadSleep();
			driver.findElement(By.id("XML")).clear();
			Thread.sleep(2000);
			driver.findElement(By.id("XML")).sendKeys(n);
			logger.info("3:" + n);
			// logger.info("2:"+script);
			driver.findElement(By.id("submitBtn")).click();
			ThreadSleep();
			readxml();
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void readxml1() {
		try {
			logger.info("readxml");
			waitForPageLoad(driver);
			WebElement hiddenDiv = driver.findElement(By
					.id("webkit-xml-viewer-source-xml"));
			String n = hiddenDiv.getText(); // does not work (returns "" as
											// expected)
			String script = "return arguments[0].innerHTML";
			n = (String) ((JavascriptExecutor) driver).executeScript(script,
					hiddenDiv);
			n = n.replace("&lt;", "<");
			Thread.sleep(1000);
			n = n.replace("&gt;", ">");
			Thread.sleep(1000);
			String myval, totalprice, departuredate, accomname, TotalPAX, customerid, Totalchd, TotalAd, TotalInf = "";
			myval = n.substring(n.indexOf("<com:Res_Id>"),
					n.indexOf("</com:Res_Id>"));
			report.log(LogStatus.INFO, "[booking refernce]<com:Res_Id> "
					+ myval);
			totalprice = n.substring(n.indexOf("<com:Sell_Prc>"),
					n.indexOf("</com:Sell_Prc>"));
			report.log(LogStatus.INFO, "[Total price]<com:Sell_Prc> "
					+ totalprice);
			departuredate = n.substring(n.indexOf("<com:St_Dt>"),
					n.indexOf("</com:St_Dt>"));
			report.log(LogStatus.INFO, "[Departure date]<com:St_Dt> "
					+ departuredate);
			customerid = n.substring(n.indexOf("<com:Owner_Clt_Id>"),
					n.indexOf("</com:Owner_Clt_Id>"));
			report.log(LogStatus.INFO, "[customer id]<com:Owner_Clt_Id> "
					+ customerid);
			TotalPAX = n.substring(n.indexOf("<com:N_Pax>"),
					n.indexOf("</com:N_Pax>"));
			report.log(LogStatus.INFO, "[Total PAX]<com:Accom_Name> "
					+ TotalPAX);
			TotalAd = n.substring(n.indexOf("<com:N_Adu>"),
					n.indexOf("</com:N_Adu>"));
			report.log(LogStatus.INFO, "[Total Adults]<com:N_Adu> " + TotalAd);
			Totalchd = n.substring(n.indexOf("<com:N_Chd>"),
					n.indexOf("</com:N_Chd>"));
			report.log(LogStatus.INFO, "[Total children]<com:N_Chd> "
					+ Totalchd);
			TotalInf = n.substring(n.indexOf("<com:N_Inf>"),
					n.indexOf("</com:N_Inf>"));
			report.log(LogStatus.INFO, "[Total Infants]<com:N_Inf> " + TotalInf);
			// accomname=n.substring(n.indexOf("<com:Accom_Name>"),
			// n.indexOf("</com:Accom_Name>"));
			// report.log(LogStatus.INFO,"[Accommodation name]<com:Accom_Name> "
			// +accomname);
			// <com:Owner_Clt_Id>
		} catch (Exception e) {
		}
	}

	// madan multi centre**********************
	public void getCentreDurations(String ObjectName)  {
		List<String> cc = performActionMultipleGetText(ObjectName);
		String D1 = cc.get(0);
		logger.info(D1.replaceAll("[^0-9]", ""));
		String D2 = cc.get(1);
		logger.info(D2.replaceAll("[^0-9]", ""));
		int dur1 = Integer.parseInt(D1.replaceAll("[^0-9]", ""));
		int dur2 = Integer.parseInt(D2.replaceAll("[^0-9]", ""));

		if (dur1 > dur2) {
			report.log(LogStatus.INFO, "Centre1 Duration -" + dur1 + " "
					+ "is more than " + "Centre2 Duration -" + dur2);
		}
		if (dur1 < dur2) {
			report.log(LogStatus.INFO, "Centre1 Duration -" + dur1 + " "
					+ "is less than " + "Centre2 Duration -" + dur2);
		}
		if (dur1 == dur2) {
			report.log(LogStatus.INFO, "Centre1 Duration -" + dur1 + " "
					+ "is same as " + "Centre2 Duration -" + dur2);
		}
	}

	// madan multi centre**********************

	// aanchal multi centre
	public void connectionST5() throws InterruptedException, Exception

	{
		Thread.sleep(3000);
		try {
			// Connection URL Syntax:
			// "jdbc:mysql://ipaddress:portnumber/db_name"
			String dbUrl = "jdbc:Oracle:thin:CDM ST2@//10.145.128.184:1521/CDMST2";

			String username = "bhavana"; // Database Username

			String password = "bhavana321"; // Database Password

			Class.forName("oracle.jdbc.driver.OracleDriver"); // Load mysql jdbc
																// driver

			Connection con = DriverManager.getConnection(dbUrl, username,
					password); // Create
								// Connection
								// to
								// DB

			Statement stmt = con.createStatement(); // Create Statement Object
			String customerid = new String();

			// //////////////// // First query
			// running///////////////////////////
			try {
				String query = "select * from cdm_hub.customer_bookings where x_anite_booking_ref='"
						+ bookingRefID + "'";
				ResultSet rs = stmt.executeQuery(query);
				// report.log(LogStatus.INFO, "Booking reference: " +2440821);
				if (rs.next()) {
					report.log(LogStatus.PASS,
							"Records are found in customer booking table query");
					report.log(LogStatus.INFO,
							"Booking reference: " + rs.getString(1));
					report.log(LogStatus.INFO,
							"Departure date: " + rs.getString(2));
					report.log(LogStatus.INFO,
							"Booking Status: " + rs.getString(3));
					report.log(LogStatus.INFO,
							"Holiday Brand: " + rs.getString(8));
				} else
					report.log(LogStatus.FAIL,
							"No Records found in customer booking table query");

			} catch (Exception ex) {
				logger.info("No Records found in customerghfbooking table query");
			}

			// ////////////////// Second query
			// running///////////////////////////

			try {
				String query1 = "Select * from cdm_hub.ROLE_CUSTOMER_BOOKING_REL where X_BOOKING_REF_I1560166415='"
						+ bookingRefID + "'";
				ResultSet rs1 = stmt.executeQuery(query1);

				if (rs1.next()) {
					report.log(LogStatus.PASS,
							" Records are found in customer_booking_rel table query");
					report.log(LogStatus.INFO,
							"Customer id: " + rs1.getString(2));
					customerid = rs1.getString(2);
				} else
					report.log(LogStatus.FAIL,
							"No Records found in customer_booking_rel table query");

			} catch (Exception ex) {
				logger.info("No Records found in customer_booking_rel table query");
			}

			// ////////////////// THIRD query running///////////////////////////

			try {
				String query2 = "select * from cdm_hub.customer where X_CUSTOMER_ID='"
						+ customerid + "'";
				ResultSet rs2 = stmt.executeQuery(query2);

				if (rs2.next()) {
					report.log(LogStatus.PASS,
							" Records are found in customer table query");
					report.log(LogStatus.INFO, "Title:  " + rs2.getString(2));
					report.log(LogStatus.INFO,
							"First Name:  " + rs2.getString(3));
					report.log(LogStatus.INFO,
							"Last Name:  " + rs2.getString(6));
					report.log(LogStatus.INFO, "Gender:  " + rs2.getString(7));
				} else
					report.log(LogStatus.FAIL,
							"No Records found in customer_booking_rel table query");

			} catch (Exception ex) {
				logger.info("No Records found in customer_booking_rel table query");
			}

			// ////////////////// Fourth query
			// running///////////////////////////

			try {

				String query3 = "select * from cdm_hub.contactpoint where x_contactpoint_id in (select x_contactpoint__1535319457 from cdm_hub.customer_contact_point where x_customer_id_x_customer_id ='"
						+ customerid + "')";
				ResultSet rs3 = stmt.executeQuery(query3);

				if (rs3.next()) {
					report.log(LogStatus.PASS,
							" Records are found in contact point table query");
					report.log(LogStatus.INFO,
							"Contact Point type:" + rs3.getString(3) + "\t"
									+ "Contact Point value:" + rs3.getString(5));
				} else
					report.log(LogStatus.FAIL,
							"No Records found in contact point table query");
				while (rs3.next())

					report.log(LogStatus.INFO,
							"Contact Point type:" + rs3.getString(3) + "\t"
									+ "Contact Point value:" + rs3.getString(5));

			} catch (Exception ex) {
				logger.info("No Records found in contact point table query");
			}

			con.close(); // closing DB Connection
		} catch (SQLException sqlEx) {
			report.log(LogStatus.INFO, "SQL Exception:" + sqlEx.getStackTrace());
		}
	}

	public void datahub() {
		try {
			WebElement hiddenDiv = driver.findElement(By.id("FETCH_TEST_XML"));
			String n = hiddenDiv.getText(); // does not work (returns "" as
											// expected)
			String script = "return arguments[0].innerHTML";
			n = (String) ((JavascriptExecutor) driver).executeScript(script,
					hiddenDiv);
			logger.info("Text:" + n);
			String book = "3300298";
			n = n.replace("&lt;", "<");
			Thread.sleep(1000);
			n = n.replace("&gt;", ">");
			Thread.sleep(1000);

			n = n.replace("</com:Res_Id>", book + "</com:Res_Id>");
			String myval = "";
			myval = n.substring(n.indexOf("<com:Res_Id>"),
					n.indexOf("</com:Res_Id>"));

			logger.info(myval);

			// ThreadSleep();
			driver.findElement(By.id("XML")).clear();
			Thread.sleep(2000);
			driver.findElement(By.id("XML")).sendKeys(n);
			logger.info("3:" + n);
			// logger.info("2:"+script);
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void readxml() {
		try {
			WebElement hiddenDiv = driver.findElement(By
					.id("webkit-xml-viewer-source-xml"));
			String n = hiddenDiv.getText(); // does not work (returns "" as
											// expected)
			String script = "return arguments[0].innerHTML";
			n = (String) ((JavascriptExecutor) driver).executeScript(script,
					hiddenDiv);

			n = n.replace("&lt;", "<");
			Thread.sleep(1000);
			n = n.replace("&gt;", ">");
			Thread.sleep(1000);
			String myval, totalprice, departuredate, accomname, TotalPAX, customerid, Totalchd, TotalAd, TotalInf = "";

			myval = n.substring(n.indexOf("<com:Res_Id>"),
					n.indexOf("</com:Res_Id>"));
			report.log(LogStatus.INFO, "[booking refernce]<com:Res_Id> 	"
					+ myval);

			totalprice = n.substring(n.indexOf("<com:Sell_Prc>"),
					n.indexOf("</com:Sell_Prc>"));
			report.log(LogStatus.INFO, "[Total price]<com:Sell_Prc>	"
					+ totalprice);

			departuredate = n.substring(n.indexOf("<com:St_Dt>"),
					n.indexOf("</com:St_Dt>"));
			report.log(LogStatus.INFO, "[Departure date]<com:St_Dt> 	"
					+ departuredate);

			customerid = n.substring(n.indexOf("<com:Owner_Clt_Id>"),
					n.indexOf("</com:Owner_Clt_Id>"));
			report.log(LogStatus.INFO, "[customer id]<com:Owner_Clt_Id> 	"
					+ customerid);

			TotalPAX = n.substring(n.indexOf("<com:N_Pax>"),
					n.indexOf("</com:N_Pax>"));
			report.log(LogStatus.INFO, "[Total PAX]<com:Accom_Name> 	"
					+ TotalPAX);

			TotalAd = n.substring(n.indexOf("<com:N_Adu>"),
					n.indexOf("</com:N_Adu>"));
			report.log(LogStatus.INFO, "[Total Adults]<com:N_Adu> 	" + TotalAd);

			Totalchd = n.substring(n.indexOf("<com:N_Chd>"),
					n.indexOf("</com:N_Chd>"));
			report.log(LogStatus.INFO, "[Total children]<com:N_Chd> 	"
					+ Totalchd);

			TotalInf = n.substring(n.indexOf("<com:N_Inf>"),
					n.indexOf("</com:N_Inf>"));
			report.log(LogStatus.INFO, "[Total Infants]<com:N_Inf> 	"
					+ TotalInf);

			// accomname=n.substring(n.indexOf("<com:Accom_Name>"),
			// n.indexOf("</com:Accom_Name>"));
			// report.log(LogStatus.INFO,"[Accommodation name]<com:Accom_Name> "
			// +accomname);

			// <com:Owner_Clt_Id>
		} catch (Exception e) {

		}
	}

	public void pagesourcing() {
		String page_source = driver.getPageSource();
		boolean b = page_source.contains("Service Temporarily Unavailable");
		// page_source.
		// logger.info(b);
	}

	public void patternMatching() {
		String line = "This order was placed for QT3000! OK?";
		String pattern = "(.*)(\\d+)(.*)";

		// Create a Pattern object
		Pattern r = Pattern.compile(pattern);

		// Now create matcher object.
		Matcher m = r.matcher(line);
		if (m.find()) {
			logger.info("Found value: " + m.group(0));
			logger.info("Found value: " + m.group(1));
			logger.info("Found value: " + m.group(2));
		} else {
			logger.info("NO MATCH");
		}
	}

	// madan multi centre close

	/**
	 * The method is used to report page url
	 * 
	 * @author Kaushik It returns void
	 * @param
	 */
	public void reportUrl() {
		String url = null;
		try {
			url = driver.getCurrentUrl();
			report.log(LogStatus.INFO, "Page Url: " + url);
		} catch (Exception e) {
			logger.info(e);
		}
	}

	/**
	 * Method to enterpassenger details
	 * 
	 * @date nov 2016
	 * @param String
	 * 
	 * @author Kaushik
	 * @return void
	 */
	public void EnterpassengerDetailsFalcon() throws InterruptedException {
		try {
			List<WebElement> ele = driver.findElements(By
					.xpath(".//*[contains(@id,'title')]"));
			List<WebElement> fname = driver
					.findElements(By
							.xpath(".//*[contains(@id,'first-name') or contains(@id,'firstName')]"));
			List<WebElement> lname = driver
					.findElements(By
							.xpath(".//*[contains(@id,'last-name') or contains(@id,'surName')]"));
			// List<WebElement>
			// adultageBoxes=driver.findElements(By.xpath(".//*[contains(@id,'dobNode')]/input[contains(@id,'dob')]"));
			List<WebElement> adultageBoxes = driver
					.findElements(By
							.xpath("//h2[contains(.,'Adult')]/following-sibling::div[1]//div[contains(@data-dojo-type,'tui.widget.bookflow.views.passengerDetails.ValidationDOB')]/input[1]"));
			List<WebElement> childBoxes = driver
					.findElements(By
							.xpath("//h2[contains(.,'Child') or contains(.,'Infant')]/following-sibling::div[1]//div[4]//input[1]"));
			List<WebElement> insuranceSelect = driver.findElements(By
					.xpath(".//select[contains(@id,'insurance')]"));
			List<String> multVal = new ArrayList<String>();
			List<WebElement> Cages = driver.findElements(By
					.xpath(".//span[@class='size-15']"));

			// performActionSelectDropDown_Select("FO_PASSENGERPAGE_COUNTRY","Australia");

			driver.findElement(By.xpath(".//*[@id='address-one']")).sendKeys(
					"123123");
			report.log(LogStatus.INFO, "House Number is Entered as : 123123");
			driver.findElement(By.xpath(".//*[@id='address-two']")).sendKeys(
					"AutoAdderss");
			report.log(LogStatus.INFO, "Address 1 is Entered as : AutoAdderss");
			driver.findElement(By.xpath(".//*[@id='city-town']")).sendKeys(
					"Hyderabad");
			report.log(LogStatus.INFO, "Town/city is Entered as : Hyderabad");
			driver.findElement(By.xpath(".//*[@id='county']")).sendKeys(
					"Australia");
			report.log(LogStatus.INFO, "Country is Entered as : Australia");

			driver.findElement(By.xpath("//input[@name='postCode']")).sendKeys(
					"TW33TL");// fdsf
			report.log(LogStatus.INFO, "PostCode is Entered as : TW33TL");
			driver.findElement(By.xpath(".//*[@id='tel']")).sendKeys(
					"8143252685");
			report.log(LogStatus.INFO,
					"Telephone Number is Entered as : 8143252685");
			driver.findElement(By.xpath(".//*[@id='email']")).sendKeys(
					"Mobile.automation@sonata-software.com");
			report.log(LogStatus.INFO,
					"Email Address is Entered as : Mobile.automation@sonata-software.com");
			driver.findElement(By.xpath(".//*[@id='confirmationEmail']"))
					.sendKeys("Mobile.automation@sonata-software.com");
			report.log(LogStatus.INFO,
					"Confirm Email Address is Entered as : Mobile.automation@sonata-software.com");

			if (isPresent("Thomson_PassengerDetail_insuranceSelect")) {
				for (int i = 0; i < insuranceSelect.size(); i++) {
					Select sel = new Select(insuranceSelect.get(i));
					sel.selectByVisibleText("Single trip - Age 18-64");
					// driver.findElement(By.xpath(".//*[@id='dob0']")).sendKeys("30/11/1985");
					report.log(LogStatus.INFO,
							"Insurance type is selected as Single trip");
				}
			}
			Thread.sleep(1000);
			for (int i = 0; i < ele.size(); i++) {
				Select sel = new Select(ele.get(i));
				sel.selectByIndex(1);
				fname.get(i).sendKeys("Automation");
				report.log(LogStatus.INFO,
						"FirstName is Entered as : Automation");
				lname.get(i).sendKeys("Kumar");
				report.log(LogStatus.INFO, "surName is Entered as : Kumar");
			}
			int ChiladAge;
			for (int i = 0; i < Cages.size(); i++) {
				String Agetext1 = Cages.get(i).getText()
						.replaceAll("[^0-9,-]", "");
				if (Agetext1.contains("-")) {
					ChiladAge = Character.getNumericValue(Agetext1.charAt(2));
					ChiladAge = ChiladAge - 1;
				} else {
					ChiladAge = Integer.parseInt(Agetext1);
				}
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				String date = sdf.format(DateUtils.addYears(new Date(),
						-ChiladAge));
				multVal.add(String.valueOf(date));
			}
			for (int i = 0; i < childBoxes.size(); i++) {
				childBoxes.get(i).sendKeys(multVal.get(i));
				report.log(LogStatus.INFO,
						"Childage Date Of Birth Entered as :" + multVal.get(i));
			}
			for (int i = 0; i < adultageBoxes.size(); i++) {
				driver.findElement(By.xpath(".//*[@id='dob0']")).sendKeys(
						"30/11/1985");
				adultageBoxes.get(i).sendKeys("30/11/1984");
				report.log(LogStatus.INFO,
						"Adult age Date Of Birth Entered as :" + "30/11/1984");
			}

		} catch (Exception e) {
			logger.info(e);
		}
	}

	public void sortFromLowToHighPrice() {
		try {
			int index = 0;
			List<Integer> intprices = new ArrayList<Integer>();
			List<Integer> AllPrices = new ArrayList<Integer>();

			performActionClick("THOMSON_SEARCHRES_SORTBYFILTER");
			Thread.sleep(2000);
			performActionClick("THOMSON_SORTBYFILTER_LOWTOHIGHPRICE");
			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			Thread.sleep(15000);
			if (holicount == 1) {
				report.log(LogStatus.PASS,
						"Only 1 result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO, "Total Number of Holidays Found:"
						+ holicount);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					List<String> prices = performActionMultipleGetText("THOMSON_SEARCHRESULTS_PRICEPERPERSONLIST");
					;
					for (String each : prices) {
						intprices.add(Integer.parseInt(each.replaceAll(
								"[^0-9]", "")));
					}
					report.log(
							LogStatus.INFO,
							"Number of Holidays displayed in Page ("
									+ (index + 1) + ") are : "
									+ intprices.size()
									+ " and their values are : " + intprices);
					AllPrices.addAll(intprices);
					intprices.clear();
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));

				boolean sorted = Ordering.natural().isOrdered(
						(Iterable<? extends Comparable>) AllPrices);
				if (sorted) {
					report.log(LogStatus.PASS,
							"Results are sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				} else {
					report.log(
							LogStatus.FAIL,
							"Results are not sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				}
				if (AllPrices.size() == holicount) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays count : "
									+ holicount
									+ " is equal to Total number of results displayed : "
									+ AllPrices.size());

				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays count : "
									+ holicount
									+ " is not equal to Total number of results displayed : "
									+ AllPrices.size());
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	/**
	 * The method is used to Sort High to Low Results returns void
	 */
	public void sortFromHighToLowPrice() {
		try {
			int index = 0;
			List<Integer> intprices = new ArrayList<Integer>();
			List<Integer> AllPrices = new ArrayList<Integer>();

			performActionClick("THOMSON_SEARCHRES_SORTBYFILTER");
			Thread.sleep(2000);
			performActionClick("THOMSON_SORTBYFILTER_HIGHLOWTOPRICE");
			Thread.sleep(15000);
			int holicount = Integer.parseInt(performActionGetText(
					"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").replaceAll("[^0-9]",
					""));
			if (holicount == 1) {
				report.log(LogStatus.PASS,
						"Only 1 result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO, "Total Number of Holidays Found:"
						+ holicount);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					List<String> prices = performActionMultipleGetText("THOMSON_SEARCHRESULTS_PRICEPERPERSONLIST");
					;
					for (String each : prices) {
						intprices.add(Integer.parseInt(each.replaceAll(
								"[^0-9]", "")));
					}
					report.log(
							LogStatus.INFO,
							"Number of Holidays displayed in Page ("
									+ (index + 1) + ") are : "
									+ intprices.size()
									+ " and their values are : " + intprices);
					AllPrices.addAll(intprices);
					intprices.clear();
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));

				boolean sorted = Ordering.natural().reverse()
						.isOrdered((Iterable<? extends Comparable>) AllPrices);
				if (sorted) {
					report.log(LogStatus.PASS,
							"Results are sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				} else {
					report.log(
							LogStatus.FAIL,
							"Results are not sorted and displayed correctly in order and list of values are "
									+ AllPrices);
				}
				if (AllPrices.size() == holicount) {
					report.log(
							LogStatus.PASS,
							"Total Number of Holidays count : "
									+ holicount
									+ " is equal to Total number of results displayed : "
									+ AllPrices.size());
				} else {
					report.log(
							LogStatus.FAIL,
							"Total Number of Holidays count : "
									+ holicount
									+ " is not equal to Total number of results displayed : "
									+ AllPrices.size());
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	/**
	 * The method is used to Sort By Departure Date Results returns void
	 */
	public void sortFromDepDate() {
		try {
			int index = 0;
			List<Date> datestexts = new ArrayList<>();
			SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM yyyy");
			performActionClick("THOMSON_SEARCHRES_SORTBYFILTER");
			Thread.sleep(2000);
			performActionClick("THOMSON_SORTBYFILTER_DEPARTUREDATE");
			Thread.sleep(15000);
			int holicount = Integer
					.parseInt(performActionGetText(
							"THOMSON_SEARCHRESULTS_HOLIDAYSCOUNT").trim()
							.split(" ")[0]);
			if (holicount == 1) {
				report.log(LogStatus.PASS,
						"There is only one result found in Results Page.Hence it is sorted");
			} else {
				report.log(LogStatus.INFO,
						"There are more results to sort.Holiday count:"
								+ holicount);
				do {
					if (index != 0) {
						performActiongetView("THOMSON_SEARCHRESULTS_NEXTVIEW");
						performActionClick("THOMSON_SEARCHRESULTS_NEXT");
						Thread.sleep(15000);
					}
					List<WebElement> dates = driver.findElements(By
							.xpath(getObjectDetails(
									"THOMSON_SEARCHRESULTS_DEPARTUREDATESLIST")
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement each : dates) {
						datestexts.add(sdf.parse(each.getText().split("-")[1]
								.trim().substring(
										0,
										each.getText().split("-")[1].trim()
												.length() - 1)));
					}
					for (int d = 1; d < datestexts.size(); d++) {

						if ((datestexts.get(d - 1).before(datestexts.get(d)))
								|| (datestexts.get(d - 1).equals(datestexts
										.get(d)))) {
							report.log(
									LogStatus.PASS,
									"Result "
											+ (d)
											+ " is lesser or equal to Result "
											+ (d + 1)
											+ " with respect to the Departure Date in Page "
											+ (index + 1));
						} else {
							report.log(
									LogStatus.FAIL,
									"Result "
											+ (d)
											+ " is not lesser or equal to than Result "
											+ (d + 1)
											+ " with respect to the Departure Date in Page "
											+ (index + 1));
						}
					}
					Date finaldate = sdf.parse(dates.get(dates.size() - 1)
							.getText().split("-")[1].trim().substring(
							0,
							dates.get(dates.size() - 1).getText().split("-")[1]
									.trim().length() - 1));
					datestexts.clear();
					datestexts.add(finaldate);
					index++;
				} while (IsElementPresent("THOMSON_SEARCHRESULTS_NEXT"));
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public void closeMailPopup() {
		try {
			Thread.sleep(1000);

			if (driver instanceof JavascriptExecutor) {
				((JavascriptExecutor) driver)
						.executeScript("document.querySelector('acsCloseButton--link acsCloseButton acsDeclineButton').click();");
				logger.info("Mail popup closed");
			}

		} catch (Exception e) {
			// e.printStackTrace();
		}
	}

	public void ValidatingTextWithStaticText(String pageName,
			String ExpectedText, String ActualText) {
		try {
			/*
			 * String Actual = valuesMap.get( ActualText).substring(
			 * valuesMap.get( ActualText).indexOf("^^") + 2);
			 */
			String Actual = performActionGetText(ActualText);

			if (Actual.contains(ExpectedText) || ExpectedText.contains(Actual)) {
				report.log(LogStatus.PASS, "Verifying condition passed at "
						+ pageName + "Expected Text" + " " + ExpectedText + " "
						+ "Actual Text" + " " + Actual);
			} else {
				report.log(LogStatus.FAIL, "Verifying condition failed at "
						+ pageName + "Expected Text" + " " + ExpectedText + " "
						+ "Actual Text" + " " + Actual);
				report.attachScreenshot(takeScreenShotExtentReports());
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error While Validating text due to "
					+ e);
		}
	}

	public void ValidatingText(String pageName, String ExpectedText,
			String ActualText) {
		try {
			String expected = valuesMap.get(ExpectedText).substring(
					valuesMap.get(ExpectedText).indexOf("^^") + 2);

			/*
			 * String Actual = valuesMap.get( ActualText).substring(
			 * valuesMap.get( ActualText).indexOf("^^") + 2);
			 */
			String Actual = performActionGetText(ActualText);

			if (!(Actual.equals("") || expected.equals(""))) {
				if (Actual.contains(expected) || expected.contains(Actual)) {
					report.log(LogStatus.PASS, "Verifying condition passed at "
							+ pageName + "Expected Text" + " " + expected + " "
							+ "Actual Text" + " " + Actual);
				} else {
					report.log(LogStatus.FAIL, "Verifying condition failed at "
							+ pageName + "Expected Text" + " " + expected + " "
							+ "Actual Text" + " " + Actual);
					report.attachScreenshot(takeScreenShotExtentReports());
				}
			} else {
				report.log(LogStatus.FAIL,
						"unable to get the actual text or expected text");
				report.attachScreenshot(takeScreenShotExtentReports());
			}

		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error While Validating text due to "
					+ e);
		}
	}

	public void closeWindow() throws InterruptedException {
		driver.close();
		Thread.sleep(2000);
	}

	@SuppressWarnings("unused")
	public void linksValidationComponentWise(String pageName, String objectName) {
		objectDetails = getObjectDetails(objectName);
		String pageResponse = "";
		List<WebElement> urlLink, headerLink, footerLink;
		List<String> finalList, hedrFooter;
		urlLink = new ArrayList<WebElement>();
		headerLink = new ArrayList<WebElement>();
		footerLink = new ArrayList<WebElement>();

		finalList = new ArrayList<String>();
		hedrFooter = new ArrayList<String>();
		int invalidLinks = 0;
		try {

			WebElement _header = driver.findElement(By.xpath(objectDetails
					.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
			headerLink = _header.findElements(By.tagName("a"));
			finalList = filterUrls(headerLink);

			for (int i = 0; i < finalList.size(); i++) {
				URL url = null;
				HttpURLConnection connect;
				pageResponse = "";
				// HttpGet request = new HttpGet(finalList.get(i));
				try {
					String line;
					url = new URL(finalList.get(i));
					connect = (HttpURLConnection) url.openConnection();
					Thread.sleep(2000);
					connect.setRequestMethod("GET");
					connect.connect();
					int _code = connect.getResponseCode();
					Thread.sleep(2000);
					String _message = connect.getResponseMessage()
							.toLowerCase();
					Thread.sleep(5000);
					StringBuilder builder = new StringBuilder();
					// InputStream stream = connect.getInputStream();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(connect.getInputStream()));
					Thread.sleep(7000);
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
					reader.close();
					if (builder.toString().contains("Technical Difficulties")) {
						pageResponse = "Technical Difficulties";
					}
					if (builder.toString().contains(
							"Service temporarily unavailable")) {
						pageResponse = "Service temporarily unavailable";
					}
					if (builder.toString().contains("All Gone")) {
						pageResponse = "All Gone";
					}
					if (builder.toString().length() == 0) {
						pageResponse = "Server Returned Blank Page";
					}

					// logger.info("The
					// url$$$$$$$$$$$$$$:"+builder.toString().length()+"\t"+url);

					if (_code == 200 && _message.equalsIgnoreCase("ok")) {
						report.log(LogStatus.PASS, "LinksStatus"
								+ " Verifying condition is passed from "
								+ pageName + " page where Expected:"
								+ "200 and ok" + " " + "Actual:" + _code
								+ " and " + _message + " " + pageResponse
								+ " URl:" + url);
						logger.info("Comparingsummary parameters" + "-"
								+ "Expected" + "200 and ok" + " " + "Actual"
								+ +_code + " and " + _message + " "
								+ pageResponse + " URl:" + url);

						// logger.info("Links Verfifying condition is passed
						// from page where Expected:200 and ok Actual:"+_code+"
						// and "+_message);
					} else {
						report.log(LogStatus.FAIL, "LinksStatus"
								+ " Verifying condition is failed from "
								+ pageName + " page where Expected:"
								+ "200 and ok" + " " + "Actual:" + _code
								+ " and " + _message + " " + pageResponse
								+ " URl:" + url);
						logger.info("Comparingsummary parameters" + "-"
								+ "Expected" + "200 and ok" + " " + "Actual"
								+ +_code + " and " + _message + " "
								+ pageResponse + " URl:" + url);
						invalidLinks++;
					}
				} catch (FileNotFoundException e) {
					report.log(LogStatus.FAIL, "LinksStatus"
							+ " Verifying condition is failed from " + pageName
							+ " page where Expected:" + "200 and ok" + " "
							+ "Actual:" + "File Not found Exception" + " URl:"
							+ url);
					logger.info("Links"
							+ " could not captured from the web page **********");
					logger.error(e);
				} catch (Exception e) {
					report.log(LogStatus.FAIL, "LinksStatus"
							+ " Verifying condition is failed from " + pageName
							+ " page where Expected:" + "200 and ok" + " "
							+ "Actual:" + "" + " URl:" + url);
					logger.info("Links"
							+ " could not captured from the web page **********");
					logger.error(e);
				}
			}

		} catch (Exception e) {
			logger.info("Links"
					+ " could not captured from the web page **********");
			logger.error(e);
		}

	}

	public void bookingVerificationatPortal(String hotel, String depDate,
			String reference) {
		boolean flag = false;
		String newDt;
		try {
			List<WebElement> hotelName = driver
					.findElements(By
							.xpath("//h2[text()='Your Current Bookings']/../following-sibling::div//h2"));
			List<WebElement> date = driver
					.findElements(By
							.xpath("//h2[text()='Your Current Bookings']/../following-sibling::div//p[2]"));

			for (int i = 0; i < hotelName.size(); i++) {

				logger.info("hotel: " + hotelName.get(i).getText()
						+ "departure: " + date.get(i).getText());
				newDt = monthFormat(depDate);
				if (hotel.contains(hotelName.get(i).getText())
						&& date.get(i).getText().equalsIgnoreCase(newDt)) {
					flag = true;
					break;
				}
			}
			if (flag) {
				logger.info("Booking is diplayed in portal page with hotel name: "
						+ hotel);
				report.log(LogStatus.PASS,
						"Booking is displayed at portal page having accomodation name "
								+ hotel + " and reference id: " + reference);
			} else {
				logger.info("Booking has not diplayed in portal page with hotel name: "
						+ hotel);
				report.log(LogStatus.FAIL,
						"Booking has not displayed at portal page having accomodation name "
								+ hotel + " and reference id: " + reference);
			}

		} catch (Exception e) {

		}
	}

	public String monthFormat(String date) throws java.text.ParseException {
		// String a="28 Mar 2017";
		String[] split = null;
		try {
			split = date.trim().split(" ");

			Date date1 = new SimpleDateFormat("MMM", Locale.ENGLISH)
					.parse(split[2]);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date1);
			int month = cal.get(Calendar.MONTH);
			String aa = new DateFormatSymbols().getMonths()[month];
			logger.info(aa);

			return aa;
		} catch (Exception e) {
			logger.info(e);
		}
		return date;

	}

	// =======Aanchal======

	public void selectCountry(String objectName, String testData) {
		try {
			String[] objVal = objectName.split(",");
			// List<WebElement> textBoxes;
			testData = fetchTestDataFromMap(testData);

			logger.info("Type is :" + testData);
			int flagtype = 0, flagtype2 = 0, index = 0;
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("window.scrollBy(0,-250)", "");
			do {
				if (index != 0) {
					performActionClick(objVal[0]);
					ThreadSleep();
				}
				jse.executeScript("window.scrollBy(0,-250)", "");

				List<WebElement> names = driver.findElements(By
						.xpath(getObjectDetails(objVal[1]).get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));
				List<WebElement> buttons = driver.findElements(By
						.xpath(getObjectDetails(objVal[2]).get(
								CONSTANTS_OBJECT_LOCATOR_VALUE)));

				logger.info("Names count , Select buttons count:"
						+ names.size() + " and " + buttons.size());

				if (!testData.equalsIgnoreCase("Tour")) {
					logger.info("We need to select 2 Hotels Stay!");
					report.log(LogStatus.INFO,
							"We need to select 2 Hotels Stay!");
					for (int i = 0; i < names.size(); i++) {
						logger.info("Name:" + names.get(i).getText());
						if (!names.get(i).getText().contains("Tour")) {
							flagtype = 1;
							logger.info("Found Hotel Stay!!!");
							report.log(LogStatus.INFO, "Found Hotel Stay!!!");
							buttons.get(i).click();
							ThreadSleep();
							break;
						}
					}
				}

				else {
					logger.info("We need to select Tour Stay!");
					report.log(LogStatus.INFO, "We need to select Tour Stay!");
					for (int i = 0; i < names.size(); i++) {

						if (names.get(i).getText().contains("Tour")) {
							logger.info("Name:" + names.get(i).getText());
							flagtype = 1;
							logger.info("Found Tour Stay!!!");
							report.log(LogStatus.INFO, "Found Tour Stay!!!");
							buttons.get(i).click();
							ThreadSleep();
							break;
						}
					}
				}
				if (flagtype == 1) {
					logger.info("Given type is found in Search Results Page.Hence moving to Hotel Select Hub Page");
					flagtype2 = 1;
					break;
				} else {
					index++;
				}
			} while (driver.findElement(By.xpath(getObjectDetails("NxtBut")
					.get(CONSTANTS_OBJECT_LOCATOR_VALUE))) != null);
			if (flagtype2 == 1) {
				report.log(
						LogStatus.INFO,
						"Given type is found in Search Results Page.Hence moving to Hotel Select Hub Page");
			} else {
				logger.info("Given type is not found in Search Results Page.Hence not moving to Hotel Select Hub Page");
				report.log(
						LogStatus.ERROR,
						"Given type is not found in Search Results Page.Hence not moving to Hotel Select Hub Page");
				report.attachScreenshot(takeScreenShotExtentReports());
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR, "Error due to:" + e.getMessage());
		}
	}

	public List<String> randomClickandGetText_MC(String objectName1,
			String objectName2) {
		objectDetails = getObjectDetails(objectName1);
		String value1 = null, value2 = null;
		List<WebElement> wd1;
		List<String> wd2, wd4;
		wd4 = new ArrayList<String>();
		Random rd = new Random();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				Thread.sleep(100);
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				int y = rd.nextInt(wd1.size());
				logger.info("the y:" + y + "\t" + wd1.size() + "\t"
						+ wd2.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				Actions actions = new Actions(driver);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName2);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty() && value2.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + "is selcted as" + "<strong>"
						+ value1 + "</strong>");
				report.log(LogStatus.INFO, objectName1 + " " + "is selcted as"
						+ "<strong>" + value1 + "</strong>");
			}
			wd4.add(value1);
		} catch (Exception e) {
			logger.info(objectName1 + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);

		}
		return wd4;

	}

	public void roomupgradation_MC(String tpPrice,
			String totalPriceAfterupgradation, String diffTotalPrice,
			String Roomupgradebutton)  {
		DecimalFormat fmt = new DecimalFormat("#.00");
		String totPrcBfrSel, totPrcAftrSel, addedTotal = null, tpp = null;
		boolean flag;

		totPrcBfrSel = performActionGetText(tpPrice);
		report.log(LogStatus.INFO,
				"The total price before selecting alternate: " + totPrcBfrSel);
		List<String> prcDetails = randomClickandGetText_MC(diffTotalPrice);
		try {
			tpp = prcDetails.get(0).replaceAll("\\s*\\([^\\)]+\\)", "")
					.replaceAll("[^0-9\\.]+", "");

			report.log(LogStatus.INFO,
					"Diffrence total person price after selecting alternate: "
							+ tpp);

			if (totPrcBfrSel != null) {
				addedTotal = prcAdder(tpp, totPrcBfrSel);
				report.log(
						LogStatus.INFO,
						"The alternate differe  total price and total price before select "
								+ prcDetails.get(0).replaceAll(
										"\\s*\\([^\\)]+\\)", "") + "+"
								+ totPrcBfrSel.replaceAll("[^0-9\\.]+", "")
								+ "=" + addedTotal);
			}
			PerformActionSleep("1000");
			totPrcAftrSel = performActionGetText(tpPrice);
			try {
				totPrcAftrSel = totPrcAftrSel.replaceAll("[^0-9\\.]+", "");

				if (Math.round(Double.parseDouble(addedTotal)) * 100.0 / 100.0 == Math
						.round(Double.parseDouble(totPrcAftrSel)) * 100.0 / 100.0) {
					flag = true;
					compareStringOnePage(tpPrice, flag, addedTotal,
							totPrcAftrSel);
				} else {
					flag = true;
					compareStringOnePage(tpPrice, flag, addedTotal,
							totPrcAftrSel);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error occured in the roomupgradation_MC method :");
		}
	}

	public List<String> randomClickandGetText_MC(String objectName1) {
		objectDetails = getObjectDetails(objectName1);
		String value1 = null;
		List<WebElement> wd1;
		List<String> wd2, wd4;
		wd4 = new ArrayList<String>();
		Random rd = new Random();
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR).equalsIgnoreCase(
					CONSTANTS_OBJECT_LOCATOR_ID)) {

				wd1 = driver.findElements(By.id(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName1);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
				wd1 = driver.findElements(By.name(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName1);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
				Thread.sleep(100);
				wd1 = driver.findElements(By.xpath(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName1);
				int y = rd.nextInt(wd1.size());
				logger.info("the y:" + y + "\t" + wd1.size() + "\t"
						+ wd2.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				Actions actions = new Actions(driver);
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + " is selcted");

			}

			else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
					.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {

				wd1 = driver.findElements(By.cssSelector(objectDetails
						.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
				wd2 = performActionMultipleGetText(objectName1);
				rd = new Random();
				int y = rd.nextInt(wd1.size());
				value1 = wd2.get(y);
				if (!(value1.isEmpty())) {
					value1 = value1.replace(pound, "").replace(euro, "");
				}
				wd1.get(y).click();
				report.log(LogStatus.INFO, objectName1 + " is clicked! ");
				logger.info(objectName1 + " " + "is selcted as" + "<strong>"
						+ value1 + "</strong>");
				report.log(LogStatus.INFO, objectName1 + " " + "is selcted as"
						+ "<strong>" + value1 + "</strong>");
			}
			wd4.add(value1);
		} catch (Exception e) {
			logger.info(objectName1 + " could not click");
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);

		}
		return wd4;

	}

	public void browserBack() throws InterruptedException {
		Thread.sleep(1000);
		driver.navigate().back();
		logger.info("browser navigating back");
		waitForPageLoad(driver);

	}

	public void cancelledBookingVerification(String ObjectName, String expected) {
		boolean flag = false;
		// String expected= valuesMap.get(Expected.replace("Your",
		// "").replace("multi-centre holiday", "").replace(" ", ""));

		logger.info("Holiday Name:" + expected);

		List<WebElement> names = driver.findElements(By.xpath(getObjectDetails(
				ObjectName).get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
		;

		logger.info("Names count:" + names.size());

		for (int i = 0; i <= names.size(); i++) {
			if (names.get(i).getText().equalsIgnoreCase(expected)) {
				logger.info("Cancelled Booking is Present");
				flag = true;

				break;
			}

			else {
				logger.info("Cancelled Booking is not Present");
				flag = false;

			}
		}
		if (flag) {
			report.log(LogStatus.PASS, "Cancelled Booking is Present");
		} else {
			report.log(LogStatus.FAIL, "Cancelled Booking is not Present");
			report.attachScreenshot(takeScreenShotExtentReports());
		}
	}

	public void selectholidayInCA(String name, String button, String TestData,
			String index) throws IOException {
		boolean flag = false;

		List<WebElement> names = driver.findElements(By.xpath(getObjectDetails(
				name).get(CONSTANTS_OBJECT_LOCATOR_VALUE).replace("  ", "")));
		List<WebElement> buttons = driver.findElements(By
				.xpath(getObjectDetails(button).get(
						CONSTANTS_OBJECT_LOCATOR_VALUE)));

		in = new FileInputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);

		Properties props = new Properties();
		props.load(in);

		String testData = props.getProperty(TestData).replace("\\ ", "");
		logger.info("Holiday need to select" + testData);
		for (int i = 0; i <= names.size(); i++) {
			if (names.get(i).getText().equalsIgnoreCase(testData)) {
				buttons.get(i).click();
				flag = true;
				break;
			} else {
				logger.info("Holiday is not Present");
				flag = false;
			}
		}
		if (flag) {
			report.log(LogStatus.PASS, "Holiday is Present");
		} else {
			report.log(LogStatus.FAIL, "Holiday is not Present");
			report.attachScreenshot(takeScreenShotExtentReports());
		}

	}

	public void performActionEnterText_Prop(String objectName, String TestData)
			throws IOException {
		objectDetails = getObjectDetails(objectName);
		result = true;
		in = new FileInputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);

		Properties props = new Properties();
		props.load(in);
		String testData = props.getProperty(TestData);

		try {
			// logger.info("The incremented value:" + Driver.rep);

			// testData = fetchDataFromMap(testData);
			// testData=fetchTestDataFromMap(testData);
			// logger.info("The object and test dat:"+objectName+"\t"+testData);
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				waitForElementToBeDisplayed(objectName);

				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {

					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();

					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {

					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.clear();
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ "<strong>" + testData + "</strong>");
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {

			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	public void getdeparturedate(String index) throws IOException,
			java.text.ParseException {
		in = new FileInputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);

		Properties props = new Properties();
		props.load(in);
		String expected = props.getProperty("departureDateMonth" + index);

		String acdd1 = expected
				.replaceAll("([A-Za-z,\\s]+) ([A-Za-z,\\s]+)", "")
				.replace("OUT:", "").replace(".", "");
		// String acdd1= acd1.replace(" ", "/");

		final String OLD_FORMAT = "dd MMM yyy";
		final String NEW_FORMAT = "dd MMMM yyyy";

		String oldDateString = acdd1;
		String newDateString;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = sdf.parse(oldDateString);
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
		logger.info(newDateString);

		String acd4 = newDateString.substring(0, 3);
		acd4 = acd4.replace(" ", "");
		logger.info(acd4);
		String acd2 = newDateString.substring(3);
		logger.info(acd2);
		// String departuredate;
		// String departuremonth;
		props.setProperty("departuredate" + index, acd4);
		props.setProperty("departuremonth" + index, acd2);
		out = new FileOutputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);
		props.store(out, null);

		out.close();

	}

	public void performActionSelectGivenTextFromProp(String objectName,
			String TestData) throws IOException {
		objectDetails = getObjectDetails(objectName);
		in = new FileInputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);
		Properties props = new Properties();
		props.load(in);
		String testData = props.getProperty(TestData);
		result = true;
		List<WebElement> ele;
		try {

			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)
					|| objectDetails.get(CONSTANTS_OBJECT_TYPE)
							.equalsIgnoreCase(CONSTANTS_OBJECT_TYPE_BUTTON)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					ele = driver.findElements(By.id(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Thread.sleep(1000L);
							ele1.click();
							break;
						}
					}
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					ele = driver.findElements(By.name(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							ele1.click();
							Thread.sleep(1000L);
							break;
						}
					}
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				} else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					ele = driver.findElements(By.xpath(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Thread.sleep(1000L);
							ele1.click();
							break;
						}
					}
					logger.info(objectName + "---text is set as " + testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					ele = driver.findElements(By.cssSelector(objectDetails
							.get(CONSTANTS_OBJECT_LOCATOR_VALUE)));
					for (WebElement ele1 : ele) {
						if (ele1.getText().toLowerCase().trim()
								.contains(testData.toLowerCase())) {
							Thread.sleep(1000L);
							ele1.click();
							break;
						}
					}
					logger.info(objectName + "---text is set as " + testData);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ testData);
					logger.info(objectName + " text is set as " + testData);
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ testData);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
		}
	}

	public void getleadsurname(String index) throws Exception {
		in = new FileInputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);

		Properties props = new Properties();
		props.load(in);
		String expected = props.getProperty("LeadName" + index);
		expected = expected.replace("(Lead passenger)", "");
		String asd[] = expected.split("\\s");
		// String LeadSurname;
		props.setProperty("LeadSurname" + index, asd[2]);
		out = new FileOutputStream(getProjectPath()
				+ CONSTANTS_SETVALUE_STORE_PROPERTY);
		props.store(out, null);

		out.close();
	}

	public void setBookingDetails(String referenceIdLoc, String storename,
			String index) throws IOException {
		try {
			out = new FileOutputStream(getProjectPath()
					+ CONSTANTS_SETVALUE_STORE_PROPERTY, true);
			in = new FileInputStream(getProjectPath()
					+ CONSTANTS_SETVALUE_STORE_PROPERTY);
			String[] REFENT = referenceIdLoc.split(",");
			String[] REFENTENTER = storename.split(",");

			String referenceId;
			Properties props = new Properties();
			props.load(in);
			for (int i = 0; i < REFENT.length; i++) {

				referenceId = performActionGetText(REFENT[i])
						.replace(multiply.concat(space).concat("1"), "")
						.replace(multiply.concat(space).concat("2"), "")
						.replace(multiply.concat(space).concat("3"), "")
						.replace(multiply.concat(space).concat("4"), "")
						.replace(pound, "");

				logger.info(referenceId);

				props.setProperty(REFENTENTER[i] + index, referenceId);
				out = new FileOutputStream(getProjectPath()
						+ CONSTANTS_SETVALUE_STORE_PROPERTY);
				props.store(out, null);

				// }

			}
			out.close();
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while writing Reference No into properties File due to "
							+ e.getMessage());
		}
	}

	public void GetBookingDetails(String referenceIdLoc, String storename,
			String index) throws IOException {
		try {
			out = new FileOutputStream(getProjectPath()
					+ CONSTANTS_SETVALUE_STORE_PROPERTY, true);
			in = new FileInputStream(getProjectPath()
					+ CONSTANTS_SETVALUE_STORE_PROPERTY);
			String[] REFENT = referenceIdLoc.split(",");
			String[] REFENTENTER = storename.split(",");
			String Confirmation;
			in = new FileInputStream(getProjectPath()
					+ CONSTANTS_SETVALUE_STORE_PROPERTY);

			Properties props = new Properties();
			props.load(in);

			for (int i = 0; i < REFENT.length; i++) {

				Confirmation = performActionGetText(REFENT[i])
						.replace("multi-centre holiday", "").replace(pound, "")
						.replace("  ", "");

				logger.info(REFENTENTER[i] + index);

				String CA1 = (REFENTENTER[i] + index);
				String CA = props.getProperty(CA1);
				logger.info(CA);

				if (Confirmation.contains(CA)) {
					report.log(LogStatus.PASS,
							" Verifying condition is Passed where Expected:"
									+ Confirmation + " " + "Actual:" + CA);
					logger.info("======pass=======");
				} else {
					report.log(LogStatus.FAIL,
							" Verifying condition is Failed where Expected:"
									+ Confirmation + " " + "Actual" + CA);
					logger.info("======fail=======");
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Reading Reference No from properties File due to "
							+ e.getMessage());
		}
	}

	public boolean performActionEnterTextfromHold(String objectName, String key) {
		result = true;
		objectDetails = getObjectDetails(objectName);
		String enterkey = valuesMap.get(key).substring(
				valuesMap.get(key).indexOf("^^") + 2);
		try {
			if (objectDetails.get(CONSTANTS_OBJECT_TYPE).equalsIgnoreCase(
					CONSTANTS_OBJECT_TYPE_TEXT)) {
				waitForElementToBeDisplayed(objectName);
				if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_ID)) {
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.clear();
					driver.findElement(
							By.id(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(enterkey);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ enterkey);
					logger.info(objectName + " text is set as " + testData);
					return true;
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_NAME)) {
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.clear();
					driver.findElement(
							By.name(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(enterkey);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ enterkey);
					logger.info(objectName + " text is set as " + testData);
					return true;
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_XPATH)) {
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.clear();

					driver.findElement(
							By.xpath(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(enterkey);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ enterkey);
					logger.info(objectName + " text is set as " + testData);
					return true;
				}

				else if (objectDetails.get(CONSTANTS_OBJECT_LOCATOR)
						.equalsIgnoreCase(CONSTANTS_OBJECT_LOCATOR_CSS)) {
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.click();
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.clear();
					driver.findElement(
							By.cssSelector(objectDetails
									.get(CONSTANTS_OBJECT_LOCATOR_VALUE)))
							.sendKeys(enterkey);
					report.log(LogStatus.INFO, objectName + " text is set as "
							+ enterkey);
					logger.info(objectName + " text is set as " + testData);
					return true;
				}
			}
		} catch (Exception e) {
			logger.info(objectName + " text is not entered **********");
			report.log(LogStatus.ERROR, objectName + " text is not set as "
					+ enterkey);
			report.attachScreenshot(takeScreenShotExtentReports());
			logger.error(e);
			result = false;
			return false;
		}
		return false;
	}

	public void captureTextValuesandHoldfromAttributes(String objectName,
			String key, String Attribute) {
		objectDetails = getObjectDetails(objectName);
		String tempHold = "";
		try {
			String valuesElement = performActionGetAttributeValue(objectName,
					Attribute);
			tempHold = tempHold + valuesElement;
			report.log(LogStatus.INFO, "Value stored " + tempHold + " at "
					+ key);
			tempHold = objectName + "^^" + tempHold;
			valuesMap.put(key, tempHold);
			logger.info("Value stored " + tempHold + " at " + key);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to capture and store at " + key);
		}
	}

	/**
	 * Method to upgrade or downgrade seating in A&C
	 * 
	 * @param totalprice
	 * @param selectedseatinbookkflow
	 * @param selectedseattypeinbookflow
	 * @param availableseatssize
	 * @param newlyselectedseatalldetailstoupgrade
	 * @param selectedseatinbookflowpricedetails
	 * @param defaultseatselectiontodowngrade
	 * @author Dheeraj
	 */

	public void ChangeSeatingAnC(String Totalprice, String seatinBookflow,
			String selectedseattype, String seatsizeavailable,
			String availbaleseats, String summaryseatprice, String defaultseat)
			throws InterruptedException {
		try {
			String selectedseat = null, selectedseatdetails;
			double selectedppprice, totalseatingprice = 0, seatselectedbeforeprice;
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			String seatselectedbefore = performActionGetText(seatinBookflow);
			boolean check = performActionGetAttributeValue(selectedseattype,
					"block-value").contains("DEF_SEAT");
			int seatsize = performActionGetSize(seatsizeavailable);
			if (seatsize > 0) {
				if (check) {
					report.log(LogStatus.INFO,
							"Selected Seating type in Bookflow is :"
									+ seatselectedbefore
									+ ",Included as Default Seating");
					selectedseatdetails = performActionRandomSelection(
							availbaleseats).trim();
				} else {
					seatselectedbeforeprice = Double
							.parseDouble(performActionGetText(summaryseatprice)
									.replace("£", "").replace("€", ""));
					report.log(LogStatus.INFO,
							"Selected Seating type in Bookflow is : "
									+ seatselectedbefore + " , Totalprice of "
									+ seatselectedbefore + " is : "
									+ seatselectedbeforeprice);
					selectedseatdetails = performActionRandomSelection(
							defaultseat).trim();
				}
				String pattern1 = "([A-Za-z\\s]+)\\n([\\d.+-]+)\\n\\(([\\d.+-]+)";
				Pattern p = Pattern.compile(pattern1);
				Matcher m = p.matcher(selectedseatdetails);
				if (m.find()) {
					selectedseat = m.group(1);
					totalseatingprice = Double.parseDouble(m.group(2));
					selectedppprice = Double.parseDouble(m.group(3).replaceAll(
							"[^0-9.+-]", ""));
					report.log(LogStatus.INFO, "Seating type changed from "
							+ seatselectedbefore + " to " + selectedseat);
					report.log(LogStatus.INFO, selectedseat
							+ " Perperson price is : " + selectedppprice
							+ " and Totalprice is : " + totalseatingprice);
				}
				PerformActionSleep("10000");

				double Totalpriceafter = Double
						.parseDouble(performActionGetText(Totalprice)
								.replaceAll("[^0-9.+-]", ""));
				if ((Totalpriceafter - Totalpricebefore) > 1) {
					report.log(LogStatus.INFO,
							"Upgrading Seattype and no Amendment charges");
					if (Totalpriceafter == (Totalpricebefore + totalseatingprice)) {
						report.log(LogStatus.PASS, "After selecting"
								+ selectedseat + "Totalprice( "
								+ Totalpriceafter
								+ ") = Totalprice before Seat selection ( "
								+ Totalpricebefore + " + " + totalseatingprice
								+ " ) Selected SeatingType Totalprice");
					} else {
						report.log(
								LogStatus.FAIL,
								"After selecting"
										+ selectedseat
										+ "Totalprice( "
										+ Totalpriceafter
										+ ") is not equal to  Totalprice before Seat selection ( "
										+ Totalpricebefore + " + "
										+ totalseatingprice
										+ " ) Selected SeatingType Totalprice");
					}
				} else {
					report.log(LogStatus.INFO,
							"Downgrading Seattype and it leads to Amendment charges");
					if (Totalpriceafter == (Totalpricebefore + totalseatingprice)) {
						report.log(LogStatus.PASS, "After selecting"
								+ selectedseat + "Totalprice( "
								+ Totalpriceafter
								+ ") = Totalprice before Seat selection ( "
								+ Totalpricebefore + totalseatingprice
								+ " ) Selected SeatingType Totalprice");
					} else {
						report.log(
								LogStatus.FAIL,
								"After selecting"
										+ selectedseat
										+ "Totalprice( "
										+ Totalpriceafter
										+ ") is not equal to  Totalprice before Seat selection ( "
										+ Totalpricebefore + " + "
										+ totalseatingprice
										+ " ) Selected SeatingType Totalprice");
					}
				}
			} else {
				report.log(LogStatus.INFO,
						"Selected Seating type in Bookflow is :"
								+ seatselectedbefore
								+ ",Included as Default Seating");
				report.log(LogStatus.INFO,
						"There are no alternative Seating types to select");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Changing Seat due to " + e.getMessage());
		}
	}

	/**
	 * Method to upgrade or downgrade luggage in A&C pdp flows , not for FO
	 * 
	 * @param totalprice
	 * @param selectedluggageinbookkflow
	 * @param firstpaxname
	 * @param availableluggagessize
	 *            (or)newlyselectedluggagealldetailstoupgrade
	 * @param defaultluggageselectiontodowngrade
	 * @author Dheeraj
	 */
	public void ChangeLuggageAnC(String totalprice, String baginBookflow,
			String firstbagpaxname, String bagsizeavailable, String defaultbag)
			throws InterruptedException {
		try {
			double oldluggageprice, selectedluggageprice = 0;
			String oldluggageweight = null, selectedLuggagewight = null, selectedLuggagedetails = null;
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					totalprice).replaceAll("[^0-9.]", ""));
			String luggageselectedbefore = performActionGetText(baginBookflow)
					.replace("£", "").replace("€", "");
			String[] luggageselecteddetails = luggageselectedbefore.split(" ");
			String paxname = performActionGetText(firstbagpaxname);
			boolean check = luggageselectedbefore.contains("Included");
			int bagsize = performActionGetSize(bagsizeavailable);
			if (bagsize > 0) {
				if (check) {
					report.log(LogStatus.INFO,
							"Selected luggage type in Bookflow is :"
									+ luggageselectedbefore
									+ ",Included as Default luggage");
					oldluggageweight = luggageselecteddetails[0];
					oldluggageprice = 0;
					selectedLuggagedetails = performActionRandomSelection(
							bagsizeavailable).trim();
				} else {
					selectedluggageprice = 0;
					oldluggageweight = luggageselecteddetails[0];
					oldluggageprice = Double
							.parseDouble(luggageselecteddetails[1]);
					selectedLuggagedetails = performActionRandomSelection(
							defaultbag).trim();
				}
				String pattern1 = "([\\dA-Za-z]+) ([A-Za-z\\d.+-]+)";
				Pattern p = Pattern.compile(pattern1);
				Matcher m = p.matcher(selectedLuggagedetails);
				if (m.find()) {
					selectedLuggagewight = m.group(1);
					if (m.group(2).contains("Included"))
						selectedluggageprice = 0;
					else
						selectedluggageprice = Double.parseDouble(m.group(2));
					report.log(LogStatus.INFO,
							"Luggageweight type changed from "
									+ oldluggageweight + " to "
									+ selectedLuggagewight + " for " + paxname);
				}
				PerformActionSleep("10000");
				double Totalpriceafter = Double
						.parseDouble(performActionGetText(totalprice)
								.replaceAll("[^0-9.+-]", ""));
				if ((Totalpriceafter - Totalpricebefore) > 1) {
					report.log(LogStatus.INFO,
							"Upgrading LuggageType and no Amendment charges");
					if (Totalpriceafter == (Totalpricebefore + selectedluggageprice))
						report.log(
								LogStatus.PASS,
								" After selecting "
										+ selectedLuggagewight
										+ " Totalprice( "
										+ Totalpriceafter
										+ ") = Totalprice before LuggageType selection ( "
										+ Totalpricebefore
										+ " + "
										+ selectedluggageprice
										+ " ) Selected LuggageType Totalprice for pax : "
										+ paxname);
					else
						report.log(
								LogStatus.FAIL,
								" After selecting "
										+ selectedLuggagewight
										+ " Totalprice( "
										+ Totalpriceafter
										+ ") is not equal to  Totalprice before LuggageType selection ( "
										+ Totalpricebefore
										+ " + "
										+ selectedluggageprice
										+ " ) Selected LuggageType Totalprice for pax : "
										+ paxname);
				} else {
					report.log(LogStatus.INFO,
							"Downgrading LuggageType and it leads to Amendment charges");
					if (Totalpriceafter == (Totalpricebefore - oldluggageprice)) {
						report.log(
								LogStatus.PASS,
								" After selecting "
										+ selectedLuggagewight
										+ " Totalprice( "
										+ Totalpriceafter
										+ ") = Totalprice before LuggageType selection ( "
										+ Totalpricebefore + " - "
										+ oldluggageprice + " for ("
										+ oldluggageweight + ")) for pax : "
										+ paxname);
					} else {
						report.log(
								LogStatus.FAIL,
								" After selecting "
										+ selectedLuggagewight
										+ " Totalprice( "
										+ Totalpriceafter
										+ ") is not equal to  Totalprice before LuggageType selection ( "
										+ Totalpricebefore + " - "
										+ oldluggageprice + " for ("
										+ oldluggageweight + ")) for pax : "
										+ paxname);
					}
				}
			} else {
				report.log(LogStatus.INFO,
						"Selected luggage type in Bookflow is :"
								+ luggageselectedbefore
								+ ",Included as Default luggage");
				report.log(LogStatus.INFO,
						"There are no alternative baggage types to select");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while Changing Luggagetype due to " + e.getMessage());
		}
	}

	/**
	 * Method to upgrade or downgrade luggage in A&C in FlightsOnly flows , not
	 * for pdp
	 * 
	 * @param totalprice
	 * @param availableluggagessizetoupgrade
	 *            (or)newlyselectedluggagealldetailstoupgrade
	 * @author Dheeraj
	 */
	public void ChangeLuggageFOAnC(String totalprice, String bagsizeavailable)
			throws InterruptedException {
		try {
			int bagsize = performActionGetSize(bagsizeavailable);
			if (bagsize > 0) {
				double selectedluggageprice = 0;
				String selectedLuggagedetails = null, selectedLuggagewight = null;
				double Totalpricebefore = Double
						.parseDouble(performActionGetText(totalprice)
								.replaceAll("[^0-9.+-]", ""));
				selectedLuggagedetails = performActionRandomSelection(
						bagsizeavailable).trim();
				String pattern1 = "([\\dA-Za-z]+) ([A-Za-z\\d.+-]+)";
				Pattern p = Pattern.compile(pattern1);
				Matcher m = p.matcher(selectedLuggagedetails);
				if (m.find()) {
					selectedLuggagewight = m.group(1);
					selectedluggageprice = Double.parseDouble(m.group(2));
				}
				PerformActionSleep("10000");
				double Totalpriceafter = Double
						.parseDouble(performActionGetText(totalprice)
								.replaceAll("[^0-9.+-]", ""));
				if ((Totalpriceafter - Totalpricebefore) > 1) {
					report.log(LogStatus.INFO,
							"Upgrading luggage and no Amendment charges");
					if (Totalpriceafter == (Totalpricebefore + selectedluggageprice))
						report.log(
								LogStatus.PASS,
								" After selecting "
										+ selectedLuggagewight
										+ " Totalprice( "
										+ Totalpriceafter
										+ ") = Totalprice before LuggageType selection ( "
										+ Totalpricebefore + " + "
										+ selectedluggageprice
										+ " ) Selected LuggageType Totalprice");
					else
						report.log(
								LogStatus.FAIL,
								" After selecting "
										+ selectedLuggagewight
										+ " Totalprice( "
										+ Totalpriceafter
										+ ") is not equal to  Totalprice before LuggageType selection ( "
										+ Totalpricebefore + " + "
										+ selectedluggageprice
										+ " ) Selected LuggageType Totalprice ");
				}
			} else {
				report.log(LogStatus.INFO,
						"As type of flow is Flights Only(FO) no default baggage will be included");
				report.log(LogStatus.INFO,
						"There are no any baggage types to select");
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error while upgraging Luggagetype due to "
							+ e.getMessage());

		}
	}

	/**
	 * Method to upgrade or downgrade lateroomcheckout in A&C pdp flows
	 * 
	 * @param totalprice
	 * @param latecheckoutperroomprice
	 * @param latecheckoutTotalpricebefore
	 * @param lateroomdropdown
	 * @param lateroomdropdownselectionlast
	 * @param lateroomdropdownselectionzero
	 * @param setchargebutton
	 * @param latecheckoutTotalpriceafter
	 * @author Dheeraj
	 */

	public void latecheckout1(String totalprice,
			String priceperroomlatecheckout, String lateroomprice,
			String lateroomdropdown, String lateroomdropdownselectionlast,
			String lateroomdropdownselectionzero, String setchargebutton)
			throws InterruptedException {
		try {
			double Totalpricebefore, latecheckoutperroomprice, latecheckoutTotalpricebefore, latecheckoutTotalpriceafter, Totalpriceafter;

			Totalpricebefore = Double.parseDouble(performActionGetText(
					totalprice).replaceAll("[^0-9.]", ""));
			latecheckoutperroomprice = Double.parseDouble(performActionGetText(
					priceperroomlatecheckout).replaceAll("[^0-9.]", ""));
			latecheckoutTotalpricebefore = Double
					.parseDouble(performActionGetText(lateroomprice)
							.replaceAll("[^0-9.]", ""));
			if (latecheckoutTotalpricebefore == 0.00) {
				performActionClick(lateroomdropdown);
				performActionClick(lateroomdropdownselectionlast);
				// performActionSelectDropDown("THRETAIL_LATEROOMCHECKOUT_DROPDOWNSELECTION","1");
			} else {
				performActionSelectDropDown(lateroomdropdownselectionzero, "0");
			}
			performActionClick(setchargebutton);
			latecheckoutTotalpriceafter = Double
					.parseDouble(performActionGetText(lateroomprice)
							.replaceAll("[^0-9.]", ""));
			Totalpriceafter = Double.parseDouble(performActionGetText(
					totalprice).replaceAll("[^0-9.]", ""));
			if ((Totalpriceafter - Totalpricebefore) > 1) {
				report.log(LogStatus.INFO,
						"Upgrading Seattype and no Amendment charges");
				if (Totalpriceafter == (Totalpricebefore + latecheckoutTotalpriceafter)) {
					report.log(LogStatus.PASS,
							"After selecting(Upgrade) Late Room Checkout , Totalprice( "
									+ Totalpriceafter
									+ ") = Totalprice before ( "
									+ Totalpricebefore + " + "
									+ latecheckoutTotalpriceafter
									+ " )LateRoom CheckoutPrice");
				} else {
					report.log(LogStatus.FAIL,
							"After selecting(Upgrade) Late Room Checkout , Totalprice( "
									+ Totalpriceafter
									+ ") is not equal to Totalprice before ( "
									+ Totalpricebefore + " + "
									+ latecheckoutTotalpriceafter
									+ " )LateRoom CheckoutPrice");
				}
			} else {
				report.log(LogStatus.INFO,
						"Downgrading LuggageType and it leads to Amendment charges");
				if (Totalpriceafter == (Totalpricebefore - latecheckoutTotalpricebefore)) {
					report.log(LogStatus.PASS,
							"After selecting(Downgrade) Late Room Checkout , Totalprice( "
									+ Totalpriceafter
									+ ") = Totalprice before ( "
									+ Totalpricebefore + " + "
									+ latecheckoutTotalpriceafter
									+ " )LateRoom CheckoutPrice");
				} else {
					report.log(LogStatus.FAIL,
							"After selecting(Downgrade) Late Room Checkout , Totalprice( "
									+ Totalpriceafter
									+ ") is not equal to Totalprice before ( "
									+ Totalpricebefore + " + "
									+ latecheckoutTotalpriceafter
									+ " )LateRoom CheckoutPrice");
				}
			}
		}

		catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error while changing LateRoom Checkout Type due to "
							+ e.getMessage());
		}
	}

	/**
	 * Method to Remove selected transfer and select no transfer in A&C
	 * 
	 * @param totalprice
	 * @param selectedTransferinbookflowtext
	 * @param notransfer
	 * @param okpopupbutton
	 * @author Dheeraj
	 */
	public void RemoveTransfers(String totalprice, String selectedTransfer,
			String NoTrasfterButtonprice, String OkPopupButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					totalprice).replaceAll("[^0-9.]", ""));
			String transferselectedbefore = performActionGetText(selectedTransfer);
			double SelectedTranferPrice = Double
					.parseDouble(performActionGetText(NoTrasfterButtonprice)
							.replaceAll("[^0-9.-]", ""));
			performActionClick(NoTrasfterButtonprice);
			performActionClick(OkPopupButton);
			PerformActionSleep("10000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + SelectedTranferPrice)) {
				report.log(LogStatus.INFO,
						"Downgrading Transfers and it leads to Amendment charges");
				report.log(LogStatus.PASS,
						"After selecting(Downgrading) No Tranfers required , Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ SelectedTranferPrice + "("
								+ transferselectedbefore + ")" + " )");
			} else {
				report.log(LogStatus.FAIL,
						"After selecting(Downgrading) No Tranfers required , Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto Totalprice before ( "
								+ Totalpricebefore + " + "
								+ SelectedTranferPrice + "("
								+ transferselectedbefore + ")" + " )");
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while selecting(Downgrading)  no tranfers required  "
							+ e.getMessage());
		}
	}

	/**
	 * Method to Store Globally Baggage,Seating Extras Types for comparing with
	 * A&C Application
	 * 
	 * @param Seatslist
	 * @param BaggageList
	 * @author Dheeraj
	 */
	public void Flightextraslist(String Seatslist, String BaggageList) {
		try {
			SeatsList.add("STANDARD SEATS");
			List<String> addedList = new ArrayList<String>();
			List<String> SeatsList1 = performActionMultipleGetText(Seatslist);
			for (int i = 0; i < SeatsList1.size(); i++) {
				if (SeatsList1.get(i) != null) {
					addedList.add(SeatsList1.get(i).trim().toUpperCase());
				}
			}
			// SeatsList1.replaceAll(String::toUpperCase);
			SeatsList.addAll(addedList);
			report.log(LogStatus.INFO, "SeatsList" + SeatsList);
			BaggageWeightValues = performActionMultipleGetTextfromattributes(
					BaggageList, "class");
			report.log(LogStatus.INFO, "BaggageweightsList"
					+ BaggageWeightValues);
			flightextrasBookflowimage = takeScreenShotExtentReports();
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while storing All type of Baggage,Seating Types  "
							+ e.getMessage());
		}
	}

	/**
	 * Method to Compare Baggage,Seating Extras Types From A&C Application and
	 * In Bookflow
	 * 
	 * @param Seatslist
	 * @param BaggageList
	 * @author Dheeraj
	 */
	public void Flightextraslistretreieve(String Seatslist, String BaggageList,
			String infantsbaggage) {
		try {
			List<String> SeatsListretrieve = performActionMultipleGetText(Seatslist);
			Set<String> SeatsListretrieve1 = new HashSet<String>(
					SeatsListretrieve);
			String flightextrasamendimage = takeScreenShotExtentReports();
			report.log(LogStatus.INFO, "SeatsType values In A&C : "
					+ SeatsListretrieve);
			if (SeatsList.size() == SeatsListretrieve1.size()) {
				report.log(LogStatus.PASS,
						"SeatsList values in Bookflow and retrieve page are same");
				if (SeatsList.equals(SeatsListretrieve1)) {
					report.log(LogStatus.PASS,
							"SeatsList in FlightExtras Page is : " + SeatsList
									+ "SeatsList in Retrieve Page is : "
									+ SeatsListretrieve);
				} else {
					report.log(LogStatus.FAIL,
							"SeatsList value in FlightExtras Page is : "
									+ SeatsList
									+ "SeatsList value in Retrieve Page is : "
									+ SeatsListretrieve);
				}
			} else {
				report.attachScreenshot(flightextrasBookflowimage);
				report.attachScreenshot(flightextrasamendimage);
				report.log(LogStatus.FAIL,
						"SeatsList value in FlightExtras Page is : "
								+ SeatsList
								+ "SeatsList value in Retrieve Page is : "
								+ SeatsListretrieve);
			}
			List<String> BaggageWeightValuesretrieve = performActionMultipleGetText(BaggageList);
			if (IsElementPresent(infantsbaggage)) {
				BaggageWeightValuesretrieve
						.add(performActionGetText(infantsbaggage));
			}
			report.log(LogStatus.INFO, "BaggageWeights In A&C : "
					+ BaggageWeightValuesretrieve);
			if (BaggageWeightValues.size() == BaggageWeightValuesretrieve
					.size()) {
				report.log(LogStatus.PASS,
						"Baggageweights in Bookflow and retrieve page are same");
				report.log(LogStatus.PASS,
						"Baggageweights in FlightExtras Page is : "
								+ BaggageWeightValues
								+ "Baggageweights in Retrieve Page is : "
								+ BaggageWeightValuesretrieve);
			} else {
				report.attachScreenshot(flightextrasBookflowimage);
				report.attachScreenshot(flightextrasamendimage);
				report.log(LogStatus.FAIL,
						"Baggageweight values in Bookflow and retrieve page are not same");
				report.log(LogStatus.FAIL, "Baggageweight values in Bookflow"
						+ BaggageWeightValues
						+ "and Baggageweight values in retrieve "
						+ BaggageWeightValuesretrieve);
			}
		} catch (Exception e) {
			report.log(
					LogStatus.ERROR,
					"Error while Comparing All type of Baggage,Seating between  BookFlow   and A&C Application "
							+ e.getMessage());
		}

	}

	/**
	 * @author Omar
	 * @param bookingRef
	 */
	public void recallbooking(String refno) {
		String bookingRef;
		try {
			if (valuesMap.get(refno).contains("_")) {
				bookingRef = valuesMap.get(refno).substring(
						valuesMap.get(refno).indexOf("^^") + 2);
			} else {
				bookingRef = valuesMap.get(refno);
			}
		} catch (Exception e) {
			bookingRef = refno;
		}
		if (null == bookingRef) {
			bookingRef = refno;
		}
		performActionClick("ATCORE_HOMEPAGE_MENUBOOKING");
		performActionClick("ATCORE_HOMEPAGE_MENUBOOKINGRECALL");
		performActionEnterText("ATCORE_RECALLPAGE_BOOKINGREFBOX", bookingRef);
		performActionClick("ATCORE_RECALLPAGE_SUBMITBUTTON");
	}

	/**
	 * Method to close currentActiveWindow when switched from firstwindow to
	 * secondwindow
	 * 
	 * @author Dheeraj
	 */
	public void closeCurrentWindow() {
		driver.close();
	}

	/**
	 * @author Dheeraj The method is used to Store Environment,Brand Details of
	 *         current page to compare them with next page
	 */
	public static String BrandandEnv() {
		try {
			String url = reportUrl1();
			URL input = new URL(url);
			UrlDetails = new String(input.getHost());
			report.log(LogStatus.INFO,
					"Brand,Environment Details in current page : " + UrlDetails);
			return UrlDetails;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @author Dheeraj The method is used to verify Environment,Brand Details of
	 *         current page with previous page
	 */
	public static String BrandandEnvCheck() {
		try {
			String url = reportUrl1();
			URL input = new URL(url);
			String host = new String(input.getHost());
			report.log(LogStatus.INFO,
					"Brand,Environment Details after page navigation: " + host);

			if (UrlDetails.equals(host)) {
				report.log(LogStatus.PASS,
						"Brand,Environment in previous page : " + UrlDetails
								+ " and in current Page are same : " + host);
			} else {
				report.log(LogStatus.FAIL,
						"Brand,Environment in previous page : " + UrlDetails
								+ " and in current Page are different : "
								+ host);
			}
			return host;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * @author Dheeraj The method is used to check and store Globally in
	 *         Accommodation page in BookFlow
	 * @param GlobalGalleryComponent
	 * @param GlobalVideoComponent
	 */
	public void GalleryandVideoAccomPage(String GalleryComponent,
			String VideoComponent) {
		try {
			Gallery = IsElementPresent(GalleryComponent);
			Video = IsElementPresent(VideoComponent);
			Gallerybookflowimage = takeScreenShotExtentReports();
		} catch (Exception ae) {
			report.log(LogStatus.ERROR,
					"Error while Checking Gallery and Video components due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to verify presence of Gallery and
	 *         Video component in A&C application in sink with Bookflow
	 * @param GalleryComponent
	 * @param VideoComponent
	 */

	public void GalleryandVideoRetrievePage(String GalleryComponent,
			String VideoComponent) {
		try {
			boolean GalleryRetrieve = IsElementPresent(GalleryComponent);
			boolean VideoRetrieve = IsElementPresent(VideoComponent);
			String Galleryamendimage = takeScreenShotExtentReports();
			if (GalleryRetrieve == Gallery) {
				report.log(LogStatus.PASS,
						"Gallery component presence in Accommodation page : "
								+ Gallery + " and in Booking Overview page :"
								+ GalleryRetrieve);
			} else {
				report.log(LogStatus.FAIL,
						"Gallery component presence in Accommodation page : "
								+ Gallery + " and in Booking Overview page :"
								+ GalleryRetrieve);
				report.attachScreenshot(Galleryamendimage);
				report.attachScreenshot(Gallerybookflowimage);
			}
			if (VideoRetrieve == Video) {
				report.log(LogStatus.PASS,
						"Video component presence in Accommodation page : "
								+ Video + " and in Booking Overview page :"
								+ VideoRetrieve);
			} else {
				report.log(LogStatus.FAIL,
						"Video component presence in Accommodation page : "
								+ Video + " and in Booking Overview page :"
								+ VideoRetrieve);
				report.attachScreenshot(Galleryamendimage);
				report.attachScreenshot(Gallerybookflowimage);
			}
		} catch (Exception ae) {
			report.log(
					LogStatus.ERROR,
					"Error while verifying Gallery and Video components in A&C Application W.R.T Bookflow due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to select ChildExtras(SwimAcademy) in
	 *         Extras page
	 * @param totalPrice
	 * @param Viewdetailsbutton
	 * @param FirstSwimtypeDuration
	 * @param FirstSwimtypePrice
	 * @param AddButton
	 */
	public void AddswimAcademy(String Totalprice, String Viewdetailsbutton,
			String SoccerAcademyFirstDuration,
			String SelectedSocceracademytprice, String AddButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			performActionClick(Viewdetailsbutton);
			performActionClick(SoccerAcademyFirstDuration);
			double selectedswimtypeprice = Double
					.parseDouble(performActionGetText(
							SelectedSocceracademytprice).replaceAll("[^0-9.]",
							""));
			performActionClick(AddButton);
			PerformActionSleep("5000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + selectedswimtypeprice))
				report.log(LogStatus.PASS,
						"After selecting  SwimAcademy, Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedswimtypeprice + " )");
			else
				report.log(LogStatus.FAIL,
						"After selecting  SwimAcademy, Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto  Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedswimtypeprice + " )");
		} catch (Exception ae) {
			report.log(
					LogStatus.ERROR,
					"Error while Adding SwimAcademy ChildExtras due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to select ChildExtras(FootballAcademy)
	 *         in Extras page
	 * @param totalPrice
	 * @param Viewdetailsbutton
	 * @param FirsttypeSoccerAcademyDuration
	 * @param FirstsoccerAcademyTypePrice
	 * @param AddButton
	 */
	public void AddFootballAcademy(String Totalprice, String Viewdetailsbutton,
			String SoccerAcademyFirstDuration,
			String SelectedSocceracademytprice, String AddButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			performActionClick(Viewdetailsbutton);
			performActionClick(SoccerAcademyFirstDuration);
			double selectedsoccertypeprice = Double
					.parseDouble(performActionGetText(
							SelectedSocceracademytprice).replaceAll("[^0-9.]",
							""));
			performActionClick(AddButton);
			PerformActionSleep("5000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + selectedsoccertypeprice))
				report.log(LogStatus.PASS,
						"After selecting  FootballAcademy, Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedsoccertypeprice + " )");
			else
				report.log(LogStatus.FAIL,
						"After selecting  FootballAcademy, Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto  Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedsoccertypeprice + " )");
		} catch (Exception ae) {
			report.log(LogStatus.ERROR,
					"Error while Adding FootballAcademy ChildExtras due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to select InfantExtras(Creche) in
	 *         Extras page
	 * @param totalPrice
	 * @param Viewdetailsbutton
	 * @param FirsttypeCrecheDuration
	 * @param FirstCrecheTypePrice
	 * @param AddButton
	 */
	public void AddCrecheforInfants(String Totalprice,
			String Viewdetailsbutton, String CrecheFirstDuration,
			String SelectedCrecheprice, String AddButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			performActionClick(Viewdetailsbutton);
			performActionClick(CrecheFirstDuration);
			double selectedcrechetypeprice = Double
					.parseDouble(performActionGetText(SelectedCrecheprice)
							.replaceAll("[^0-9.]", ""));
			performActionClick(AddButton);
			PerformActionSleep("5000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + selectedcrechetypeprice))
				report.log(LogStatus.PASS,
						"After selecting  Creche, Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedcrechetypeprice + " )");
			else
				report.log(LogStatus.FAIL,
						"After selecting  Creche, Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto  Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedcrechetypeprice + " )");
		} catch (Exception ae) {
			report.log(
					LogStatus.ERROR,
					"Error while Adding Creche InfantExtras due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to select
	 *         InfantExtras(PreBookEquipment) in Extras page
	 * @param totalPrice
	 * @param Viewdetailsbutton
	 * @param SelectedEquipmentprice
	 * @param PaxDropdown
	 * @param PaxDropdownCount
	 * @param AddButton
	 */
	public void AddpreBookequipmentforInfants(String Totalprice,
			String Viewdetailsbutton, String SelectedEquipmentprice,
			String Dropdown, String DropdownSelection, String AddButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			performActionClick(Viewdetailsbutton);
			performActionClick(Dropdown);
			performActionSelectGivenText(DropdownSelection, "1");
			double selectedprebookequipmenttypeprice = Double
					.parseDouble(performActionGetText(SelectedEquipmentprice)
							.replaceAll("[^0-9.]", ""));
			performActionClick(AddButton);
			PerformActionSleep("5000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + selectedprebookequipmenttypeprice))
				report.log(LogStatus.PASS,
						"After selecting  InfantEquipment, Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedprebookequipmenttypeprice + " )");
			else
				report.log(LogStatus.FAIL,
						"After selecting  InfantEquipment, Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto  Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedprebookequipmenttypeprice + " )");
		} catch (Exception ae) {
			report.log(LogStatus.ERROR,
					"Error while Adding PreBookEquipment InfantExtras due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to select Attraction(Disneyworld) in
	 *         Extras page
	 * @param totalPrice
	 * @param Viewdetailsbutton
	 * @param FirstAttractionType
	 * @param PaxDropdown
	 * @param PaxDropdownCount
	 * @param FirstattractionPrice
	 * @param AddButton
	 */
	public void AddDisneyworldattractions(String Totalprice,
			String Viewdetailsbutton, String SelectedAttraction,
			String Dropdown, String DropdownSelection,
			String SelectedAttractionprice, String AddButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			performActionClick(Viewdetailsbutton);
			performActionClick(SelectedAttraction);
			performActionClick(Dropdown);
			performActionClick(DropdownSelection);
			double selectedAttractionPrice = Double
					.parseDouble(performActionGetText(SelectedAttractionprice)
							.replaceAll("[^0-9.]", ""));
			performActionClick(AddButton);
			PerformActionSleep("5000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + selectedAttractionPrice))
				report.log(LogStatus.PASS,
						"After selecting  Attractions, Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedAttractionPrice + " )");
			else
				report.log(LogStatus.FAIL,
						"After selecting  Attractions, Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto  Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedAttractionPrice + " )");
		} catch (Exception ae) {
			report.log(
					LogStatus.ERROR,
					"Error while Adding Disneyworld Attraction due to "
							+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to select Attraction(Seaworld) in
	 *         Extras page
	 * @param totalPrice
	 * @param Viewdetailsbutton
	 * @param FirstAttractionType
	 * @param PaxDropdown
	 * @param PaxDropdownCount
	 * @param FirstattractionPrice
	 * @param AddButton
	 */
	public void AdddSeaworldattractions(String Totalprice,
			String Viewdetailsbutton, String SelectedAttraction,
			String Dropdown, String DropdownSelection,
			String SelectedAttractionprice, String AddButton) {
		try {
			double Totalpricebefore = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			performActionClick(Viewdetailsbutton);
			performActionClick(SelectedAttraction);
			performActionClick(Dropdown);
			performActionClick(DropdownSelection);
			double selectedAttractionPrice = Double
					.parseDouble(performActionGetText(SelectedAttractionprice)
							.replaceAll("[^0-9.]", ""));
			performActionClick(AddButton);
			PerformActionSleep("5000");
			double TotalpriceAfter = Double.parseDouble(performActionGetText(
					Totalprice).replaceAll("[^0-9.]", ""));
			if (TotalpriceAfter == (Totalpricebefore + selectedAttractionPrice))
				report.log(LogStatus.PASS,
						"After selecting  Attractions, Totalprice( "
								+ TotalpriceAfter + ") = Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedAttractionPrice + " )");
			else
				report.log(LogStatus.FAIL,
						"After selecting  Attractions, Totalprice( "
								+ TotalpriceAfter
								+ ") is not equalto  Totalprice before ( "
								+ Totalpricebefore + " + "
								+ selectedAttractionPrice + " )");
		} catch (Exception ae) {
			report.log(
					LogStatus.ERROR,
					"Error while Adding Seaworld Attraction due to "
							+ ae.getMessage());
		}
	}

	/**
	 * The method is write the A&C details(surname,dep date,booking ref) in
	 * excel sheet It used for A&C web
	 * 
	 * @param bookingRefNum
	 *            ,depDate,surName
	 * @Author Swati
	 * @Date Mar 2017
	 */
	public void createWritableExcelnWriteAnCDetails(String bookingRefNum,
			String depDate, String surName) throws IOException,
			RowsExceededException, WriteException {
		// TODO Auto-generated method stub
		// String depDat = "25-05-2017";
		int rowCount = 0;
		boolean excelFile = true;
		String depDat = performActionGetText(depDate);
		String bookingRefNu = performActionGetText(bookingRefNum);
		String surNam = performActionGetText(surName);
		Date currDate = new Date();
		try {
			SimpleDateFormat fmt = new SimpleDateFormat();
			if (depDat != null) {
				depDat = depDat.trim().replace(" ", "");
				try {
					if (depDat.matches("\\w{3}\\d{1,2}\\w{3,}\\d{4}")) {
						fmt.applyPattern("EEEddMMMyyyy");
					} else if (depDat.matches("\\d{4}\\-\\d{1,2}\\-\\d{1,2}")) {
						fmt.applyPattern("yyyy-MM-dd");
					} else if (depDat.matches("\\d{1,2}\\w{3,}\\d{4}")) {
						fmt.applyPattern("ddMMMyyyy");
					} else {
						fmt.applyPattern("dd-MM-yyyy");
					}
					Date depD = fmt.parse(depDat);
					// Date currDate = new Date();
				} catch (Exception e) {
					logger.error("The exception occured in createWritableExcelnWriteAnCDetails method:"
							+ e.getMessage());
				}
			}

			Workbook workbook1 = Workbook.getWorkbook(new File(
					"E:\\WorkSpace\\A&C_PhaseTwo\\FC_TestData_A&C_" + currDate
							+ ".xls"));
			WritableWorkbook wwb = Workbook.createWorkbook(new File(
					"E:\\WorkSpace\\A&C_PhaseTwo\\FC_TestData_A&C5_" + currDate
							+ ".xls"), workbook1);
			WritableSheet wsh = wwb.getSheet(0);

			for (int i = 0; i < 50; i++) {
				String a = wsh.getCell(0, i).getContents();
				logger.info(a);
				if (a.equals("")) {
					rowCount = i;
					break;
				}

			}

			logger.info(rowCount);

			Label l1 = new Label(0, rowCount, depDat);
			Label l2 = new Label(1, rowCount, bookingRefNum);
			Label l3 = new Label(2, rowCount, surName);

			wsh.addCell(l1);
			wsh.addCell(l2);
			wsh.addCell(l3);
			// }
			wwb.write();
			wwb.close();
		} catch (Exception e) {
			excelFile = false;
			logger.info(e);
		} finally {
			if (excelFile == false) {
				File f = new File(
						"E:\\WorkSpace\\A&C_PhaseTwo\\FC_TestData_A&C5_"
								+ currDate + ".xls");
				WritableWorkbook wwb = Workbook.createWorkbook(f);
				WritableSheet wsh = wwb.createSheet("Booking_Ref_Number", 0);

				Label l1 = new Label(0, rowCount, depDat);
				Label l2 = new Label(1, rowCount, bookingRefNum);
				Label l3 = new Label(2, rowCount, surName);

				wsh.addCell(l1);
				wsh.addCell(l2);
				wsh.addCell(l3);
				// }
				wwb.write();
				wwb.close();
			}
		}
	}

	/**
	 * The method is save data from the testdata file It used for A&C project
	 * 
	 * @param filePath
	 *            ,sheetName,keyword
	 * @Author Swati
	 * @Modiified Kaushik
	 * @Date 07-03-2017
	 */
	public Hashtable<String, String> getApplicationTestDataByKeyword(
			String filePath, String sheetName, String keyword)
			throws IOException {

		try {
			int totRow;
			File fin = new File(filePath);
			Workbook wbk = Workbook.getWorkbook(fin);

			Sheet sht = wbk.getSheet(sheetName);
			totRow = sht.getRows();
			logger.info("Total rows:" + totRow);

			for (int r = 0; r < totRow; r++) {
				logger.info(sht.getCell(3, r).getContents());
				if (sht.getCell(3, r).getContents().equalsIgnoreCase(keyword)) {
					bookingDetails.put("journeyDate", sht.getCell(0, r)
							.getContents());
					bookingDetails.put("bookId", sht.getCell(1, r)
							.getContents());
					valuesMap.put("bookRefID", "Application_boking_RefId^^"
							+ sht.getCell(1, r).getContents());
					bookingDetails.put("surName", sht.getCell(2, r)
							.getContents());
					logger.info(bookingDetails);
				}
			}
		} catch (FileNotFoundException f) {
			logger.info("File not found in the given path:" + f.getMessage());
		} catch (BiffException e) {
			e.printStackTrace();
		}
		return bookingDetails;
	}

	/**
	 * The method is to enter the login page details from test data file as per
	 * keyword. It used for A&C project
	 * 
	 * @param fileName
	 *            ,sheetName,depositType
	 * @Author Swati
	 * @Date March 2017
	 */

	public void enterBookingDetails(String fileName, String sheetName,
			String depositType, String calenderAndBookMnthClick)
			throws IOException, InterruptedException {
		try {

			String date;
			getApplicationTestDataByKeyword(getProjectPath()
					+ "\\src\\com\\TestData\\" + fileName + ".xls", sheetName,
					depositType);
			performActionEnterText("AnC_Login_bookingRefNum",
					bookingDetails.get("bookId"));
			performActionEnterText("AnC_Login_LeadPassSurname",
					bookingDetails.get("surName"));

			String[] mnth = { "January", "February", "March", "April", "May",
					"June", "July", "August", "September", "October",
					"November", "December" };

			String[] a = bookingDetails.get("journeyDate").split("/");
			String mnthName = mnth[Integer.parseInt(a[1]) - 1];
			mnthName = mnthName + " " + a[2];
			logger.info(mnthName);
			date = a[0].substring(0, 1);

			if (a[0].substring(0, 1).equals("0")) {
				a[0] = a[0].replace("0", "");
			}
			String[] depdateCalAndMnthDropdow = calenderAndBookMnthClick
					.split(",");
			performActionClick(depdateCalAndMnthDropdow[0]);
			Thread.sleep(1000);
			performActionClick(depdateCalAndMnthDropdow[1]);
			Thread.sleep(1500);
			driver.findElement(
					By.xpath(".//*[@id='contentDiv']/div/div[1]/div/select/option[text()='"
							+ mnthName + "']")).click();
			Thread.sleep(1500);
			driver.findElement(
					By.xpath(".//*[@id='contentDiv']//i[text()='" + a[0] + "']"))
					.click();
		} catch (Exception e) {
			logger.info("Exception occured: " + e);
		}

	}

	/**
	 * The method is to recall the booking in Anite It used for A&C Phase 2
	 * project
	 * 
	 * @param fileName
	 *            ,sheetName,depositType
	 * @Date March 2017
	 * @Author Swati
	 */

	public void bookingRecall(String fileName, String sheetName,
			String depositType) throws InterruptedException, IOException {
		getApplicationTestDataByKeyword(getProjectPath()
				+ "\\src\\com\\TestData\\" + fileName + ".xls", sheetName,
				depositType);
		String bookId = valuesMap.get("bookRefID").substring(
				valuesMap.get("bookRefID").indexOf("^^") + 2);
		performActionMoveToElementAndClick("ATCOMRES_BOOKING",
				"ATCOMRES_BOOKING_RECALL");
		// performActionEnterText("ATCOMRES_BOOKING_REF",
		// bookingDetails.get("bookId"));
		performActionEnterText("ATCOMRES_BOOKING_REF", bookId);
		performActionClick("ATCORE_BOOKING_SEARCH");
	}

	/**
	 * The method is to recall the booking in Anite It used for A&C and CA
	 * integration
	 * 
	 * @param fileName
	 *            ,sheetName,depositType
	 * @Date March 2017
	 * @Author Swati
	 */
	public void bookingRecallCA(String bookingId) throws InterruptedException,
			IOException {

		String bookId = valuesMap.get("bookRefID").substring(
				valuesMap.get(bookingId).indexOf("^^") + 2);
		performActionMoveToElementAndClick("ATCOMRES_BOOKING",
				"ATCOMRES_BOOKING_RECALL");
		// performActionEnterText("ATCOMRES_BOOKING_REF",
		// bookingDetails.get("bookId"));
		performActionEnterText("ATCOMRES_BOOKING_REF", bookId);
		performActionClick("ATCORE_BOOKING_SEARCH");
	}
	
	//Samson function started here
	/**
	 * The method is used to display the page url
	 * @date July 2017
	 * @author Samson
	 */
	public void displayPageUrl() {
		String url = null;

		try {
			url = driver.getCurrentUrl();
			logger.info("Page Url is captured as: " + url);
			report.log(LogStatus.INFO, "Page Url is captured as: " + url);
		} catch (Exception e) {
			report.log(LogStatus.FAIL, "Text could not be captured");
			logger.error("Error occured :" + e.getMessage());
			report.attachScreenshot(takeScreenShotExtentReports());
		}
	}
		
	/**
	 * Method to Store one or more values metioned in the Testscript sheet
	 * 
	 * @date July 2017
	 * @param Store
	 * @author Samson
	 * @return boolean
	 */
	public ArrayList<String> ValuesStoreFromTextBox(String Store) {
		ArrayList<String> at = new ArrayList<String>();
		String[] bb = Store.split(",");
		for (int j = 0; j < bb.length; j++) {
			try {
				//String val = performActionGetTextFromTextBox(bb[j]);
				String val = performActionGetAttributeValue(bb[j],"value");
				// val = val.replaceAll("\\?", "x");
				// val = val.replaceAll("x","");
				// val = val.replaceAll("�","");
				// val = val.replaceAll("&nbsp;", ""); //dont add here
				if (val != null) {
					if (bb[j].contains("PRICE") || bb[j].contains("DISCOUNT")) {
						at.add(bb[j]
								+ "^^"
								+ val.replaceAll("[^0-9.]", "").replaceAll(
										"&nbsp;", " "));
					} else {
						at.add(bb[j]
								+ "^^"
								+ val.replace("*", "")
										.replaceAll("&nbsp;", " "));
					}
				} else {
					at.add(bb[j] + "^^" + val);
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				logger.error("Array Index out of bound Exception in ValuesStore:"
								+ a.getMessage());
			} catch (Exception e) {
				logger.error("Exception occured in ValuesStore method:"
						+ e.getMessage());
			}
		}
		return at;
	}
	
	/**
	 * The method is used to validate a page url
	 * 
	 * @author Samson
	 * @param
	 */
	public void validatePageUrl(String objectText) {
		String url = null;
		String expected = objectText;
		try {
			url = driver.getCurrentUrl();
			report.log(LogStatus.INFO, "Page Url is captured as: " + url);
			if (url.equalsIgnoreCase(expected)) {
				logger.info("URL captured is the same as expected");
				report.log(LogStatus.PASS,
						"URL captured is the same as expected");
			} else {
				report.log(LogStatus.FAIL,
						"URL captured is NOT the same as expected."
								+ " Expected: " + expected + " Actual: " + url);
				report.attachScreenshot(takeScreenShotExtentReports());
				logger.info("URL captured is NOT the same as expected."
						+ " Expected: " + expected + " Actual: " + url);
			}

		} catch (Exception e) {
			logger.info(e);
		}
	}
	
//Dheeraj functioncs
	

	/**
	 * Method to Create Testdata properties file with static name
	 * 
	 * @author Dheeraj
	 */
	public void Createtestdatafile() {
		String curDat, fldPath = null;
		try {

			File rptFolder = null;
			Date d = new Date();
			SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
			curDat = fmt.format(d);
			fmt.applyPattern("hh a");
			fldPath = getProjectPath() + "\\" + curDat + "\\";
			rptFolder = new File(fldPath);
			if (!rptFolder.exists()) {
				rptFolder.mkdir();
			}

			file = new File(fldPath + "BookingTestdata.properties");
			report.log(LogStatus.INFO,
					"New file created with name BookingTestdata.properties in "
							+ fldPath);
			if (!file.exists()) {
				file.createNewFile();
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error while creating new file with name BookingTestdata.properties in "
							+ fldPath + "due to : " + e);
		}
	}

	/**
	 * Method to store BookingReference Number from Booking Confirmation Page
	 * into properties File
	 * 
	 * @param BookingReferenceNumberobjectDetails
	 * @param StoreName
	 * 
	 * @author Dheeraj
	 */

	public void setBookingReferenceNo(String referenceIdLoc, String storename)
			throws IOException {
		try {
			Createtestdatafile();
			String referenceId = performActionGetText(referenceIdLoc);
			Properties props = new Properties();
			in1 = new FileInputStream(file);
			out1 = new FileOutputStream(file, true);

			props.load(in1);
			if (props.getProperty(storename) != null) {
				props.remove(storename);
				props.setProperty(storename, referenceId);
				props.store(out1, null);
				out1.close();
			} else {
				props.setProperty(storename, referenceId);
				props.store(out1, null);
				out1.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e);
			report.log(LogStatus.ERROR,
					"Error while writing Reference No into properties File due to "
							+ e.getMessage());
		}
	}

	/**
	 * Method to Read BookingReference No from properties File
	 * 
	 * @param EnterBookingReferenceNumbeobjectDetails
	 * @param StoreNameRecall
	 * @author Dheeraj
	 */
	public void GetBookingReferenceNos(String Recallpath, String referenceIdLoc)
			throws IOException {
		try {
			Createtestdatafile();
			in1 = new FileInputStream(file);
			Properties props = new Properties();
			props.load(in1);
			logger.info(in1);
			logger.info(props.size());
			bookingref = props.getProperty(referenceIdLoc);
			performActionEnterText(Recallpath,
					props.getProperty(referenceIdLoc));

		} catch (Exception e) {
			e.printStackTrace();
			report.log(LogStatus.ERROR,
					"Error while Reading Reference No from properties File due to "
							+ e.getMessage());

		}
	}

	/**
	 * @author Dheeraj The method is used to store all values from Atcore
	 *         application
	 * @param values
	 *            seperated by ,
	 */
	public void Holdvaluesfromconfirmationpage(String BCPdetails) {
		try {
			String[] BCPArray = BCPdetails.split(",");
			String[] text;
			for (int j = 0; j < BCPArray.length; j++) {

				String val = performActionGetText(BCPArray[j]);
				if (val.contains(":")) {
					text = val.split(" ");
					val = text[0];
				} else if (BCPArray[j].contains("ACCOMMODATIONCODE")) {
					String[] accom = val.split(" ");
					val = accom[accom.length - 1].trim();
				} else if (BCPArray[j].contains("FROM")) {
					String[] accom = val.split("/");
					val = accom[0].trim();
				} else if (BCPArray[j].contains("TO")) {
					String[] accom = val.split("/");
					val = accom[1].trim();
				}
				BCPagevalues.add(val);
			}
		} catch (Exception ae) {
			report.log(LogStatus.ERROR, "Error while storing values  due to "
					+ ae.getMessage());
		}
	}

	/**
	 * @author Dheeraj The method is used to connect to database
	 * @param DBURL
	 * @param Username
	 * @param Password
	 */

	public void connectiontoDBdynamic(String DBUrl, String DBusername,
			String DBPassword) {
		try {
			report.log(LogStatus.INFO, "Connecting to database with details");
			report.log(LogStatus.INFO, "Database url :" + DBUrl);
			report.log(LogStatus.INFO, "Database Username :" + DBusername);
			report.log(LogStatus.INFO, "Database Password :" + DBPassword);
			Class.forName("oracle.jdbc.driver.OracleDriver");
			dbconn = DriverManager.getConnection(DBUrl, DBusername, DBPassword);
			report.log(LogStatus.PASS, "Connected to database");
		} catch (Exception dberror) {
			dberror.printStackTrace();
			report.log(LogStatus.ERROR,
					"Error while connecting  to database due to" + dberror);
		}
	}

	/**
	 * @author Dheeraj The method is used to close database connection
	 * 
	 */
	public void closeDBconnection() {
		try {
			report.log(LogStatus.INFO, "Closing database connection");
			dbconn.close();
			report.log(LogStatus.PASS, "Closed database connection");
		} catch (Exception dberror) {
			dberror.printStackTrace();
			report.log(LogStatus.ERROR,
					"Error while closing database connection due to" + dberror);
		}
	}

	/**
	 * @author Dheeraj The method is used to store query and use while
	 *         connecting to cdm
	 */
	public void store(String data, String key) {
		logger.info("Booking Reference No given is :" + dynamicurl);

		try {
			// refno =
			// valuesMap.get(refno).substring(valuesMap.get(refno).indexOf("^^")
			// + 2);
			// refno=bookingref;

			report.log(LogStatus.INFO, "Booking Reference No given is :"
					+ bookingref);
			if (dynamicurl.contains("/ST5/")) {
				data = data.replace("_CA", "");
			} else if (dynamicurl.contains("/ST2/")) {
				data = data.replace("_CA", "_RTL");
			}
			data = data.replace("default", bookingref);
			report.log(LogStatus.INFO, "Given query : " + data);
			valuesMap.put(key, data);
		} catch (Exception e) {
			logger.info("Error occcured while querying  due to : " + e);
			report.log(LogStatus.ERROR,
					"Error occcured while querying  due to : " + e);
		}

	}

	/**
	 * @author Dheeraj The method is used to store query and use while
	 *         connecting to cdm
	 */

	public void store(String data, String key, String Keyfromdb) {
		try {
			// if(!Keyfromdb.equals("keygen")){
			// refno =
			// valuesMap.get(refno).substring(valuesMap.get(refno).indexOf("^^")
			// + 2);
			report.log(LogStatus.INFO, "Booking Reference No given is :"
					+ bookingref);
			if (dynamicurl.contains("/ST5/")) {
				data = data.replace("_CA", "");
			} else if (dynamicurl.contains("/ST2/")) {
				data = data.replace("_CA", "_RTL");
			}
			data = data.replace("default", bookingref);
			Keyfromdb = valuesMap.get(Keyfromdb);
			data = data.replace("keygen", Keyfromdb);
			report.log(LogStatus.INFO, "Given query : " + data);
			valuesMap.put(key, data);
			Action action = new Action();
			action.logger.info(data + " caprured at  " + key);
			report.log(LogStatus.INFO, "quere used ----" + data);
			// }
			// else{
			// report.log(LogStatus.ERROR, "Issue with CDM connection");
			// }
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error occcured while querying  due to : " + e);

		}
	}

	/**
	 * @author Dheeraj The method is used to compare Atcore and CDM values
	 */
	public void comparevaluesfromDBandApplication() {
		try {
			for (int i = 0; i < BCPagevalues.size(); i++) {

				logger.info(BCPagevalues.get(i));
				logger.info(DBvalues.get(i));
				if (BCPagevalues.get(i).equals(DBvalues.get(i))) {
					report.log(
							LogStatus.PASS,
							"Value captured from application: "
									+ BCPagevalues.get(i)
									+ " == Value captured from CDM table."
									+ DBvalues.get(i));
				} else {
					report.log(
							LogStatus.FAIL,
							"Value captured from application: "
									+ BCPagevalues.get(i)
									+ " not equal to  Value captured from CDM table."
									+ DBvalues.get(i));
				}
			}
		} catch (Exception e) {
			report.log(LogStatus.ERROR,
					"Error occcured while comparing CDM and Atcore Application values due to : "
							+ e);

		}
	}

	/**
	 * @author Dheeraj The method is used to get values from Database by using
	 *         column names
	 * @param querystored
	 * @param valuesstore
	 * @param Columnnames
	 */
	public void RunQuerygetvaluesfromDB(String Query, String valuesload,
			String ColumnNames) {
		try {
			java.sql.Statement stmt = null;
			String query = Query;
			if (null != valuesMap.get(Query)) {
				query = valuesMap.get(Query);
			}
			String[] LoadData = valuesload.split(",");
			String[] TablecolumnNames = ColumnNames.split(",");
			stmt = dbconn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				rs.previous();
				if (LoadData.length == TablecolumnNames.length) {

					while (rs.next()) {
						for (int i = 0; i < LoadData.length; i++) {
							String colValue = rs.getString(TablecolumnNames[i]);
							valuesMap.put(LoadData[i], colValue);
							if (!LoadData[i].equals("keygen")) {
								if (LoadData[i].equals("Depdate")) {

									DateFormat givenFormat = new SimpleDateFormat(
											"yyyy-MM-dd");
									DateFormat requiredFormat = new SimpleDateFormat(
											"ddMMMyy");
									Date date = new Date();
									date = givenFormat.parse(colValue);
									colValue = requiredFormat.format(date);
									colValue = colValue.trim().toString();
									report.log(
											LogStatus.INFO,
											"ColumnName in CDM table is :"
													+ TablecolumnNames[i]
													+ "and Columnvalue in CDM table is : "
													+ colValue);
									DBvalues.add(colValue);
								} else {
									report.log(
											LogStatus.INFO,
											"ColumnName in CDM table is :"
													+ TablecolumnNames[i]
													+ "and Columnvalue in CDM table is : "
													+ colValue);
									DBvalues.add(colValue);
								}
							}
						}
					}
				}

			} else {
				report.log(
						LogStatus.ERROR,
						"No values found for given query. Probably Bookings are not reflecting into CDM.");
				tearDownTest();
			}
			rs.close();
			stmt.close();
		} catch (Exception dberror) {
			report.log(LogStatus.ERROR,
					"Error occured while fetching values from db due to : "
							+ dberror);
		}
	}

	public void closeWindowanite() throws InterruptedException {
		dynamicurl = driver.getCurrentUrl();
		driver.close();
		Thread.sleep(2000);
	}
	// dheeraj functions end
	
	//Kauhika hello Tui Starts
	
	public void captureSearchParam(String fromAirport,String toAirport,String travelDate,String stayDuration,String adult,String noChild,String allGone){
		String fromAir,destAir,date,duration,adu,child,firstRecentSearchRow,secondRecentSearchRow;
		String[] param=allGone.split(",");
		try{
			String pageSource=driver.getPageSource();
			if(!pageSource.contains("All gone!")){
			destAir=performActionGetText(toAirport);
			date=performActionGetText(travelDate);
			
			if(destAir.contains("Any Destination")){
				destAir="Any";
			}
			
			date=date.substring(date.indexOf(" ")+1, date.indexOf("(")).trim().replace("2017", "17").replace("2018", "18");
			String tom=date.substring(date.indexOf(" ")+1, date.indexOf(" ")+4);
			String[] arr=date.split(" ");
			date=arr[0]+" "+tom+" "+arr[2];
			logger.info(date);	
			 duration=performActionGetText(stayDuration);
			adu=performActionGetText(adult);
			child=performActionGetText(noChild);
			performActionClick("THFA_searchResult_editSearch");	
			 fromAir=performActionGetText(fromAirport);
			firstRecentSearchRow=fromAir+" - "+destAir;
			valuesMap.put("firstRow", "recentSearchfirst^^"+firstRecentSearchRow);
			secondRecentSearchRow=date+" - "+duration+", "+adu+" adults, "+child+" child";
			valuesMap.put("secondRow", "recentSearchSecond^^"+secondRecentSearchRow);
			}else{
				fromAir=performActionGetText(param[0]);
				destAir=performActionGetText(param[1]);
				if(destAir.contains("Any Destination")){
					destAir="Any";
				}
				date=performActionGetText(param[2]);
				date=date.substring(date.indexOf(" ")+1, date.length()).trim().replace("2017", "17").replace("2018", "18");
				String tom=date.substring(date.indexOf(" ")+1, date.indexOf(" ")+4);
				String[] arr=date.split(" ");
				date=arr[0]+" "+tom+" "+arr[2];
				logger.info(date);
				duration=performActionGetText(param[3]);
				adu=performActionGetText(param[4]);
				child=performActionGetText(param[5]);
			}
			
		}catch(Exception e){
			logger.info("Exception "+e);
		}
	}
	//Kauhika hello Tui ends
	
	
}// End of class
