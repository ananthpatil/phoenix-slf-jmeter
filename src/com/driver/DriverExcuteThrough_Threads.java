package com.driver;

import com.utilities.CreateXMLSummery;

public class DriverExcuteThrough_Threads {
	public static void main(String... args) throws InterruptedException {
		try {

			String brandName, prptyPath;
			Driver d = new Driver();
			if (args.length == 2) {
				brandName = args[0].trim();
				prptyPath = args[1].trim();
				if(brandName.toLowerCase().contains("jmeter")){
					d.jmeterFileToRead(brandName, prptyPath);
				}else{
				  CreateXMLSummery.createXmlFile();
				  CreateXMLSummery.createXmlForFail();
				  d.fileToRead(brandName, prptyPath);
				}
			} else {
				brandName = "TH-Jmeter";
				prptyPath = "";
				if(brandName.toLowerCase().contains("jmeter")){
					d.jmeterFileToRead(brandName, prptyPath);
				}else{
				  CreateXMLSummery.createXmlFile();
				  CreateXMLSummery.createXmlForFail();
				  d.fileToRead(brandName, prptyPath);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
