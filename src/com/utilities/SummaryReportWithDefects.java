package com.utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class SummaryReportWithDefects {

	public String filename, htmlContent;

	/**
	 * The method is used to extract the file name
	 * 
	 * @return string file name
	 * @author Anant Patil
	 */
	public String getFileName() {
		return filename;
	}

	/**
	 * Method is used to set the file name return void
	 * 
	 * @author Anant Patil
	 */
	public void setFileName(String filename) {
		this.filename = filename;
	}

	/**
	 * The following method is used to create the xml file It returns the void
	 * 
	 * @author Anant Patil
	 * @param filename
	 */
	public void createXmlFile() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();

			Element root = doc.createElement("SummaryResult");
			doc.appendChild(root);

			TransformerFactory tFatcory = TransformerFactory.newInstance();
			Transformer trans = tFatcory.newTransformer();
			DOMSource src = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(getFileName()));
			trans.transform(src, result);
			System.out.println("File created!");
		} catch (ParserConfigurationException e1) {
			System.out.println("The parseConfiguration exception:" + e1);
		} catch (TransformerConfigurationException e2) {
			System.out.println("The Transformer Configuration error:" + e2);
		} catch (TransformerException e3) {
			System.out.println("TransfomerException:" + e3);
		}
	}

	/**
	 * The following method is used to add the node to xml file It returns void
	 * 
	 * @author Anant Patil
	 * @param ScenarioName
	 * @param Passed
	 * @param Failed
	 * @param percentagePass
	 * @param perFailed
	 * @param defects
	 */
	public void addSummary(float Total, float Passed, float Failed, float percentagePass, float perFailed) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File file = new File(getFileName());
			if (file.exists()) {
				Document doc1 = builder.parse(file);
				Element root1 = doc1.getDocumentElement();

				Element status = doc1.createElement("Status");
				root1.appendChild(status);

				Element tot = doc1.createElement("Total");
				tot.appendChild(doc1.createTextNode(String.valueOf(Total)));
				status.appendChild(tot);

				Element pass = doc1.createElement("Passed");
				pass.appendChild(doc1.createTextNode(String.valueOf(Passed)));
				status.appendChild(pass);

				Element fail = doc1.createElement("Failed");
				fail.appendChild(doc1.createTextNode(String.valueOf(Failed)));
				status.appendChild(fail);

				Element perpass = doc1.createElement("PercentagePassed");
				perpass.appendChild(doc1.createTextNode(String.valueOf(percentagePass)));
				status.appendChild(perpass);

				Element perfail = doc1.createElement("PercentageFailed");
				perfail.appendChild(doc1.createTextNode(String.valueOf(perFailed)));
				status.appendChild(perfail);

				TransformerFactory tFatcory = TransformerFactory.newInstance();
				Transformer trans = tFatcory.newTransformer();
				DOMSource src = new DOMSource(doc1);
				StreamResult result = new StreamResult(new File(getFileName()));
				trans.transform(src, result);
				System.out.println("file updated and added summary successfully");
			}
		} catch (SAXException e1) {
			System.out.println("The Sax error:" + e1);
		} catch (IOException e2) {
			System.out.println("The I/O error while adding the summary" + e2);
		} catch (ParserConfigurationException e3) {
			System.out.println("The parser error while adding summary:" + e3);
		} catch (TransformerConfigurationException e4) {
			System.out.println("The transformer configuration error while adding the summary:" + e4);
		} catch (TransformerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * The following method is used to add the scenario and status details It
	 * returns void
	 * 
	 * @author Anant Patil
	 * @param scenStatus
	 */
	public void addDetails(List<String[]> scenStatus) {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			File fin = new File(getFileName());
			if (fin.exists()) {
				Document doc = builder.parse(fin);
				Element root1 = doc.getDocumentElement();
				Element details = doc.createElement("Scenarios");
				root1.appendChild(details);
				for (int i = 0; i < scenStatus.size(); i++) {
					Element snSt = doc.createElement("ScenDescStatus");
					details.appendChild(snSt);
					String[] arr = scenStatus.get(i);
					Element secn = doc.createElement("ScenarioName");
					secn.appendChild(doc.createTextNode(arr[0]));
					snSt.appendChild(secn);

					Element sctatus = doc.createElement("ScenStatus");
					sctatus.appendChild(doc.createTextNode(arr[1]));
					snSt.appendChild(sctatus);

					Element Filename = doc.createElement("FileName");
					Filename.appendChild(doc.createTextNode(arr[2]));
					snSt.appendChild(Filename);

					Element ErrorDetails = doc.createElement("ErrorDetails");
					snSt.appendChild(ErrorDetails);
					String[] splitVal = arr[3].split("\n");
					for (int j = 0; j < splitVal.length; j++) {
						Element tracks = doc.createElement("Tracks");
						ErrorDetails.appendChild(tracks);

						Element PassFail = doc.createElement("PassFail");
						PassFail.appendChild(doc.createTextNode(splitVal[j]));
						tracks.appendChild(PassFail);
					}

					Element urlLog = doc.createElement("URLToLog");
					snSt.appendChild(urlLog);
					String[] splitUrl = arr[4].split("\n");
					for (int j = 0; j < splitUrl.length; j++) {
						Element applnUrl = doc.createElement("ApplnURL");
						urlLog.appendChild(applnUrl);

						Element URLVal = doc.createElement("URLVal");
						URLVal.appendChild(doc.createTextNode(splitUrl[j]));
						applnUrl.appendChild(URLVal);
					}
					// urlLog.appendChild(doc.createTextNode(arr[4]));
					// snSt.appendChild(urlLog);

				}
				TransformerFactory tfactory = TransformerFactory.newInstance();
				Transformer trans = tfactory.newTransformer();
				DOMSource src = new DOMSource(doc);
				StreamResult result = new StreamResult(fin);
				trans.transform(src, result);
			}
		} catch (ParserConfigurationException e) {
			System.out.println("ParseConfiguration Error:" + e);
		} catch (SAXException e1) {
			System.out.println("SAX error:" + e1);
		} catch (IOException e2) {
			System.out.println("The I/O error:" + e2);
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e3) {
			System.out.println("Transformer Exception:" + e3);
		}
	}

	/**
	 * The method is used to convert the XML to HTML It returns void
	 * 
	 * @author Anant
	 * @param path
	 * @param xmlPath
	 * @param xslFile
	 */
	public static void transormXmlToHtml(String path, String rsltHtmlFile, String xslFile) {
		try {
			System.out.println("the file:" + xslFile);
			File dltHtmlFile = new File(rsltHtmlFile + ".html");
			if (dltHtmlFile.exists()) {
				if (dltHtmlFile.getName().contains(".html")) {
					dltHtmlFile.delete();
				}
			}
			TransformerFactory tfactory = TransformerFactory.newInstance();
			StreamSource xmlfile = new StreamSource(path + ".xml");
			StreamSource xslfile = new StreamSource(xslFile);
			FileOutputStream htmlfile = new FileOutputStream(rsltHtmlFile + ".html");
			Transformer trans = tfactory.newTransformer(xslfile);
			trans.transform(xmlfile, new StreamResult(htmlfile));

		} catch (TransformerConfigurationException e1) {
			System.out.println("The TransformerConfigurationException:" + e1);
		} catch (FileNotFoundException e2) {
			System.out.println("The FileNotFoundException:" + e2);
		} catch (TransformerException e3) {
			System.out.println("The TransformerException:" + e3);
		}
	}

	/**
	 * @author anant Patil
	 * @param args
	 * @throws ParseException
	 * @throws IOException
	 */
	@SuppressWarnings("static-access")
	public static void reportForamtion() throws ParseException, IOException {

		String rptFlderPath, finalResultFldPath, prjPath;
		boolean isDirectryFlag;
		List<File> datFolder = new ArrayList<File>();

		// File readPrjPath = new File("");
		// prjPath = readPrjPath.getAbsolutePath();
		// System.out.println(prjPath);

		// rptFlderPath = prjPath+"//reports//";
		// finalResultFldPath = prjPath+"//reports//";

		rptFlderPath = System.getProperty("user.dir") + File.separator + "reports" + File.separator;
		finalResultFldPath = rptFlderPath;
		System.out.println("The report folder path :" + rptFlderPath);

		SummaryReportWithDefects report = new SummaryReportWithDefects();
		SimpleDateFormat fmt = new SimpleDateFormat();
		Date sysDate = new Date();

		// Presentation ppt = new Presentation();

		File readFlder = new File(rptFlderPath);
		File[] listOfFolder = readFlder.listFiles();
		for (File lstFlder : listOfFolder) {
			isDirectryFlag = lstFlder.isDirectory();
			if (isDirectryFlag && !lstFlder.toString().contains("images")) {
				if (lstFlder.getName().matches("\\d{1,2}\\-\\d{1,2}\\-\\d{4}")) {
					fmt.applyPattern("dd-MM-yyyy");
					datFolder.add(lstFlder);
				}
			}
		}

		/*if (datFolder.size() != 0) {
			Date strdDate;
			long small = 0, diff, diffDays;
			File filPath;
			String dateValue, dtVal;

			filPath = datFolder.get(0);
			dateValue = filPath.getAbsolutePath();
			try {
				strdDate = fmt.parse(filPath.getName().trim());
				diff = sysDate.getTime() - strdDate.getTime();
				diffDays = diff / (24 * 60 * 60 * 1000);
				small = diffDays;
			} catch (ParseException e) {
				e.printStackTrace();
			}

			for (int j = 1; j < datFolder.size(); j++) {
				dtVal = datFolder.get(j).getName();
				try {
					strdDate = fmt.parse(dtVal);
					diff = sysDate.getTime() - strdDate.getTime();
					diffDays = diff / (24 * 60 * 60 * 1000);
					if (diffDays < small) {
						small = diffDays;
						dateValue = datFolder.get(j).getAbsolutePath();
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}

			File amPmFilVal = new File(dateValue);
			long lastMod = Long.MIN_VALUE;
			File choice = null;
			if (amPmFilVal.exists()) {
				File[] subFolder = amPmFilVal.listFiles();
				for (File subFld : subFolder) {
					if (subFld.isDirectory()) {
						if (subFld.lastModified() > lastMod) {
							lastMod = subFld.lastModified();
							choice = subFld;
						}
					}
				}*/

				File choice = readFlder;
				String line, scenarioName = null, line1 = "", testCaseName = null, flagStatus;
				float total = 0, passed = 0, failed = 0, perPass, perFail;
				int lineCt = 0, failCt, failCt1;
				boolean failFound = true, repeatSteps = false, testCaseId, testCaseIdFound;
				List<String[]> finalList = new ArrayList<String[]>();
				List<String[]> scenarioList = null;
				if (choice != null) {
					String[] htmlFlName = choice.list();
					for (String htFlName : htmlFlName) {
						testCaseIdFound = false;
						if (!htFlName.contains("images")) {
							flagStatus = "PASS";
							failCt = 0;
							failCt1 = 0;
							scenarioList = new ArrayList<String[]>();
							File htmlFileName = new File(choice + File.separator + htFlName);
							BufferedReader reader;
							try {
								reader = new BufferedReader(new FileReader(htmlFileName));
								scenarioName = htmlFileName.getName().replace(".html", "");
								if (htmlFileName.length() != 0) {
									String[] arr1 = new String[5];
									String failString1 = "", urlToLog = "";
									arr1[2] = htmlFileName.getName();
									failFound = true;
									while ((line = reader.readLine()) != null) {
										testCaseId = false;
										line = line.replace("<td class='step-details' colspan='2'>", "")
												.replace("</td>", "").trim();
										line1 = line1.replace("<td class='step-details' colspan='2'>", "")
												.replace("</td>", "").trim();
										if (line.contains("Launching URL") || line.contains("Page Url")) {
											// urlToLog =
											// line.replace("Launching URL:",
											// "").replace("Page Url:", "");
											urlToLog = urlToLog
													+ line.replace("Launching URL:", "").replace("Page Url:", "")
													+ "\n";
										}
										if (line.contains("ScenarioName:")) {
											scenarioName = line.replace("ScenarioName", "").replace(":", "").trim();
										}
										arr1[0] = scenarioName;
										if (repeatSteps && lineCt == 1) {
											testCaseName = line1;
											if (line1.contains(">>") || line1.contains(
													"Test case desription not specifed in the description column")) {
												testCaseId = true;
												testCaseIdFound = true;
											}
										} else if (line.contains(">>") || line.contains(
												"Test case desription not specifed in the description column")) {
											testCaseName = line;
											testCaseId = true;
											testCaseIdFound = true;
										}

										lineCt++;
										if (testCaseId) {
											failFound = true;
											failCt = 0;
											String failString = "";
											String[] arr = new String[5];
											if (scenarioList.size() != 0) {
												scenarioList.clear();
											}
											while ((line = reader.readLine()) != null) {
												lineCt++;
												repeatSteps = false;
												if (line.contains("title='FAIL") || line.contains("title='ERROR")) {
													flagStatus = "FAIL";
													failFound = false;
													line = reader.readLine()
															.replace("<td class='step-details' colspan='2'>", "")
															.replace("</td>", "").trim();
													failString = failString + line + "\n";
													arr[4] = urlToLog;
													if (failCt == 0) {
														failed++;
													}
													failCt++;
												}
												if (line.contains(">>") || line.contains(
														"Test case desription not specifed in the description column")) {
													repeatSteps = true;
													lineCt = 1;
													line1 = line;
													break;
												}
											}
											if (failFound == true) {
												flagStatus = "PASS";
												arr[4] = "";
												passed++;
											}
											arr[0] = testCaseName;
											arr[1] = flagStatus;
											arr[2] = htmlFileName.getName();
											arr[3] = failString;
											finalList.add(arr);
										} else if (testCaseIdFound == false) {
											if (line.contains("title='FAIL'") || line.contains("title='ERROR")) {
												flagStatus = "FAIL";
												failFound = false;
												arr1[1] = flagStatus;
												failString1 = failString1 + reader.readLine()
														.replace("<td class='step-details' colspan='2'>", "")
														.replace("</td>", "").trim() + "\n";
												arr1[3] = failString1;
												arr1[4] = urlToLog;
												scenarioList.add(arr1);
												if (failCt1 == 0) {
													failed++;
												}
												failCt1++;
											}
										}
									}
									if (failFound == true && testCaseIdFound == false) {
										flagStatus = "PASS";
										arr1[1] = flagStatus;
										arr1[3] = failString1;
										arr1[4] = "";
										finalList.add(arr1);
										passed++;
									}
									if (scenarioList.size() != 0) {
										finalList.add(scenarioList.get(scenarioList.size() - 1));
									}
								} else {
									System.out.println("The html file doesn't have the content:");
								}
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					total = (int) (passed + failed);
					perPass = (passed) / (total) * 100;
					perFail = (failed) / (total) * 100;
					report.setFileName("src\\AutomationSummaryReport.xml");
					report.createXmlFile();
					report.addSummary(total, passed, failed, perPass, perFail);
					report.addDetails(finalList);
					report.transormXmlToHtml("src\\" + "AutomationSummaryReport",
							finalResultFldPath + "AutomationSummaryReport", "src\\reportInterfaceWithDefects.xsl");
				}
			}
	}
	//}
//}
