package com.utilities;

import java.util.HashMap;

public class TestCaseCount {

	private HashMap<String, Browser> browserClass;
	private String browser;
	private float passed;
	private float failed;
	private float total;
	private long executionTime;

	public String getBrowser() {
		return browser;
	}

	/**
	 * @param browser
	 *            the browser to set
	 */
	public void setBrowser(String browser) {
		this.browser = browser;
	}

	/**
	 * @return the passed
	 */
	public float getPassed() {
		return passed;
	}

	/**
	 * @param passed
	 *            the passed to set
	 */
	public void setPassed(float passed) {
		this.passed = passed;
	}

	/**
	 * @return the failed
	 */
	public float getFailed() {
		return failed;
	}

	/**
	 * @param failed
	 *            the failed to set
	 */
	public void setFailed(float failed) {
		this.failed = failed;
	}

	/**
	 * @return the total
	 */
	public float getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(float total) {
		this.total = total;
	}

	/**
	 * @return the browserClass
	 */
	public HashMap<String, Browser> getBrowserClass() {
		return browserClass;
	}

	/**
	 * @param browserClass
	 *            the browserClass to set
	 */
	public void setBrowserClass(HashMap<String, Browser> browserClass) {
		this.browserClass = browserClass;
	}

	/**
	 * @return the executionTime
	 */
	public long getExecutionTime() {
		return executionTime;
	}

	/**
	 * @param executionTime
	 *            the executionTime to set
	 */
	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

}
