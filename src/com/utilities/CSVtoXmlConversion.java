package com.utilities;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import au.com.bytecode.opencsv.CSVReader;

public class CSVtoXmlConversion {

	public static void main(String[] args) throws ParseException {

		String FLDERPATH = "C:\\Users\\ananthreddy.s\\Desktop\\TestData\\";

		File flder = new File(FLDERPATH);
		if (flder.isDirectory()) {
			File[] flNames = flder.listFiles();
			for (File flName : flNames) {
				if (flName.getName().endsWith(".csv")) {
					Map<Integer, List<String[]>> csvDatMap = methodToReadValueFromCSV(flName.getAbsolutePath());
					createXmlFile(csvDatMap, flName.getAbsolutePath());
					readXmlFile(flName.getAbsolutePath());
				}
			}
		} else {
			System.out.println("The mentioned dirctory is not present in the machine");
		}
	}

	/**
	 * The following method used to read data form CSV file It accepts one
	 * parameter as filename of CSV It returns the map
	 *
	 * @param csvFilePath
	 */
	public static Map<Integer, List<String[]>> methodToReadValueFromCSV(String csvFilePath) {
		String[] header, colVal;
		int ctr = 1;
		CSVReader reader;
		List<String[]> csvRows;
		Map<Integer, List<String[]>> csvDatMap = new HashMap<Integer, List<String[]>>();
		try {
			reader = new CSVReader(new FileReader(csvFilePath));
			List<String[]> csvData = reader.readAll();
			if (csvData.size() != 0) {
				header = csvData.get(0);
				for (int index = 1; index < csvData.size(); index++) {
					csvRows = new ArrayList<String[]>();
					String[] st = csvData.get(index);
					if (st.length >= header.length) {
						for (int i = 0; i < header.length; i++) {
							colVal = new String[2];
							colVal[0] = header[i];
							colVal[1] = st[i];
							csvRows.add(colVal);
						}
						csvDatMap.put(ctr, csvRows);
						ctr++;
					}
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			System.out.println("The file not found Exception:" + e);
		} catch (IOException e) {
			System.out.println("The IO Exception:" + e);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(csvDatMap.size());
		return csvDatMap;
	} // The methodToReadValueFromCSV method ends here

	/**
	 * The following method used to read data form XML file It accepts one
	 * parameter as filename of XML It returns the void
	 *
	 * @param xmlFilName
	 */
	public static void readXmlFile(String xmlFilName) {
		try {
			Map<Integer, List<String[]>> atCoreTestData = new HashMap<Integer, List<String[]>>();
			xmlFilName = xmlFilName.replace(".csv", ".xml");
			File flName = new File(xmlFilName);
			List<String[]> keyVal = new ArrayList<String[]>();
			String[] arr;
			if (flName.exists() && flName.getName().endsWith(".xml")) {
				try {
					DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder builder = dFactory.newDocumentBuilder();
					Document doc = builder.parse(new File(xmlFilName));
					NodeList mainNode = doc.getElementsByTagName("BookingReference");
					System.out.println("MainNode size in XML:" + mainNode.getLength());
					for (int i = 0; i < mainNode.getLength(); i++) {
						Element el = (Element) mainNode.item(i);
						int key = Integer.parseInt(el.getAttribute("id"));
						NodeList nd = mainNode.item(i).getChildNodes();

						for (int s = 0; s < nd.getLength(); s++) {
							arr = new String[2];
							Node subNode = nd.item(s);
							if (subNode.getNodeType() == Node.ELEMENT_NODE) {
								arr[0] = subNode.getNodeName();
								arr[1] = subNode.getTextContent();
								keyVal.add(arr);
							}
						}
						atCoreTestData.put(key, keyVal);
						keyVal.clear();
					}
				} catch (NullPointerException n) {
					System.out.println("Null pointer execption while reading the data from xml file:" + n);
				}
			} else {
				System.out.println("The given file doesnt exist:");
			}
		} catch (ParserConfigurationException | SAXException e) {
			System.out.println("The ParserConfigurationException | SAXException:" + e);
		} catch (IOException e) {
			System.out.println("IOException occured while reading the xml file:" + e);
		} catch (Exception e) {
			System.out.println("Exception occuerd while reading the xml file:" + e);
		}
	} // The readXmlFile method ends here

	/**
	 * The following method used to create the XML file It accepts two
	 * parameters,One as map and other one is fileName to create It Returns the
	 * void
	 *
	 * @param csvDatMap
	 * @param string
	 */
	public static void createXmlFile(Map<Integer, List<String[]>> csvDatMap, String string) {
		if (csvDatMap.size() != 0) {
			string = string.replace(".csv", ".xml");
			File fileRead = new File(string);
			if (fileRead.exists()) {
				fileRead.delete();
			}
			try {
				DocumentBuilderFactory dFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dFactory.newDocumentBuilder();
				Document doc = dBuilder.newDocument();

				Element root = doc.createElement("ACData");
				doc.appendChild(root);

				Set<Integer> setVal = csvDatMap.keySet();
				Iterator<Integer> it = setVal.iterator();
				while (it.hasNext()) {
					Integer intVal = it.next();
					Element bookRef = doc.createElement("BookingReference");
					root.appendChild(bookRef);

					Attr attr = doc.createAttribute("id");
					attr.setValue(intVal.toString());
					bookRef.setAttributeNode(attr);

					List<String[]> lt = csvDatMap.get(intVal);

					for (int j = 0; j < lt.size(); j++) {
						String s1, s2;
						String[] retVal = lt.get(j);
						s1 = retVal[0].replace(" ", "").trim();
						s2 = retVal[1].replace(" ", "").trim();
						Element depdate = doc.createElement(s1);
						depdate.appendChild(doc.createTextNode(s2));
						bookRef.appendChild(depdate);
					}
				}
				TransformerFactory tFactory = TransformerFactory.newInstance();
				Transformer tTransform = tFactory.newTransformer();
				DOMSource dom = new DOMSource(doc);
				StreamResult result = new StreamResult(new File(string));

				tTransform.transform(dom, result);
				tTransform.setOutputProperty(OutputKeys.INDENT, "yes");
				tTransform.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
				System.out.println("File created succesfully");

			} catch (ParserConfigurationException p) {
				System.out.println("Parser Configuration Exception in creating the xml file:" + p);
			} catch (TransformerException t) {
				System.out.println("Transformer Exception in creating the xml file:" + t);
			} catch (Exception e) {
				System.out.println("Exception in creating the xml file:" + e);
			}
		} else {
			System.out.println("The map size is zero hence csv doesn't have the content");
		}
	} // The readXmlFile method ends here

}
