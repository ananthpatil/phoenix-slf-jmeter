package com.utilities;

public class Common_Bean {

	private int noOfAdults = 0;
	private int noOfChild = 0;
	private int noOfInfant = 0;
	private String childAges, url;
	private String browserName;

	public void setBrowserName(String browserName) {
		this.browserName = browserName;
	}

	public String getBrowserName() {
		return browserName;
	}

	public void setAdults(int noOfAdults) {
		this.noOfAdults = noOfAdults;
	}

	public int getAdults() {
		return noOfAdults;
	}

	public void setChilds(int noOfChild) {
		this.noOfChild = noOfChild;
	}

	public int getChilds() {
		return noOfChild;
	}

	public void setInfants(int noOfInfant) {
		this.noOfInfant = noOfInfant;
	}

	public int getInfants() {
		return noOfInfant;
	}

	public void setChildAges(String childAges) {
		this.childAges = childAges;
	}

	public String getChildAges() {
		return childAges;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

}
