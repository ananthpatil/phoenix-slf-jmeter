
package com.utilities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.*;

public class DataBaseConnectivity {

	public void dataBaseExtraction(String query) {
		try {
			int incr = 0;
			HashMap<Integer, List<String[]>> dbValue = new LinkedHashMap<Integer, List<String[]>>();
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection connect = DriverManager.getConnection("jdbc:oracle:thin:@10.145.130.30:1523/CDMST", "cdm_hub",
					"cdm_hub123");
			Statement stat = connect.createStatement();
			ResultSet rst = stat.executeQuery(query);
			while (rst.next()) {
				incr++;
				List<String[]> lt = null;
				ResultSetMetaData metDat = rst.getMetaData();
				for (int i = 1; i <= metDat.getColumnCount(); i++) {
					int type = metDat.getColumnType(i);
					lt = new ArrayList<String[]>();
					String[] arr = new String[2];
					if (type == Types.VARCHAR || type == Types.CHAR) {
						arr[0] = metDat.getColumnName(i);
						arr[1] = rst.getString(i);
						lt.add(arr);
					} else {
						arr[0] = metDat.getColumnName(i);
						Long val = rst.getLong(i);
						arr[1] = val.toString();
						lt.add(arr);
					}
				}
				dbValue.put(incr, lt);
			}
		} catch (ClassNotFoundException e) {
			System.out.println("orcel driver class not found execption:");
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Sql Exception:");
		}
	}

	public static void main(String[] args) throws InterruptedException {

		DataBaseConnectivity db = new DataBaseConnectivity();
		db.dataBaseExtraction("select * from CDM_HUB.ROLE_CUSTOMER_BOOKING_REL");
		// db.dataBaseExtraction("select * from CDM_HUB.customer");

	}
}