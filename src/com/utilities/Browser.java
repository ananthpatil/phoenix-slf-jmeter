package com.utilities;

public class Browser {
	private Float passPercentage;
	private Float failPercentage;
	private Float passed;
	private Float failed;
	private Float total;

	/**
	 * @return the passPercentage
	 */
	public Float getPassPercentage() {
		return passPercentage;
	}

	/**
	 * @param passPercentage
	 *            the passPercentage to set
	 */
	public void setPassPercentage(Float passPercentage) {
		this.passPercentage = passPercentage;
	}

	/**
	 * @return the failPercentage
	 */
	public Float getFailPercentage() {
		return failPercentage;
	}

	/**
	 * @param failPercentage
	 *            the failPercentage to set
	 */
	public void setFailPercentage(Float failPercentage) {
		this.failPercentage = failPercentage;
	}

	/**
	 * @return the total
	 */
	public Float getTotal() {
		return total;
	}

	/**
	 * @param total
	 *            the total to set
	 */
	public void setTotal(Float total) {
		this.total = total;
	}

	/**
	 * @return the passed
	 */
	public Float getPassed() {
		return passed;
	}

	/**
	 * @param passed
	 *            the passed to set
	 */
	public void setPassed(Float passed) {
		this.passed = passed;
	}

	/**
	 * @return the failed
	 */
	public Float getFailed() {
		return failed;
	}

	/**
	 * @param failed
	 *            the failed to set
	 */
	public void setFailed(Float failed) {
		this.failed = failed;
	}

}
