<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/"> 
  <html>
  <head>
  
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"/>
    <script type='text/javascript' src='http://code.jquery.com/jquery-1.10.1.min.js'></script>
	<script type='text/javascript' src='https://www.google.com/jsapi'></script>
	<script src='http://cdn.rawgit.com/noelboss/featherlight/1.0.4/release/featherlight.min.js' type='text/javascript' charset='utf-8'></script>
	<script type='text/javascript'>
	
	    google.load('visualization','1', {packages:['corechart']})
	    google.setOnLoadCallback(testsChart)
	    
	   function testsChart() {
		var data = google.visualization.arrayToDataTable([
				  ['Test Status','count'],
				  ['Pass', $('td.pass').length],
				  ['Fail', $('td.fail').length]
				]);
				var options = {
				  backgroundColor: { fill:'transparent' },
				  chartArea: {'width': '65%', 'height': '65%'},
				  colors: ['#00af00', 'red'],
				  fontName: 'Source Sans Pro',
				  fontSize: '12',
				  titleTextStyle: { color: '#1366d7', fontSize: '14' },
				  height: 275,
				  is3D: true,
				  pieSliceText: 'value', 
				  title: 'Pass/Fail summary', 
				  width: 450
				};
				var chart = new google.visualization.PieChart(document.getElementById('step-status-dashboard'));
				chart.draw(data, options);
			  }
	 </script>
 </head>
   <body>
   <div style='margin:0 auto;'>
     <div align="center" id ="header" style="display: block; height: 10%; position: fixed; width: 100%; top: 0px;background: none repeat scroll 0% 0% rgb(194, 194, 195);">
	    <h1 style="font-size:30px"><u>Automation Summary Result</u></h1>
	 </div>
	 <br/><br/><br/><br/>
	
	<div id='step-status-dashboard' style='width: 900px; height: 200px; margin-left:500px;'></div>	
    <br/><br/><br/><br/>
    
    
    <div class="executeinfo" style="margin-left:340px; margin-right:0px;">
      <table border="3" style="width:800px;">
      <thead>
	    <tr bgcolor="yellow">
	      <th style="text-align:center;">Total No Of Scenario</th>
	      <th style="text-align:center;">No Of Passed</th>
	      <th style="text-align:center;">No Of Failed</th>
	      <th style="text-align:center;">Percentage Of Passed</th>
	      <th style="text-align:center;">Percentage Of Failed</th>
	   </tr>
	  </thead>
	  <tbody>
	   <xsl:for-each select="SummaryResult/Status">  
	    <tr>
	       <td style="text-align:left"><xsl:value-of select="Total"></xsl:value-of></td>
	       <td style="text-align:left"><xsl:value-of select="Passed"></xsl:value-of></td>
	       <td style="text-align:left"><xsl:value-of select="Failed"></xsl:value-of></td>
	       <td style="text-align:left"><xsl:value-of select="PercentagePassed"></xsl:value-of></td>
	       <td style="text-align:left"><xsl:value-of select="PercentageFailed"></xsl:value-of></td>
        </tr>
        </xsl:for-each>
       </tbody>
     </table>
    </div>
    <br/><br/><br/><br/>
    
    
    <div class="scenarioinfo" style="margin-left:340px; margin-right:0px;">
	 <table border="2" style="width:800px;">
	      <thead>
		   <tr bgcolor="yellow">
		      <th style="text-align:center;">Staus Icon</th>
			  <th style="text-align:center;">Scenario Name</th>
			  <th style="text-align:center;">Execution Status</th>
			  <th style="text-align:center;">FileName</th>
		   </tr>
          </thead>
         <tbody>
          <xsl:for-each select="SummaryResult/Scenarios/ScenDescStatus">
          <xsl:choose>
            <xsl:when test="ScenStatus = 'FAIL'">
             <tr>
			   <td title='FAIL' class='status fail' style="text-align:center;"><i style="color: #eb4549;" class="fa fa-times-circle-o"></i></td>
			   <td style="text-align:left;"><xsl:value-of select="ScenarioName"></xsl:value-of></td>
			   <td style="text-align:left;" bgcolor="red"><xsl:value-of select="ScenStatus"></xsl:value-of></td>
			   <td style="text-align:left;"><xsl:value-of select="FileName"></xsl:value-of></td>
		   </tr>
		   </xsl:when>
		  
		  <xsl:otherwise>
           <tr>
			  <td title='PASS' class='status pass' style="text-align:center;"><i style="color: #32CD32;" class="fa fa-check-circle"></i></td>
			  <td style="text-align:left;"><xsl:value-of select="ScenarioName"></xsl:value-of></td>
			  <td style="text-align:left;" bgcolor="green"><xsl:value-of select="ScenStatus"></xsl:value-of></td>
			  <td style="text-align:left;"><xsl:value-of select="FileName"></xsl:value-of></td>
		    </tr>	
		   </xsl:otherwise> 
		   </xsl:choose>
		 </xsl:for-each>
		</tbody>
     </table>
   </div>
   </div>
</body>
 </html>
</xsl:template>
</xsl:stylesheet>