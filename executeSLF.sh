#!/bin/bash
slfFrameworkPath="$1"
slfJavaPath="$2"
executionArg1="$3"
executionArg2="$4"
jarpath=$slfFrameworkPath/../Jars
export PATH=$PATH:$slfJavaPath
cd $slfFrameworkPath
#Compile .java files 
#javac -d bin -cp "$jarpath/*" $slfFrameworkPath/src/com/driver/DriverExcuteThrough_Threads.java $slfFrameworkPath/src/com/driver/Driver.java $slfFrameworkPath/src/com/utilities/DriverReusable.java $slfFrameworkPath/src/com/commonfunctions/CommonFunctions.java $slfFrameworkPath/src/com/commonfunctions/Action.java $slfFrameworkPath/src/com/utilities/Constants.java $slfFrameworkPath/src/com/utilities/ReadExcel.java
#Execute Test
java -cp ".:$jarpath/*" com.driver.DriverExcuteThrough_Threads $executionArg1 $executionArg2
